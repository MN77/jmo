/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang;

import org.jaymo_lang.api.TypeDictionary;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.JMo_Charset;
import org.jaymo_lang.object.JMo_Cycle;
import org.jaymo_lang.object.JMo_Group;
import org.jaymo_lang.object.JMo_Help;
import org.jaymo_lang.object.JMo_Random;
import org.jaymo_lang.object.JMo_Range;
import org.jaymo_lang.object.JMo_RegEx;
import org.jaymo_lang.object.JMo_Use;
import org.jaymo_lang.object.atom.JMo_BigDec;
import org.jaymo_lang.object.atom.JMo_BigInt;
import org.jaymo_lang.object.atom.JMo_Byte;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Float;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.JMo_Short;
import org.jaymo_lang.object.classic.JMo_Async;
import org.jaymo_lang.object.classic.JMo_Break;
import org.jaymo_lang.object.classic.JMo_Count;
import org.jaymo_lang.object.classic.JMo_Describe;
import org.jaymo_lang.object.classic.JMo_Each;
import org.jaymo_lang.object.classic.JMo_Echo;
import org.jaymo_lang.object.classic.JMo_EchoErr;
import org.jaymo_lang.object.classic.JMo_Exit;
import org.jaymo_lang.object.classic.JMo_For;
import org.jaymo_lang.object.classic.JMo_Ident;
import org.jaymo_lang.object.classic.JMo_If;
import org.jaymo_lang.object.classic.JMo_IfNot;
import org.jaymo_lang.object.classic.JMo_Loop;
import org.jaymo_lang.object.classic.JMo_Next;
import org.jaymo_lang.object.classic.JMo_Print;
import org.jaymo_lang.object.classic.JMo_PrintErr;
import org.jaymo_lang.object.classic.JMo_PrintLog;
import org.jaymo_lang.object.classic.JMo_Repeat;
import org.jaymo_lang.object.classic.JMo_Return;
import org.jaymo_lang.object.classic.JMo_Sleep;
import org.jaymo_lang.object.classic.JMo_Sync;
import org.jaymo_lang.object.classic.JMo_Try;
import org.jaymo_lang.object.classic.JMo_Which;
import org.jaymo_lang.object.classic.JMo_While;
import org.jaymo_lang.object.filesys.JMo_Dir;
import org.jaymo_lang.object.filesys.JMo_File;
import org.jaymo_lang.object.filesys.JMo_Path;
import org.jaymo_lang.object.filesys.JMo_RandomAccessFile;
import org.jaymo_lang.object.immute.JMo_Enum;
import org.jaymo_lang.object.immute.JMo_KeyValue;
import org.jaymo_lang.object.immute.JMo_Type;
import org.jaymo_lang.object.immute.datetime.JMo_Date;
import org.jaymo_lang.object.immute.datetime.JMo_DateTime;
import org.jaymo_lang.object.immute.datetime.JMo_Time;
import org.jaymo_lang.object.pseudo.JMo_Error;
import org.jaymo_lang.object.pseudo.JMo_Warning;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.object.struct.JMo_FunctionMap;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.struct.JMo_Macro;
import org.jaymo_lang.object.struct.JMo_Map;
import org.jaymo_lang.object.struct.JMo_Set;
import org.jaymo_lang.object.struct.JMo_Table;
import org.jaymo_lang.object.struct.JMo_Tree;
import org.jaymo_lang.object.struct.JMo_TreeNode;
import org.jaymo_lang.object.sys.JMo_Cmd;
import org.jaymo_lang.object.sys.JMo_Input;
import org.jaymo_lang.object.sys.JMo_Java;
import org.jaymo_lang.object.sys.JMo_Sys;
import org.jaymo_lang.object.test.JMo_ArgumentTestDummy;
import org.jaymo_lang.object.test.JMo_AutoTestDummy;


/**
 * @author Michael Nitsche
 * @created 20.06.2022
 */
public class CoreTypes implements TypeDictionary {

	public String[] allTypes() {
		return new String[]{"ArgumentTestDummy", "Async", "AutoTestDummy", "BigDec", "BigInt", "Break", "Byte", "ByteArray", "Charset", "Cmd", "Count", "Cycle", "Date", "DateTime", "Dec", "Describe",
			"Dir", "Double", "Each", "Echo", "EchoErr", "Enum", "Error", "Exit", "File", "Float", "For", "FunctionMap", "Group", "Help", "Ident", "If", "IfNot", "Input", "Java", "KeyValue", "List",
			"Long", "Loop", "Macro", "Map", "Next", "Path", "Print", "PrintErr", "PrintLog", "Random", "RandomAccessFile", "Range", "RegEx", "Repeat", "Return", "Set", "Short", "Sleep", "Sync", "Sys",
			"Table", "Time", "Tree", "TreeNode", "Try", "Type", "Use", "Warning", "Which", "While"};
	}

	public Class<? extends A_Object> lookup(final String typename) {

		switch(typename) {
			case "ArgumentTestDummy":
				return JMo_ArgumentTestDummy.class;
			case "Async":
				return JMo_Async.class;
			case "AutoTestDummy":
				return JMo_AutoTestDummy.class;
			case "BigDec":
				return JMo_BigDec.class;
			case "BigInt":
				return JMo_BigInt.class;
			case "Break":
				return JMo_Break.class;
			case "Byte":
				return JMo_Byte.class;
			case "ByteArray":
				return JMo_ByteArray.class;
			case "Charset":
				return JMo_Charset.class;
			case "Cmd":
				return JMo_Cmd.class;
			case "Count":
				return JMo_Count.class;
			case "Cycle":
				return JMo_Cycle.class;
			case "Date":
				return JMo_Date.class;
			case "DateTime":
				return JMo_DateTime.class;
			case "Dec":
				return JMo_Dec.class;
			case "Describe":
				return JMo_Describe.class;
			case "Dir":
				return JMo_Dir.class;
			case "Double":
				return JMo_Double.class;
			case "Each":
				return JMo_Each.class;
			case "Echo":
				return JMo_Echo.class;
			case "EchoErr":
				return JMo_EchoErr.class;
			case "Enum":
				return JMo_Enum.class;
			case "Error":
				return JMo_Error.class;
			case "Exit":
				return JMo_Exit.class;
			case "File":
				return JMo_File.class;
			case "Float":
				return JMo_Float.class;
			case "For":
				return JMo_For.class;
			case "FunctionMap":
				return JMo_FunctionMap.class;
			case "Group":
				return JMo_Group.class;
			case "Help":
				return JMo_Help.class;
			case "Ident":
				return JMo_Ident.class;
			case "If":
				return JMo_If.class;
			case "IfNot":
				return JMo_IfNot.class;
			case "Input":
				return JMo_Input.class;
			case "Java":
				return JMo_Java.class;
			case "KeyValue":
				return JMo_KeyValue.class;
			case "List":
				return JMo_List.class;
			case "Long":
				return JMo_Long.class;
			case "Loop":
				return JMo_Loop.class;
			case "Macro":
				return JMo_Macro.class;
			case "Map":
				return JMo_Map.class;
			case "Next":
				return JMo_Next.class;
			case "Path":
				return JMo_Path.class;
			case "Print":
				return JMo_Print.class;
			case "PrintErr":
				return JMo_PrintErr.class;
			case "PrintLog":
				return JMo_PrintLog.class;
			case "Random":
				return JMo_Random.class;
			case "RandomAccessFile":
				return JMo_RandomAccessFile.class;
			case "Range":
				return JMo_Range.class;
			case "RegEx":
				return JMo_RegEx.class;
			case "Repeat":
				return JMo_Repeat.class;
			case "Return":
				return JMo_Return.class;
			case "Set":
				return JMo_Set.class;
			case "Short":
				return JMo_Short.class;
			case "Sleep":
				return JMo_Sleep.class;
			case "Sync":
				return JMo_Sync.class;
			case "Sys":
				return JMo_Sys.class;
			case "Table":
				return JMo_Table.class;
			case "Time":
				return JMo_Time.class;
			case "Tree":
				return JMo_Tree.class;
			case "TreeNode":
				return JMo_TreeNode.class;
			case "Try":
				return JMo_Try.class;
			case "Type":
				return JMo_Type.class;
			case "Use":
				return JMo_Use.class;
			case "Warning":
				return JMo_Warning.class;
			case "Which":
				return JMo_Which.class;
			case "While":
				return JMo_While.class;
		}
//		case "Regex":
//			return JMo_RegEx.class;

		return null;
	}

}
