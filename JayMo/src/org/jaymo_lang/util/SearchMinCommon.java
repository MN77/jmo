/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import java.util.List;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 10.12.2018
 */
public class SearchMinCommon {

	/**
	 * bekommt eine Liste mit String[]. Links das Höchste, rechts das Niedrigste
	 * Einträge mit null werden ignoriert!
	 **/
	public static String getMinCommon(final List<String[]> data) {
		Err.ifNull(data);
		if(data.size() == 0)
			return null;

		final String[] test = data.get(0);
		if(test == null)
			return null;

		for(final String search : test) {
			int found = 0;
			for(final String[] dataz : data)
				if(dataz == null || dataz.length == 0) // Leer-Einträge ignorieren
					found++;
				else
					for(final String datazs : dataz)
						if(datazs.equals(search))
							found++;
			if(found == data.size())
				return search;
		}
		return null;
	}

}
