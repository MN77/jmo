/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.model.Call;


/**
 * @author Michael Nitsche
 * @created 14.02.2019
 */
public class Lib_Prio {

	public static boolean streamPrio(final Call c, final Call next) {
		if(next == null || c.method == null)
			return false;

		return next.prio > c.prio;
	}

	/**
	 * 99 is the highest Prio. This always will be executed at first.
	 * 0 is the lowest Prio. This always will be executed at last.
	 */
	public static byte streamPrio(final String s, final DebugInfo debug) {
		final char c = s.charAt(0);
//		if(c >= 'a' && c <= 'z' || c == '?' || c == '@') // || c=='.' || c=='!'
		if("abcdefghijklmnopqrstuvwxyz?@:ABCDEFGHIJKLMNOPQRSTUVWXYZ_".indexOf(c) >= 0) // Faster
			return 99;

		switch(s.length()) {
			case 1:
				switch(c) {
					case '+':
					case '-':
						return 32;

					case '&':
						return 23;
					case '|':
						return 21;
					case '^':
						return 22;

					case '*':
					case '/':
					case '%':
						return 33;

					case '~': // Floor
					case '!': // Binary Not
						return 60;

//					case '=':
//						return 0;

					case '<':
					case '>':
						return 15;

//					case '≈':
//						return 30;

//					case '¶': // MultiCallEnd
//						return 0;
				}
			case 2:
				switch(s) {
//					case "=~":
//						return 0;

					case "==":
					case "!=":
					case "<>":
						return 14;

					case "<=":
					case ">=":
						return 15;

					case "&&":
						return 13;
					case "||":
						return 11;
					case "^^":
						return 12;

					case "++":
					case "--":
					case "**":
					case "//":
						return 50;

					case "<<":
					case ">>":
						return 31;

					case "/~":
						return 33;

					case "+=":
					case "-=":
						return 43;

					case "*=":
					case "/=":
					case "%=":
						return 44;

					case "&=":
						return 42;
					case "|=":
						return 40;
					case "^=":
						return 41;

					case "->":
						return 1; // KeyValue

//					case "=!": // is not
//						return 99; // 60
//					case "..": // range
//						return 9;
				}
			case 3:
				switch(s) {
					case "===":
					case "==~":
					case "!==":
					case "!=~":
						return 14;

					case "!&&":
						return 13;
					case "!||":
						return 11;
					case "!^^":
						return 12;

					case "++=":
					case "--=":
					case "**=":
					case "//=":

					case "=++":
					case "=--":
					case "=**":
					case "=//":
						return 50;

					case "&&=":
						return 42;
					case "||=":
						return 40;
					case "^^=":
						return 41;
					case "<<=":
					case ">>=":
					case "=<<":
					case "=>>":
						return 31;

//					case "+=>":
//					case "-=>":
//						return 0;
				}
		}
//		throw new CodeError("Unknown function", "Can't get priority for: " + s, debug);
		return 0;
	}

}
