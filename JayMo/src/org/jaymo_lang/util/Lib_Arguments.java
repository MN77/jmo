/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_Atomic;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.passthrough.I_Mem;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.Instance;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 31.07.2019
 */
public class Lib_Arguments {

//	public static void checkBlock(final CallRuntime cr) {
////		if(cr.getStrict().isValid_AutoBlockAfterFunc(cr))
////			return;
//		Lib_Par.checkBlockForbidden(cr);
//	}

	public static I_Object checkArg(final CallRuntime cr, final I_Object arg, final Class<?> type, final Integer argIdx, final Integer argAmount, final boolean isInstanceInit) {
		final I_Object parReal = Lib_Arguments.iCheckArg(cr, arg, type, argIdx, argAmount, isInstanceInit);
		if(parReal == null)
			throw Lib_Arguments.iCreateInvalidArgTypeError(cr, arg, Lib_Arguments.iTypelistToText(type), argIdx, argAmount, isInstanceInit);
		else
			return parReal;
	}

	public static I_Object checkArgAdvance(final CallRuntime cr, final I_Object streamIt, final int argIdx, final Integer argAmount, final Class<?> type, final boolean isInstanceInit) {
		final I_Object arg = cr.getArgAdvance(streamIt, argIdx);
		return Lib_Arguments.checkArg(cr, arg, type, argIdx, argAmount, isInstanceInit);
	}

	public static I_Object checkArgAdvanceExt(final CallRuntime cr, final I_Object streamIt, final int argIdx, final Integer argAmount, final Class<?>[] types, final boolean isInstanceInit) {
		final I_Object arg = cr.getArgAdvance(streamIt, argIdx);
		return Lib_Arguments.checkArgExt(cr, arg, types, argIdx, argAmount, isInstanceInit);
	}

	public static I_Object checkArgEach(final CallRuntime cr, final I_Object streamIt, final int argIdx, final I_Object eachIt, final Class<?> type) {
		Err.ifNull(cr.call, streamIt);
		final I_Object result = cr.getArgEach(streamIt, argIdx, eachIt);
		return Lib_Arguments.checkArg(cr, result, type, argIdx, null, false);
	}

	public static I_Object checkArgExt(final CallRuntime cr, final I_Object arg, final Class<?>[] types, final Integer argIdx, final Integer argAmount, final boolean isInstanceInit) {

		for(final Class<?> type : types) {
			final I_Object parReal = Lib_Arguments.iCheckArg(cr, arg, type, argIdx, argAmount, isInstanceInit);
			if(parReal != null)
				return parReal;
		}
		throw Lib_Arguments.iCreateInvalidArgTypeError(cr, arg, Lib_Arguments.iTypelistToText(new Class<?>[][]{types}), argIdx, argAmount, isInstanceInit);
	}

	public static void checkArgMem(final CallRuntime cr, final I_Object arg, final Class<? extends I_Mem> type, final Integer argIdx, final Integer argAmount, final boolean isInstanceInit) {
		if(!type.isAssignableFrom(arg.getClass()))
			throw Lib_Arguments.iCreateInvalidArgTypeError(cr, arg, Lib_Arguments.iTypelistToText(type), argIdx, argAmount, isInstanceInit);
	}

	public static I_Object[] checkArgs(final CallRuntime cr, final I_Object streamIt, final Class<?>[] types) {
		if(types == null)
			if(cr.argCount() > 0)
				throw new CodeError(cr, "Invalid number of arguments", "No arguments allowed");
			else
				return new I_Object[0];

		if(types.length == 0)
			Err.invalid("No types definied. If this is okay, then please use: 'cr.args()'");

		Err.ifNull(cr.call);
		final I_Object[] args = cr.getArgs(streamIt);
		final int oal = args.length;
		Lib_Error.ifArgs(oal, types.length, types.length, cr, streamIt);
		for(int i = 0; i < oal; i++)
			args[i] = Lib_Arguments.checkArg(cr, args[i], types[i], i, oal, false);
		return args;
	}

	public static I_Object checkArgsEach(final CallRuntime cr, final I_Object streamIt, final int argIdx, final I_Object[] eachIt, final Class<?> type) {
		Err.ifNull(cr.call, streamIt);
		final I_Object result = cr.getArgsEach(streamIt, argIdx, eachIt);
		return Lib_Arguments.checkArg(cr, result, type, argIdx, null, false);
	}

	public static I_Object[] checkArgsExt(final CallRuntime cr, final I_Object streamIt, final Class<?>[]... types) {
		final I_Object[] args = cr.getArgs(streamIt);
		Lib_Error.ifArgs(args.length, types.length, types.length, cr, streamIt);
		for(int i = 0; i < args.length; i++) // {
			args[i] = Lib_Arguments.checkArgExt(cr, args[i], types[i], i, args.length, false);

		return args;
	}

	public static I_Object[] checkArgsFlex(final CallRuntime cr, final I_Object streamIt, final int par_min, final Integer par_max) {
		final I_Object[] args = cr.getArgs(streamIt);
		final int len = args.length;
		Lib_Error.ifArgs(len, par_min, par_max, cr, streamIt);

		for(int i = 0; i < len; i++)
			args[i] = Lib_Convert.getValue(cr, args[i]);

		return args;
	}

	public static void checkBlockForbidden(final CallRuntime cr) {
		if(cr.hasBlock())
			throw new CodeError(cr, "Invalid function-call", "No Block allowed for this function");
	}

	public static void checkBlockNecessary(final CallRuntime cr) {
		if(!cr.hasBlock())
			throw new CodeError(cr, "Invalid function-call", "Block needed for this function");
	}


	// --- Private ---

//	private static I_Object iAutoVarConstLet(final CallRuntime cr, final I_Object arg, final Class<?> wantedType) {
//		if(!cr.getStrict().isValid_AutoVarLet())
//			return null;
//
//		if(wantedType == A_MemLet.class) {
//			if(arg instanceof Var)
//				return new VarLet((Var)arg); // Auto-VarLet
//			if(arg instanceof Const)
//				return new ConstLet((Const)arg); // Auto-ConstLet
//		}
//		else if(wantedType == VarLet.class && arg instanceof Var)
//			return new VarLet((Var)arg); // Auto-VarLet
//		else if(wantedType == ConstLet.class && arg instanceof Const)
//			return new ConstLet((Const)arg); // Auto-ConstLet
//
//		return null;
//	}

	private static I_Object iCheckArg(final CallRuntime cr, final I_Object arg, final Class<?> type, final Integer argIdx, final Integer argAmount, final boolean isInstanceInit) {
//		final I_Object autoVarConstLet = Lib_Arguments.iAutoVarConstLet(cr, arg, type);
//		if(autoVarConstLet != null)
//			return autoVarConstLet;

		I_Object parReal = Lib_Convert.getValue(cr, arg);
		boolean valid = type == null || type.isAssignableFrom(parReal.getClass());

		if(!valid && parReal instanceof A_Atomic && cr.getStrict().isValid_ArgConvert()) {
			final String neededTypeName = Lib_Type.getName(type, null);
			parReal = Lib_Convert.toSafeType(cr, (I_Atomic)parReal, neededTypeName);
			valid = Lib_Type.isInstanceOf(parReal, neededTypeName); // Another check
		}

		if(!valid) {
			I_Object inst = parReal;

			while(inst != null && inst instanceof Instance) {
				final I_Object parent = ((Instance)inst).internalGetParent();
				if(parent != null && type.isAssignableFrom(parent.getClass()))
					return parent; // TODO Wirklich parent, parent, parent ?!? Was wenn überschrieben?!?
				inst = parent;
			}
		}
		return valid ? parReal : null;
	}

//	private static String objTypesToText(final CallRuntime cr, final I_Object[] oa) {
//		final StringBuilder sb = new StringBuilder();
//		sb.append("(");
//		for(int ioa = 0; ioa < oa.length; ioa++) {
//			final I_Object o = oa[ioa];
//			if(ioa != 0)
//				sb.append(", ");
////			sb.append(o.getTypeName());
//			sb.append(Lib_Type.getTypeString(o));
//		}
//		sb.append(")");
//		return sb.toString();
//	}

	private static CodeError iCreateInvalidArgTypeError(final CallRuntime cr, final I_Object arg, final String type, final Integer argIdx, final Integer argAmount, final boolean isInstanceInit) {
		// Generate error message:
		String m = cr.call.method;
		if(m != null && !Lib_Function.isMathematicFunction(m))
			m = '.' + m;

		final String eTarget = isInstanceInit || m == null ? cr.call.object.getTypeName() : m;
		final String eArg = argIdx != null && (argAmount == null || argAmount > 1) ? " as argument " + (argIdx + 1) : "";
		final String typeDebug = Lib_Type.toIdentWithType(cr, arg);

		final String eDet = "Got " + typeDebug + eArg + " for '" + eTarget + "', expected: " + type;
		return new CodeError(cr, "Invalid type of argument", eDet);
	}

	private static String iTypelistToText(final Class<?> type) {
		return Lib_Type.getTypeString(type, null);
	}

	private static String iTypelistToText(final Class<?>[][] types) {
		final StringBuilder sb = new StringBuilder();
		final boolean more = types.length > 1;
		if(more)
			sb.append("(");

		for(int ica = 0; ica < types.length; ica++) {
			final Class<?>[] ca = types[ica];
			if(ica != 0)
				sb.append(", ");

			sb.append('<');

			for(int i = 0; i < ca.length; i++) {
				if(i != 0)
					sb.append(" | ");
				sb.append(Lib_Type.getName(ca[i], null));
			}
			sb.append('>');
		}
		if(more)
			sb.append(")");
		return sb.toString();
	}

}
