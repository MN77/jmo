/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.ErrorBase;
import org.jaymo_lang.error.JayMoError;
import org.jaymo_lang.error.ReturnException;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.App;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.pseudo.Return.LEVEL;
import org.jaymo_lang.parser.Parser_App;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 14.05.2019
 */
public class Lib_Exec {

	/**
	 * Checks, if Return-Level is Return
	 */
	public static void checkExceptionIsEnd(final CallRuntime cr, final Return r) {
		if(r.getLevel() != LEVEL.RETURN)
			throw new JayMoError(cr, "Invalid Return-Type", "Got " + r.getLevel());
	}

	public static void checkLoopWithout(final CallRuntime crOld) {
		if(crOld.getStream() == null && crOld.getCallBlock() == null)
			throw new CodeError(crOld, "Invalid call", "No argument, no stream, no block ... what should I do?");
	}

	/**
	 * @apiNote This function should only be used in exceptional cases!!!
	 */
	public static I_Object execBlockOnly(final CallRuntime crNew, I_Object it) {
		final Block block = crNew.getCallBlock();

		if(block != null)
			try {
				it = block.exec(crNew, it);
			}
			catch(final ReturnException e) {
				final Return r = e.get();
				r.updateEmptyResult(it); // This will be checked and updated multiple times to get value of the highest block
				return r;
			}
		return it;
	}

	public static I_Object execBlockStream(final CallRuntime crNew, I_Object it) {
		final Call stream = crNew.getStream();
		final Block block = crNew.getCallBlock();

		if(block != null)
			try {
				it = block.exec(crNew, it);
			}
			catch(final ReturnException e) {
				final Return r = e.get();
				r.updateEmptyResult(it); // This will be checked and updated multiple times to get value of the highest block
				return r;
			}
		if(stream != null)
			it = crNew.execInit(stream, it);

		return it;
	}

	public static I_Object execIf(final CallRuntime cr, final boolean check, final I_Object it, final boolean passToStream) {
		final boolean streamOnFalse = passToStream ? false : cr.getCallBlock() != null;
		return check
			? Lib_Exec.execBlockStream(cr, it)
			: streamOnFalse
				? Lib_Exec.execStreamOnly(cr, it)
				: passToStream
					? Bool.FALSE
					: it;
	}

	/**
	 * @return Str or Nil
	 */
	public static A_Immutable execJayMo(final CallRuntime cr, final String script, final String infoTypeFunc, final String[] args) {
		return Lib_Exec.iExecJayMo(cr, script, infoTypeFunc, false, args);
	}

	/**
	 * @return Str or Nil
	 */
	public static A_Immutable execJayMoHot(final CallRuntime cr, final String script, final String infoTypeFunc) {
		return Lib_Exec.iExecJayMo(cr, script, infoTypeFunc, true, null);
	}

	public static I_Object execStreamOnly(final CallRuntime crNew, I_Object it) {
		final Call stream = crNew.getStream();

		if(stream != null)
			it = crNew.execInit(stream, it);

		return it;
	}

	public static I_Object loopResult(final I_Object result) {
		if(result instanceof Return && ((Return)result).getLevel() == LEVEL.NEXT)
			return ((Return)result).getResult();
		return result;
	}

	/**
	 * Returns always one value. If no argument is set, Nil will be returned
	 */
	public static I_Object mSingleArgument(final CallRuntime cr, final I_Object itStream, final int min, final int max, final I_Object defaultValue) {
		Err.ifToBig(1, max);
		final I_Object[] args = cr.argsFlex(itStream, min, max);
		return args.length == 0
			? defaultValue
			: args[0];
	}

	/**
	 * @return Str or Nil
	 */
	private static A_Immutable iExecJayMo(final CallRuntime cr, final String script, final String infoTypeFunc, final boolean hotDirect, final String[] args) {
		cr.getStrict().checkSandbox(cr, infoTypeFunc);

		try {
			final Parser_App parser = new Parser_App();
			final App appHot = parser.parseText(script);
			appHot.setOutputFile(cr.getApp().getOutputFile());
			final String result = hotDirect ? cr.getApp().exec(cr, appHot) : appHot.exec(args);
			return result == null
				? Nil.NIL
				: new Str(result);
		}
		catch(final CodeError e) {
			throw new RuntimeError(cr, "Code-Error", e.getMessage());
		}
		catch(final ErrorBase e) {
			throw e;
		}
	}

}
