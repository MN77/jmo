/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.parser.Parser_Script;


/**
 * @author Michael Nitsche
 * @created 17.10.2020
 */
public class Lib_Prefix {

//	public static final String PREFIX_STD_NAMESPACE = "Std_";	// currently forbidden

	public static String addPrefix(final String typename, final Parser_Script parser) {

		if(typename.charAt(0) == '_' && parser != null) {
			final String currentPrefix = parser.getCurrentPrefix();
			if(currentPrefix == null)
				throw new CodeError("No prefix set", "For using a type with leading '_', the flag '>prefix' must be set!", parser);
			return currentPrefix + typename;
		}
		else
			return typename;
	}

	public static void checkPrefix(final String prefix, final DebugInfo debug) {
		boolean first = true;

		for(final char c : prefix.toCharArray()) { // Fast check, faster than Regex
			if(first && (c < 'A' || c > 'Z'))
				throw new CodeError("Invalid prefix!", "A prefix must start with a capital letter: " + prefix, debug);
			first = false;
			if(c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= '0' && c <= '9')
				continue;
			if(c == '_')
				throw new CodeError("Invalid prefix!", "A prefix can't contain a underline char. Got: " + prefix, debug);
			throw new CodeError("Invalid prefix!", "Invalid chars in prefix. Got: " + prefix, debug);
		}
		for(final String f : BLOCKED.PREFIX)
			if(prefix.toLowerCase().equals(f))
				throw new CodeError("Invalid prefix!", "The prefix name '" + prefix + "' is currently prohibited.", debug);
	}

	public static String cutJava(final String name) {
		return name.substring(5);
	}

	public static boolean isNameWithPrefix(final String name) {
//		return name.indexOf('_') > -1;
		for(int i = 0; i < name.length(); i++)
			if(name.charAt(i) == '_')
				return true;
		return false;
	}

}
