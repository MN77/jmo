/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.object.atom.Dec;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 19.12.2022
 */
public class Lib_ErrorConv {

	public static void checkConversion(final CallRuntime cr, final BigDecimal min, final BigDecimal top, final BigDecimal value, final ATOMIC to) {
		if(value.compareTo(min) < 0 || value.compareTo(top) > 0)
			Lib_ErrorConv.iThrowError(cr, to, min, top, value);
	}

	public static void checkConversion(final CallRuntime cr, final Dec min, final BigDecimal top, final BigDecimal value, final ATOMIC to) {
		Lib_ErrorConv.checkConversion(cr, min.bigDecimalValue(), top, value, to);
	}

	public static void checkConversion(final CallRuntime cr, final Dec min, final BigDecimal top, final BigInteger value, final ATOMIC to) {
		Lib_ErrorConv.checkConversion(cr, min.bigDecimalValue(), top, new BigDecimal(value), to);
	}

	public static void checkConversion(final CallRuntime cr, final Dec min, final Dec max, final BigDecimal top, final double value, final ATOMIC to) {
		if(value >= min.doubleValue() && value <= max.doubleValue())
			return;
		if(top.compareTo(BigDecimal.valueOf(value)) < 0)
			Lib_ErrorConv.iThrowError(cr, to, min, top, value);
	}

	public static void checkConversion(final CallRuntime cr, final double min, final double max, final double value, final ATOMIC to) {
		if(value < min || value > max)
			Lib_ErrorConv.iThrowError(cr, to, min, max, value);
	}

	public static void checkConversion(final CallRuntime cr, final int min, final int top, final int value, final ATOMIC to) {
		if(value < min || value > top)
			Lib_ErrorConv.iThrowError(cr, to, min, top, value);
	}

	public static void checkConversion(final CallRuntime cr, final long min, final BigDecimal top, final BigDecimal value, final ATOMIC to) {
		Lib_ErrorConv.checkConversion(cr, new BigDecimal(min), top, value, to);
	}

	public static void checkConversion(final CallRuntime cr, final long min, final BigDecimal top, final BigInteger value, final ATOMIC to) {
		Lib_ErrorConv.checkConversion(cr, new BigDecimal(min), top, new BigDecimal(value), to);
	}

	public static void checkConversion(final CallRuntime cr, final long min, final long top, final BigDecimal value, final ATOMIC to) {
		Lib_ErrorConv.checkConversion(cr, new BigDecimal(min), new BigDecimal(top), value, to);
	}

	public static void checkConversion(final CallRuntime cr, final long min, final long max, final BigDecimal top, final double value, final ATOMIC to) {
		if(value >= min && value <= max)
			return;
		if(value < min || top.compareTo(BigDecimal.valueOf(value)) < 0)
			Lib_ErrorConv.iThrowError(cr, to, min, top, value);
	}

	public static void checkConversion(final CallRuntime cr, final long min, final long top, final BigInteger value, final ATOMIC to) {
		Lib_ErrorConv.checkConversion(cr, new BigDecimal(min), new BigDecimal(top), new BigDecimal(value), to);
	}

	public static void checkConversion(final CallRuntime cr, final long min, final long max, final long value, final ATOMIC to) {
		if(value < min || value > max)
			Lib_ErrorConv.iThrowError(cr, to, min, max, value);
	}

	private static void iThrowError(final CallRuntime cr, final ATOMIC to, final Object min, final Object max, final Object value) {
		final StringBuilder detail = new StringBuilder();
		detail.append("Allowed are ");
		detail.append(min);
		detail.append(" to ");
		detail.append(max);
		detail.append(", but got: ");
		detail.append(value);

		throw new RuntimeError(cr, "Value out of bounds for conversion to " + to.name, detail.toString());
	}

}
