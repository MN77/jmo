/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.error.ErrorBaseDebug;
import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.JayMoError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.Type;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.I_BigNumber;
import org.jaymo_lang.object.passthrough.I_Mem;
import org.jaymo_lang.object.pseudo.JMo_Error;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.data.convert.ConvertSequence;
import de.mn77.base.data.group.Group2;
import de.mn77.base.data.util.Lib_Math;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_DataBase;
import de.mn77.base.error.Err_Exception;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.error.Err_Network;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.error.I_ErrorDetails;


/**
 * @author Michael Nitsche
 * @created 2019-08-03
 * @apiNote
 *          Library for JayMo-Errors
 */
public class Lib_Error {

	public static String collectMessages(Throwable t) {
		final StringBuffer sb = new StringBuffer();

		String message = t.getMessage();
		if(message == null)
			message = t.getClass().getSimpleName();

		sb.append(message);

		while(t.getCause() != null) {
			t = t.getCause();
			sb.append(" | ");
			sb.append(t.getMessage());
		}
		return sb.toString();
	}

	public static String errorTypeString(final Throwable ex) {
		// JayMo-Errors
		if(ex instanceof ErrorBaseDebug)
			return ((ErrorBaseDebug)ex).getErrorTypeInfo();
		// Base-External-Errors
		if(ex instanceof Err_FileSys || ex instanceof Err_DataBase || ex instanceof Err_Network)
			return ExternalError.ERROR_TYPE_INFO;
		// Other errors
		return JayMoError.ERROR_TYPE_INFO;
	}

	public static void handleThreadErrorEnd(final CallRuntime cr, final Throwable t) {

		try {

			if(cr.getApp().eventHasHandler(cr, "@error"))
				cr.getApp().eventRunRaw(cr, "@error", new JMo_Error(t));
			else {
				final String s = t instanceof ErrorBaseDebug
					? ((ErrorBaseDebug)t).toInfo()
					: t instanceof I_Object
						? ((I_Object)t).toString(cr, STYPE.DESCRIBE)
						: Lib_Error.iWrap(t);

				Lib_Output.err(cr, s, true);
			}
		}
		catch(final Throwable t2) {

			// Fallback
			try {
				final String s = Lib_Error.iWrap(t);
				Lib_Output.err(cr, s, true);
			}
			catch(final Throwable t3) {
				// Super Fallback
//				Err.show(t);
//				Err.show(t2);
//				Err.show(t3);
				System.err.println(t);
				System.err.println(t2);
				System.err.println(t3);
			}
		}
	}

	public static void ifArgs(final int got, final int min, final Integer max, final CallRuntime cr, final I_Object obj) {
		String want = null;

		if(max == null) {
			if(got < min)
				want = "at least " + min;
		}
		else if(!Lib_Math.isBetween(got, min, max))
			want = "" + min + (min != max ? " to " + max : "");

		if(want != null) {
			final Group2<String, Boolean> g = Lib_Output.toObjectWithFunction(cr, obj, cr.call.method, true);	//, STYPE.IDENT
			throw new CodeError(cr, "Invalid amount of arguments", "Need " + want + ", but got " + got + " for: " + g.o1);
		}
	}

	public static void ifArgs(final int got, final int min, final Integer max, final String subject, final DebugInfo debug) {
		String want = null;

		if(max == null) {
			if(got < min)
				want = "" + min;
		}
		else if(!Lib_Math.isBetween(got, min, max))
			want = min != max ? min + " to " + max : "" + min;
		if(want != null)
			throw new CodeError("Invalid amount of arguments", "Need " + want + ", but got " + got + " for: " + subject, debug);
	}

	public static void ifBigNumber(final CallRuntime cr, final I_Object obj) {
		if(obj instanceof I_BigNumber)
			throw new RuntimeError(cr, "Invalid type", "This function is currently not available for big numbers. Got " + obj.getTypeName() + ", maybe try Int, Dec or Double.");
	}

	public static void ifEmpty(final CallRuntime cr, final int size, final String type) {
		if(size == 0)
			throw new RuntimeError(cr, "Can't access position", type + " is empty");
	}

	public static void ifEmpty(final CallRuntime cr, final String value, final String type) {
		if(value == null || value.length() == 0)
			throw new RuntimeError(cr, "Got empty string", type + " is empty");
	}

	public static void ifIs(final CallRuntime cr, final int shouldNotBe, final int value, final String title) {
		if(value == shouldNotBe)
			throw new RuntimeError(cr, "Invalid " + title, "Value is not allowed: " + shouldNotBe);
	}

	public static void ifNot(final CallRuntime cr, final int shouldBe, final int value, final String title) {
		if(value != shouldBe)
			throw new RuntimeError(cr, "Invalid " + title, "Value should be " + shouldBe + ", but got: " + value);
	}

	public static void ifNotBetween(final CallRuntime cr, final double min, final double max, final BigDecimal value, final String title) {
		final double doubleValue = value.doubleValue();
		if(min == max && doubleValue != min)
			throw new RuntimeError(cr, "Value out of bounds, invalid " + title, title + " is not: " + value);
		if(min > max)
			Err.forbidden(min, max, value);

		if(doubleValue < min || doubleValue > max) {
			final String range = min < max ? min + " to " + max : "" + min;
			throw new RuntimeError(cr, "Value out of bounds, invalid " + title, "Allowed are " + range + ", but got: " + value);
		}
	}

	public static void ifNotBetween(final CallRuntime cr, final double min, final double max, final double value, final String title) {
		if(min == max && value != min)
			throw new RuntimeError(cr, "Value out of bounds, invalid " + title, title + " is not: " + value);
		if(min > max)
			Err.forbidden(min, max, value);

		if(value < min || value > max) {
			final String range = min < max ? min + " to " + max : "" + min;
			throw new RuntimeError(cr, "Value out of bounds, invalid " + title, "Allowed are " + range + ", but got: " + value);
		}
	}

	public static void ifNotBetween(final CallRuntime cr, final double abs, final double value, final String title) {
		Err.ifTooSmall(0, abs);
		if(value > abs || value < -abs)
			throw new RuntimeError(cr, "Value out of bounds, invalid " + title, title + " must be between -" + abs + " and " + abs + ", but got: " + value);
	}

	public static void ifNotBetween(final CallRuntime cr, final int min, final int max, final int value, final String title) {
		if(min == max && value != min)
			throw new RuntimeError(cr, "Value out of bounds, invalid " + title, title + " must be " + min + ", but got: " + value);
		if(min > max)
			Err.forbidden(min, max, value);

		if(value < min || value > max) {
			final String range = min < max ? min + " to " + max : "" + min;
			throw new RuntimeError(cr, "Value out of bounds, invalid " + title, "Allowed are " + range + ", but got: " + value);
		}
	}

	public static void ifNotBetween(final CallRuntime cr, final int abs, final int value, final String title) {
		Err.ifTooSmall(0, abs);
		if(value > abs || value < -abs)
			throw new RuntimeError(cr, "Value out of bounds, invalid " + title, title + " must be between -" + abs + " and " + abs + ", but got: " + value);
	}

	public static void ifNotBetween(final CallRuntime cr, final long min, final long max, final BigInteger value, final String title) {
		long longValue;

		try {
			longValue = value.longValueExact();
		}
		catch(final ArithmeticException e) {
			final String range = min < max ? min + " to " + max : "" + min;
			throw new RuntimeError(cr, "Value out of bounds, invalid " + title, "Allowed are " + range + ", but got: " + value);
		}
		if(min == max && longValue != min)
			throw new RuntimeError(cr, "Value out of bounds, invalid " + title, title + " must be " + min + ", but got: " + value);
		if(min > max)
			Err.forbidden(min, max, value);

		if(longValue < min || longValue > max) {
			final String range = min < max ? min + " to " + max : "" + min;
			throw new RuntimeError(cr, "Value out of bounds, invalid " + title, "Allowed are " + range + ", but got: " + value);
		}
	}

	public static void ifNotBetween(final int min, final int max, final int value, final String title, final DebugInfo debug) {
		if(min == max && value != min)
			throw new CodeError("Value out of bounds, invalid " + title, title + " is not: " + value, debug);
		if(min > max)
			Err.forbidden(min, max, value);

		if(value < min || value > max) {
			final String range = min < max ? min + " to " + max : "" + min;
			throw new CodeError("Value out of bounds, invalid " + title, "Allowed are " + range + ", but got: " + value, debug);
		}
	}

	public static void ifNotControl(final CallRuntime cr, final Type type) {
		if(!type.isControl())
			throw new RuntimeError(cr, "This type has no control functionality!", "Maybe '!!' is missing at the end of the type definition.");
	}

	public static void ifTooHigh(final CallRuntime cr, final int max, final int value) { // TODO Title ?!?
		if(value > max)
			throw new RuntimeError(cr, "Value too big", "Maximum is " + max + ", but got: " + value);
	}

	public static void ifTooSmall(final CallRuntime cr, final long min, final long value) { // TODO Title ?!?
		if(value < min)
			throw new RuntimeError(cr, "Value too small", "Minimum is " + min + ", but got: " + value);
	}

//	public static String collectDetailLines(Throwable t) {
//		SimpleList<String> al = new SimpleList<>();
//		while(t != null) {
//			al.add(Lib_Error.errorToString(t));
//			if(t instanceof I_ErrorDetails)
//				for(Object o : ((I_ErrorDetails)t).getDetails())
//					al.add(o.toString());
//			t = t.getCause();
//		}
//		return String.join("\n", al);
//	}

//	public static String errorToString(Throwable t) {
//		return t instanceof I_ErrorInfo
//		? ((I_ErrorInfo)t).toInfo()
//		: t instanceof I_Object
//			? ((I_Object)t).toString(null, STYPE.DESCRIBE)
//			: t.toString();
////			: t.getMessage();
//	}

	public static CodeError memNotInitialized(final CallRuntime cr, final I_Mem vc) {
		String type = vc.getMemTypeString();
		type = Lib_String.capitalize(type, true);
		throw new CodeError(cr, type + " not initialized", "No value assigned to: " + vc.getName());
	}

	public static CodeError notAllowed(final CallRuntime cr) {
		throw new CodeError(cr, "Not allowed!", "You can't run this function!");
	}

	public static void notImplementedYet(final CallRuntime cr, final String detail) {
		throw new JayMoError(cr, "Not implemented yet", detail);
	}

	public static Throwable wrap(final Throwable t) {
		if(t instanceof ErrorBaseDebug)
			return t;
		if(t instanceof Err_Exception || t instanceof Err_Runtime)
			return new JayMoError(t, Lib_Error.collectMessages(t), ConvertSequence.toString(", ", ((I_ErrorDetails)t).getDetails()), null);

		return new JayMoError(t, t.getClass().getSimpleName(), Lib_Error.collectMessages(t), null);
	}

	private static String iWrap(final Throwable t) {

		try {
			return new ErrorComposer(t, null).compose();
		}
		catch(final Throwable x) {
			// Fallback
			return t.toString();
		}
	}

}
