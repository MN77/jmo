/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.object.passthrough.Var;
import org.jaymo_lang.parser.obj.ParseObj_MagicConst;


/**
 * @author Michael Nitsche
 * @created 21.11.2020
 */
public class BLOCKED {

	public static final String[] VAR_CONST_LOWER = {"app", "cur", "each", "false", "func", "jaymo", "jmo", "nil", "null", "loop", "super", "this", "true", "event", ATOMIC.INFINITY,
		ATOMIC.NOT_A_NUMBER, "nan"}; // "it" is okay
	public static final String[] VAR_CONST_UPPER = {"APP", "CUR", "EACH", "FALSE", "FUNC", "IT", "JAYMO", "JMO", "NIL", "NULL", "LOOP", "SUPER", "THIS", "TRUE", "EVENT", ATOMIC.INFINITY_UP,
		ATOMIC.NOT_A_NUMBER_UP, "NAN"};

	public static final String[] FUNCTIONS = {
		/*
		 * = From A_Object:
		 *
		 * cd git/jaymo/JayMo/src/org/jaymo_lang/object/
		 * cat A_Object.java |grep case|trim|grep -v "^//" | grep '"'|sed "s#case ##g"|sed 's#:.*##'|trim|sort|grep -v "[<>=\!¶]" | xargs | sed -e 's/ /", "/g'|sed 's#^#"#'|sed 's#$#"#'
		 * Remove: "toDescribe", "toIdent", "toStr"
		 * Clean up
		 */
		"assert", "assertIf", "assertIs", "assertType", "async", "at", "atNot", "breakAt", "breakAtNot", "breakIf", "breakIfNot", "breakIs", "breakIsNot", "catch", "compare", "compares", "compareTo",
		"convertMem", "debug", "describe", "differs", "diffly", "echo", "echoErr", "equally", "equals", "format", "ident", "if", "ifExec", "ifNil", "ifNot", "ifNotNil", "inherits", "is",
		"isDifferent", "isDiffly", "isEqual", "isEqually", "isError", "isNil", "isNot", "isNotNil", "isOther", "isSame", "isType", "mem", "nilUse", "other", "pass", "passAt", "passAtNot", "passIf",
		"passIfNot", "passIs", "passIsNot", "pipe", "pipeBuffer", "pipeError", "pipeLive", "pipeOutput", "print", "printErr", "printLog", "proc", "repeat", "replaceNil", "same", "sync", "tee",
		"toTempLet", "try", "tryLazy", "tryUse", "type", "types", "typeUse", "use", "which", "while"

		/* = From Var: */
		, "let", "convertLet"

		/* = Other: */
//		,"end", "return"
//		,"init", "_init"
	};

	public static final String[] EVENT_NAMES = {Var.CHANGED_EVENT};

	public static final String[] PREFIX = {"std"}; // only lowercase

	public static final String[] MAGIC_CONSTS = ParseObj_MagicConst.MAGIC_CONSTS;

}
