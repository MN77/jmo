/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.object.atom.A_Number;
import org.jaymo_lang.object.atom.I_Decimal;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.group.Group3;


/**
 * @author Michael Nitsche
 * @created 19.07.2022
 */
public class Lib_NumberStyle {

	public static String style(final CallRuntime cr, final A_Number number, final String format) {
		final Group3<Integer, Boolean, String> formatDotIndex = Lib_NumberStyle.iFormatDotIndex(format); // index,required,dot
		final boolean useDot = formatDotIndex != null && (formatDotIndex.o2 || number instanceof I_Decimal);
		final String nums = useDot
			? "" + Lib_Convert.getDoubleValue(cr, number)
			: "" + Lib_Convert.getIntValue(cr, number);

		final int numberDotIndex = Lib_NumberStyle.iNumberDotIndex(nums);

		final String numberLeft = numberDotIndex == -1
			? nums
			: nums.substring(0, numberDotIndex);
		final String numberRight = numberDotIndex == -1
			? ""
			: nums.substring(numberDotIndex + 1);

		final String formatLeft = formatDotIndex == null
			? format
			: format.substring(0, formatDotIndex.o1);
		final String formatRight = formatDotIndex == null
			? ""
			: format.substring(formatDotIndex.o1 + 1);

		final String dot = useDot
			? formatDotIndex.o3
			: "";


		// Left
		String left = "";
		int nPos = numberLeft.length() - 1;

		for(int fPos = formatLeft.length() - 1; fPos >= 0; fPos--) {
			final char f = formatLeft.charAt(fPos);

			// Jump
			if(fPos > 0 && formatLeft.charAt(fPos - 1) == '~') {
				left = f + left;
				fPos--;
				continue;
			}

			switch(f) {
				case '0':
					if(nPos >= 0)
						left = numberLeft.charAt(nPos) + left;
					else
						left = "0" + left;
					nPos--;
					break;
				case '#':
					if(nPos >= 0)
						left = numberLeft.charAt(nPos) + left;
					else
						left = " " + left;
					nPos--;
					break;
				case '?':
					if(nPos >= 0)
						left = numberLeft.charAt(nPos) + left;
					nPos--;
					break;
				case '_':
					if(nPos >= 0)
						left = " " + left;
					break;
				case '*':
					if(nPos >= 0)
						left = "." + left;
					break;

				default:
					left = f + left;
			}
		}
		// Add remeaning integer numbers
		if(nPos >= 0)
			left = numberLeft.substring(0, nPos + 1) + left;


		// Right
		boolean jump = false;
		StringBuilder right = new StringBuilder();
		nPos = 0;
		boolean hasLeftNum = false;
		final int nLen = numberRight.length();

		for(int fPos = 0; fPos < formatRight.length(); fPos++) {
			final char f = formatRight.charAt(fPos);

			if(jump) {
				right = right.append(f);
				jump = false;
				continue;
			}

			if(f == '~') {
				jump = true;
				continue;
			}

			switch(f) {
				case '0':
					if(nPos < nLen)
						right = right.append(numberRight.charAt(nPos));
					else
						right = right.append('0');
					hasLeftNum = true;
					nPos++;
					break;
				case '#':
					if(nPos < nLen) {
						right = right.append(numberRight.charAt(nPos));
						hasLeftNum = true;
					}
					else
						right = right.append(' ');
					nPos++;
					break;
				case '?':
					if(nPos < nLen) {
						right = right.append(numberRight.charAt(nPos));
						hasLeftNum = true;
					}
					nPos++;
					break;
				case '_':
					if(hasLeftNum)
						right = right.append(' ');
					break;
				case '*':
					if(hasLeftNum)
						right = right.append('.');
					break;

				default:
					right = right.append(f);
			}
		}
		return left + dot + right.toString();
	}

	/**
	 * @return index,required,dot
	 */
	private static Group3<Integer, Boolean, String> iFormatDotIndex(final String s) {
		boolean jump = false;

		for(int i = 0; i < s.length(); i++) {

			if(jump) {
				jump = false;
				continue;
			}

			switch(s.charAt(i)) {
				case '~':
					jump = true;
					continue;
				case '.':
					return new Group3<>(i, true, ".");
				case ',':
					return new Group3<>(i, true, ",");
				case ':':
					return new Group3<>(i, false, ".");
				case ';':
					return new Group3<>(i, false, ",");
			}
		}
		return null;
	}

	private static int iNumberDotIndex(final String s) {
		for(int i = 0; i < s.length(); i++)
			if(s.charAt(i) == '.')
				return i;

		return -1;
	}

}
