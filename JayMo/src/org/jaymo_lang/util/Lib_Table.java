/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_Number;
import org.jaymo_lang.object.struct.A_Sequence;
import org.jaymo_lang.object.struct.JMo_Table;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.form.FormString;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.util.Lib_String;


/**
 * @author Michael Nitsche
 * @created 31.08.2021
 */
public class Lib_Table {

	public static String toText(final boolean describe, final CallRuntime cr, final JMo_Table tabObj, final ArrayTable<I_Object> tab, final I_Object[] titles) {
		if(tab == null)
			return "Table";

		final int width = tab.width();
		final boolean[][] right = new boolean[width][tab.size()];

		// Convert all Objects to Text
		final SimpleList<String[]> otab = new SimpleList<>(tab.size());
		int y = 0;

		for(final I_Object[] z : tab) {
			final String[] sa = new String[z.length];

			for(int x = 0; x < sa.length; x++) {
				final I_Object o = z[x];
				right[x][y] = o instanceof A_Number;

				if(o == null)
					sa[x] = "nil";
				else if(o == tabObj)
					sa[x] = A_Sequence.CYCLE_IDENT;
				else if(!describe)
					sa[x] = o.toString(cr, STYPE.NESTED);
				else {
					String so = o.toString(cr, STYPE.DESCRIBE);
					so = Lib_Output.indentLines(so, false);
					sa[x] = so;
				}
			}
			otab.add(sa);
			y++;
		}
		// Convert titles to text
		final String[] headers = titles == null ? null : new String[width];
		if(titles != null)
			for(int column = 0; column < width; column++) {
				final I_Object obj = titles[column];
				final String title = describe
					? obj.toString(cr, STYPE.DESCRIBE)
					: obj.toString();
				headers[column] = title;
			}

		// Estimate needed width
		final int[] maxw = new int[width];

		for(int column = 0; column < width; column++) {
			int max = 0;

			for(int row = 0; row < tab.size(); row++)
				max = Math.max(max, otab.get(row)[column].length());

			if(headers != null)
				max = Math.max(max, headers[column].length());

			maxw[column] = max;
		}
		// Convert to String
		final StringBuilder sb = new StringBuilder();

		if(headers != null) {

			for(int spalte = 0; spalte < width; spalte++) {
				final String o = headers[spalte];
				sb.append(FormString.width(maxw[spalte], ' ', o, ALIGN.LEFT, false));
				if(spalte < width - 1)
					sb.append('|');
			}
			sb.append('\n');

			for(int spalte = 0; spalte < width; spalte++) {
				final String o = Lib_String.sequence('=', maxw[spalte]);
				sb.append(FormString.width(maxw[spalte], ' ', o, ALIGN.LEFT, false));
				if(spalte < width - 1)
					sb.append('|');
			}
			sb.append('\n');
		}
		y = 0;

		for(final String[] zeile : otab) {
//			if(debug && y == 1) {
//				sb.replace(sb.length() - 1, sb.length(), "¶…");
//				break;
//			}

			for(int spalte = 0; spalte < width; spalte++) {
				final String o = zeile[spalte];
				final ALIGN align = right[spalte][y]
					? ALIGN.RIGHT
					: ALIGN.LEFT;
				sb.append(FormString.width(maxw[spalte], ' ', o, align, false));
				if(spalte < width - 1)
					sb.append('|');
			}
			y++;
			sb.append('\n');
		}
		Lib_Output.removeEnd(sb, '\n');

		return sb.toString();
	}

}
