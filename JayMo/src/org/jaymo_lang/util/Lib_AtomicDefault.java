/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Char;
import org.jaymo_lang.object.atom.Dec;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_BigDec;
import org.jaymo_lang.object.atom.JMo_BigInt;
import org.jaymo_lang.object.atom.JMo_Byte;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.JMo_Float;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.JMo_Short;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.Nil;


/**
 * @author Michael Nitsche
 * @created 06.08.2022
 */
public class Lib_AtomicDefault {

	public static final Bool       BOOL   = Bool.FALSE;
	public static final JMo_Byte   BYTE   = new JMo_Byte((byte)0);
	public static final JMo_Short  SHORT  = new JMo_Short((short)0);
	public static final Int        INT    = new Int(0);
	public static final JMo_Long   LONG   = new JMo_Long(0l);
	public static final JMo_BigInt BIGINT = new JMo_BigInt(BigInteger.ZERO);
	public static final JMo_Dec    DEC    = new JMo_Dec(Dec.ZERO);
	public static final JMo_Float  FLOAT  = new JMo_Float(0f);
	public static final JMo_Double DOUBLE = new JMo_Double(0d);
	public static final JMo_BigDec BIGDEC = new JMo_BigDec(BigDecimal.ZERO);
	public static final Char       CHAR   = new Char(' ');
	public static final Str        STR    = new Str("");


	public static A_Immutable getDefault(final ATOMIC type) {
//		if(type == null || type.endsWith("?"))
//			return Nil.NIL;

		switch(type) {
			case BOOL:
				return Lib_AtomicDefault.BOOL;
			case BYTE:
				return Lib_AtomicDefault.BYTE;
			case SHORT:
				return Lib_AtomicDefault.SHORT;
			case INT:
				return Lib_AtomicDefault.INT;
			case LONG:
				return Lib_AtomicDefault.LONG;
			case BIGINT:
				return Lib_AtomicDefault.BIGINT;
			case DEC:
				return Lib_AtomicDefault.DEC;
			case FLOAT:
				return Lib_AtomicDefault.FLOAT;
			case DOUBLE:
				return Lib_AtomicDefault.DOUBLE;
			case BIGDEC:
				return Lib_AtomicDefault.BIGDEC;
			case CHAR:
				return Lib_AtomicDefault.CHAR;
			case STR:
				return Lib_AtomicDefault.STR;

			default:
				return Nil.NIL;
		}
	}

}
