/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.util;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.JMo_Range;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.atom.A_Number;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Char;
import org.jaymo_lang.object.atom.Dec;
import org.jaymo_lang.object.atom.I_Decimal;
import org.jaymo_lang.object.atom.I_Integer;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 20.08.2022
 */
public class Lib_Count {

	@SuppressWarnings("unchecked")
	public static final Class<? extends I_Object>[] typesStart = new Class[]{A_Number.class, Bool.class, Char.class, JMo_Range.class};
	@SuppressWarnings("unchecked")
	public static final Class<? extends I_Object>[] typesEnd   = new Class[]{A_Number.class, Bool.class, Char.class};
	@SuppressWarnings("unchecked")
	public static final Class<? extends I_Object>[] typesStep  = new Class[]{A_Number.class};


	/**
	 * @implNote
	 *           execBlockStream is here because of RangeToList
	 *           TODO Don't use ArgCallBuffer for the loop
	 **/
	public static ObjectCallResult each(final CallRuntime crOld, final I_Object cur, ArgCallBuffer start, ArgCallBuffer end, ArgCallBuffer step, final boolean toList, final boolean execBlockStream) {
		if(crOld.call == null && execBlockStream)
			Err.invalid(crOld.call, toList, execBlockStream);

		// Prevent senseless Count-Shortcuts: {{{123}}}
		if(execBlockStream && !crOld.hasBlock() && !crOld.hasStream())
			throw new CodeError(crOld, "Missing block or stream", "Block or stream needed for: " + Lib_Type.getTypeString(cur) + "." + crOld.call.method);

		I_Object result = cur;
		final SimpleList<I_Object> list = toList
			? new SimpleList<>()
			: null;

		if(Lib_Type.typeIs(crOld, start.get(), JMo_Range.class)) {
			if(step != null)
				throw new CodeError(crOld, "Invalid amount of arguments", "With Range, only 1 or 2 args are allowed.");

			// --- Range,null,null ---
			// --- Range,Int,null ---
			final JMo_Range range = (JMo_Range)start.get();
			final Group2<I_Object, I_Object> rangePars = range.getInternalValues();

			start = new ArgCallBuffer(0, new Call(crOld, rangePars.o1));
			start.initExt(crOld, range, A_Number.class, Char.class);

			if(end != null)
				step = end;

			end = new ArgCallBuffer(0, new Call(crOld, rangePars.o2));
			end.initExt(crOld, range, A_Number.class, Char.class);
		}

		// --- Only one argument ---
		if(start != null && end == null && step == null) {
			end = start;
			final double endVal = Lib_Convert.getDoubleValue(crOld, end.get());

			if(endVal == 0)
				return toList
					? new ObjectCallResult(new JMo_List(list), false)
					: new ObjectCallResult(result, true);

			if(endVal < 1)
				throw new RuntimeError(crOld, "Invalid arguments", "Please use two or three arguments for end value: " + endVal);

			start = new ArgCallBuffer(0, new Call(crOld, new Int(1)));
			start.init(crOld, cur, Int.class);
		}
		if(execBlockStream && crOld.getStream() == null && crOld.getCallBlock() == null) // && args==null
			return new ObjectCallResult(end.get(), true);

		final LoopHandle handle = new LoopHandle(cur);
		final CallRuntime crNew = crOld.copyLoop(handle);

		//------------- Loops -------------

		if(Lib_Type.typeIs(crNew, start.get(), I_Integer.class, Char.class) && Lib_Type.typeIs(crNew, end.get(), I_Integer.class, Char.class)
			&& (step == null || Lib_Type.typeIs(crNew, step.get(), I_Integer.class))) {
			// --- {Integer,Integer,Integer} ---
			// --- {Char,Char,Integer} ---
			int i1 = Lib_Convert.getIntValue(crNew, start.get(), true);
			int i2 = Lib_Convert.getIntValue(crNew, end.get(), true);
			boolean desc = i2 < i1;
			int i3 = step == null
				? desc
					? -1
					: 1
				: Lib_Convert.getIntValue(crNew, step.get());

			if(desc ? i3 > 0 : i3 < 0)
				throw new RuntimeError(crNew, "Endless running loop", "From " + i1 + " to " + i2 + " with stepping " + i3);

			int i = 0;
			I_Object it = null;

			for(i = i1; desc
				? i >= i2
				: i <= i2; i += i3) {
				handle.startLap();
				final boolean last = desc ? i <= i2 : i >= i2;

				it = Lib_Convert.intToObject(Lib_Type.getType(crNew, start.get()), Lib_Type.getType(crNew, end.get()), i); // , varlet
				if(toList)
					list.add(it);
				else if(execBlockStream) {
					result = Lib_Exec.execBlockStream(crNew, it);

					if((result = Lib_Exec.loopResult(result)) instanceof Return)
						return ((Return)result).getLoopResult();
				}

				if(!last) {
					i1 = Lib_Convert.getIntValue(crNew, start.getInitLoop(crNew, cur, Lib_Count.typesStart), true);
					i2 = Lib_Convert.getIntValue(crNew, end.getInitLoop(crNew, cur, Lib_Count.typesEnd), true);
					desc = i2 < i1;
					i3 = step == null
						? desc
							? -1
							: 1
						: Lib_Convert.getIntValue(crNew, crNew.argType(step.getInitLoop(crNew, cur, Lib_Count.typesStep), Int.class));
				}
			}
		}
		else if(Lib_Type.typeIs(crNew, start.get(), JMo_Dec.class, I_Integer.class) && Lib_Type.typeIs(crNew, end.get(), JMo_Dec.class, I_Integer.class)
			&& (step == null || Lib_Type.typeIs(crNew, step.get(), JMo_Dec.class, I_Integer.class))) {
			// --- => Decimal ---
			Dec d1 = Lib_Convert.getDecValue(crNew, start.get());
			Dec d2 = Lib_Convert.getDecValue(crNew, end.get());
			boolean desc = d2.isLess(d1);
			Dec d3 = step == null
				? desc
					? Dec.NEGATIVE_ONE
					: Dec.ONE
				: Lib_Convert.getDecValue(crNew, step.get());

			if(desc ? d3.isGreaterZero() : d3.isLessZero())
				throw new RuntimeError(crNew, "Endless running Counter", "From " + d1 + " to " + d2 + " with stepping " + d3);

			for(Dec d = d1; desc
				? d.isGreaterOrEqual(d2)
				: d.isLessOrEqual(d2); d = d.add(d3)) {
//				d = Lib_Math.normalize(d, 10); // Correct fractal!!! Current limit is 10 digits!

				handle.startLap();
				final boolean last = desc ? d.isLessOrEqual(d2) : d.isGreaterOrEqual(d2);

				final I_Object it = new JMo_Dec(d);
				if(toList)
					list.add(it);
				else if(execBlockStream) {
					result = Lib_Exec.execBlockStream(crNew, it);

					if((result = Lib_Exec.loopResult(result)) instanceof Return)
						return ((Return)result).getLoopResult();
				}

				if(!last) {
					d1 = Lib_Convert.getDecValue(crNew, start.getInitLoop(crNew, cur));
					d2 = Lib_Convert.getDecValue(crNew, end.getInitLoop(crNew, cur));
					desc = d2.isLess(d1);
					d3 = step == null
						? desc
							? Dec.NEGATIVE_ONE
							: Dec.ONE
						: Lib_Convert.getDecValue(crNew, crNew.argTypeExt(step.getInitLoop(crNew, cur), Lib_Count.typesStep));
				}
			}
		}
		else if(Lib_Type.typeIs(crNew, start.get(), I_Decimal.class, I_Integer.class) && Lib_Type.typeIs(crNew, end.get(), I_Decimal.class, I_Integer.class)
			&& (step == null || Lib_Type.typeIs(crNew, step.get(), I_Decimal.class, I_Integer.class))) {
			// --- => Decimal ---
			double d1 = Lib_Convert.getDoubleValue(crNew, start.get());
			double d2 = Lib_Convert.getDoubleValue(crNew, end.get());
			boolean desc = d2 < d1;
			double d3 = step == null
				? desc
					? -1
					: 1
				: Lib_Convert.getDoubleValue(crNew, step.get());

			if(desc ? d3 > 0 : d3 < 0)
				throw new RuntimeError(crNew, "Endless running Counter", "From " + d1 + " to " + d2 + " with stepping " + d3);

			for(double d = d1; desc
				? d >= d2
				: d <= d2; d += d3) {
//				d = Lib_Math.normalize(d, 10); // Correct fractal!!! Current limit is 10 digits!

				handle.startLap();
				final boolean last = desc ? d <= d2 : d >= d2;

				final I_Object it = new JMo_Double(d);
				if(toList)
					list.add(it);
				else if(execBlockStream) {
					result = Lib_Exec.execBlockStream(crNew, it);

					if((result = Lib_Exec.loopResult(result)) instanceof Return)
						return ((Return)result).getLoopResult();
				}

				if(!last) {
					d1 = Lib_Convert.getDoubleValue(crNew, start.getInitLoop(crNew, cur));
					d2 = Lib_Convert.getDoubleValue(crNew, end.getInitLoop(crNew, cur));
					desc = d2 < d1;
					d3 = step == null
						? desc
							? -1
							: 1
						: Lib_Convert.getDoubleValue(crNew, crNew.argTypeExt(step.getInitLoop(crNew, cur), Lib_Count.typesStep));
				}
			}
		}
		else
			throw new CodeError(crNew, "Invalid parameters", cur.toString(crNew, STYPE.IDENT));
		return toList
			? new ObjectCallResult(new JMo_List(list), false)
			: new ObjectCallResult(result, true);
	}

}
