/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang._test;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.model.App;
import org.jaymo_lang.parser.Parser_App;
import org.jaymo_lang.util.ErrorComposer;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.debug.DEBUG_MODE;
import de.mn77.base.debug.Stopwatch;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.file.I_File;
import de.mn77.base.sys.file.MFile;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class TestFile {

	private static final DEBUG_MODE debug = DEBUG_MODE.NO;
//	private static final DEBUG_MODE debug = DEBUG_MODE.MINIMAL;
//	private static final DEBUG_MODE debug = DEBUG_MODE.DETAIL;

	private final String auto  = "/home/mike/Prog/JayMo/tests_auto/";
	private final String local = "/home/mike/Prog/JayMo/tests_local/";
	private final String manu  = "/home/mike/Prog/JayMo/tests_man/";
	private final String work  = "/home/mike/Prog/JayMo/work/";


//	private final String TEST = this.local + "help_quickly_1.jmo";
//	private final String TEST = this.local + "help_all_types.jmo";

//	private final String TEST = this.work + "table_sort.jmo";
//	private final String TEST = this.work + "kaninchen2.jmo";
//	private final String TEST = this.work + "list_sort.jmo";
//	private final String TEST = this.work + "str_modifiers.jmo";
//	private final String TEST = this.work + "type_classic.jmo";
	private final String TEST = this.work + "";


	private final String ARGS = "";


	//----------------------------------------------------------------------


	public static void main(final String[] args) {

		try {
			final TestFile tester = new TestFile();
			tester.start();
		}
		catch(final Throwable t) {
			Err.exit(Lib_Error.wrap(t));
		}
	}

	private void start() {
		MOut.setDebug(TestFile.debug);
		final MFile d = new MFile(this.TEST);

		try {
			final Stopwatch uhr = new Stopwatch();
			this.test(d, this.ARGS);
			uhr.print();
		}
		catch(final Throwable t) {
			final ErrorComposer composer = new ErrorComposer(t, null);
			final String err = composer.compose();
			MOut.printErr(err);
		}
	}

	private void test(final I_File datei, final String... args) throws Exception {
		final Parser_App parser = new Parser_App(true);
		if(TestFile.debug != DEBUG_MODE.NO)
			parser.setDebug();

		// MOut.print("JayMo "+version.toFormat("%y.%n%?tf - %dd.mm.yyyy"));
		MOut.print(JayMo.NAME + "  " + parser.getVersionString(false, true));

		final Stopwatch uhr = new Stopwatch();
		MOut.print("===== " + datei.getName());
		final String line = "-----------------------------------";
//		final App app = parser.parseText("> lazyErrors\n");
//		parser.parseFile(app, datei);
		final App app = parser.parseFile(datei.getFile());

		MOut.print(uhr);
		MOut.print(line);
		app.describe();
		MOut.print(uhr);
		MOut.print(line);
		// main.exec();
//		final String result = app.exec(args);
		final String result = app.execToDescribe(args);
//		MOut.print(line);
//		MOut.print("=> " + result);
		MOut.print("================ RESULT ================\n" + result);
//		MOut.print(uhr);
		MOut.print("========================================\n" + uhr);
	}

}
