/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang._test;

import org.jaymo_lang._test.lib.AutoTest;
import org.jaymo_lang._test.lib.FAIL_ACTION;

import de.mn77.base.debug.DEBUG_MODE;
import de.mn77.base.error.Err;
import de.mn77.base.sys.file.I_Directory;
import de.mn77.base.sys.file.SysDir;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class TestOne_Local {

	private static final String TEST = "";


	private static final FAIL_ACTION FAIL       = FAIL_ACTION.STOP;
	private static final DEBUG_MODE  OUTPUT     = DEBUG_MODE.NO;
	private static final boolean     PARSE_ONLY = false;


	public static void main(final String[] args) {

		try {
			final I_Directory dir = SysDir.current().dirMust("test").dirMust("local");
			AutoTest.testOne(TestOne_Local.FAIL, TestOne_Local.OUTPUT, TestOne_Local.PARSE_ONLY, TestOne_Local.TEST, dir);
		}
		catch(final Throwable t) {
			Err.exit(t);
		}
	}

}
