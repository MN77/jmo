/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang._test;

import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 30.08.2019
 *
 *          This class is for testing to access Java functions from scripts
 */
public class Test_Java4JMo {

	public final boolean b;
	public final int     i;
	public final String  s;


	// -----


	public static int stat() {
		MOut.print("2 : ");
		return 2;
	}

	public static int stat(final int i) {
		MOut.print("3 : " + i);
		return 3;
	}

	public static String stat(final String s) {
		MOut.print("4 : " + s);
		return "4";
	}

	public static void statVoid() {
		MOut.print("1 : ");
	}

	// -----

	public Test_Java4JMo() {
		this.b = false;
		this.i = 4;
		this.s = "Hello";
	}

	public Test_Java4JMo(final boolean b2) {
		this.b = b2;
		this.i = 4;
		this.s = "Hello";
	}

	public Test_Java4JMo(final boolean b2, final int i2, final String s2) {
		this.b = b2;
		this.i = i2;
		this.s = s2;
	}

	public Test_Java4JMo(final int i2) {
		this.b = false;
		this.i = i2;
		this.s = "Hello";
	}

	// -----

	public boolean getBool() {
		return this.b;
	}

	public int getInt() {
		return this.i;
	}

	public String getString() {
		return this.s;
	}

	public int pubVal() {
		MOut.print("6 : ");
		return 6;
	}

	public int pubVal(final boolean i) {
		MOut.print("7 : " + i);
		return 7;
	}

	// -----

	public int pubVal(final int i) {
		MOut.print("8 : " + i);
		return 8;
	}

	public String pubVal(final String s) {
		MOut.print("9 : " + s);
		return "9";
	}

	public void pubVoid() {
		MOut.print("5 : ");
	}

	// -----

//	 public static void main(String[] args) {
//		 Test_Java4JMo t = new Test_Java4JMo();
//		 t.pubVal(3);
//	 }
}
