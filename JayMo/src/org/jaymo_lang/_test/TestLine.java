/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang._test;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.model.App;
import org.jaymo_lang.parser.Parser_App;
import org.jaymo_lang.util.ErrorComposer;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.debug.DEBUG_MODE;
import de.mn77.base.debug.Stopwatch;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.MOut;
import de.mn77.base.sys.Sys;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class TestLine {

	private static boolean WAIT_AFTER_EXEC = false;


	public static void main(final String[] args) {

		try {
			final Stopwatch uhr = new Stopwatch();
			TestLine.start();
			MOut.setDebug(DEBUG_MODE.MINIMAL);
			uhr.print();
			if(TestLine.WAIT_AFTER_EXEC)
				Sys.sleep(250);
//			MOut.print("-----------------------------------");
		}
		catch(final Throwable t) {
			Err.exit(Lib_Error.wrap(t));
		}
	}

	// --------------------------------------------------------------------------------------------------------------------

	protected static void start() throws Err_FileSys {
		// MOut.sProduktiv();
//		MOut.setDebug(DEBUG_MODE.MINIMAL);
//		MOut.setDebug(DEBUG_LEVEL.DETAIL);
		// ----------------------------------------------------------------------------------

//		TestLine.run("c¿ = 'x'; c.type.print; \"{} - {5}\".fill(c¿, c¿.type).print");
//		TestLine.run("a = 2s; b=3i;c¿=a/b;\"{5} / {5} = {6} {}\".fill(a, b, c¿.type, c¿).print");
//		TestLine.run("3..9.print");
//		TestLine.run("3..9.print");	//rangeTo   eachTo    3..9
//		TestLine.run("h='f';('c'..h).each.print");
//		TestLine.run("h='f'; 'c'..h.do.print");
//		TestLine.run("3..7 => x; x.print");

//		TestLine.run("Print 'a' ; Print 'b'; Echo 'c';Print 'd'");


//		TestLine.run("s=\"abcdefg\";s.set('T',3);s.print");	//TODO

//		TestLine.run("_JMO.exec(\"123.print\")");
//		TestLine.run("a=1; 9.print(a); _APP.exec(\"123.print; a=2\").print; a.print"); //; a.print


//		TestLine.run("(1..9).calcStr.print; Random.char(\"AB\", 'a'..'4', 5, '+', \",.\").print; Random.str(30, \"abc\",0, 'A'..'Z').print; Random.str(30, 'a'..'z', 'A'..'Z', '0'..'9').print");

//		TestLine.run("[1,5,6,7,8,4,3,5,2,9,8,6,4,5].begin(0).print");

//		TestLine.run("a = 5; a--; a.print; a = 5; a++.print; a.print; a=5; (a++.**2).print; a.print");	// 5 / 6, 5 / 36, 5			// 4 / 6, 6 / 36, 6
//		TestLine.run("a = 5; a--; a.print; a = 5; a++.print; a.print; a=5; a++.**(1).print; a.print");	// TESTEN (1) ?!?
//		TestLine.run("a = 4; ((a)++-3).print; a.print");
//		TestLine.run("a = 4; ((a)++3).print; a.print");
//		TestLine.run("a = 5; a <<= 2; a.print");
//		TestLine.run("(2 <= 2*2 || false).print");
//		TestLine.run("2 <= 4 || false");
//		TestLine.run("it=5; it.print");
//		TestLine.run("a=\"s\u00aeg\"; a.print");
//		TestLine.run("\"s\u00aeg\".print");
//		TestLine.run("('a'+\"\u00ae\"+'z').print");

//		TestLine.run("Date(2021,1,09).dayOfWeek.print");
//		TestLine.run("\"ab\".reverse.reverse.print");
//		TestLine.run("a=3; a::@VarChanged");


//		TestLine.run("(1..9).calcStr.print; Random.char(\"AB\", 'a'..'4', 5, '+', \",.\").print; Random.str(30, \"abc\",0, 'A'..'Z').print; Random.str(30, 'a'..'z', 'A'..'Z', '0'..'9').print");
//		TestLine.run("_THIS._sleep(4000)");


//		TestLine.run("PrintErr 'a'; Print 'b'; EchoErr 'c'; Echo 'd'; Print");
//		TestLine.run("55357.toBin.print");
//		TestLine.run("\"abcdef\".replace(this, 'a').print");
//		TestLine.run("'a'..'h'.calcStr.print.insert( 'z', -3).print");
//		TestLine.run("jmo.version.print");
//		TestLine.run("5b.toStrExt.print");


//		TestLine.run("l1 = [3,5,6]; l2 = [7,9,0]; l = [l1,l2]; l.print; l.toIdent.print; l.toDescribe.print; l.describe");
		// ----------------------------------------------------------------------------------
//		TestLine.run("´ffmpeg -i /tmp/test.mp4 -vcodec libx264 -acodec copy -to 0:05 /tmp/done.mp4´");
//		TestLine.run("´echo 1; read OK; echo $OK; echo 2´??.describe");
//		TestLine.run("´ffmpeg -i /tmp/test/test.mp4 -vcodec libx264 -acodec copy -to 0:05 /tmp/test/done.mp4´??.describe");
//		TestLine.run("Cmd(\"ffmpeg -i /tmp/test/test.mp4 -vcodec libx264 -acodec copy -to 0:05 /tmp/test/done.mp4\").wait.print");

//		TestLine.run("a=1; b=2; c=3; \"{5>} + {5} = {6} {}\".fill(a, b, c.type, c).print");
//		TestLine.run("a=1; b=2; c=3; \"{5} + {5} = {6} {}\".print");

//		TestLine.run("Print 123**49");
//		TestLine.run("x = 123**49; l = []; l.add(x); x.print;l.print");
//		TestLine.run("x = 123a**28; y = 123z**28; l = []; l.add(x, y); x.print.describe.type.print; y.print.describe.type.print; l.print");
//		TestLine.run("IntNumber a = 3; a**=100; Print a");
//		TestLine.run("a = 3; a**=100; Print a");
//		TestLine.run("Print 3**2.1");
//		TestLine.run("1.21**0.5");

//		TestLine.run(">prefix(\"Foo\")");
//		TestLine.run(">prefix = \"Foo\"");
//		TestLine.run("DateTime(1579669628000l).print.diff(DateTime(2019,1,19,22,33,44,555).print).print");
//		TestLine.run("Describe 2d ++ 2a");
//		TestLine.run("\"abc{:tE2hu}def\".fill(\"123x \").print");
//		TestLine.run("\"abc{s:tE2hu}def\".fill(123).print");

//		TestLine.run("??Table");
//		TestLine.run("??Object.doPassMem");
//		TestLine.run("??Str.len");	//Problem!
//		TestLine.run("t=Table(3);t.add(1,2,3).add(3,4,5).add(\"abc\",\"def\",\"ghi\"); t.print; t.style(\"b|-C:!R L: \").print");
		TestLine.run("");

		// ----------------------------------------------------------------------------------
//		TestLine.run("");
	}

	private static void run(final String s, final String... args) throws Err_FileSys {
		final Parser_App parser = new Parser_App(true);
		parser.setDebug();
		final App app = parser.parseText(s);

		MOut.print(JayMo.NAME + "  " + parser.getVersionString(false, true));

		MOut.print("-----------------------------------");
		app.describe();
		MOut.print("-----------------------------------");

		try {
			final String result = app.exec(args);

			MOut.print("-----------------------------------");
			MOut.print("=> " + result);
		}
		catch(final Throwable t) {
			final ErrorComposer composer = new ErrorComposer(t, null);
			final String err = composer.compose();
			MOut.printErr(err);
		}
	}

}
