/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.error;

import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @apiNote
 *          Errors at Runtime, which can be safely handled by a lazy way. Maybe with default values.
 */
public class RuntimeWarning extends ErrorBaseDebug {

	private static final long serialVersionUID = 1136627314089553236L;


	public RuntimeWarning(final CallRuntime cr, final String text, final String detail) {
		// Call darf null sein, damit in App die ARGS gesetzt werden können!
		super(text, detail, cr, cr.call == null // TODO Letztes Argument vereinfachen und vereinheitlichen?!?
			? null
			: cr.getDebugInfo());
	}

	@Override
	public String getErrorTypeInfo() {
		return "Runtime warning";
	}

}
