/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.error;

import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 2021-04-07
 * @apiNote This Error will be only thrown by the assert functions
 */
public class AssertError extends ErrorBaseDebug {

	private static final long serialVersionUID = 2696501375703247825L;


	public AssertError(final CallRuntime cr, final String text, final String detail) {
		// Call can be null (needed for ARGS in App)
		super(text, detail, cr, cr.call == null
			? null
			: cr.getDebugInfo());
	}

	@Override
	public String getErrorTypeInfo() {
//		return "Illegal condition";
		return "Assert failed";
	}

}
