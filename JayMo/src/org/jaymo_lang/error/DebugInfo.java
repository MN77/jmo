/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.error;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.Nil;


/**
 * @author Michael Nitsche
 * @created 03.03.2019
 */
public class DebugInfo {

	private final String  file;
	private final Integer linenr;


	public DebugInfo(final String filepath, final int linenr) {
		this.file = filepath;
		this.linenr = linenr;
	}

	public I_Object getFile() {
		return this.file == null
			? Nil.NIL
			: new Str(this.file);
	}

	public I_Object getLine() {
		return this.linenr == null
			? Nil.NIL
			: new Int(this.linenr);
	}

	public String getRawFile() {
		return this.file;
	}

	public Integer getRawLine() {
		return this.linenr;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();

		if(this.file != null) {
			sb.append(this.file);
			sb.append(':');
		}
		else
			sb.append("Line ");
		sb.append(this.linenr);
		return sb.toString();
	}

}
