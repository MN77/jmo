/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.error;

import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 *
 * @apiNote
 *          Self-created and scripted error from user
 */
public class CustomError extends ErrorBaseDebug {

	private static final long serialVersionUID = -2059004754539806572L;


	public CustomError(final CallRuntime cr, final String text, final String detail) {
		// Call darf null sein, damit in App die ARGS gesetzt werden können!
		super(text, detail, cr, cr.call == null
			? null
			: cr.getDebugInfo());
	}

	@Override
	public String getErrorTypeInfo() {
		return "Self defined error";
	}

}
