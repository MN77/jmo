/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import org.jaymo_lang.object.I_Object;


/**
 * @author Michael Nitsche
 * @created 16.02.2018
 */
public class ObjectCallResult {

	public final I_Object obj;
	public final boolean  is_Attachment_Exec;


	public ObjectCallResult(final I_Object obj, final boolean is_Attachment_Exec) {
		this.obj = obj;
		this.is_Attachment_Exec = is_Attachment_Exec;
	}

	@Override
	public String toString() {
		return "Result_Obj(\"" + this.obj + "\"," + this.is_Attachment_Exec + ")";
	}

}
