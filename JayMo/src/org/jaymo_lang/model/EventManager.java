/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import java.util.HashMap;

import org.jaymo_lang.object.A_ObjectToString;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class EventManager extends A_ObjectToString {

	private HashMap<String, Event> events; // Key = Plain event name without '@'


	/**
	 * name without ':' and '@'!
	 * add("event", e);
	 */
	public void add(final String name, final Event f) {
		if(this.events == null)
			this.events = new HashMap<>();
		this.events.put(name, f);
	}

//	public Event get(final String event, final DebugInfo debug) {
//		if(events == null)
//			return null;
//		final Event f = events.getOrDefault(event, null);
//		if(f == null)
//			throw new CodeError("Unknown event", event, debug);
//		return f;
//	}

	/**
	 * @return Return value can be null!
	 */
	public Iterable<String> getList() {
		return this.events == null
			? null
			: this.events.keySet();
	}

	public boolean knows(final String name) {
		if(this.events == null)
			return false;
		if(this.events.containsKey(name))
			return true;
		return false;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final StringBuilder sb = new StringBuilder();

		if(this.events != null)
			for(final String key : this.events.keySet())
				sb.append(":" + key + '\n');

		return sb.toString();
	}

}
