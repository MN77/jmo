/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import java.util.HashMap;
import java.util.Map.Entry;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.object.A_ObjectToString;
import org.jaymo_lang.object.magic.var.MV_THIS;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Output;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class FuncManager extends A_ObjectToString {

	private HashMap<String, Function> func;


	public void add(final String name, final Function f) {
		if(this.func == null)
			this.func = new HashMap<>();
		this.func.put(name, f);
	}

	public Function get(final CallRuntime cr, final String method) {
		if(this.func == null)
			throw new CodeError(cr, "Unknown Function", MV_THIS.ID + '.' + method);

		Function f = this.func.getOrDefault(method, null);

		//--- Auto-Property ---  // Sync this with A_Object.call
		if(f == null && cr.getStrict().isValid_AutoProperty()) {
			final char c0 = method.charAt(0);
			if(c0 >= 'a' && c0 <= 'z')
				if(cr.argCount() == 0) {
					//--- Auto-Get ---
					final String methodGet = "get" + Character.toUpperCase(c0) + method.substring(1);
					f = this.func.getOrDefault(methodGet, null);
				}
				else {
					//--- Auto-Set ---
					final String methodSet = "set" + Character.toUpperCase(c0) + method.substring(1);
					f = this.func.getOrDefault(methodSet, null);
				}
		}
		if(f == null)
			throw new CodeError(cr, "Unknown Function", cr.getType().toString() + "." + method);
		else
			return f;
	}

	public boolean knows(final String name) {
		if(this.func == null)
			return false;
		final String nameLow = name.toLowerCase();
		for(final String key : this.func.keySet())
			if(name.equals(key) || nameLow.equals(key.toLowerCase()))
				return true;
		return false;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final StringBuilder sb = new StringBuilder();

		if(this.func != null)
			for(final Entry<String, Function> g : this.func.entrySet()) {
//				sb.append("::" + g.getKey());
				final String sv = g.getValue().toString(cr, STYPE.DESCRIBE);
				sb.append(sv);
				sb.append('\n');
			}

		Lib_Output.removeEnd(sb, '\n');
		return sb.toString();
	}

}
