/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

/**
 * @author Michael Nitsche
 * @created 22.02.2018
 */
public class Event {

	private final String        name;
	private final FunctionPar[] args;


	public Event(final String name) {
		this.name = name;
		this.args = null;
	}

	public Event(final String name, final FunctionPar[] args) {
		this.name = name;
		this.args = args;
	}

	public String gName() {
		return this.name;
	}

	public FunctionPar[] gVars() {
		return this.args;
	}

	@Override
	public String toString() {
		return this.toStringIdent();
	}

	public String toStringIdent() {
		return "Event: " + this.name;
	}

}
