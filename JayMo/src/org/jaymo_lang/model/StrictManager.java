/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.parser.PARSER_LEVEL;
import org.jaymo_lang.parser.ParseManagerFunc;
import org.jaymo_lang.parser.ParseManagerObj;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 27.12.2019
 */
public class StrictManager {

	private final ParseManagerObj parsemanager_obj;


	private final ParseManagerFunc parsemanager_func;
	private boolean                strictAutoBlock = false;

	private boolean strictFuncResultType = false;
	private boolean strictAutoProperty   = false;
	//	private boolean testAutoBlockAfterFunc = false;
	private boolean strictFuncParType   = false;
	private boolean strictShortenMethod = false;
	//	private boolean strictShortenType = false;
	private boolean strict_GroupIf = false;
//	private boolean strictJavaDirect = false;
	private boolean strictShortImports     = false;
	private boolean strictVarConstShortcut = false;
//	private boolean strictAutoVarLet        = false;
	private boolean strictArgConvert        = false;
	private boolean strictAtomicAutoUpgrade = false;
	private boolean strictSandbox           = false;

	private boolean      strictWebstart      = false; // Prevents "Input"
	private boolean      strictJava          = false;
	private boolean      strictAnotherScript = false;
	private boolean      strictOpenArg       = false;
	private PARSER_LEVEL parserLevel         = PARSER_LEVEL.OPEN;
	private boolean      lazyErrors          = false;
	private boolean      noWarnings          = false;
	private boolean      unsafeHotCode       = false;
	private boolean      clearFlow           = false;


	public StrictManager(final ParseManagerObj parsemanager_obj, final ParseManagerFunc parsemanager_func) {
		this.parsemanager_obj = parsemanager_obj;
		this.parsemanager_func = parsemanager_func;
	}


	// --- Set ---

	public void checkAutoBlock(final CallRuntime cr) {
		if(this.strictAutoBlock)
			throw new CodeError(cr, "Strict! Auto-Block-Function and .do are forbidden!", "Use it with the correct function, for example '.each'");
	}

	public void checkClearFlow(final DebugInfo debug) {
		if(this.clearFlow)
			throw new CodeError("Strict! Clear flow enforced!", "Function.return, Loop.break and Loop.next are unavailable.", debug);
	}

	public void checkFuncParType(final String pType, final String pName, final DebugInfo debug) {
		if(this.strictFuncParType && pType == null)
			throw new CodeError("Strict! Missing type of parameter!", pName, debug);
	}

	public void checkFuncResultType(final String fType, final boolean fReturnNil, final String fName, final DebugInfo debug) {
		if(this.strictFuncResultType && fType == null && !fReturnNil)
			throw new CodeError("Strict! Missing return-type for function!", fName, debug);
	}

	public void checkGroupIf(final CallRuntime cr) {
		if(this.strict_GroupIf)
			throw new CodeError(cr, "Strict! Auto-Block-Function for Group is forbidden!", "Use If()");
	}

	public void checkNoJava(final DebugInfo debug) {
		if(this.strictJava)
			throw new CodeError("Java restricted!", "Pure Java objects are forbidden by parser switch!", debug);
	}

	public void checkOpenArg(final DebugInfo debug) {
		if(this.strictOpenArg)
			throw new CodeError("Strict! Open argument is forbidden!", "Please use brackets for arguments.", debug);
	}

	public void checkParseTimeSwitch(final String s, final DebugInfo debug) {
		if(this.strictAnotherScript)
			throw new CodeError("Strict! This command is invalid or only allowed at the top of the first files!", s, debug);
	}

	public void checkSandbox(final CallRuntime cr, final String of) {
		if(this.strictSandbox)
			throw new CodeError(cr, "Sandbox-Mode! This object/function is forbidden!", of);
	}

	public void checkSandbox(final DebugInfo debug, final String of) {
		if(this.strictSandbox)
			throw new CodeError("Sandbox-Mode! This object/function is forbidden!", of, debug);
	}

	public void checkShortImports(final DebugInfo debug) {
		if(this.strictShortImports)
			throw new CodeError("Strict! Short imports are forbidden!", "Use >import() and >java()", debug);
	}

//	public void setTestAutoBlockAfterFunction() {
//		this.testAutoBlockAfterFunc = true;
//	}

	public void checkUnsafeHotCode(final CallRuntime cr, final String infoTypeCall) {
		if(!this.unsafeHotCode)
			throw new CodeError(cr, "Execution of unsafe hot code is prohibited.", "Forbidden call: " + infoTypeCall);
	}

	// --- Check ---

	public void checkVarConstShortcut(final DebugInfo debug, final String raw, final String full) {
		if(this.strictVarConstShortcut)
			throw new CodeError("Strict! Shortcut of variables and constants are not allowed!", "Please write '" + full + "' instead of '" + raw + "'!", debug);
	}

	public void checkWebstart(final CallRuntime cr, final String of) {
		if(this.strictWebstart)
			throw new CodeError(cr, "Webstart-Mode! This object/function is not available!", of);
	}

	public boolean getLazyErrors() {
		return this.lazyErrors;
	}

	public boolean getNoWarnings() {
		return this.noWarnings;
	}

	public boolean isValid_ArgConvert() {
		return !this.strictArgConvert;
	}

	// --- Check Strict ---

//	public void checkJavaDirect(final DebugInfo debug) {
//		this.checkSave(debug, "Java");
//		if(this.strictJavaDirect)
//			throw new CodeError("Strict! Direct Java-Access is forbidden!", "Maybe use a lower strict-level.", debug);
//	}

	public boolean isValid_AtomicAutoUpgrade() {
		return !this.strictAtomicAutoUpgrade;
	}

	public boolean isValid_AutoProperty() {
		return !this.strictAutoProperty;
	}

//	public boolean isValid_AutoVarLet() {
//		return !this.strictAutoVarLet;
//	}

	public boolean isValid_ShortenMethod() {
		return !this.strictShortenMethod;
	}

	public void set(final PARSER_LEVEL level, final DebugInfo debug) {
		if(this.parserLevel != PARSER_LEVEL.OPEN && level != this.parserLevel)
			throw new CodeError("Parser-Level already set!", level.name().toLowerCase(), debug);
		this.parserLevel = level;

		// Strict Parser:
		this.parsemanager_obj.strictLevel(level);
		this.parsemanager_func.strictLevel(level);

		// Strict Runtime
		switch(level) { // No breaks, should run through!
			case INSANE:
//				this.strictJavaDirect = true;
			case HIGH:
				this.strictAutoProperty = true;
//				this.strictShortenType = true;
				this.strictAutoBlock = true;
				this.strictFuncResultType = true; //TODO: Medium?
//				this.strictFunctionReturnNil = true; // This is combined and checked with strictFuncResultType
			case MEDIUM:
				this.strictVarConstShortcut = true;
				this.strictShortImports = true;
				this.strictOpenArg = true;
				this.strictArgConvert = true;
				this.strictAtomicAutoUpgrade = true;
			case LOW:
//				this.strictAutoVarLet = true;
//				this.strictAutoBlockAfterFunc = true;
//				this.testAutoBlockAfterFunc = false;
				this.strictFuncParType = true;
				this.strictShortenMethod = true;
				this.strict_GroupIf = true;
			case OPEN:
				break;
		}
	}

	public void setAnotherScript() {
		this.strictAnotherScript = true;
	}

	public void setClearFlow() {
		this.clearFlow = true;
	}

	public void setLazyErrors() {
		this.lazyErrors = true;
	}

	public void setNoJava() {
		this.strictJava = true;
	}


	// --- is valid ---

	public void setNoWarnings() {
		this.noWarnings = true;
	}

	public void setSandbox() {
		this.strictSandbox = true;
		this.strictJava = true;
		this.parsemanager_obj.strictSandbox();
		this.parsemanager_func.strictSandbox();
	}

	public void setUnsafeHotCode() {
		this.unsafeHotCode = true;
	}

	public void setWebstart() {
		this.strictWebstart = true;
	}

}
