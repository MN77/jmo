/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import org.jaymo_lang.object.A_ObjectToString;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 17.10.2019
 */
public class ArgCallBuffer extends A_ObjectToString {

	private Call                        call;
	private I_Object                    obj       = null;
	private boolean                     init      = false; // Only to prevent coding errors, but maybe remove this later!
	private Class<? extends I_Object>[] typecheck = null;
	private final int                   argIdx;


	public ArgCallBuffer(final int argIdx, final Call c) {
		Err.ifNull(c);
		this.call = c;
		this.argIdx = argIdx;
	}

	public I_Object get() {
		if(!this.init)
			Err.invalid("ArgCallBuffer not initialized!", this.call);
		return this.obj;
	}

	@SafeVarargs
	public final I_Object getInitEach(final CallRuntime cr, final I_Object streamIt, final I_Object each, final Class<? extends I_Object>... types) {

		if(!this.init) {
			this.typecheck = types;
			this.init = true;
		}
		return cr.argTypeExt(cr.execEachInit(this.call, streamIt, each), this.typecheck);
	}

	/**
	 * @apiNote
	 *          Only Necessary for Loops!
	 */
	@SafeVarargs
	public final I_Object getInitLoop(final CallRuntime cr, final I_Object streamIt, final Class<? extends I_Object>... types) {

		if(!this.init) {
			this.typecheck = types;
			this.init = true;
		}
		return cr.argTypeExt(cr.execInit(this.call, streamIt), this.typecheck);
	}

	public Call getInternalCall() {
		return this.call;
	}

	@SuppressWarnings("unchecked")
	public <TA extends I_Object> TA init(final CallRuntime cr, final I_Object streamIt, final Class<TA> type) {
		if(this.init)
			Err.invalid("Already init!", this.obj, type);

		this.obj = cr.instanceArgType(this.argIdx, cr.execInit(this.call, streamIt), type);
		this.typecheck = new Class[]{type};
		this.init = true;
		return (TA)this.obj;
	}

	@SafeVarargs
	public final I_Object initExt(final CallRuntime cr, final I_Object streamIt, final Class<? extends I_Object>... types) {
		if(this.init)
			Err.todo("Already init!", this.obj, this.call, streamIt, types);

		this.obj = cr.instanceArgTypeExt(this.argIdx, cr.execInit(this.call, streamIt), types);
		this.typecheck = types;
		this.init = true;
		return this.obj;
	}

	public void replace(final Call c) {
		this.call = c;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return this.obj != null
			? this.obj.toString(cr, type)
			: this.call.toString(cr, type);
	}

}
