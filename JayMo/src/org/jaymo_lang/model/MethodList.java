/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.model;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.convert.ConvertArray;
import de.mn77.base.data.struct.I_List;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;


/**
 * @author Michael Nitsche
 * @created 21.06.2018
 */
public class MethodList {

	private final ArrayTable<String> tab;


	public MethodList() {
		this.tab = new ArrayTable<>(4); // name, alias, args, result
	}

	@SuppressWarnings("unchecked")
	public void add(final Class<? extends I_Object> returns, final String name, final Class<? extends I_Object>... args) {
		final String sr = returns == null
			? "?"
			: Lib_Type.getName(returns, null);
		final String sp = args == null || args.length == 0
			? ""
			: Lib_Convert.typelistToText(args);
		this.tab.addRow(name, null, sp, sr);
	}

	@SuppressWarnings("unchecked")
	public void add(final Class<? extends I_Object> returns, final String name, final String alias, final Class<? extends I_Object>... args) {
		final String sr = returns == null
			? "?"
			: Lib_Type.getName(returns, null);
		final String sp = args == null || args.length == 0
			? ""
			: Lib_Convert.typelistToText(args);
		this.tab.addRow(name, alias, sp, sr);
	}

	public void add(final String returns, final String name, final String[] args) {
		final String sr = returns == null
			? "?"
			: returns;
		final String sa = args == null || args.length == 0
			? ""
			: "(" + ConvertArray.toString(",", (Object)args) + ")";
		this.tab.addRow(name, null, sa, sr);
	}

	public String get(final String search) {
		this.tab.sort(1);
		final I_List<String> list = new SimpleList<>();

		String last = "";
		for(int i = 0; i <= 1; i++)
			for(final String r : this.tab.getColumn(i))
				if(r != null && !r.equals(last))
					if(r.startsWith(search)) {
						list.add(r);
						last = r;
					}

		list.sort();
		final StringBuilder sb = new StringBuilder();
		boolean first = true;

		for(final String s : list) {
			if(!first)
				sb.append("\n");
			sb.append(s);
			first = false;
		}
		return sb.toString();
	}

//	public String getPars(final String search) {
//		this.tab.sort(1);
//		final TextStyler ts = new TextStyler();
//		for(final I_Sequence<String> r : this.tab) {
//			if(r.get(1).startsWith(search)) {
//				String arg = r.get(3);
//				if(arg.length() == 0)
//					arg = "()";
//				ts.newLine().l(r.get(1) + arg).t(1).l("-> " + r.get(4));
//			}
//			if(r.get(2) != null && r.get(2).startsWith(search)) {
//				String arg = r.get(3);
//				if(arg.length() == 0)
//					arg = "()";
//				ts.newLine().l(r.get(2) + arg).t(1).l("-> " + r.get(4));
//			}
//		}
//		return ts.toString();
//	}

}
