/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.info;

import java.io.InputStream;
import java.util.List;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.struct.JMo_Table;
import org.jaymo_lang.runtime.CallRuntime;

import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.error.Err;
import de.mn77.base.stream.Lib_Stream;
import de.mn77.base.sys.file.Lib_Jar;


/**
 * @author Michael Nitsche
 * @created 29.04.2020
 */
public class Lib_Info {

	public static I_Object developers(final CallRuntime cr) {
		final ArrayTable<I_Object> table = new ArrayTable<>(3);

		table.addRow(new Str("Michael Nitsche"), new Str("de"), new Str("info@jaymo-lang.org"));

		return new JMo_Table(table);
	}

	public static I_Object sponsors(final CallRuntime cr) {

		try {
			final InputStream is = Lib_Jar.getStream(JayMo.JAR_PATH_INFO + "/sponsors.txt");
			final String s = Lib_Stream.readUTF8(is);

			final List<String> lines = ConvertString.toLines(s);
			final ArrayTable<I_Object> table = new ArrayTable<>(3);
			lines.forEach(e-> {
				final String et = e.trim();

				if(et.length() > 0 && !et.startsWith("#")) {
					final List<String> row1 = ConvertString.toList("\t", et);
					final Str[] row2 = new Str[3];
					row2[0] = new Str(row1.size() >= 1 ? row1.get(0) : "");
					row2[1] = new Str(row1.size() >= 2 ? row1.get(1) : "");
					row2[2] = new Str(row1.size() >= 3 ? row1.get(2) : "");
					table.add(row2);
				}
			});
			return new JMo_Table(table);
		}
		catch(final Exception e) {
			Err.show(e);
		}
		// Fallback
		return new JMo_Table(new ArrayTable<I_Object>(3));
	}

	public static I_Object supporters(final CallRuntime cr) {

		try {
			final InputStream is = Lib_Jar.getStream(JayMo.JAR_PATH_INFO + "/supporters.txt");
			final String s = Lib_Stream.readUTF8(is);

			final List<String> lines = ConvertString.toLines(s);
			final SimpleList<I_Object> al = new SimpleList<>(lines.size());
			lines.forEach(e-> {
				final String et = e.trim();
				if(et.length() > 0 && !et.startsWith("#"))
					al.add(new Str(et));
			});
			return new JMo_List(al);
		}
		catch(final Exception e) {
			Err.show(e);
		}
		// Fallback
		return new JMo_List();
	}

}
