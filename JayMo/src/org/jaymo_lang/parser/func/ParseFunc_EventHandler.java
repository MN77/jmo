/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.func;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Comply;

import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 09.05.2019
 */
public class ParseFunc_EventHandler implements I_ParseFunc {

	public boolean hits(final char c0, final String s) {
		return c0 == ':' && s.startsWith("::@"); // && Lib_Comply.checkEventName(s.substring(2))
	}

	public Group2<Call, String> parse(final Parser_Script parser, final Block current, final I_Object obj, final String s, final boolean isArgument) {
		int end = s.length();

		for(int i = 2; i < s.length(); i++) {
			final char c = s.charAt(i);

			if(!(c == '@' || c == '_' || c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= '0' && c <= '9')) {
				end = i;
				break;
			}
		}
		final String rem = s.substring(end);
		final String eventName = s.substring(2, end);

		if(!Lib_Comply.checkEventName(eventName))
			throw new CodeError("Invalid event name", "Got: " + eventName, parser);

		if(rem.startsWith("("))
			throw new CodeError("Invalid event handler definition", "No arguments allowed here. Got: " + s.substring(2), parser);

		final Call c = new Call(current, obj, "::" + eventName, null, parser.getDebugInfo());

		return new Group2<>(c, rem.trim());
	}

}
