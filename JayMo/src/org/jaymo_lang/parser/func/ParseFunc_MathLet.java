/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.func;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 *
 * @apiNote
 *          Hits: let left & let right
 *          Left: ++=, &&=, +=, %=, &=, ...
 *          Right: +=>, %=>, &=>, ...
 */
public class ParseFunc_MathLet implements I_ParseFunc {

	private static final String l1 = "+-*/%&^|~";
	private static final String l2 = "+-*/&^|<>";


	public boolean hits(final char c0, final String s) {
		if(s.length() < 3)
			return false;

		final char c1 = s.charAt(1);

		if(ParseFunc_MathLet.l1.indexOf(c0) > -1 && c1 == '=')
			return true;

		if(c0 != c1 || ParseFunc_MathLet.l2.indexOf(c1) == -1)
			return false;

		return s.charAt(2) == '='; //s.length() >= 3 &&
	}

	public Group2<Call, String> parse(final Parser_Script parser, final Block current, final I_Object obj, final String s, final boolean isArgument) {
		final int eIdx = s.indexOf('=', 1);
		if(eIdx == s.length() - 1)
			throw new CodeError("Argument is missing", "Got only: " + s, parser);

		final boolean letRight = s.charAt(eIdx + 1) == '>';
//		if(letRight) {	// This will be checked at execution time
//			final String varName = s.substring(3).trim();
////			if(!Lib_Comply.checkVarName(varName))
//			if(!Lib_Parser.fastCheckIsVar(varName))
//				throw new CodeError("Invalid name of Var", varName, parser.getDebugInfo());
//		}

		final int ePos = letRight ? eIdx + 2 : eIdx + 1;
		final String m1 = s.substring(0, ePos);
		final String arg = s.substring(ePos).trim();

		// Calc
		final Call[] args = Parser_Call.parseArguments(parser, current, arg);
		Lib_Error.ifArgs(args.length, 1, 1, m1, parser.getDebugInfo());

		final Call c = new Call(current, obj, m1, args, parser.getDebugInfo());
		return new Group2<>(c, null);
	}

}
