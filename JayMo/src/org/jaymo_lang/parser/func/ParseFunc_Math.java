/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.func;

import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Parser;
import org.jaymo_lang.util.Lib_StringParser;

import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 * @apiNote
 *          Matches all operators with 1-3 special-chars
 */
public class ParseFunc_Math implements I_ParseFunc {

//	private static final String regex = "^([\\+\\*-/><%&\\|\\^!~≈=¶][\\+\\*\\-/><&\\|\\^=]?[=&\\|\\^~]?).*$";
//	private static final char[] hitChars = "+*!</=&->|^%~≈¶".toCharArray(); // Sorted by frequency of use
//	private static final String SHORTCUTS = "%°$§€"; //±
	private static final String SHORTCUTS = "°§";


	public boolean hits(final char c0, final String s) {
		return c0 == 33 // !
			|| c0 == 37 // %
			|| c0 == 38 // &
			|| c0 == 42 // *
			|| c0 == 43 // +
			|| c0 == 45 // -
			|| c0 == 47 // /
			|| c0 == 60 // <
			|| c0 == 61 // =
			|| c0 == 62 // >
			|| c0 == 94 // ^
			|| c0 > 122; // >z
	}

	public Group2<Call, String> parse(final Parser_Script parser, final Block current, final I_Object obj, final String s, final boolean isArgument) {
//		String m = s.replaceFirst(ParseFunc_Math.regex, "$1");
		String m = this.iGetOperator(s);
		int len = m.length();
		String rem1 = s.substring(len).trim();

		// Correct rem1		// z.B. *-
		if(m.length() > 1) {
			final char c0 = m.charAt(0);
			final char c1 = m.charAt(1);
			final boolean b0 = c0 == '+' || c0 == '-';
			final boolean b1 = c1 == '+' || c1 == '-';

			if(b1 && !b0) {
				final String temp = m.substring(1);
				m = "" + c0;
				len = 1;
				rem1 = temp + rem1;
			}
		}

		// Following: Nothing
		if(rem1.length() == 0) {
			final Call c = new Call(current, obj, m, null, parser.getDebugInfo());
			return new Group2<>(c, null);
		}
		final char rem1_0 = rem1.charAt(0);

		// Following: Another function with Dot
		if(rem1_0 == '.') {
			final Call c = new Call(current, obj, m, null, parser.getDebugInfo());
			return new Group2<>(c, rem1);
		}

		// Following: Argument-Group
		if(rem1_0 == '(') {
			String ps = Lib_Parser.group('(', ')', s.substring(m.length()), parser.getDebugInfo());
			String rem = rem1.substring(1 + ps.length() + 1);

			ps = ps.trim();
			rem = rem.trim();

			final Call[] args = Parser_Call.parseArguments(parser, current, ps);
			final Call c = new Call(current, obj, m, args, parser.getDebugInfo());
			return new Group2<>(c, rem);
		}
		// Following: Another function
//		else if(rest.matches("^[^0-9a-zA-Z_\"'<\\(%°$§].*$")) {

		// TODO ++ and -- will not be correctly matched!
		if(("0123456789+-abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_\"'<([?" + ParseFunc_Math.SHORTCUTS).indexOf(rem1_0) < 0) {
			//TODO : ?€
//			if(rem1.charAt(0) != ' ' && rem1.charAt(0) == m.charAt(m.length()-1))
//				throw new CodeError("Big salad of mathematical functions", "Please put a whitespace or dot between mathematical functions: "+m+rem1, parser.gDebugInfo());

			final Call c = new Call(current, obj, m, null, parser.getDebugInfo());
			return new Group2<>(c, rem1);
		}
		// Following: Argument
//		Group2<I_Object, String> g =Parser_Call.parseObject(parser, current, rem1, true);	// Doesn't work, because of appending functions with higher prio: '.'
		final int newpos = this.iFollowingArgument(rem1, parser.getDebugInfo());

		if(newpos == 1) { // Bugfix for: 5** + 3**
			final char c0 = rem1.charAt(0);

			if(c0 == '+' || c0 == '-') {
				final Call c = new Call(current, obj, m, null, parser.getDebugInfo());
				return new Group2<>(c, rem1);
			}
		}
		final String arg = newpos == -1
			? rem1
			: rem1.substring(0, newpos);
		final String rem2 = newpos == -1
			? null
			: rem1.substring(newpos);

		// !!! Argument ist alles in diesem Level, bis zu etwas, das eine niedrigere Priorität hat !!!
		final Call[] args = Parser_Call.parseArguments(parser, current, arg);
		final Call c = new Call(current, obj, m, args, parser.getDebugInfo());
		return new Group2<>(c, rem2);

//		Group2<Call[],String> g = Parser_Call.parseArgumentsOpenEnd(parser, current, rem1);
//		Group2<I_Object, String> g =Parser_Call.parseObject(parser, current, rem1, true);
//		Call arg = new Call(current, g.o1, parser.gDebugInfo());
//		Call[] args = new Call[] {arg};
//		final Call c = new Call(current, obj, m, g.o1, parser.gDebugInfo());
//		return new Group2<>(c, g.o2);
	}

	private int iFollowingArgument(final String s, final DebugInfo debugInfo) {
		final char[] ca = s.toCharArray();

		for(int index = 0; index < s.length(); index++) {
			final char c = ca[index];

			// Filter Group ()
			if(c == '(') {
				index = Lib_StringParser.checkGroup(s, index, true, debugInfo);
				continue;
			}

			// Filter Group <> (Type)
			if(c == '<') {
				final int posT = Lib_StringParser.checkType(s, index, false, debugInfo);
				if(posT != -1)
					index = posT;
				else
					return index;
				continue;
			}

			// Filter Group []
			if(c == '[') {
				index = Lib_StringParser.checkGetOrList(s, index, true, debugInfo);
				continue;
			}

			// Filter Strings
			if(c == '"') {
				index += 1 + Lib_StringParser.checkString(s.substring(index), true, debugInfo);
				continue;
			}

			// Filter Char
			if(c == '\'') {
				index += Lib_StringParser.checkChar(s.substring(index), true, debugInfo) - 1;
				continue;
			}
//			final String filter1 = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_%°$§?";
//			if(filter1.indexOf(c) >= 0)
			if(c >= '0' && c <= '9' || c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || ("_?" + ParseFunc_Math.SHORTCUTS).indexOf(c) >= 0)
				continue;

			if(index == 0 && (c == '+' || c == '-')) {
				final String st = s.substring(index + 1).trim();

				if(st.length() > 0) {
					final char cp1 = st.charAt(0);
					if(cp1 >= '0' && cp1 <= '9')
						continue;
				}
			}
			final String filter2 = ".[]";
			if(filter2.indexOf(c) >= 0)
				continue;

			return index;
		}
		return -1;
	}

	private String iGetOperator(final String s) {
//		"^([\\+\\*-/><%&\\|\\^!~≈=¶][\\+\\*\\-/><&\\|\\^=]?[=&\\|\\^~]?).*$"
		final int sLen = s.length();
		byte len = 1;

		if(sLen > 1 && "+*-/><&|^=~".indexOf(s.charAt(1)) > -1) {
			len++;

			if(sLen > 2 && "=&|^~".indexOf(s.charAt(2)) > -1)
				len++;
		}
		return s.substring(0, len);
	}

}
