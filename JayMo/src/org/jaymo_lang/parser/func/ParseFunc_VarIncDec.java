/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.func;

import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.parser.Parser_Script;

import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 12.05.2020
 *
 *          TODO in Math integrieren?
 *
 *          Hits: =++, =--, =**, =//, =<<, =>>, =!
 */
public class ParseFunc_VarIncDec implements I_ParseFunc {

	public boolean hits(final char c0, final String s) {
		if(c0 != '=' || s.length() < 2)
			return false;

		final char c1 = s.charAt(1);
		if(c1 == '!')
			return true;

		if(s.length() < 3)
			return false;

		final char c2 = s.charAt(2);
		if(c1 != c2)
			return false;

		return "+-*/<>".indexOf(c1) >= 0;
	}

	public Group2<Call, String> parse(final Parser_Script parser, final Block current, final I_Object obj, final String s, final boolean isArgument) {
		final int from = s.charAt(1) == '!' ? 2 : 3;
		final String m = s.substring(0, from);
		final String rem = s.length() == from ? null : s.substring(from);

		final Call c1 = new Call(current, obj, m, null, parser.getDebugInfo());
		return new Group2<>(c1, rem);
	}

}
