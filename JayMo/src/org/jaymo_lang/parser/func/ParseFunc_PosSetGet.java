/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.func;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.passthrough.I_Mem;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 12.08.2019
 */
public class ParseFunc_PosSetGet implements I_ParseFunc {

	public boolean hits(final char c0, final String s) {
		return c0 == '[';// && Lib_StringParser.checkGetSet(s, '[', ']');
	}

	public Group2<Call, String> parse(final Parser_Script parser, final Block current, final I_Object obj, final String s, final boolean isArgument) {
		String pos = Lib_Parser.group('[', ']', s, parser.getDebugInfo());
		String rem = s.substring(pos.length() + 2);
		final boolean lazy = rem.length() >= 1 && rem.charAt(0) == '?';
		if(lazy)
			rem = rem.substring(1).trim();

		pos = pos.trim();
//		final boolean isSet = rest.matches("^\\s*=[^=].*$");
		final String rest2 = rem.trim();
		final boolean isSet = rest2.length() >= 2 && rest2.charAt(0) == '=' && rest2.charAt(1) != '=';

		// =++ =-- =** =//
		if(isSet && !lazy && obj != null && rest2.length() == 3) {
			if(!(obj instanceof I_Mem))
				throw new CodeError("Invalid object type", "Got " + obj.toString() + ", but need variable or constant for: " + '[' + pos + ']' + rest2, parser);

			final char rc1 = rest2.charAt(1);
			final char rc2 = rest2.charAt(2);
			if(rc1 == rc2)
				switch(rc1) {
					case '+':
						return this.iSet(parser, current, obj, pos, "=" + ((I_Mem)obj).getName() + '[' + pos + "]++", false);
					case '-':
						return this.iSet(parser, current, obj, pos, "=" + ((I_Mem)obj).getName() + '[' + pos + "]--", false);
					case '*':
						return this.iSet(parser, current, obj, pos, "=" + ((I_Mem)obj).getName() + '[' + pos + "]**", false);
					case '/':
						return this.iSet(parser, current, obj, pos, "=" + ((I_Mem)obj).getName() + '[' + pos + "]//", false);
				}
		}
		return isSet
			? this.iSet(parser, current, obj, pos, rem, lazy)
			: this.iGet(parser, current, obj, pos, rem, lazy);
	}

	private Group2<Call, String> iGet(final Parser_Script parser, final Block current, final I_Object obj, final String pos, final String rest, final boolean lazy) {
		final Call[] aPos = Parser_Call.parseArguments(parser, current, pos);
		final String m = lazy ? "pull" : "get";
		Lib_Error.ifArgs(aPos.length, 0, null, m, parser.getDebugInfo()); // 0 for self defined, 1 for List, 2 for Table, 3 for self defined ...
		final Call c = new Call(current, obj, m, aPos, parser.getDebugInfo());
		return new Group2<>(c, rest);
	}

	private Group2<Call, String> iSet(final Parser_Script parser, final Block current, final I_Object obj, final String pos, final String rest, final boolean lazy) {
		final String arg = rest.substring(rest.indexOf('=') + 1).trim();
		final Call[] aPar = Parser_Call.parseArguments(parser, current, arg);
		Err.ifNot(1, aPar.length);
		final Call cPar = aPar[0];

		final Call[] aPos = Parser_Call.parseArguments(parser, current, pos);
		final String m = lazy ? "put" : "set";

		// TODO Ist es wirklich möglich, dass aPos.size > 1 ist ?!?!?!
		Lib_Error.ifArgs(aPos.length, 0, null, m, parser.getDebugInfo());
		final int len = aPos.length;
		final Call[] ca = new Call[len + 1];
		System.arraycopy(aPos, 0, ca, 1, len);
		ca[0] = cPar;

//		final Call c = new Call(current, obj, "set", new Call[]{cPos, cPar}, debugInfo);
		final Call c = new Call(current, obj, m, ca, parser.getDebugInfo());
		return new Group2<>(c, null);
	}

}
