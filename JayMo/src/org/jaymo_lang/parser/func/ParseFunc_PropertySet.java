/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.func;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.util.Lib_String;


/**
 * @author Michael Nitsche
 * @created 07.04.2020
 */
public class ParseFunc_PropertySet implements I_ParseFunc {

	public boolean hits(final char c0, final String s) {
//		[a-z][A-Za-z0-9_]\\s*=[^=>].*	 // Not matching: == =>

		if(c0 < 'a' || c0 > 'z')
			return false;

		byte state = 1; // 1 = name, 2 = space, 3 = equals

		for(final char c : s.toCharArray()) {

			if(state == 1) {
				if(c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= '0' && c <= '9' || c == '_')
					continue;

				if(c == ' ') {
					state = 2;
					continue;
				}

				if(c == '=') {
					state = 3;
					continue;
				}
				return false;
			}

			if(state == 2) {
				if(c == ' ')
					continue;

				if(c == '=') {
					state = 3;
					continue;
				}
				return false;
			}

			if(state == 3) {
				if(c == '=' || c == '>')
					return false;
				return true;
			}
		}
		return false;
	}

	public Group2<Call, String> parse(final Parser_Script parser, final Block current, final I_Object obj, final String s, final boolean isArgument) {
		final String m = s.substring(0, s.indexOf('=')).trim();
		final String m2 = "set" + Lib_String.capitalize(m, true);

		final String arg = s.substring(m.length()).trim().substring(1);
		final Call[] args = Parser_Call.parseArguments(parser, current, arg);

		if(args.length != 1)
			throw new CodeError("Invalid amount of arguments", "Only 1 argument is allowed, but got: " + args.length, parser);

		final Call c = new Call(current, obj, m2, args, parser.getDebugInfo());
		return new Group2<>(c, null);
	}

}
