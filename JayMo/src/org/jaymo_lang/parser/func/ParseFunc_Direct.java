/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.func;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 */
public class ParseFunc_Direct implements I_ParseFunc {

	private static final String NO_OPEN_ARG = "+-*/%!=<>&|^~¶\\"; // Check also ParseObj_NewWithOpenArg.NOT
	// Combine with ParseFunc_Math?!?


	public boolean hits(final char c0, final String s) {
		return c0 == '_' || c0 >= 'a' && c0 <= 'z' || c0 >= 'A' && c0 <= 'Z';
	}

	public Group2<Call, String> parse(final Parser_Script parser, final Block current, final I_Object obj, final String s, final boolean isArgument) {
		final String m = Lib_Parser.getTypeName(s, parser);
		String rem = s.substring(m.length());

		final String remTrimmed = rem.trim(); // If there are only whitespace and tabs, that's okay
		Call[] argCalls = null;

		if(remTrimmed.length() > 0) {
			final char r0 = rem.charAt(0);

			if(r0 == '(') {
				final String args = Lib_Parser.group('(', ')', remTrimmed, parser.getDebugInfo());
				rem = remTrimmed.substring(1 + args.length() + 1);
				argCalls = Parser_Call.parseArguments(parser, current, args);
			}
			else if(r0 == '\t')
				throw new CodeError("Invalid function call", "Please remove tab char before: " + remTrimmed, parser);
			else {
				final char rt0 = remTrimmed.charAt(0);

				if(rt0 != '.' && r0 == ' ' && ParseFunc_Direct.NO_OPEN_ARG.indexOf(rt0) == -1) {
					if(isArgument)
						throw new CodeError("Invalid use of open argument", "For nested argument, please try: ." + m + "( " + remTrimmed + " )", parser);

					parser.app.strict.checkOpenArg(parser.getDebugInfo());
					argCalls = Parser_Call.parseArguments(parser, current, remTrimmed);

					if(argCalls.length != 1)
						throw new CodeError("Invalid use of open argument", "For more arguments, please try: ." + m + "( " + remTrimmed + " )", parser);
					if(parser.buffer.nextLevelDiff() > 0 || argCalls[0].hasBlock())
						throw new CodeError("Invalid use of open argument", "For using block, please try: ." + m + "( " + remTrimmed + " )", parser);
					if(argCalls[0].hasStream())
						throw new CodeError("Invalid use of open argument", "For using stream, please try: ." + m + "( " + remTrimmed + " )", parser);
					if(argCalls[0].method != null)
						throw new CodeError("Invalid use of open argument", "No method allowed, please try: ." + m + "( " + remTrimmed + " )", parser);

					rem = null;
				}
			}
		}
		final Call c = new Call(current, obj, m, argCalls, parser.getDebugInfo());
		return new Group2<>(c, rem);
	}

}
