/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.func;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 */
public class ParseFunc_Let implements I_ParseFunc {

	public boolean hits(final char c0, final String s) {
		return c0 == '=' && !s.startsWith("==");
	}

	public Group2<Call, String> parse(final Parser_Script parser, final Block current, final I_Object obj, final String s, final boolean isArgument) {
		if(s.length() == 1)
			throw new CodeError("Nothing to set", obj + " " + s, parser);

		if(s.charAt(1) == '>') { // =>
			final String vcName = s.substring(2).trim();
//			if(!Lib_Comply.checkVarName(varName))	// This will be checked at execution time
//				throw new CodeError("Invalid name of Var", varName, parser.getDebugInfo());

//			if(varName.equals("it")) {
//				final Call c = new Call(current, obj, "mem", new Call[]{new Call(current, new VarLet(new MC_IT_BLOCK(false)), parser.gDebugInfo())}, parser.gDebugInfo());
//				return new Group2<>(c, null);
//			}

//			final A_Object let = Lib_Parser.fastCheckIsVar(vcName, parser.getDebugInfo())
//				? new VarLet(current.getVarManager().use_ParseTime(parser, vcName, false))
//				: new ConstLet(current.getConstManager().use_ParseTime(parser, vcName, false));
//			final Call c = new Call(current, obj, "=>", new Call[]{new Call(current, let, parser.getDebugInfo())}, parser.getDebugInfo());

			final Call[] args = Parser_Call.parseArguments(parser, current, vcName);
			Lib_Error.ifArgs(args.length, 1, 1, "=>", parser.getDebugInfo());
			final Call c = new Call(current, obj, "=>", args, parser.getDebugInfo());

			return new Group2<>(c, null);
		}
//		else if(s.length() >= 3 && s.charAt(1) == '~' && s.charAt(2) == '>') { // =~>
//			final String varName = s.substring(3).trim();
//			if(!Lib_Comply.checkVarName(varName))
//				throw new CodeError("Invalid name of Var", varName, parser.getDebugInfo());
//
////			if(varName.equals("it")) {
////				final Call c = new Call(current, obj, "mem", new Call[]{new Call(current, new VarLet(new MC_IT_BLOCK(false)), parser.gDebugInfo())}, parser.gDebugInfo());
////				return new Group2<>(c, null);
////			}
//			final Call c = new Call(current, obj, "=~>", new Call[]{new Call(current, new VarLet(current.gVarManager().use_ParseTime(parser, varName, false)), parser.getDebugInfo())},
//				parser.getDebugInfo());
//			// c.sLastCall(args[0]);
//			return new Group2<>(c, null);
//		}
//		else if(s.charAt(1) == '~') { // =~
//			final String arg = s.substring(2);
//			final Call[] args = Parser_Call.parseArguments(parser, current, arg);
//			final Call c = new Call(current, obj, "=~", args, parser.getDebugInfo());
//			Err.ifNot(1, args.length, "Only 1 argument allowed");
//			return new Group2<>(c, null);
//		}
		else { // =
			final String arg = s.substring(1);
			// rem="("+rem+")";
			final Call[] args = Parser_Call.parseArguments(parser, current, arg);
			final Call c = new Call(current, obj, "=", args, parser.getDebugInfo());
			Err.ifNot(1, args.length, "Only 1 argument allowed");
//			Lib_Error.ifArgs(args.length, 1, 1, "=>", parser.getDebugInfo());	// TODO ?!?
			return new Group2<>(c, null);
		}
	}

}
