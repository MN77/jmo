/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.func;

import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.JMo_Range;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;

import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 */
public class ParseFunc_Range implements I_ParseFunc {

	public boolean hits(final char c0, final String s) {
		return c0 == '.' && s.length() > 1 && s.charAt(1) == '.';
	}

	public Group2<Call, String> parse(final Parser_Script parser, final Block current, final I_Object obj, final String s, final boolean isArgument) {
		final String to = s.substring(2).trim();

		final Group2<I_Object, String> g2 = Parser_Call.parseObject(parser, current, to, true);
		final I_Object argObj = g2.o1;
		final String rem = g2.o2;
		final Call argCall = new Call(current, argObj, parser.getDebugInfo());
		final Call objCall = new Call(current, obj, parser.getDebugInfo());
		final JMo_Range range = new JMo_Range(objCall, argCall);

		if(rem.length() > 0) {
			final Call c = Parser_Call.parseFunction(parser, current, range, rem.trim(), false);
			return new Group2<>(c, null);
		}
		else {
			final Call c = new Call(current, range, parser.getDebugInfo());
			return new Group2<>(c, rem);
		}
	}

}
