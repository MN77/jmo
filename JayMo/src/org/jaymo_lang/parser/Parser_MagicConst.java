/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.Type;
import org.jaymo_lang.object.magic.con.MagicConstSelf;
import org.jaymo_lang.util.BLOCKED;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.data.constant.CHARS;
import de.mn77.base.data.filter.FilterString;


/**
 * @author Michael Nitsche
 * @created 03.08.2019
 */
public class Parser_MagicConst {

	public static void parse(final Parser_Script parser, final Type currentType, final Block currentBlock, final String s) {
		// Starting "__" is already tested
		if(!currentType.getName().equals("Root"))
			throw new CodeError("Invalid definition", "A magic constant must be defined in the root type!", parser.getDebugInfo());
		if(currentBlock.getParent() != null)
			throw new CodeError("Invalid definition", "A magic constant must be defined at root level!", parser.getDebugInfo());

		final String var = FilterString.matchingLeft('_' + CHARS.ABC_UPPER, s).substring(2);
		String arg = s.substring(var.length() + 2).trim();

		if(var.length() == 0)
			throw new CodeError("Invalid definition", "Missing name of the magic constant: " + s, parser.getDebugInfo());
		if(arg.length() == 0)
			throw new CodeError("Invalid definition", "Missing argument for magic constant: " + s, parser.getDebugInfo());
		if(arg.charAt(0) != '=')
			throw new CodeError("Invalid definition", "Missing '=' after the magic constant: __" + var, parser.getDebugInfo());

		for(final String blocked : BLOCKED.MAGIC_CONSTS)
			if(var.equals(blocked))
				throw new CodeError("Invalid definition", "Name of magic constant is already used: __" + var, parser.getDebugInfo());

		arg = arg.substring(1).trim();
		final Call[] args = Parser_Call.parseArguments(parser, currentBlock, arg);
		Lib_Error.ifArgs(args.length, 1, 1, "Magic constant", parser.getDebugInfo());
		final MagicConstSelf mcc = new MagicConstSelf(var, args[0]);

		parser.app.addMagicConst(var, mcc, parser.getDebugInfo());
	}

}
