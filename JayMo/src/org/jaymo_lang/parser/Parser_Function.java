/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Function;
import org.jaymo_lang.model.Type;
import org.jaymo_lang.parser.fdef.ParseFDef;
import org.jaymo_lang.util.ALLOWED;
import org.jaymo_lang.util.BLOCKED;

import de.mn77.base.data.util.Lib_Array;


/**
 * @author Michael Nitsche
 * @created 23.02.2018
 */
public class Parser_Function {

//	private static final I_ParseFDef[] funcparser = new I_ParseFDef[]{new ParseFDef_Direct(), new ParseFDef_ParMulti()};

	private static final ParseFDef fDefParser = new ParseFDef();


	public static void parse(final Parser_Script parser, final Type currentType, final String s) {
//		MOut.dev("Parse Function-Head", s);

//		for(final I_ParseFDef pf : Parser_Function.funcparser)
//			if(pf.hits(s)) {
//				MOut.dev("Match: " + pf.getClass().getSimpleName());
		final Function f = Parser_Function.fDefParser.parse(parser, currentType, s);
//				final Function f = pf.parse(parser, currentType, s);
		final String[] fNames = f.getNames();

		for(final String fName : fNames) {
			if(currentType.getFunctions().knows(fName))
				throw new CodeError("Name of the function is already used!", "::" + fName, parser);

			if(Lib_Array.contains(BLOCKED.FUNCTIONS, fName))
				throw new CodeError("Invalid definition, name for Function reserved!", "::" + fName, parser);

			// Check case for override
			final String fNameLow = fName.toLowerCase();
			for(final String owOk : ALLOWED.overwriteFunctions)
				if(fNameLow.equals(owOk.toLowerCase()) && !fName.equals(owOk))
					throw new CodeError("Invalid overwrite of a function", "Got \"" + fName + "\" but correct name is \"" + owOk + "\"", parser);

			currentType.getFunctions().add(fName, f);
		}

		if(parser.buffer.nextLevelDiff() > 0) {
			if(f.isOneLine())
				throw new CodeError("No Block allowed for one-line-function!", "::" + fNames[0], parser);

			new Parser_Block().parse(parser, currentType, f, f.getBlock(), false);
		}
//				else {

//					if(!f.isOneLine())
//						throw new CodeError("Missing Block or one-line-function!", "::" + fNames[0], parser.gDebugInfo());
//				}

//		throw new CodeError("Unknown Function-Definition", s, parser.gDebugInfo());
	}

}
