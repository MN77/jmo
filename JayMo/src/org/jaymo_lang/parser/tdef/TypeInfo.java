/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.tdef;

/**
 * @author Michael Nitsche
 * @created 28.01.2021
 */
public class TypeInfo {

	public final String  name;
	public final boolean control;
	public final String  args;
	public final String  extend_type;
	public final String  extend_pars;
	public final String  abFunction;


	public TypeInfo(final String name, final boolean con, final String args, final String extend_type, final String extend_pars, final String abf) {
		this.name = name;
		this.control = con;
		this.args = args;
		this.extend_type = extend_type;
		this.extend_pars = extend_pars;
		this.abFunction = abf;
	}

}
