/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.tdef;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.FunctionPar;
import org.jaymo_lang.model.Type;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.parser.obj.I_ParseObject;
import org.jaymo_lang.util.Lib_Comply;
import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.MN;
import de.mn77.base.data.group.Group2;
import de.mn77.base.data.group.Group3;


/**
 * @author Michael Nitsche
 * @created 28.01.2021
 * @apiNote
 *          name(?,?,?, ...)
 */
public class ParseTDef {

//	private static final String regex = "^" + REGEX.TYPE + "\\s*[({]" + REGEX.PARDEF_PARS + "\\s*" + "(\\.\\.\\.\\s*)?" + "[})]" + REGEX.DEF_ABF + "$";

	private enum PHASE {
		NAME,
		PARS,
		AFTER_PARS,
		EXT_TYPE,
		EXT_PARS,
		ABFUNCTION
	}


	private static String checkAbFunction(final Parser_Script parser, String name) {
		name = name.strip();
		if(name.length() == 0)
			return null;
		if(name.charAt(0) != '=') // != ':'
			throw new CodeError("Invalid type definition", "Missing '=' for default function", parser);
		return name.substring(1).trim();
	}

	private static String checkExtName(final Parser_Script parser, String name) {
		name = name.trim();
		if(name.length() <= 1)
			return null;
		return ParseTDef.checkName(parser, name.substring(1));
	}

	private static String checkName(final Parser_Script parser, String name) {
		name = name.strip();
		if(name.length() == 0)
			return null;
		if(!Lib_Comply.checkTypeName(name))
			throw new CodeError("Invalid type name", "Illegal type name: " + name, parser);
		return name;
	}

	public Type parse(final Parser_Script parser, final Type type, final String s) {
		final TypeInfo info = this.scan(parser, s);

		final Group3<FunctionPar[], Integer, Boolean> g = Lib_Parser.getFunctionPars(parser, type.getBlock(), info.args == null ? "" : info.args, '(', ')');

		if(info.extend_type != null) {
			final Block current = type.getBlock();
			String ext = info.extend_type;
			if(info.extend_pars != null)
				ext += info.extend_pars;
//			MOut.temp(ext);
			final I_ParseObject po = parser.app.parsemanager_obj.getParser(current, ext, parser, true); // TODO true?
			final Group2<I_Object, String> extG = po.parse(parser, current, ext);
			if(!MN.isEmpty(extG.o2))
				throw new CodeError("Invalid type definition", "Unusable statement for extending type.", parser);
			return new Type(info.name, g.o1, info.abFunction, info.control, g.o3, parser, extG.o1, parser.getDebugInfo());
		}
		return new Type(info.name, g.o1, info.abFunction, info.control, g.o3, parser, parser.getDebugInfo());
	}

	private TypeInfo scan(final Parser_Script parser, final String s) {
		boolean isControl = false;
		String name = null;
		String args = null;
		String extType = null;
		String extPars = null;
		String abFunction = null;

		PHASE phase = PHASE.NAME;
		int start = 0;
		boolean nameLastIsSpace = false;
		boolean after_pars_space = false;
		boolean hasExtPars = false;

		for(int i = 0; i < s.length(); i++) {
			final char c = s.charAt(i);

			switch(phase) {
				case NAME:
					if(c == '(') {
						if(nameLastIsSpace)
							throw new CodeError("Invalid type definition", "Please remove whitespace(s) between type name and '('", parser);
						name = ParseTDef.checkName(parser, s.substring(start, i));
						phase = PHASE.PARS;
//						i--; // Necessary if pars will be parsed with Lib_Parser.group
						start = i;
						continue;
					}
					if(c == '=' || nameLastIsSpace && c >= 'a' && c <= 'z') {
						name = ParseTDef.checkName(parser, s.substring(start, i));
						phase = PHASE.ABFUNCTION;
						start = i;
						continue;
					}
					if(c == '^' || c == ':') {
						name = ParseTDef.checkName(parser, s.substring(start, i));
						phase = PHASE.EXT_TYPE;
						start = i;
						continue;
					}
					if(c == ' ') {
						nameLastIsSpace = true;
						continue;
					}
					if(c == '\t')
						throw new CodeError("Invalid type definition", "Please remove tabulator after: " + s.substring(0, i), parser);
					// This will be checked later:
//					if((c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c < '0' || c > '9') && c != '_')
//						throw new CodeError("Invalid function name", "Invalid char in function name: " + s, parser.gDebugInfo());

					nameLastIsSpace = false;
					break;

				case PARS:
					if(c == '(') {
						final String group = Lib_Parser.group('(', ')', s.substring(i), parser.getDebugInfo());
						i += group.length() + 1;
						continue;
					}
					if(c == ')') {
						args = s.substring(start, i + 1);
						phase = PHASE.AFTER_PARS;
						start = i + 1;
						continue;
					}
//					if(c == '(') {
//						args = Lib_Parser.group('(', ')', s.substring(i), parser.getDebugInfo());
//						phase = PHASE.AFTER_PARS;
//						i = i+ args.length() + 1;
//						start = i+1;	// +1 +1 for two brackets
//						args = '('+args.trim()+')';
//						continue;
//					}
					if(c == '^' || c == ':') //	 || c == '='
						throw new CodeError("Invalid type definition", "Closing bracket is missing.", parser);
					break;

				case AFTER_PARS:
					if(c == '!') {
						Lib_Parser.checkControl(s, i, parser);
						i++;
						if(after_pars_space)
							throw new CodeError("Invalid type definition", "Spaces between closing bracket and modifier '?'!", parser);
						isControl = true;
						continue;
					}
					if(c == '^' || c == ':') {
						phase = PHASE.EXT_TYPE;
						start = i;
						continue;
					}
					else if(c == '=') {
						phase = PHASE.ABFUNCTION;
						start = i;
						continue;
					}
					else if(c == ' ') {
						after_pars_space = true;
						continue;
					}
					else
						throw new CodeError("Invalid type definition", "Invalid chars after closing bracket. Want '^', ':' or '=', but got: " + s.substring(i), parser);

				case EXT_TYPE:
					if(c == '=') {
						if(start + 1 < i)
							extType = ParseTDef.checkExtName(parser, s.substring(start, i));
						phase = PHASE.ABFUNCTION;
						start = i;
						continue;
					}
					if(c == '(') {
						if(start + 1 < i)
							extType = ParseTDef.checkExtName(parser, s.substring(start, i));
						phase = PHASE.EXT_PARS;
						i--;
						start = i;
						continue;
					}
					break;

				case EXT_PARS:
					if(c == '(') {
						extPars = Lib_Parser.group('(', ')', s.substring(i), parser.getDebugInfo());
						phase = PHASE.ABFUNCTION;
						i = i + extPars.length() + 1;
						start = i + 1; // +1 +1 for two brackets

						extPars = extPars.trim();
						hasExtPars = extPars.length() > 0;
						extPars = '(' + extPars + ')';
						continue;
					}
					break;

				case ABFUNCTION:
//					abFunction = ParseTDef.checkAbFunction(parser, s.substring(start));
//					MOut.temp(s, phase, name, isControl, args, ext_type, ext_pars, abFunction);
//					return new TypeInfo(name, isControl, args, ext_type, ext_pars, abFunction);
					break;
			}
		}
		if(phase == PHASE.NAME)
			name = ParseTDef.checkName(parser, s.substring(start));
		if(phase == PHASE.PARS || phase == PHASE.EXT_PARS)
			throw new CodeError("Invalid type definition", "Missing closing bracket: " + s, parser);
		if(phase == PHASE.EXT_TYPE && start < s.length())
			extType = ParseTDef.checkExtName(parser, s.substring(start));
		if(phase == PHASE.ABFUNCTION)
			abFunction = ParseTDef.checkAbFunction(parser, s.substring(start));

//		Allow default control function only for control types?
//		if(abFunction != null && !isControl)
//			throw new CodeError("Invalid type definition", "Default function is only available for control types!", parser.getDebugInfo());

		if(!hasExtPars && extType != null && extType.equals("Object")) {
			extType = null;
			extPars = null;
		}
//		MOut.temp(s, phase, name, isControl, args, ext_type, ext_pars, abFunction);
		return new TypeInfo(name, isControl, args, extType, extPars, abFunction);
	}

}
