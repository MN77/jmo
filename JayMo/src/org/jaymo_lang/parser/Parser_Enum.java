/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.Type;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.JMo_Enum;
import org.jaymo_lang.util.Lib_Enum;
import org.jaymo_lang.util.Lib_Parser;


/**
 * @author Michael Nitsche
 * @created 15.08.2021
 */
public class Parser_Enum {

	public static void parse(final Parser_Script parser, final Type currentType, final Block currentBlock, final String s) {
		final String name = Lib_Parser.getTypeName(s, parser);
		Lib_Enum.isValid(name, parser.getDebugInfo());

		final String rem = s.substring(name.length());
		String remTrimmed = rem.trim();
		Call[] argCalls = null;

		if(remTrimmed.length() > 0)
			if(rem.charAt(0) == '(') {
				final String args = Lib_Parser.group('(', ')', remTrimmed, parser.getDebugInfo());
				remTrimmed = remTrimmed.substring(1 + args.length() + 1).trim();
				if(remTrimmed.length() > 0)
					throw new CodeError("Invalid enum definition", "Invalid argument passing: " + rem, parser);

				argCalls = Parser_Call.parseArguments(parser, currentBlock, args);
			}
			else if(remTrimmed.charAt(0) == '=') {
				final String args = remTrimmed.substring(1);
				argCalls = Parser_Call.parseArguments(parser, currentBlock, args);
			}
			else // if(r0 == '\t')
				throw new CodeError("Invalid enum definition", "Missing open bracket before: " + remTrimmed, parser);

		// Error check for "Args"
		if(argCalls != null) {
			if(argCalls.length > 1)
				throw new CodeError("Invalid enum definition", "Only 1 argument is allowed!", parser);

			if(argCalls.length == 1) {
				final Call ac = argCalls[0];
				if(ac.method != null || ac.hasBlock() || ac.hasStream() || ac.argCalls != null)
					throw new CodeError("Invalid enum definition", "Invalid argument: " + ac.toString(), parser);
				if(!(ac.object instanceof A_Immutable))
					throw new CodeError("Invalid enum definition", "Argument is not immutable: " + ac.object.toString(), parser);
			}
		}
		final I_Object constant = argCalls == null || argCalls.length == 0
			? new JMo_Enum(currentType, s)
			: argCalls[0].object;

		currentType.addConstant(name, (A_Immutable)constant, parser.getDebugInfo());

		if(parser.buffer.nextLevelDiff() > 0)
			throw new CodeError("Invalid block", "No block allowed for enum!", parser);
	}

}
