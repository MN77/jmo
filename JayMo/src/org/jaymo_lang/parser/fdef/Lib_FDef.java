/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.fdef;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.Function;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Comply;

import de.mn77.base.data.constant.CONDITION;
import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 01.09.2019
 */
public class Lib_FDef {

//	public static final String FDEF_RETURN = "(" + REGEX.TYPE + "[?]?)?";
//	public static final String FDEF_RESULT = "(\\s*=\\s*.*)?";

	public static boolean isPrivate(final String s) {
		return s.charAt(0) == '_';
	}

	/**
	 * ATTENTION!!! This deletes the leading '='!!!
	 */
	public static void setReturnCall(String sReturnValue, final Parser_Script parser, final Function f) {

		if(sReturnValue.length() > 0) {
			sReturnValue = sReturnValue.substring(1).trim(); // Filter '='

			// Calc
			final Call[] ca = Parser_Call.parseArguments(parser, f.getBlock(), sReturnValue);
			if(ca.length != 1)
				throw new CodeError("Invalid function definition", "Invalid result for one-line-function", parser);

			// Set
			final Call resultc = new Call(f.getBlock(), f, "return", ca, parser.getDebugInfo());
			f.getBlock().add(resultc);

			f.setOneLine();
		}
	}

	/**
	 * @apiNote Splits a String like "foo | bar | bak" to a trimmed String[]
	 *          Name will be checked for complience
	 */
	public static String[] toNames(final Parser_Script parser, final String s) {
		final SimpleList<String> al = new SimpleList<>();
		int start = 0;
		int end = 0;
		Boolean isPrivate = null;
		String name = null;

		while((end = s.indexOf('|', start)) > -1) {
			name = s.substring(start, end).strip();
			if(isPrivate == null)
				isPrivate = name.length() > 0 && Lib_FDef.isPrivate(name);
			Lib_FDef.checkName(parser, name, isPrivate);
			al.add(name);

			start = end + 1;
		}
		name = s.substring(start).strip();
		Lib_FDef.checkName(parser, name, isPrivate);
		al.add(name);

		return al.toArray(new String[al.size()]);
	}

	private static void checkName(final Parser_Script parser, final String name, final Boolean isPrivate) {
		if(!Lib_Comply.checkFunctionName(name, isPrivate == null ? CONDITION.MAYBE : isPrivate ? CONDITION.ALWAYS : CONDITION.NEVER))
			throw new CodeError("Invalid function name", "Illegal function name: " + name, parser);
	}

}
