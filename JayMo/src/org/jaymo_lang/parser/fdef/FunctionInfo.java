/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.fdef;

/**
 * @author Michael Nitsche
 * @created 27.01.2021
 */
public class FunctionInfo {

	public final String[] names;
	public final boolean  isPrivate;
	public final boolean  control;
	public final String   args;
//	public final boolean varargs;
	public final String  returnType;
	public final boolean returnNil;
	public final String  returnDef;


	public FunctionInfo(final String[] names, final boolean priv, final boolean con, final String args, /* boolean va, */ final String rType, final boolean rNil, final String rDef) {
		this.names = names;
		this.isPrivate = priv;
		this.control = con;
		this.args = args;
//		this.varargs = va;
		this.returnType = rType == null || rType.length() == 0 ? null : rType;
		this.returnNil = rNil;
		this.returnDef = rDef;
	}

}
