/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Char;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Parser;
import org.jaymo_lang.util.Lib_StringParser;

import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 */
public class ParseObj_Char implements I_ParseObject {

	// private static final String regex_char="^'(.{1,2})'.*$";

	public boolean hits(final char c0, final Block current, final String s) {
		// return s.charAt(0)=='\'' && s.matches(regex_char);
		return c0 == '\''; // Validity will be chacked at parsing: && Lib_StringParser.checkChar(s, false, null) > 0;
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		final int len = Lib_StringParser.checkChar(s, false, null);

		switch(len) {
			case 3:
				return new Group2<>(new Char(s.charAt(1)), s.substring(len));
			case 4:
				final char c = Lib_Parser.replaceEscapeChar(s.charAt(2), parser.getDebugInfo());
				return new Group2<>(new Char(c), s.substring(len));
			case 8:
				final char utf = Lib_Parser.replaceUtfChar(s, 2, parser.getDebugInfo());
				return new Group2<>(new Char(utf), s.substring(len));

			default:
				throw new CodeError("Unclosed char", s, parser);
		}
	}

}
