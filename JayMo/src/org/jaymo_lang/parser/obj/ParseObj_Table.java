/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import java.util.List;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectManager;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Parser;
import org.jaymo_lang.util.Lib_StringParser;

import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 10.12.2020
 */
public class ParseObj_Table implements I_ParseObject {

	public boolean hits(final char c0, final Block current, final String s) {
//		return c0 == '|' && s.startsWith("||||");
		return c0 == '|'; // Test for "||||" moved to ".parse"
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		if(!s.startsWith("||||"))
			throw new CodeError("Invalid table block", "A TableBlock must start with '||||'.", parser);

		final int baseLevel = parser.buffer.getCurrentLevel();

		String rem = null;
		final List<String> lines = ConvertString.toList("\u0001", s.substring(4));
		if(lines.size() > 0 && lines.get(0).length() != 0)
			throw new CodeError("Invalid table block", "Missing linebreak after '||||'.", parser);

		final StringBuilder sb = new StringBuilder();
		int tableWidth = -1;

		for(final String line : lines) {
			String lineTrimmed = line.trim();
			if(lineTrimmed.length() == 0)
				continue;
			lineTrimmed = lineTrimmed.replace('\u0002', ',');

			if(line.charAt(0) == ' ')
				throw new CodeError("Line starts with Space-Char(s)", "Line: " + lineTrimmed, parser);

			final int lineDepth = Lib_Parser.getDepth(line);

			if(lineDepth != baseLevel)
				throw new CodeError("Invalid indent", "Row must be on the same level as the table block: " + lineTrimmed, parser);

			if(lineTrimmed.startsWith("||||")) {
				rem = lineTrimmed.substring(4);
				break; // This happens only at the last line, so it's okay.
			}
			sb.append(".add(");

			// Only check the first line, this improves speed. Args will be checked later whith 'add'.
			if(tableWidth == -1) {
				final List<String> args = Lib_StringParser.splitArgs(lineTrimmed, parser.getDebugInfo());
				tableWidth = args.size();
			}
//			else

//				Lib_Error.ifArgs(lineWidth, tableWidth, tableWidth, "cells in row", parser.getDebugInfo());

			sb.append(lineTrimmed);
			sb.append(")");
		}
		if(rem == null)
			throw new CodeError("Invalid table block", "The terminating '||||' is missing.", parser.getDebugInfo());

		if(tableWidth == -1)
			tableWidth = 0;

		final Call[] args = {new Call(current, new Int(tableWidth), parser.getDebugInfo())};
		final I_Object obj = ObjectManager.createNew(parser.app, current, "Table", args, parser.getDebugInfo()); // TODO Direkt?!?	TODO ohne args

		return new Group2<>(obj, sb.toString() + rem);
	}

}
