/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectManager;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 10.12.2020
 *
 *          TODO Needs a lot of optimization!
 */
public class ParseObj_Tree implements I_ParseObject {

	public boolean hits(final char c0, final Block current, final String s) {
//		return c0 == '>' && s.startsWith(">>>>");
		return c0 == '>'; // Test for 4x'>' moved to .parse
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		if(!s.startsWith(">>>>"))
			throw new CodeError("Invalid TreeBlock", "A TreeBlock must start with '>>>>'", parser);

		final int end = s.indexOf("<<<<", 4);
		if(end == -1)
			throw new CodeError("Unclosed TreeBlock", "Missing '<<<<' at the end of the TreeBlock", parser);

		String rem = end == -1 ? "" : s.substring(end + 4);
		final String s2 = s.substring(4, end);

		boolean lazy = false;

		if(rem.startsWith("?")) {
			lazy = true;
			rem = rem.substring(1);
		}
//		MOut.temp(s,end,rem, lazy);

		final StringBuilder sb = new StringBuilder();
		sb.append(".build(");
//		sb.append(".addNodes(");
		this.iParse(sb, s2, lazy);
		sb.append(")");

		final I_Object obj = ObjectManager.createNew(parser.app, current, "Tree", new Call[0], parser.getDebugInfo()); // TODO Direkt?!?	TODO ohne args
		return new Group2<>(obj, sb.toString() + rem);
	}

	private int iNextCombineChar(final String key, final int offset) {
		boolean step1 = false;

		for(int i = offset; i < key.length(); i++) {
			final char c = key.charAt(i);

			if(c == '\\') {
				step1 = true;
				continue;
			}
			if(step1 && c == 't')
				return i - 1;
			step1 = false;
		}
		return -1;
	}

	private void iParse(final StringBuilder sb, final String spar, final boolean lazy) {
//		final String[] lines = spar.split("\\n");
		final SimpleList<String> lines = this.iParseSplit(spar);
//		final SimpleList<String> lines = ConvString.toList("\\n", spar);	// Possible, but maybe slower

		this.iParseAddNode(sb, lines, 0, -1, lazy);
	}

	private int iParseAddNode(final StringBuilder sb, final SimpleList<String> lines, final int next, int baseIndent, final boolean lazy) {
		boolean open = false;

		for(int i = next; i < lines.size(); i++) {
			final String s = lines.get(i);
			if(s.trim().length() == 0)
				continue;

			if(baseIndent == -1)
				baseIndent = Lib_Parser.getDepth(s);

			final int curIndent = Lib_Parser.getDepth(s);
//			Lib_Error.ifTooSmall(cr, base, curIndent); // TODO reaktivieren, + Title!

			if(curIndent < baseIndent) {
				sb.append(')');
				return i;
			}

			if(curIndent == baseIndent) {

				if(open) {
					sb.append("),");
					open = false; // eigentlich unsinnig
				}
				final Group2<String, String> name_value = this.iSplitNameValue(s.trim(), lazy);
				sb.append("TreeNode(" + name_value.o1 + ',' + name_value.o2);
				open = true;
			}
			else {
				Err.ifToBig(baseIndent + 1, curIndent); //TODO title
				sb.append(',');
				i = this.iParseAddNode(sb, lines, i, curIndent, lazy) - 1;
			}
		}

		if(open) {
			sb.append(')');
			open = false; // unsinnig
		}
		return lines.size();
	}

	private SimpleList<String> iParseSplit(final String s) {
		final SimpleList<String> list = new SimpleList<>();
		int start = 0;
		for(int i = 0; i < s.length(); i++)
			if(s.startsWith("\\n", i)) {
//				if(curline.trim().endsWith(","))
//					continue;

				list.add(s.substring(start, i));
				i++;
				start = i + 1;
			}

		if(start < s.length())
			list.add(s.substring(start));

		return list;
	}

	private String iReplaceCombineChars(final String key) {
		int offset = 0;
		String buffer = null;

		do {
			final int next = this.iNextCombineChar(key, offset);

			if(next == -1) {
				if(buffer == null)
					return key;
				else
					return buffer + key.substring(offset).trim();
			}
			else {
				final String part = key.substring(offset, next) + ',';
//				offset+= 1;
				if(buffer == null)
					buffer = part;
				else
					buffer += part;
			}
			offset = next < 0 ? next : next + 2;
		}
		while(offset >= 0);
		throw Err.invalid(key, offset, buffer);
	}

	private Group2<String, String> iSplitNameValue(final String s, final boolean lazy) {
		String name = s;
		String value = null;

		int idx = s.indexOf(':');
		if(idx == -1)
			idx = s.indexOf('=');

		if(idx >= 0) {
			name = s.substring(0, idx).trim();
			value = s.substring(idx + 1).trim();
		}
		if(name.indexOf("\\t") > -1)
			name = this.iReplaceCombineChars(name);
		if(value != null && value.indexOf("\\t") > -1)
			value = value.replace("\\t", ",");

		return new Group2<>('"' + name + '"', value == null ? "nil" : lazy ? '"' + value + '"' : value);
	}

}
