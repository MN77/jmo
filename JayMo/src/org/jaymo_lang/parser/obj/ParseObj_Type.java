/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.model.Block;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.JMo_Type;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 04.04.2021
 */
public class ParseObj_Type implements I_ParseObject {

	public boolean hits(final char c0, final Block current, final String s) {
		return c0 == '<';
//
//		if(c0 != '<' || s.length() < 3)
//			return false;
//
//		final char c1 = s.charAt(1);
//		return c1 >= 'A' && c1 <= 'Z';
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		final String typeName = Lib_Parser.group('<', '>', s, parser.getDebugInfo());
		final String rem = s.substring(1 + typeName.length() + 1);
//		MOut.dev(part, rem);

//		for(ATOMIC a : ATOMIC.values())
//			if(a.name.equals(typeName)) {
//				final I_Object obj = new JMo_Type(typeName);
//				return new Group2<>(obj, rem);
//			}

//		Class<?> clazz = ObjectManager.getClass(typeName, false);
//		if(clazz == null)
//			throw new CodeError("Unknown type name", "Type not found: '"+typeName+"'", parser.gDebugInfo());

		final I_Object obj = new JMo_Type(typeName, parser);
		return new Group2<>(obj, rem);
	}

}
