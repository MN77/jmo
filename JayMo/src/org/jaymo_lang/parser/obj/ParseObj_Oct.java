/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.model.Block;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 */
public class ParseObj_Oct implements I_ParseObject {

	public boolean hits(final char c0, final Block current, final String s) {
		return c0 == '0' && s.startsWith("0o"); // Suffix 'o' is not used for numbers
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		boolean hasUnderline = false;
		int end = 2;

		for(int i = 2; i < s.length(); i++) {
			final char c = s.charAt(i);

			if(c >= '0' && c <= '7' || c == '_') {
				end = i;
				if(c == '_')
					hasUnderline = true;
			}
			else
				break;
		}
		String nr = s.substring(2, end + 1);
		final int nrLen = nr.length();
//		final String nr = FilterString.matchingLeft("01234567", s.substring(2));
		if(hasUnderline)
			nr = Lib_Parser.removeUnderlines(nr, parser.getDebugInfo());
		final long i = Long.parseLong(nr, 8);
		return new Group2<>(new JMo_Long(i), s.substring(nrLen + 2));
	}

}
