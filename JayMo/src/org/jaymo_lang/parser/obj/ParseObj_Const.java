/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.model.Block;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.passthrough.Const;
import org.jaymo_lang.parser.Parser_Script;

import de.mn77.base.data.constant.CHARS;
import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 */
public class ParseObj_Const implements I_ParseObject {

	private static final String matching = CHARS.ABC_UPPER + '_' + CHARS.NUMBERS + '?';


	public boolean hits(final char c0, final Block current, final String s) {
		if(c0 < 'A' || c0 > 'Z')
			return false;

		for(final char c : s.toCharArray()) {
			if(c >= 'A' && c <= 'Z' || c >= '0' && c <= '9' || c == '_')
				continue;
			if(c >= 'a' && c <= 'z')
				return false;
			return true;
		}
		return true;
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		Err.ifNull(current);
		final String var = FilterString.matchingLeft(ParseObj_Const.matching, s); //TODO Improvement is possible by using: c >= 'A' ... instead of ".indexof"
		final String rem = s.substring(var.length());
		final Const vo = current.getConstManager().use_ParseTime(parser, var, false);
		return new Group2<>(vo, rem);
	}

}
