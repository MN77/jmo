/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.passthrough.Count;
import org.jaymo_lang.object.passthrough.Group;
import org.jaymo_lang.object.pseudo.NonAtomic;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.struct.JMo_Set;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Parser;

import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 */
public class ParseObj_ShortCuts implements I_ParseObject {

	private boolean group_ok = true;
	private boolean list_ok  = true;
	private boolean count_ok = true;


	public boolean hits(final char c0, final Block current, final String s) {
		return c0 == '(' || c0 == '[' || c0 == '{';
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {

		switch(s.charAt(0)) {
			case '(':
				return this.parseGroup(parser, current, s);
			case '[':
				return this.parseList(parser, current, s);
			case '{':
				return this.parseCount(parser, current, s);
		}
		throw Err.impossible(s);
	}

	public void strict(final boolean group_ok, final boolean list_ok, final boolean count_ok) {
		this.group_ok = group_ok;
		this.list_ok = list_ok;
		this.count_ok = count_ok;
	}

	private Group2<I_Object, String> parseCount(final Parser_Script parser, final Block current, final String s) {
		if(!this.count_ok)
			throw new CodeError("Strict: Shortcut of For is forbidden. Use For()!", s, parser);

		final String part = Lib_Parser.group('{', '}', s, parser.getDebugInfo());
		final String rem = s.substring(1 + part.length() + 1);

		final Call[] args = Parser_Call.parseArguments(parser, current, part);

		if(args.length == 0)
			throw new CodeError("Missing argument", s, parser);
		if(args.length > 3)
			throw new CodeError("Too many arguments", s, parser);

		final Call from = args[0];
		final Call to = args.length >= 2
			? args[1]
			: null;
		final Call step = args.length == 3
			? args[2]
			: null;

//		rem = Lib_Parser.shortcutVarLet(rem);
		return new Group2<>(new NonAtomic(Count.class, new Call[]{from, to, step}), ".each()" + rem);
	}

	private Group2<I_Object, String> parseGroup(final Parser_Script parser, final Block current, final String s) {
		if(!this.group_ok)
			throw new CodeError("Strict: Shortcut of group is forbidden. Use Group()!", s, parser);

		String group = Lib_Parser.group('(', ')', s, parser.getDebugInfo());
		final String rem = s.substring(group.length() + 2);
		group = group.trim();
		if(group.length() == 0)
			throw new CodeError("Missing argument for Group", s, parser);

		final Call arg = Parser_Call.parseCall(parser, current, group, true);
//		rem = Lib_Parser.shortcutVarLet(rem);
		return new Group2<>(new NonAtomic(Group.class, new Call[]{arg}), ".get()" + rem);
	}

	private Group2<I_Object, String> parseList(final Parser_Script parser, final Block current, final String s) {
		if(!this.list_ok)
			throw new CodeError("Strict: Shortcut of list is forbidden. Use List()!", s, parser);

		final String list = Lib_Parser.group('[', ']', s, parser.getDebugInfo());
		String rem = s.substring(list.length() + 2);
		Call[] args = Parser_Call.parseArguments(parser, current, list);
		Class<? extends I_Object> cl = JMo_List.class;

		if(rem.length() > 0) {
			final char c = rem.charAt(0);

			if(c == 'l')
				rem = rem.substring(1);
			else if(c == 's') {	// Set
				rem = rem.substring(1);
				cl = JMo_Set.class;
			}
			else if(c == 'b') {	// ByteArray
				// ByteArray needs 'size' as parameter, so '.content' is used.
				final StringBuilder sb = new StringBuilder();
				sb.append(".content(");
				sb.append(list);
				sb.append(')');
				sb.append(rem.substring(1));
				rem = sb.toString();

				cl = JMo_ByteArray.class;
				args = new Call[]{new Call(current, new Int(args.length), parser.getDebugInfo())};
			}
		}

		return new Group2<>(new NonAtomic(cl, args), rem);
	}

}
