/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectManager;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Parser;
import org.jaymo_lang.util.Lib_Prefix;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 14.05.2021
 */
public class ParseObj_NewWithOpenArg implements I_ParseObject {

	public static final String NOT = ".=+-*/!&>|^~\\"; // ≈%¶(<		// !!! Check also ParseFunc_Direct.NO_OPEN_ARG !!!


	public boolean hits(final char c0, final Block current, final String s) {
		if(c0 != '_' && (c0 < 'A' || c0 > 'Z'))
			return false;

		byte state = 0; // 0 = type, 1 = spaces, (2 = arg)
		int index = -1;

		for(final char c : s.toCharArray()) {
			index++;

			switch(state) {
				case 0:
					if(c == ' ' || c == '\t') // tab is not okay, but handle this here
						state = 1;
					else if(!(c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c >= '0' && c <= '9' || c == '_'))
						return false;
					break;
				case 1:
					if(c == ' ') // Allow more than one whitespace
						continue;

					if(c == '<') { // Filter Type
						final String s2 = s.substring(index);
						return Lib_Parser.fastCheckStartWithTypeNotation(s2);
					}
					else if(ParseObj_NewWithOpenArg.NOT.indexOf(c) != -1) {
						if(index < s.length() - 1 && (c == '+' || c == '-')) // +5, -infinity
							return s.charAt(index + 1) != ' ';
					}
					else if(c != ' ' && c != '\t') // tab is not okay, but handle this here
						return true;

					return false;
			}
		}
		return false;
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		parser.app.strict.checkOpenArg(parser.getDebugInfo());

		String m = Lib_Parser.getTypeName(s, parser);
//		String ps = Lib_Parser.group('(', ')', s.substring(m.length()), parser.getDebugInfo());
		String ps = s.substring(m.length());
		final String rem = ""; //s.substring(m.length() + ps.length() );
//		MOut.temp(s, m, ps, rem);

//		m = m.trim();
//		ps = ps.trim();
		ps = FilterString.trimSpace(ps, true, true); // trim only space-chars, so tabs can be catched
//		rem = rem.trim();

		if(parser.buffer.nextLevelDiff() > 0)
			throw new CodeError("Invalid use of open argument", "For using block, please try: " + m + "( " + ps + " )", parser);

		if(ps.charAt(0) == '\t')
			throw new CodeError("Invalid arguments for type", "Please remove tabulator char after: " + m, parser);

//		if(rem.length() != 0)
//			throw new CodeError("Invalid argument for type", "Attachment can't be parsed: "+rem, parser.getDebugInfo());
//			throw Err.invalid(rem); //

		m = Lib_Prefix.addPrefix(m, parser);
		Lib_Type.checkValidity(m, parser);

		final Call[] args = Parser_Call.parseArguments(parser, current, ps);
		if(args.length != 1)
			throw new CodeError("Invalid amount of arguments", "For more arguments, please try: " + m + '(' + ps + ')', parser);
		if(args[0].hasBlock())
			throw new CodeError("Invalid use of open argument", "For using block, please try: " + m + "( " + ps + " )", parser);
//		if(args[0].hasStream())	// For objects it's okay
//			throw new CodeError("Invalid use of open argument", "For using stream, please try: " + m + "( " + ps + " )", parser.getDebugInfo());
//		if(args[0].method != null) // Method is also okay

		final I_Object o = ObjectManager.createNew(parser.app, current, m, args, parser.getDebugInfo());

		return new Group2<>(o, rem);
	}

}
