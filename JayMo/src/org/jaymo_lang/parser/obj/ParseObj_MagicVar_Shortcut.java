/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.model.Block;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.magic.var.MV_SUPER;
import org.jaymo_lang.object.magic.var.MV_THIS;
import org.jaymo_lang.object.magic.var.MagicVar;
import org.jaymo_lang.object.magic.var.MagicVar.MAGICVAR;
import org.jaymo_lang.parser.Parser_Script;

import de.mn77.base.data.group.Group2;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 06.02.2018
 */
public class ParseObj_MagicVar_Shortcut implements I_ParseObject {

//	private final String shortcuts = "°%$€§"; // &±
	private final String shortcuts = "°§"; // Also update: ParseManagerObj.iGetParser1


	public boolean hits(final char c0, final Block current, final String s) {
		return this.shortcuts.indexOf(c0) >= 0;
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
		Err.ifNull(current);
		final char c = s.charAt(0);

		switch(c) {
			// !!! UPDATE ParseFunc_Math IF SOMETHING IS CHANGED HERE !!! (Without '&')

			case '°':
				if(s.length() >= 2 && s.charAt(1) == '°')
					return new Group2<>(new MV_SUPER(true), s.substring(2));
				else
					return new Group2<>(new MV_THIS(true), s.substring(1)); //TODO Function "iAddDot"?
//			case '%':
////				return new Group2<>(new MC_IT_BLOCK(true), s.substring(1));
//				final Var vo = current.getVarManager().use_ParseTime(parser, Var.IT_FULL, false);
//				String rem = s.substring(1);
//				if(rem.length() > 0 && rem.charAt(0) >= 'a' && rem.charAt(0) <= 'z')
//					rem = '.' + rem;
//				return new Group2<>(vo, rem);

//			case '$':
//				if(s.length() >= 2 && s.charAt(1) == '$')
//					return new Group2<>(new MV_EACH(true), s.substring(2));
//				else
//					return new Group2<>(new MV_CUR(true), s.substring(1));
//			case '€':
//			case '£':
//				return new Group2<>(new MV_EACH(true), s.substring(1));
			case '§':
				return new Group2<>(new MagicVar(MAGICVAR.FUNC), this.iAddDot(s));
//			case '&': // This can't be used as argument!!!
//			case '±':
//				return new Group2<>(new MagicVar(MAGICVAR.LOOP), this.iAddDot(s));

			default:
				throw Err.impossible(c, "" + c);
		}
	}

	private String iAddDot(String s) {
		s = s.substring(1).trim();
		if(s.length() == 0)
			return "";
		return s.charAt(0) != '.' ? '.' + s : s;
	}

}
