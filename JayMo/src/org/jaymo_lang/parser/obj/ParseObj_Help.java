/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.obj;

import org.jaymo_lang.model.Block;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.JMo_Help;
import org.jaymo_lang.parser.Parser_Script;

import de.mn77.base.data.group.Group2;


/**
 * @author Michael Nitsche
 * @created 11.08.2022
 */
public class ParseObj_Help implements I_ParseObject {

	public boolean hits(final char c0, final Block current, final String s) {
		return c0 == '?';
	}

	public Group2<I_Object, String> parse(final Parser_Script parser, final Block current, final String s) {
//		if(s.length() < 2 || s.charAt(1) != '?')
//			throw new CodeError("Invalid help format", "Allowed are '??' and '???' but got only '?'!", parser.getDebugInfo());

		return s.startsWith("??")
			? new Group2<>(new JMo_Help(), ".explain(\"" + s.substring(2) + "\")")
			: new Group2<>(new JMo_Help(), ".quickly(\"" + s.substring(1) + "\")");
	}

}
