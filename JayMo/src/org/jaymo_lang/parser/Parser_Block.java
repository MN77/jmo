/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.ErrorBase;
import org.jaymo_lang.model.App;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.Event;
import org.jaymo_lang.model.Function;
import org.jaymo_lang.model.IncludeInfo;
import org.jaymo_lang.model.Type;
import org.jaymo_lang.util.Lib_Parser;
import org.jaymo_lang.util.LineBuffer;

import de.mn77.base.data.convert.ConvertObject;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 23.02.2018
 */
public class Parser_Block {

	private Call    last               = null; // TODO anders lösen und Parser_Block static ?!?
	private boolean header_switches_ok = true;


	public void parse(final Parser_Script parser, final Type currentType, final Function currentFunction, final Block currentBlock, final boolean root) {
		final LineBuffer buffer = parser.buffer;
		final App app = parser.app;
		final ParseTimeSwitches pts = new ParseTimeSwitches();

		try {

			while(buffer.hasNext()) {
				final int nextLevelDiff = buffer.nextLevelDiff();

				if(nextLevelDiff < 0) {
					buffer.levelDownchanged();
					return;
				}
				if(nextLevelDiff > 1)
					throw new CodeError("To much indents for the new blocklevel", "Got " + nextLevelDiff + " Tabs, 1 allowed", buffer);

				final String s = buffer.getNext();
				final int sLen = s.length();
				if(sLen == 0)
					continue;
				if(s.charAt(0) == ' ')
					throw new CodeError("Leading whitespace", "Line " + buffer.getDebugInfo().getLine() + " starts with whitespace: " + s.replace(' ', '_'), buffer);
//				s.trim();

				// Switches at Parse-Time
				if(sLen > 1 && s.charAt(0) == '>' && !s.startsWith(">>>>")) {
					if(!root)
						throw new CodeError("Invalid use of parsetime-switch!", "It can only be used at the root-level.", buffer);
					else if(!this.header_switches_ok)
						throw new CodeError("Invalid use of parsetime-switch!", "It can only be used at the top of the script.", buffer);

					pts.parse(parser, s, buffer.getDebugInfo());

					// Parse other Scripts, before going on
					IncludeInfo fileToImport = null;

					while((fileToImport = app.importsReadNext(parser.getDebugInfo())) != null) {
						app.strict.setAnotherScript();
						final Parser_Script ps = new Parser_Script(app, fileToImport.fileBase, fileToImport.fileInfo, fileToImport.code);
						ps.parse();
					}
//					continue;
				}
				else { //	if(sLen > 0) {
					this.header_switches_ok = false;
					this.parseTypeFunc(parser, currentType, currentFunction, currentBlock, s, true);
				}
			}
		}
		catch(final ErrorBase t) {
			throw t;
		}
		// Convert other Errors to JayMo-Errors
		catch(final Err_Runtime t) { // |Err_Exception
			if(parser.app.isDebug())
				Err.show(t);
			throw new CodeError(t.getMessage(), ConvertObject.toStringIdent(t.getDetails()), buffer);
		}
		catch(final RuntimeException t) {
			if(parser.app.isDebug())
				Err.show(t);
			throw new CodeError(t.getClass().getSimpleName(), t.getMessage(), buffer);
		}
	}

	private Call parseCall(final Parser_Script parser, final Block pc, final String s) {
		Err.ifNull(pc, s);
		return Parser_Call.parseCall(parser, pc, s, false);
	}

	private void parseTypeFunc(final Parser_Script parser, final Type currentType, final Function currentFunction, final Block currentBlock, String s, final boolean isLastInLine) {
		s = s.trim();
		if(s.length() == 0)
			return;

		// Define something new
		if(s.charAt(0) == ':' && s.startsWith("::")) {
			s = s.substring(2).trim();
			char ch0 = s.charAt(0);
			if(ch0 == '_' && s.length() > 1)
				ch0 = s.charAt(1);

			// New Type
			if(ch0 >= 'A' && ch0 <= 'Z') {
				if(Lib_Parser.fastCheckIsType(s, parser.getDebugInfo()))
					Parser_Type.parse(parser, currentType, s);
				else
					Parser_Enum.parse(parser, currentType, currentBlock, s);
				return;
			}

			// New Function
			if(ch0 >= 'a' && ch0 <= 'z') {
//				MOut.dev("New Function: " + s);
				Parser_Function.parse(parser, currentType, s);
				return;
			}

			// New Event-Definition
			if(ch0 == '@') {
//				MOut.dev("New Event: " + s);
				final Event ev = Parser_EventHead.parse(parser, currentType, s, parser.getDebugInfo());
				currentType.getEvents().add(ev.gName().substring(1), ev);
				if(parser.buffer.nextLevelDiff() > 0)
					throw new CodeError("No Block allowed for Event-Definition", s, parser);
				return;
			}

			// New magic constant
			if(ch0 == '_') {
//				MOut.dev("New magic constant: " + s);
				Parser_MagicConst.parse(parser, currentType, currentBlock, s);
				if(parser.buffer.nextLevelDiff() > 0)
					throw new CodeError("No Block allowed for Constant-Definition", s, parser);
				return;
			}
			throw new CodeError("Invalid definition", "::" + s, parser);
		}
		final Call c = this.parseCall(parser, currentBlock, s);


		// --- Combine ---

//		if(last != null && s.matches("^\\s*[" + Lib_Parser.COMBINE_CHARS + "].*$"))
//		if(this.last != null && Lib_Parser.COMBINE_CHARS.indexOf(s.trim().charAt(0)) >= 0)
//			this.last.setStream(c);
//		else
//			currentBlock.add(c);

		if(this.last != null && Lib_Parser.combineBegin(s) > 0)
			this.last.setStream(c);
		else
			currentBlock.add(c);

		this.last = c.searchLastCall();

		if(isLastInLine && parser.buffer.nextLevelDiff() > 0) {
			final Parser_Block pb = new Parser_Block();
			final Block b = new Block(currentType, currentFunction, currentBlock);
			this.last.setBlock(b);

			pb.parse(parser, currentType, currentFunction, b, false);
		}
	}

}
