/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.magic.I_MagicReplace;
import org.jaymo_lang.parser.func.I_ParseFunc;
import org.jaymo_lang.parser.func.ParseFunc_Command;
import org.jaymo_lang.parser.func.ParseFunc_Direct;
import org.jaymo_lang.parser.func.ParseFunc_EventHandler;
import org.jaymo_lang.parser.func.ParseFunc_EventStart;
import org.jaymo_lang.parser.func.ParseFunc_Let;
import org.jaymo_lang.parser.func.ParseFunc_Math;
import org.jaymo_lang.parser.func.ParseFunc_MathLet;
import org.jaymo_lang.parser.func.ParseFunc_PosSetGet;
import org.jaymo_lang.parser.func.ParseFunc_PropertySet;
import org.jaymo_lang.parser.func.ParseFunc_Range;
import org.jaymo_lang.parser.func.ParseFunc_SelectUpdate;
import org.jaymo_lang.parser.func.ParseFunc_StringSetGet;
import org.jaymo_lang.parser.func.ParseFunc_TempLet;
import org.jaymo_lang.parser.func.ParseFunc_VarIncDec;
import org.jaymo_lang.util.Lib_MagicVar;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 28.12.2019
 */
public class ParseManagerFunc {

	private final I_ParseFunc[] pool = {
		new ParseFunc_PosSetGet(),
		new ParseFunc_StringSetGet(),
		new ParseFunc_SelectUpdate(),
		new ParseFunc_VarIncDec(),
		new ParseFunc_Let(),
		new ParseFunc_MathLet(),
//		new ParseFunc_Search(),		// Currently not working right!
		// new ParseFunc_Inc(),
		new ParseFunc_Command(),
		new ParseFunc_Range(),
		new ParseFunc_Math(),
//		new ParseFunc_DeepSetGet(),
		new ParseFunc_EventStart(),
		new ParseFunc_PropertySet(),
		new ParseFunc_Direct(),
		new ParseFunc_EventHandler(),
//		new ParseFunc_EvHandle_Pars(),
		new ParseFunc_TempLet(),
		// new ParseFunc_Par1Free(), // TODO: Möglich, funktioniert, aber für klarere Struktur deaktiviert
	};

	private I_ParseFunc[] active = this.pool.clone();


	public ParseManagerFunc() {
		this.active = new I_ParseFunc[this.pool.length];
		System.arraycopy(this.pool, 0, this.active, 0, this.pool.length);
	}

	public I_ParseFunc getParser(final I_Object obj, final String s, final Parser_Script parser) {
//		MOut.dev("ParseMethod: " + s);
		final char c0 = s.charAt(0);

		for(final I_ParseFunc pf : this.active)
			if(pf.hits(c0, s))
				return pf;

		// --- Unexpected term ---

		if(c0 == ',')
			throw new CodeError("Unexpected argument separator", s, parser);
		if(c0 == ')' || c0 == '}' || c0 == ']')
			throw new CodeError("Unexpected closing bracket", "Got: " + s, parser);
		if(c0 >= '0' && c0 <= '9' || c0 >= 'a' && c0 <= 'z' || c0 >= 'A' && c0 <= 'Z' || c0 == '_')
			throw new CodeError("Unexpected argument", "Missing brackets, got: " + s, parser);

		final String detail = obj == null
			? "Unexpected argument(s) or missing brackets for"
			: obj instanceof I_MagicReplace
				? "For magic " + Lib_MagicVar.magicTypeToVarConstString((I_MagicReplace)obj) + " '" + obj.toString() + "' got"
				: "For object '" + obj.toString() + "' from type " + Lib_Type.getTypeString(obj) + " got";

		throw new CodeError("Invalid function call", detail + ": " + s, parser);
	}

	public void strictLevel(final PARSER_LEVEL level) {
		final SimpleList<I_ParseFunc> list = new SimpleList<>(this.pool.length);

		for(final I_ParseFunc pf : this.active) {

			switch(level) { // No breaks, should run through!
				case INSANE:
					if(pf instanceof ParseFunc_Let
//						|| pf instanceof ParseFunc_Search
						|| pf instanceof ParseFunc_Math)
						continue;
				case HIGH:
					if(pf instanceof ParseFunc_PosSetGet
						|| pf instanceof ParseFunc_VarIncDec
						|| pf instanceof ParseFunc_MathLet
						|| pf instanceof ParseFunc_SelectUpdate
						|| pf instanceof ParseFunc_StringSetGet
						|| pf instanceof ParseFunc_PropertySet
						|| pf instanceof ParseFunc_Range
						|| pf instanceof ParseFunc_TempLet)
						continue;
				case MEDIUM:
					if(pf instanceof ParseFunc_SelectUpdate)
						continue;
				case LOW:
				case OPEN:
			}
			list.add(pf);
		}
		this.active = list.toArray(new I_ParseFunc[list.size()]);
	}

	public void strictSandbox() {
		final SimpleList<I_ParseFunc> list = new SimpleList<>(this.pool.length);

		for(final I_ParseFunc pf : this.active) {
//			if(pf instanceof ParseFunc_Search)
//				continue;
			if(pf instanceof ParseFunc_Command)
				continue;

			list.add(pf);
		}
		this.active = list.toArray(new I_ParseFunc[list.size()]);
	}

}
