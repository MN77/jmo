/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser;

import java.io.File;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.model.App;

import de.mn77.base.error.Err;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.file.Lib_TextFile;
import de.mn77.base.version.Lib_Version;


/**
 * @author Michael Nitsche
 * @created 23.02.2018
 * @implSpec
 *           1. App
 *           2. Script
 *           3. Type
 *           4. Function
 *           5. Block
 *           6. Call
 */
public class Parser_App {

	private final boolean isVersionInit;
	private boolean       terminalModeRaw = false;
	private boolean       debug           = false;


	public Parser_App() {
		this(false);
	}

	public Parser_App(final boolean versionUpdate) {
		if(versionUpdate)
			this.versionInit(versionUpdate); // Needs ~200ms, without this no build-number is available

		this.isVersionInit = versionUpdate;
	}


//	public I_VersionData getVersion() {
//		return JayMo.VERSION;
//	}

	public String getVersionString(final boolean withBuild, boolean withDate) {
		final StringBuilder format = new StringBuilder();
		boolean isDev = false;

		switch(JayMo.STAGE) {
			case RELEASE:
				format.append("%1.%2.%3");	//%D
				break;
			case BETA:
//				format.append("BETA %y.%2%?3");
				format.append("BETA_%YYYY-MM-DD");
				isDev = true;
				break;
			case SID:
			default:
				format.append("SID_%YYYY-MM-DD");
				isDev = true;
				break;
		}

		if(isDev) {
			if(!this.isVersionInit)
				this.versionInit(false);

			withDate = false;

			if(withBuild)
//				format.append(" (Build: %b)");
				format.append(" (%b)");

//			format.append( " - %YYYY.MM.DD" );
		}
		if(withDate)
			format.append(" - %DD.MM.YYYY"); // TODO Init first ?!?

		return JayMo.version().format(format.toString());
	}

	public boolean isDevVersion() {
		return JayMo.STAGE.isDevelopment();
	}

	public void parseFile(final App app, final File file) throws Err_FileSys {
		final String source = Lib_TextFile.read(file);
		this.iParseText(app, file.getParent(), file.getName(), source);
	}

	public App parseFile(final File file) throws Err_FileSys {
		final App app = new App(this.terminalModeRaw, this.debug);
		this.parseFile(app, file);
		return app;
	}

	public void parseText(final App app, final String source) {
		this.iParseText(app, null, null, source);
	}

	public App parseText(final String source) {
		final App app = new App(this.terminalModeRaw, this.debug);
		this.iParseText(app, null, null, source);
		return app;
	}

	public App parseText(final String basePath, final String filePath, final String code) {
		final App app = new App(this.terminalModeRaw, this.debug);
		this.iParseText(app, basePath, filePath, code);
		return app;
	}

	public void setDebug() {
		this.debug = true;
	}

	public void setTerminalRawMode(final boolean inRawMode) {
		this.terminalModeRaw = inRawMode;
	}

	private void iParseText(final App app, final String basePath, final String fileInfo, final String code) {
		final Parser_Script ps = new Parser_Script(app, basePath, fileInfo, code);
		ps.parse();
	}

	private void versionInit(final boolean update) {

		try {
			Lib_Version.init(JayMo.version(), false, update);
		}
		catch(final Err_FileSys e) {
			Err.show(e);
		}
	}

}
