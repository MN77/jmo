/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.event;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.parser.Parser_Call;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.Lib_Comply;
import org.jaymo_lang.util.Lib_Error;


/**
 * @author Michael Nitsche
 * @created 24.11.2020
 */
public class ParserEvStart_Args implements I_ParserEvStart {

	public boolean hits(final String s) {
		final int i = s.indexOf('(');
		if(i == -1)
			return false;
		return Lib_Comply.checkEventName(s.substring(0, i).trim());
	}

	public Call parse(final Parser_Script parser, final Block current, final I_Object obj, final String s) {
		final int bracketOpen = s.indexOf('(');
		final int bracketClose = s.lastIndexOf(')');
		if(bracketClose == -1)
			throw new CodeError("Invalid event start", "Closing bracket is missing", parser);
		if(s.length() > bracketClose + 1)
			throw new CodeError("Invalid event start", "Invalid things after closing bracket: " + s.substring(bracketClose + 1), parser);

		final String m = s.substring(0, bracketOpen).trim();
		final String argsString = s.substring(bracketOpen + 1, bracketClose);
		final Call[] args = Parser_Call.parseArguments(parser, current, argsString);
		Lib_Error.ifArgs(args.length, 0, 1, m, parser.getDebugInfo());

		return new Call(current, obj, m, args, parser.getDebugInfo());
	}

}
