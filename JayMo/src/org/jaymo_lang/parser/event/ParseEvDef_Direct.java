/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.parser.event;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Event;
import org.jaymo_lang.model.Type;
import org.jaymo_lang.parser.Parser_Script;
import org.jaymo_lang.util.BLOCKED;
import org.jaymo_lang.util.Lib_Comply;


/**
 * @author Michael Nitsche
 * @created 22.05.2018
 */
public class ParseEvDef_Direct implements I_ParseEvDef {

	public boolean hits(final String s) {
		return s.charAt(0) == '@';
	}

	public Event parse(final Parser_Script parser, final Type type, final String s) {
		for(final String e : BLOCKED.EVENT_NAMES)
			if(e.equals(s))
				throw new CodeError("Invalid event definition", "This event-name is forbidden: " + s, parser);

		if(!Lib_Comply.checkEventName(s)) {
			String detail = "This event name is invalid: " + s;
			if(s.length() == 2)
				detail = "This event name is too short: " + s;
			if(s.indexOf('.') > -1)
				detail = "No attached stream allowed, but got: " + s;
			throw new CodeError("Invalid event definition", detail, parser);
		}
		return new Event(s);
	}

}
