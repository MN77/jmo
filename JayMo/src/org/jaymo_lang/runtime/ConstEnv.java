/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.runtime;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.passthrough.I_Mem;


/**
 * @author Michael Nitsche
 * @created 30.11.2022
 *
 * @apiNote
 *          Runtime: Hold the type and value of const's
 */
public class ConstEnv extends A_MemEnv {

	public ConstEnv(final ConstEnv parent) {
		super(parent);
	}

	public I_Object get(final CallRuntime cr, final I_Mem vc) {
		return super.get(cr, vc, vc.getName());
	}

	public boolean isInitialized(final I_Mem vc) {
		return super.isInitialized(vc, vc.getName());
	}

	public boolean knows(final I_Mem vc) {
		return super.knows(vc, vc.getName());
	}

	public void set(final CallRuntime cr, final I_Mem vc, final I_Object obj, final boolean nilable, final boolean convert) {
		super.set(cr, vc, obj, nilable, convert, vc.getName());
	}

}
