/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.runtime;

import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.passthrough.I_Mem;
import org.jaymo_lang.object.passthrough.Var;
import org.jaymo_lang.util.Lib_Convert;


/**
 * @author Michael Nitsche
 * @created 03.09.2019
 *
 * @apiNote
 *          Runtime: Hold the type and value of var's and 'it'
 */
public class VarEnv extends A_MemEnv {

	private I_Object itValue = Nil.NIL; // Its okay to store this here. Every block has its own Var-Environment


	public VarEnv(final VarEnv parent) {
		super(parent);
	}

	/**
	 * @return Returns the stored value or null if it is not initialized;
	 */
	public I_Object get(final CallRuntime cr, final I_Mem vc) {
		final String name = vc.getName();
		if(name.equals(Var.IT))
			return this.itValue;

		return super.get(cr, vc, name);
	}

	/**
	 * @return Returns true, if the var/const has a defined value.
	 */
	public boolean isInitialized(final I_Mem vc) {
		final String name = vc.getName();
		if(name.equals(Var.IT))
			return true;

		return super.isInitialized(vc, name);
	}

	public boolean knows(final I_Mem vc) {
		final String name = vc.getName();
		if(name.equals(Var.IT))
			return true;

		return super.knows(vc, name);
	}

	public void set(final CallRuntime cr, final I_Mem vc, final I_Object obj, final boolean nilable, final boolean convert) {
		final String vcName = vc.getName();
//		if(obj == null)	// null will be checked in "getValue" and "super.set"
//			throw new JayMoError(cr, "No Object to set", vcName);

		if(vcName.equals(Var.IT)) {
			this.itValue = Lib_Convert.getValue(cr, obj);
			return;
		}
		super.set(cr, vc, obj, nilable, convert, vcName);
	}

}
