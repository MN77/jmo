/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.immute;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Output;
import org.jaymo_lang.util.Lib_Type;


/**
 * @author Michael Nitsche
 * @created 02.12.2019
 */
public class JMo_KeyValue extends A_Immutable {

	private I_Object      key, value;
	private ArgCallBuffer cKey = null, cValue = null;


	/**
	 * +KeyValue(Object key, Object value)
	 */
	public JMo_KeyValue(final Call cKey, final Call cValue) {
		this.cKey = new ArgCallBuffer(0, cKey);
		this.cValue = new ArgCallBuffer(1, cValue);
	}

	public JMo_KeyValue(final I_Object key, final I_Object value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public boolean equals(final Object other) {

		if(other instanceof JMo_KeyValue) {
			final JMo_KeyValue other2 = (JMo_KeyValue)other;
			return this.key.equals(other2.key) && this.value.equals(other2.value);
		}
		else
			return false;
	}

	@Override
	public boolean equalsLazy(final Object other) {
		return this.equals(other);
	}

	public I_Object getKey() {
		return this.key;
	}

	public I_Object getValue() {
		return this.value;
	}

	@Override
	public void init(final CallRuntime cr) {

		if(this.cKey != null) {
			final I_Object o = this.cKey.init(cr, this, I_Object.class);
			this.key = Lib_Convert.getValue(cr, o);
		}

		if(this.cValue != null) {
			final I_Object o = this.cValue.init(cr, this, I_Object.class);
			this.value = Lib_Convert.getValue(cr, o);
		}
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {

		switch(type) {
			case REGULAR:
			case NESTED:
				return this.key + " -> " + this.value.toString();
			case IDENT:
				return this.getTypeName();
			default:
				final StringBuilder sb = new StringBuilder();
				sb.append(Lib_Type.getName(this));
				sb.append("(");
				String sk = this.key.toString(cr, STYPE.DESCRIBE);
				sk = Lib_Output.indentLines(sk, false);
				sb.append(sk);

//				sb.append(" -> ");
				sb.append(",");

				String sv = this.value.toString(cr, STYPE.DESCRIBE);
				sv = Lib_Output.indentLines(sv, false);
				sb.append(sv);

				sb.append(")");
				return sb.toString();
		}
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "key":
				return A_Object.stdResult(this.mKey(cr));
			case "value":
				return A_Object.stdResult(this.mValue(cr));
		}
		return null;
	}

	/**
	 * °key()Object # Returns the Key
	 */
	private I_Object mKey(final CallRuntime cr) {
		cr.argsNone();
		return this.key;
	}

	/**
	 * °value()Object # Returns the Value
	 */
	private I_Object mValue(final CallRuntime cr) {
		cr.argsNone();
		return this.value;
	}

}
