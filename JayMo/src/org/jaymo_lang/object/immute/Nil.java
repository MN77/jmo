/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.immute;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_Chars;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.A_Number;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.I_Integer;
import org.jaymo_lang.object.atom.JMo_BigInt;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.NOP1;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_MagicVar;

import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 23.05.2018
 */
public class Nil extends A_Immutable {

	// Singleton
	public static final Nil     NIL   = new Nil();
	private static final String IDENT = "nil";


	public static I_Object arithmetic(final CallRuntime cr, final NOP1 op, final A_Number num) {

		if(op == NOP1.MUL) {
			if(!(num instanceof I_Integer))
				throw new CodeError(cr, "Invalid number for arithmetic Nil", "Only integer numbers are possible, but got: " + num.toString());
			if(num instanceof JMo_Long || num instanceof JMo_BigInt)
				throw new CodeError(cr, "Invalid number for arithmetic Nil", "Long and BigInt are not allowed here: " + num.toString());

			final int intValue = (int)num.getValue();

			final SimpleList<I_Object> al = new SimpleList<>(intValue);
			for(int i = 0; i < intValue; i++)
				al.add(Nil.NIL);
			return new JMo_List(al);
		}
		else if(op == NOP1.ADD)
			return num;
		else
			throw new CodeError(cr, "Invalid function for Nil", op.toString());
	}

	/**
	 * ! This atomic object is a placeholder, that there's no other object
	 * + NIL
	 * #+ Nil
	 * + nil
	 */
	private Nil() {}

	@Override
	public boolean equals(final Object other) {
		return other == Nil.NIL;
	}

	@Override
	public boolean equalsLazy(final Object other) {
		return this.equals(other);
	}

	@Override
	public void init(final CallRuntime cr) {}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return Nil.IDENT;
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "*":
			case "mul":
				return A_Object.stdResult(this.mMultiply(cr));
			case "+":
			case "add":
				return A_Object.stdResult(this.mAdd(cr));

			default:
				Lib_MagicVar.checkForbiddenFuncs(cr, method);
				return null;
		}
	}

	/**
	 * °+ ^ add
	 * °add(Atomic a)Atomic # Returns the given argument or creates a string.
	 */
	private I_Object mAdd(final CallRuntime cr) {
		final I_Object arg = cr.args(this, I_Atomic.class)[0];
		return arg instanceof A_Chars
			? new Str(Nil.IDENT + ((A_Chars)arg).getValue().toString())
			: arg;
	}

	/**
	 * °*^mul
	 * °mul(IntNumber num)List # Multiply 'nil', creates a List object with 'num' items of nil.
	 */
	private I_Object mMultiply(final CallRuntime cr) {
		final A_IntNumber arg = (A_IntNumber)cr.args(this, A_IntNumber.class)[0];
		return Nil.arithmetic(cr, NOP1.MUL, arg);
	}

}
