/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.immute;

import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.runtime.STYPE;


/**
 * @author Michael Nitsche
 * @created 04.04.2021
 *
 * @apiNote "Equals" (==) and "Same" (===) will always lead to the same result.
 * @implNote Extending Objects must be absolutly immutable
 */
public abstract class A_Immutable extends A_Object {

	@Override
	public abstract boolean equals(Object other);

	@Override
	public abstract boolean equalsLazy(final Object other);

	@Override
	public boolean equalsStrict(final Object other) {
		return this.equals(other);
	}

	@Override
	public final String toString() {
		return this.toString(null, STYPE.REGULAR);
	}

}
