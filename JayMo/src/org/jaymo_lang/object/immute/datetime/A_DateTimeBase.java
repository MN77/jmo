/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.immute.datetime;

import org.jaymo_lang.model.COMPARE;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.A_Number;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.type.datetime.I_Date;
import de.mn77.base.data.type.datetime.I_Time;


/**
 * @author Michael Nitsche
 * @created 2020-01-20
 */
public abstract class A_DateTimeBase extends A_Immutable {

	public enum COUNTRY {
		DE,
		EN,
		US
	}


	@Override
	public abstract boolean equals(Object arg);

	@Override
	protected final ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "+":
			case "add":
				return A_Object.stdResult(this.mAddSub(cr, true));
			case "-":
			case "sub":
				return A_Object.stdResult(this.mAddSub(cr, false));

			case "toMilliSeconds":
			case "toMilliSec":
			case "toMSec":
				return A_Object.stdResult(this.mToMilliSeconds(cr));

			case "style":
				return A_Object.stdResult(this.mStyle(cr));

			case "toStrDE":
			case "styleDE":
//			case "formDE":
//			case "formatDE":
				return A_Object.stdResult(this.mStyle(cr, COUNTRY.DE));
			case "toStrEN":
			case "styleEN":
//			case "formEN":
//			case "formatEN":
				return A_Object.stdResult(this.mStyle(cr, COUNTRY.EN));
			case "toStrUS":
			case "styleUS":
//			case "formUS":
//			case "formatUS":
				return A_Object.stdResult(this.mStyle(cr, COUNTRY.US));

			case "toStrFull":
				return A_Object.stdResult(this.mToStrFull(cr));

			case "++":
			case "inc":
				return A_Object.stdResult(this.mIncDec(cr, true));
			case "--":
			case "dec":
				return A_Object.stdResult(this.mIncDec(cr, false));

			case "limit":
				return A_Object.stdResult(this.mLimit(cr));

			//TODO evtl.: A_Object -> A_Compare -> A_Atomic
			case "isLess":
			case "<":
				return A_Object.stdResult(this.mCompare(cr, COMPARE.L));
			case "isGreater":
			case ">":
				return A_Object.stdResult(this.mCompare(cr, COMPARE.G));
			case "isLessOrEqual":
			case "<=":
				return A_Object.stdResult(this.mCompare(cr, COMPARE.LE));
			case "isGreaterOrEqual":
			case ">=":
				return A_Object.stdResult(this.mCompare(cr, COMPARE.GE));

			default:
				return this.call3(cr, method);
		}
	}

	protected abstract ObjectCallResult call3(final CallRuntime cr, final String method);

	protected abstract I_Date getInternalDate();

	protected abstract I_Time getInternalTime();

	protected abstract A_DateTimeBase incdec2(boolean inc, int amount);

	/**
	 * °+ ^ add
	 * °add(Number num)DateTimeBase # Create a copy with 'num' added.
	 * °- ^ sub
	 * °sub(Number num)DateTimeBase # Create a copy with 'num' subtracted.
	 */
	protected final I_Object mAddSub(final CallRuntime cr, final boolean add) {
		final A_Number num = (A_Number)cr.args(this, A_Number.class)[0];
		return this.pAddSub(cr, num, add);
	}

	protected abstract boolean[] mCompare2(final CallRuntime cr);

	/**
	 * °limit(DateTimeBase min, DateTimeBase max)DateTimeBase # Limits this Date, Time or DateTime to a upper and lower level.
	 */
	protected abstract A_DateTimeBase mLimit(final CallRuntime cr);


	/**
	 * °toStrDE ^ styleDE
	 * °styleDE()Str # Converts this date to a string with german notation (ISO 5008)
	 * °toStrEN ^ styleDE
	 * °styleEN()Str # Converts this date to a string with english notation
	 * °toStrUS ^ styleDE
	 * °styleUS()Str # Converts this date to a string with american notation
	 */
	protected abstract I_Object mStyle(CallRuntime cr, COUNTRY de);

	protected abstract I_Object pAddSub(final CallRuntime cr, A_Number arg, boolean add);

	protected abstract JMo_Long pToMilliSeconds();

	protected abstract String toStringFull();

	/**
	 * #°==(DateTimeBase other)Bool Is this equal to the other?
	 * #°!=(DateTimeBase other)Bool Is this different to the other?
	 * °< ^ isLess
	 * °isLess(DateTimeBase other)Bool Is this lesser than the other?
	 * °<= ^ isLessOrEqual
	 * °isLessOrEqual(DateTimeBase other)Bool Is this lesser or equal the other?
	 * °> ^ isGreater
	 * °isGreater(DateTimeBase other)Bool Is this greater than the other?
	 * °>= ^ isGreaterOrEqual
	 * °isGreaterOrEqual(DateTimeBase other)Bool Is this greater or equal the other?
	 */
	private Bool mCompare(final CallRuntime cr, final COMPARE co) {
		final boolean[] equal_greater = this.mCompare2(cr);
		final boolean isEqual = equal_greater[0];
		final boolean isGreater = equal_greater[1];
		boolean result = false;

		switch(co) {
			case L:
				result = !isEqual && !isGreater;
				break;
			case LE:
				result = isEqual || !isGreater;
				break;
			case G:
				result = isGreater;
				break;
			case GE:
				result = isEqual || isGreater;
				break;
		}
		return Bool.getObject(result);
	}

	/**
	 * °++ ^ inc
	 * °inc()DateTimeBase # Creates a new Object for the next day.
	 * °inc(IntNumber n)DateTimeBase # Creates a new Object with n days ahead.
	 * °-- ^ dec
	 * °dec()DateTimeBase # Creates a new Object for the prior day.
	 * °dec(IntNumber n)DateTimeBase # Creates a new Date-Object with n days prior.
	 */
	private A_DateTimeBase mIncDec(final CallRuntime cr, final boolean inc) {
		final I_Object[] args = cr.argsFlex(this, 0, 1);
		int amount = 1;
		if(args.length == 1)
			amount = Lib_Convert.getIntValue(cr, cr.argType(args[0], A_IntNumber.class));

		return this.incdec2(inc, amount);
	}

	/**
	 * #°form ^ format
	 * °style(Str style)Str # Converts this datetime like 'style' to an String
	 */
	private Str mStyle(final CallRuntime cr) {
		final String s = ((Str)cr.args(this, Str.class)[0]).getValue();
		return Lib_DateTime_Format.format(cr, s, this.getInternalDate(), this.getInternalTime());
	}

	/**
	 * °toMSec ^ toMilliSeconds
	 * °toMilliSec ^ toMilliSeconds
	 * °toMilliSeconds()Long # Converts the object to a representing milliseconds value.
	 */
	private JMo_Long mToMilliSeconds(final CallRuntime cr) {
		cr.argsNone();
		return this.pToMilliSeconds();
	}

	/**
	 * °toStrFull()Str # Returns the string representation including milliseconds.
	 */
	private I_Object mToStrFull(final CallRuntime cr) {
		cr.argsNone();
		return new Str(this.toStringFull());
	}

}
