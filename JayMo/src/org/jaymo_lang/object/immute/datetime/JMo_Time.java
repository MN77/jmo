/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.immute.datetime;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.A_Number;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.convert.ConvertSequence;
import de.mn77.base.data.type.datetime.I_Date;
import de.mn77.base.data.type.datetime.I_DateTime;
import de.mn77.base.data.type.datetime.I_Time;
import de.mn77.base.data.type.datetime.MDateTime;
import de.mn77.base.data.type.datetime.MTime;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 2019-11-07
 * @apiNote The base is: "second"
 */
public class JMo_Time extends A_DateTimeBase {

	private I_Time                time = null;
	private final ArgCallBuffer[] init;


	public JMo_Time() {
		this.init = null;
	}

	public JMo_Time(final Call arg) {
		this.init = new ArgCallBuffer[]{new ArgCallBuffer(0, arg)};
	}

	public JMo_Time(final Call h, final Call m) {
		this.init = new ArgCallBuffer[]{new ArgCallBuffer(0, h), new ArgCallBuffer(1, m)};
	}

	public JMo_Time(final Call h, final Call m, final Call s) {
		this.init = new ArgCallBuffer[]{new ArgCallBuffer(0, h), new ArgCallBuffer(1, m), new ArgCallBuffer(2, s)};
	}

	public JMo_Time(final Call h, final Call m, final Call s, final Call ms) {
		this.init = new ArgCallBuffer[]{new ArgCallBuffer(0, h), new ArgCallBuffer(1, m), new ArgCallBuffer(2, s), new ArgCallBuffer(3, ms)};
	}

	/**
	 * +Time() # Time
	 * +Time(String text) # Time("12:34:56")
	 * +Time(Long millisec) # Time(45296789)
	 * +Time(IntNumber hours, IntNumber minutes) # Time(12,34)
	 * +Time(IntNumber hours, IntNumber minutes, IntNumber seconds) # Time(12,34,56)
	 * +Time(IntNumber hours, IntNumber minutes, IntNumber seconds, IntNumber millisec) # Time(12,34,56,789)
	 */
	public JMo_Time(final I_Time time) {
		this.init = null;
		this.time = time;
	}

	@Override
	public boolean equals(final Object arg) {
		return arg instanceof JMo_Time
			? this.time.isEqual(((JMo_Time)arg).getInternalTime())
			: false;
	}

	@Override
	public boolean equalsLazy(final Object other) {

		if(other instanceof Str) {
			final String s = ((Str)other).getValue();

			switch(s.length()) {
				case 8:
					return s.equals(this.time.toStringShort());
				case 12:
					return s.equals(this.time.toString());
				default:
					return false;
			}
		}
		return this.equals(other);
	}

	@Override
	public void init(final CallRuntime cr) {
		if(this.time != null)
			return;

		if(this.init == null) {
			this.time = new MTime();
			return;
		}
		String errPars = "";
		final int initLen = this.init.length;

		try {

			if(initLen == 1) {
				final I_Object o = this.init[0].initExt(cr, this, A_IntNumber.class, Str.class);

//				if(o instanceof A_IntNumber) {
//					final int ih = Lib_Convert.getIntValue(cr, o);
//					errPars = "" + ih;
//					this.time = new MTime(ih, 0, 0);
//				}
				if(o instanceof A_IntNumber) {
					final int ms = Lib_Convert.getIntValue(cr, o);
					errPars = "" + ms;
					this.time = new MTime(ms);
				}
//				else if(o instanceof A_DecNumber) {
//					final double v = Lib_Convert.getDoubleValue(cr, o);
//					errPars = "" + v;
//					this.time = new MTime((int)(v*1000));
//				}
				else {
					final String s = Lib_Convert.getStringValue(cr, o);
					errPars = s;

					try {
						this.time = new MTime(s);
					}
					catch(final Err_Runtime err) {
						throw new RuntimeError(cr, "Invalid argument for <Time>", "Parse error! Got: Time(\"" + s + "\")");
					}
				}
			}
			else if(initLen == 2) {
				final int ih = Lib_Convert.getIntValue(cr, this.init[0].init(cr, this, A_IntNumber.class));
				final int im = Lib_Convert.getIntValue(cr, this.init[1].init(cr, this, A_IntNumber.class));
				errPars = ih + "," + im;
				this.time = new MTime(ih, im, 0);
			}
			else if(initLen == 3) {
				final int ih = Lib_Convert.getIntValue(cr, this.init[0].init(cr, this, A_IntNumber.class));
				final int im = Lib_Convert.getIntValue(cr, this.init[1].init(cr, this, A_IntNumber.class));
				final int is = Lib_Convert.getIntValue(cr, this.init[2].init(cr, this, A_IntNumber.class));
				errPars = ih + "," + im + "," + is;
				this.time = new MTime(ih, im, is);
			}
			else { // 4
				final int ih = Lib_Convert.getIntValue(cr, this.init[0].init(cr, this, A_IntNumber.class));
				final int im = Lib_Convert.getIntValue(cr, this.init[1].init(cr, this, A_IntNumber.class));
				final int is = Lib_Convert.getIntValue(cr, this.init[2].init(cr, this, A_IntNumber.class));
				final int id = Lib_Convert.getIntValue(cr, this.init[3].init(cr, this, A_IntNumber.class));
				errPars = ih + "," + im + "," + is + "," + id;
				this.time = new MTime(ih, im, is, id);
			}
		}
		catch(final Err_Runtime err) {
			throw new RuntimeError(cr, "Invalid time: " + ConvertSequence.toString(", ", err.getDetails()), "Time(" + errPars + ")");
		}
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		if(this.time == null)
			return Lib_Type.getName(this);

		switch(type) {
			case REGULAR:
			case NESTED:
				return this.time.toStringShort();
			default:
				return Lib_Type.getName(this) + "(\"" + this.time.toString() + "\")";
		}
	}

	@Override
	public String toStringFull() {
		if(this.time == null)
			return Lib_Type.getName(this);
		return this.time.toString();
	}

	@Override
	protected ObjectCallResult call3(final CallRuntime cr, final String method) {

		switch(method) {
			case "getHour":
			case "getHours":
				return A_Object.stdResult(this.mGetHours(cr));
			case "getMin":
			case "getMinutes":
				return A_Object.stdResult(this.mGetMinutes(cr));
			case "getSec":
			case "getSeconds":
				return A_Object.stdResult(this.mGetSeconds(cr));
			case "getMSec":
			case "getMilliSeconds":
				return A_Object.stdResult(this.mGetMilliSeconds(cr));

			case "addMSec":
			case "addMilliSeconds":
				return A_Object.stdResult(this.mAddSubMilliSeconds(cr, true));
			case "addSec":
			case "addSeconds":
				return A_Object.stdResult(this.mAddSubSeconds(cr, true));
			case "addMin":
			case "addMinutes":
				return A_Object.stdResult(this.mAddSubMinutes(cr, true));
			case "addHours":
				return A_Object.stdResult(this.mAddSubHours(cr, true));
			case "addTime":
				return A_Object.stdResult(this.mAddSubTime(cr, true));

			case "subMSec":
			case "subMilliSeconds":
				return A_Object.stdResult(this.mAddSubMilliSeconds(cr, false));
			case "subSec":
			case "subSeconds":
				return A_Object.stdResult(this.mAddSubSeconds(cr, false));
			case "subMin":
			case "subMinutes":
				return A_Object.stdResult(this.mAddSubMinutes(cr, false));
			case "subHours":
				return A_Object.stdResult(this.mAddSubHours(cr, false));
			case "subTime":
				return A_Object.stdResult(this.mAddSubTime(cr, false));

			case "setHour":
			case "setHours":
				return A_Object.stdResult(this.mSetHour(cr));
			case "setMinute":
			case "setMinutes":
				return A_Object.stdResult(this.mSetMinute(cr));
			case "setSecond":
			case "setSeconds":
				return A_Object.stdResult(this.mSetSecond(cr));
			case "setMSec":
			case "setMilliSeconds":
				return A_Object.stdResult(this.mSetMilliSeconds(cr));

			case "combine":
				return A_Object.stdResult(this.mCombine(cr));

			case "diffHours":
				return A_Object.stdResult(this.mDiffHours(cr));
			case "diffMinutes":
				return A_Object.stdResult(this.mDiffMinutes(cr));
			case "diff":
			case "diffSeconds":
				return A_Object.stdResult(this.mDiffSeconds(cr));

			default:
				return null;
		}
	}

	@Override
	protected I_Date getInternalDate() {
		return null;
	}

	@Override
	protected I_Time getInternalTime() {
		return this.time;
	}

	/**
	 * °inc()Date # Creates a new Date-Object for the next day
	 * °inc(IntNumber n)Date # Creates a new Date-Object with n days ahead
	 * °dec()Date # Creates a new Date-Object for the prior day
	 * °dec(IntNumber n)Date # Creates a new Date-Object with n days prior
	 *
	 * °++()Date # Creates a new Date-Object for the next day
	 * °++(IntNumber n)Date # Creates a new Date-Object with n days ahead
	 * °--()Date # Creates a new Date-Object for the prior day
	 * °--(IntNumber n)Date # Creates a new Date-Object with n days prior
	 */
	@Override
	protected JMo_Time incdec2(final boolean inc, final int amount) {

		if(inc) {
			final I_Time date2 = this.time.getAddSeconds(amount);
			return new JMo_Time(date2);
		}
		else {
			final I_Time date2 = this.time.getAddSeconds(-amount);
			return new JMo_Time(date2);
		}
	}

	/**
	 * °<(Time other)Bool Is this time lesser than the other?
	 * °<=(Time other)Bool Is this time lesser or equal the other?
	 * °>(Time other)Bool Is this time greater than the other?
	 * °>=(Time other)Bool Is this time greater or equal the other?
	 */
	@Override
	protected boolean[] mCompare2(final CallRuntime cr) {
		final JMo_Time other = (JMo_Time)cr.args(this, JMo_Time.class)[0];
		final boolean isEqual = this.time.isEqual(other.getInternalValue());
		final boolean isGreater = this.time.isGreater(other.getInternalValue());
		return new boolean[]{isEqual, isGreater};
	}

	@Override
	protected JMo_Time mLimit(final CallRuntime cr) {
		final I_Object[] args = cr.args(this, JMo_Time.class, JMo_Time.class);
		final I_Time min = ((JMo_Time)args[0]).time;
		final I_Time max = ((JMo_Time)args[1]).time;

		if(this.time.isGreater(max))
			return (JMo_Time)args[1];
		if(min.isGreater(this.time))
			return (JMo_Time)args[0];
		return this;
	}

	@Override
	protected I_Object mStyle(final CallRuntime cr, final COUNTRY country) {
		cr.argsNone();

		switch(country) {
			case DE:
				return Lib_DateTime_Format.format(cr, "hh:mm:ss", this.getInternalDate(), this.getInternalTime());
			case EN:
				return Lib_DateTime_Format.format(cr, "hh:mm:ss", this.getInternalDate(), this.getInternalTime());
			case US:
				return Lib_DateTime_Format.format(cr, "i:mm:ss p", this.getInternalDate(), this.getInternalTime());
			default:
				throw Err.impossible(country);
		}
	}

	@Override
	protected I_Object pAddSub(final CallRuntime cr, final A_Number arg, final boolean add) {
		double d = Lib_Convert.getDoubleValue(cr, arg);
		if(!add)
			d = -d;
		final int ms = (int)(d * 1000);
		final I_Time date2 = this.time.getAddMilliSeconds(ms);
		return new JMo_Time(date2);
	}

	@Override
	protected JMo_Long pToMilliSeconds() {
		return new JMo_Long(this.time.getValueMilliSeconds());
	}

	private I_Time getInternalValue() {
		return this.time;
	}

	/**
	 * °addHour ^ addHours
	 * °addHours(IntNumber hours)Time # Return a new Time with hours added
	 * °subHour ^ subHours
	 * °subHours(IntNumber hours)Time # Return a new time with hours subtracted
	 */
	private JMo_Time mAddSubHours(final CallRuntime cr, final boolean add) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		int pari = Lib_Convert.getIntValue(cr, arg);
		if(!add)
			pari = -pari;
		final I_Time date2 = this.time.getAddHours(pari);
		return new JMo_Time(date2);
	}

	/**
	 * °addMSec ^ addMilliSeconds
	 * °addMilliSeconds(IntNumber msec)Time # Return a new Time with milliseconds added
	 * °subMSec ^ subMilliSeconds
	 * °subMilliSeconds(IntNumber msec)Time # Return a new time with milliseconds subtracted
	 */
	private JMo_Time mAddSubMilliSeconds(final CallRuntime cr, final boolean add) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		int pari = Lib_Convert.getIntValue(cr, arg);
		if(!add)
			pari = -pari;
		final I_Time date2 = this.time.getAddMilliSeconds(pari);
		return new JMo_Time(date2);
	}

	/**
	 * °addMin ^ addMinutes
	 * °addMinutes(IntNumber minutes)Time # Return a new Time with minutes added
	 * °subMin ^ subMinutes
	 * °subMinutes(IntNumber minutes)Time # Return a new time with minutes subtracted
	 */
	private JMo_Time mAddSubMinutes(final CallRuntime cr, final boolean add) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		int pari = Lib_Convert.getIntValue(cr, arg);
		if(!add)
			pari = -pari;
		final I_Time date2 = this.time.getAddMinutes(pari);
		return new JMo_Time(date2);
	}

	/**
	 * °addSec ^ addSeconds
	 * °addSeconds(IntNumber num)Time # Return a new Time with seconds added
	 * °subSec ^ subSeconds
	 * °subSeconds(IntNumber seconds)Time # Return a new Time with seconds subtracted
	 */
	private I_Object mAddSubSeconds(final CallRuntime cr, final boolean add) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		int pari = Lib_Convert.getIntValue(cr, arg);
		if(!add)
			pari = -pari;
		final I_Time date2 = this.time.getAddSeconds(pari);
		return new JMo_Time(date2);
	}

	/**
	 * °addTime(Time t)Time # Return a new Time with hours, minutes and seconds added
	 * °subTime(Time t)Time # Return a new Time with hours, minutes and seconds subtracted
	 */
	private I_Object mAddSubTime(final CallRuntime cr, final boolean add) {
		final I_Object arg = cr.args(this, JMo_Time.class)[0];
		final I_Time part = ((JMo_Time)arg).getInternalTime();

		I_Time temp = this.time;
		temp = temp.getAddHours(add ? part.getHours() : -part.getHours());
		temp = temp.getAddMinutes(add ? part.getMinutes() : -part.getMinutes());
		temp = temp.getAddSeconds(add ? part.getSeconds() : -part.getSeconds());
		temp = temp.getAddMilliSeconds(add ? part.getMilliSeconds() : -part.getMilliSeconds());
		return new JMo_Time(temp);

//		if(arg instanceof JMo_DateTime) {
//			I_DateTime temp = ((JMo_DateTime)arg).getInternalDateTime();
//			temp = temp.getAddHours(this.time.getHours());
//			temp = temp.getAddMinutes(this.time.getMinutes());
//			temp = temp.getAddSeconds(this.time.getSeconds());
//			return new JMo_DateTime(temp);
//		}
	}

	/**
	 * °combine(Date date)DateTime # Combine date and time to a new DateTime
	 */
	private I_Object mCombine(final CallRuntime cr) {
		final I_Object arg = cr.args(this, JMo_Date.class)[0];
		final I_DateTime dt = new MDateTime(((JMo_Date)arg).getInternalDate(), this.time);
		return new JMo_DateTime(dt);
	}

	/**
	 * °diffHours(Time other)Int Gets the difference of hours to another time
	 */
	private Int mDiffHours(final CallRuntime cr) {
		final JMo_Time arg = (JMo_Time)cr.args(this, JMo_Time.class)[0];
		final int diff = this.time.diffHours(arg.getInternalValue());
		return new Int(diff);
	}

	/**
	 * °diffMinutes(Time other)Int Gets the difference of minutes to another time
	 */
	private Int mDiffMinutes(final CallRuntime cr) {
		final JMo_Time arg = (JMo_Time)cr.args(this, JMo_Time.class)[0];
		final int diff = this.time.diffMinutes(arg.getInternalValue());
		return new Int(diff);
	}

	/**
	 * °diff ^ diffSeconds
	 * °diffSeconds(Time other)Int Gets the difference of seconds to another time
	 */
	private Int mDiffSeconds(final CallRuntime cr) {
		final JMo_Time arg = (JMo_Time)cr.args(this, JMo_Time.class)[0];
		final int diff = this.time.diffSeconds(arg.getInternalValue());
		return new Int(diff);
	}

	/**
	 * °getHour ^ getHours
	 * °getHours()Int Gets the hours of this time
	 */
	private Int mGetHours(final CallRuntime cr) {
		cr.argsNone();
		return new Int(this.time.getHours());
	}

	/**
	 * °getMSec ^ getMilliSeconds
	 * °getMilliSeconds()Int # Gets the milliseconds of this time
	 */
	private Int mGetMilliSeconds(final CallRuntime cr) {
		cr.argsNone();
		return new Int(this.time.getMilliSeconds());
	}

	/**
	 * °getMin ^ getMinutes
	 * °getMinutes()Int # Gets the minutes of this time
	 */
	private Int mGetMinutes(final CallRuntime cr) {
		cr.argsNone();
		return new Int(this.time.getMinutes());
	}

	/**
	 * °getSec ^ getSeconds
	 * °getSeconds()Int # Gets the seconds of this time
	 */
	private Int mGetSeconds(final CallRuntime cr) {
		cr.argsNone();
		return new Int(this.time.getSeconds());
	}

	/**
	 * °setHour ^ setHours
	 * °setHours(IntNumber hour)Time # Return a new Time-Object with changed hour
	 */
	private JMo_Time mSetHour(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int pari = Lib_Convert.getIntValue(cr, arg);
		final I_Time time2 = this.time.getSetHour(pari);
		return new JMo_Time(time2);
	}

	/**
	 * °setMSec ^ setMilliSeconds
	 * °setMilliSeconds(IntNumber msec)Time # Return a new Time-Object with changed milliseconds
	 */
	private JMo_Time mSetMilliSeconds(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int pari = Lib_Convert.getIntValue(cr, arg);
		final I_Time time2 = this.time.getSetMilliSecond(pari);
		return new JMo_Time(time2);
	}

	/**
	 * °setMinute ^ setMinutes
	 * °setMinutes(IntNumber minute)Time # Return a new Time-Object with changed minute
	 */
	private JMo_Time mSetMinute(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int pari = Lib_Convert.getIntValue(cr, arg);
		final I_Time time2 = this.time.getSetMinute(pari);
		return new JMo_Time(time2);
	}

	/**
	 * °setSecond ^ setSeconds
	 * °setSeconds(IntNumber second)Time # Return a new Time-Object with changed second
	 */
	private JMo_Time mSetSecond(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int pari = Lib_Convert.getIntValue(cr, arg);
		final I_Time time2 = this.time.getSetSecond(pari);
		return new JMo_Time(time2);
	}

}
