/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;


/**
 * @author Michael Nitsche
 * @since 28.07.2019
 */
public class JMo_RegEx extends A_Object {

	private String value     = null;
	private Call   initValue = null;


	/**
	 * +RegEx(String string) # RegEx("[a-z]+")
	 * +\"\"?? # "[a-z]+"??
	 */
	public JMo_RegEx(final Call c) {
		this.initValue = c;
	}

	public JMo_RegEx(final String s) {
		this.value = s;
	}

	public String getValue() {
		return this.value;
	}

	@Override
	public void init(final CallRuntime cr) {

		if(this.initValue != null) {
			final I_Object o = cr.execInit(this.initValue, this);
			this.value = ((Str)cr.argType(o, Str.class)).getValue();
			this.initValue = null;
		}
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return type == STYPE.NESTED
			? this.getTypeName()
			: this.getTypeName() + "(\"" + this.value + "\")";
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "match":
				return A_Object.stdResult(this.mMatch(cr));
//			case "filter": result = getBool(inst, c); break;
			case "replace":
				return A_Object.stdResult(this.mReplace(cr));

			default:
				return null;
		}
	}

	/**
	 * °match(Str s)Bool # Returns true if the given string (s) matches this regular expression.
	 */
	private Bool mMatch(final CallRuntime cr) {
		final I_Object arg = cr.args(this, Str.class)[0];
		final String s = Lib_Convert.getStringValue(cr, arg);
		return Bool.getObject(s.matches(this.value));
	}

	/**
	 * °replace(Str in, Atomic with)Str # Replaces all finds in the String
	 */
	private Str mReplace(final CallRuntime cr) {
		final I_Object[] args = cr.args(this, Str.class, I_Atomic.class);
		final String text = Lib_Convert.getStringValue(cr, args[0]);
		final String with = Lib_Convert.getStringValue(cr, args[1]);

		final String result = text.replaceAll(this.value, with);
		return new Str(result);
	}

}
