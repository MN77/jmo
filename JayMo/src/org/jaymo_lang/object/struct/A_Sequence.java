/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.struct;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.data.struct.I_Series;
import de.mn77.base.data.struct.SimpleList;


/**
 * @author Michael Nitsche
 * @created 13.04.2020
 */
public abstract class A_Sequence extends A_Object implements I_DeepGetSet { // implements I_Auto Block Do

	public static final String CYCLE_IDENT = "self";


	@Override
	public abstract boolean equals(final Object obj);

	@Override
	public abstract boolean equalsLazy(final Object obj);


	public abstract I_Series<? extends I_Object> getInternalCollection();

	@Override
	public abstract String toString();

	@Override
	public abstract String toString(CallRuntime cr, final STYPE type);

	@Override
	protected final ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "copy":
				cr.argsNone();
				return A_Object.stdResult(this.copy(cr));

			case "first":
				cr.argsNone();
				return A_Object.stdResult(this.getFirst(cr));
			case "last":
				cr.argsNone();
				return A_Object.stdResult(this.getLast(cr));
//			case "length":
//			case "size":
//			case "getLen":
//			case "getSize":
			case "len":
			case "length":
				return A_Object.stdResult(this.mLength(cr));

			case "get":
				return A_Object.stdResult(this.mSequenceGetPull(cr, false));
//			case "getLazy":		// ja, aber ergibt auch: .lazy
			case "pull":
				return A_Object.stdResult(this.mSequenceGetPull(cr, true));
			case "set":
				this.mSequenceSetPut(cr, false);
				return A_Object.stdResult(this);
			case "put":
//			case "setLazy":		// ja, aber ergibt auch: .lazy =
				this.mSequenceSetPut(cr, true);
				return A_Object.stdResult(this);
			case "content":
				return A_Object.stdResult(this.mContent(cr));

			case "select":
				return A_Object.stdResult(this.mSequenceSelect(cr, false));
			case "selectLazy":
				return A_Object.stdResult(this.mSequenceSelect(cr, true));
			case "update":
				this.mSequenceUpdate(cr, false);
				return A_Object.stdResult(this);
			case "updateLazy":
				this.mSequenceUpdate(cr, true);
				return A_Object.stdResult(this);

			case "isEmpty":
				return A_Object.stdResult(this.mIsEmpty(cr));

			//TODO:
//			case "each"

			default:
				return this.call3(cr, method);
		}
	}

	protected abstract ObjectCallResult call3(CallRuntime cr, String method);

	/**
	 * °copy()Sequence # Create a copy of this Sequence.
	 */
	protected abstract A_Sequence copy(CallRuntime cr);

	/**
	 * °first()Object # Returns the first Object.
	 */
	protected abstract I_Object getFirst(CallRuntime cr);

	/**
	 * °last()Object # Returns the last Object.
	 */
	protected abstract I_Object getLast(CallRuntime cr);

	/**
	 * °get(Object pos)Object # Return the object at this position or associated with this key. If the position/key is unknown, an error will be thrown.
	 * °pull(Object pos)Object # If the position or key exists, return the value. Otherwise the result is 'nil'.
	 */
	protected final I_Object mSequenceGetPull(final CallRuntime cr, final boolean lazy) {
		final I_Object[] oa = cr.argsVar(this, 1, 0);

		return oa.length == 1
			? this.sequenceGetPull(cr, lazy, oa[0])
			: this.sequenceDeepGet(cr, oa, 0, lazy);
	}

	/**
	 * °set(value, pos...)Same # Set value to position/key.
	 * °put(value, pos...)Same # Add or set value to position/key.
	 */
	protected abstract void mSequenceSetPut(CallRuntime cr, boolean lazy);

	protected abstract void sequenceContent(CallRuntime cr, I_Object[] values);

	protected abstract boolean sequenceEmpty();

	protected abstract I_Object sequenceGetPull(final CallRuntime cr, final boolean lazy, I_Object arg);

	protected abstract I_Object sequenceSelectGet(CallRuntime cr, I_Object key, boolean lazy);

	protected abstract void sequenceSet(CallRuntime cr, I_Object pos, I_Object obj, boolean lazy);

	protected abstract int sequenceSize();

	/**
	 * °content(values ...)Same # Replace the content of this Sequence with 'values'.
	 */
	private A_Sequence mContent(final CallRuntime cr) {
		final int len = this.sequenceSize();
		final I_Object[] args = cr.argsFlex(this, len, len);
		this.sequenceContent(cr, args);
		return this;
	}

	/**
	 * °isEmpty()Bool # Returns true if the sequence is empty, otherwise false.
	 */
	private Bool mIsEmpty(final CallRuntime cr) {
		cr.argsNone();
		return Bool.getObject(this.sequenceEmpty());
	}

	/**
	 * °len^length
	 * °length()Int # Returns the length of this sequence.
	 */
	private Int mLength(final CallRuntime cr) {
		cr.argsNone();
		return new Int(this.sequenceSize());
	}

	/**
	 * °select( Atomic pos... )List # Get a new List with selected items. Use 'nil' as placeholder.
	 * °selectLazy( Atomic pos... )List # Get a new List with selected items. Use 'nil' as placeholder. Missing values will be 'nil'.
	 */
	private JMo_List mSequenceSelect(final CallRuntime cr, final boolean lazy) {
		final I_Object[] oa = cr.argsVar(this, 0, 0);
		final SimpleList<I_Object> result = new SimpleList<>(oa.length);
		for(final I_Object key : oa)
			if(key == Nil.NIL)
				result.add(Nil.NIL);
			else {
				final I_Atomic key2 = (I_Atomic)cr.argType(key, I_Atomic.class);
				result.add(this.sequenceSelectGet(cr, key2, lazy));
			}
		return new JMo_List(result);
	}

	/**
	 * °update( Object value, Atomic positions... )Same # Set 'value' to all positions.
	 * °updateLazy( Object value, Atomic positions... )Same # Put 'value' to all positions.
	 */
	private final void mSequenceUpdate(final CallRuntime cr, final boolean lazy) {
		final I_Object[] oa = cr.argsVar(this, 1, 1);
		final I_Object obj = cr.argType(oa[0], null);

		for(int i = 1; i < oa.length; i++) {
			final I_Object pos = oa[i];
			this.sequenceSet(cr, pos, obj, lazy);
		}
	}

}
