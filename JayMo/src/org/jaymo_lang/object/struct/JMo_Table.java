/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.struct;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.I_AutoBlockList;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.JMo_RegEx;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.atom.A_Atomic;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.magic.con.MagicPosition;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Exec;
import org.jaymo_lang.util.Lib_Sequence;
import org.jaymo_lang.util.Lib_Table;
import org.jaymo_lang.util.Lib_TableStyle;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.constant.position.POSITION;
import de.mn77.base.data.constant.position.POSITION_H;
import de.mn77.base.data.convert.ConvertArray;
import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.struct.I_Series;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.util.Lib_Array;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 28.03.2018
 *
 * @implNote This Table is Row/Column-Based (YX)
 */
public class JMo_Table extends A_Sequence implements I_AutoBlockDo, I_AutoBlockList {

	private ArrayTable<I_Object> tab;
	private final ArgCallBuffer  width, rows, fill;
	private I_Object[]           titles = null;


	public JMo_Table(final ArrayTable<I_Object> init_tab) {
		this.tab = init_tab;
		this.width = null;
		this.rows = null;
		this.fill = null;
	}

	/**
	 * !A table of item
	 * +Table(Int columns)
	 * +Table(Int columns, Int rows, Object fill)
	 * %.each
	 *
	 * // Table is y,x ... here is x,y !!!
	 */
	public JMo_Table(final Call width) {
		this.width = new ArgCallBuffer(0, width);
		this.rows = null;
		this.fill = null;
	}

	public JMo_Table(final Call width, final Call rows, final Call fill) {
		this.width = new ArgCallBuffer(0, width);
		this.rows = new ArgCallBuffer(1, rows);
		this.fill = new ArgCallBuffer(2, fill);
	}

	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.mEach(cr, false).obj;
	}

	@Override
	public SimpleList<I_Object> autoBlockToList(final CallRuntime cr) {
		final SimpleList<I_Object> result = new SimpleList<>(this.tab.size());

		for(final I_Object[] tabRow : this.tab) {
			final SimpleList<I_Object> rowList = ConvertArray.toSimpleList(tabRow);
			result.add(new JMo_List(rowList));
		}
		return result;
	}

	@Override
	public boolean equals(final Object obj) {

		if(obj instanceof JMo_Table) {
			// Table
			final ArrayTable<I_Object> other = ((JMo_Table)obj).tab;
			if(this.tab.size() != other.size() || this.tab.width() != other.width())
				return false;
			for(int y = 0; y < this.tab.size(); y++)
				for(int x = 0; x < this.tab.width(); x++)
					if(!this.tab.get(x, y).equals(other.get(x, y)))
						return false;

			//Titles
			final I_Object[] otherTitles = ((JMo_Table)obj).titles;
			if(this.titles == null)
				return otherTitles == null;
			if(otherTitles == null || this.titles.length != otherTitles.length)
				return false;
			for(int i = 0; i < this.titles.length; i++)
				if(!this.titles[i].equals(otherTitles[i]))
					return false;

			return true;
		}
		else
			return false;
	}

	@Override
	public boolean equalsLazy(final Object obj) {
		return this.equals(obj);
	}

	@Override
	public I_Series<? extends I_Object> getInternalCollection() {
		throw Err.impossible();
//		SimpleList<I_Object> al = new SimpleList<>();
//		this.tab.forEach( e -> {
//			SimpleList<I_Object> row = new SimpleList<>();
//			e.forEach(ri -> row.add(ri));
//			al.add( new JMo_List(row) );
//		});
//		return al;
	}

	public I_Object[] getInternalColumnNames() {
		return this.titles;
	}

	public ArrayTable<I_Object> getInternalObject() {
		return this.tab;
	}

	public I_Object[] getTitles() {
		return this.titles;
	}

	@Override
	public void init(final CallRuntime cr) {

		if(this.width != null) {
			this.width.init(cr, this, A_IntNumber.class);

			if(this.tab != null)
				Err.invalid(this.tab);

			final int w = Lib_Convert.getIntValue(cr, this.width.get());
			if(w < 1)
				throw new RuntimeError(cr, "Invalid table width", "Minimum amount of columns is 1, but got " + w);
			this.tab = new ArrayTable<>(w);

			if(this.rows != null) {
				final int r = Lib_Convert.getIntValue(cr, this.rows.init(cr, this, A_IntNumber.class));
				final I_Object fill = this.fill.init(cr, this, null);

				for(int ir = 0; ir < r; ir++) {
					final I_Object[] row = new I_Object[w];
					for(int iw = 0; iw < w; iw++)
						row[iw] = fill;
					this.tab.add(row);
				}
			}
		}
	}

	@Override
	public I_Object sequenceDeepGet(final CallRuntime cr, final I_Object[] keys, final int offset, final boolean lazy) {
//		MOut.exit(keys, "!!!");
		final A_IntNumber oi = (A_IntNumber)cr.argType(keys[offset], A_IntNumber.class);
		final int i = Lib_Convert.getIntValue(cr, oi);
		final int pos = Lib_Sequence.realPosition(cr, i, this.tab.size(), lazy);
		if(pos == -1 || pos > this.tab.size())
			if(lazy)
				return Nil.NIL;
			else
				Lib_Error.ifNotBetween(cr, 1, this.tab.size(), pos, "position in list");

		if(offset == keys.length - 1) {
			final I_Object[] it = this.tab.get(pos - 1);
//			MOut.temp(keys, oi, i, pos, it);
			return this.iToList(it);
		}
		else {
			final I_Object[] it = this.tab.get(pos - 1);
			final I_Object deeper = this.iToList(it);
			if(!(deeper instanceof A_Sequence))
				throw new RuntimeError(cr, "Invalid type of item.", "Item " + (offset + 1) + " isn't a sequence, so I can't go into it!");
			final A_Sequence deeper2 = (A_Sequence)deeper;
			return deeper2.sequenceDeepGet(cr, keys, offset + 1, lazy);
		}
	}

	@Override
	public void sequenceDeepSet(final CallRuntime cr, final I_Object[] keys, final int offset, final I_Object value, final boolean lazy) {
		final A_IntNumber oiy = (A_IntNumber)cr.argType(keys[offset], A_IntNumber.class);
		final int wantedY = Lib_Convert.getIntValue(cr, oiy);
		int posY = Lib_Sequence.realPosition(cr, wantedY, this.tab.size(), lazy);

		if(lazy)
			while((posY = Lib_Sequence.realPosition(cr, wantedY, this.tab.size(), lazy)) == -1) {
				final I_Object[] emptyRow = new I_Object[this.tab.width()];
				for(int i = 0; i < this.tab.width(); i++)
					emptyRow[i] = Nil.NIL;
				this.tab.add(emptyRow);
			}

		Lib_Error.ifNotBetween(cr, 1, this.tab.size(), posY, "row");

		if(keys.length - offset == 1) {
			if(!Lib_Type.typeIs(cr, value, JMo_List.class))
				throw new RuntimeError(cr, "Invalid type of value", "Value must be a List");
			final JMo_List valueList = (JMo_List)value;
			final SimpleList<I_Object> valueArrayList = valueList.getInternalCollection();
			Lib_Error.ifNot(cr, this.tab.width(), valueArrayList.size(), "row-size");

			for(int col = 0; col < this.tab.width(); col++)
				this.tab.set(col, posY - 1, valueArrayList.get(col));
			return;
//			throw new ExecError(cr, "Two or more keys needed for table!", "Keys got: "+ keysHere);
		}
		final A_IntNumber oix = (A_IntNumber)cr.argType(keys[offset + 1], A_IntNumber.class);
		final int wantedX = Lib_Convert.getIntValue(cr, oix);
		final int posX = Lib_Sequence.realPosition(cr, wantedX, this.tab.size(), lazy);

		Lib_Error.ifNotBetween(cr, 1, this.tab.width(), wantedX, "column");

		if(offset == keys.length - 2)
			this.tab.set(posX - 1, posY - 1, value);
		else {
			I_Object deeper = this.tab.get(posX - 1, posY - 1);

			if(deeper == Nil.NIL && lazy) {
				deeper = new JMo_List();
				this.tab.set(posX - 1, posY - 1, deeper);
			}
			if(!(deeper instanceof A_Sequence))
				throw new RuntimeError(cr, "Invalid type of item.", "Item " + (offset + 1) + " isn't a sequence, so I can't go into it!");
			final A_Sequence deeper2 = (A_Sequence)deeper;
			deeper2.sequenceDeepSet(cr, keys, offset + 2, value, lazy);
		}
	}

	public void setTitles(final CallRuntime cr, final I_Object[] titles) {
		if(titles.length != this.tab.width())
			throw new RuntimeError(cr, "Invalid amount of titles for table", "Column count is " + this.tab.width() + ", but got " + titles.length + " title(s).");
		for(final I_Object title : titles)
			cr.argType(title, A_Atomic.class);

		this.titles = titles;
	}

	@Override
	public String toString() {
		return this.tab == null
			? "Table"
			: "Table<" + this.tab.width() + ',' + this.tab.size() + '>';
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {

		switch(type) {
			case REGULAR:
				return Lib_Table.toText(false, cr, this, this.tab, this.titles);
			case NESTED:
			case IDENT:
				return this.toString();
			default:
				return Lib_Table.toText(true, cr, this, this.tab, this.titles);
		}
	}

	@Override
	protected ObjectCallResult call3(final CallRuntime cr, final String method) {

		switch(method) {
			case "init":
				this.mInit(cr);
				return A_Object.stdResult(this);
//			case "getItem":
//				return A_Object.stdResult(this.getItem(cr));
			case "row":
//			case "getRow":
				return A_Object.stdResult(this.mGetRow(cr, false));
//			case "map":
//			case "getMap": // This is confusing
//			case "getRowMap":
			case "rowMap":
				return A_Object.stdResult(this.mGetRow(cr, true));
//			case "col":
//			case "getCol":
//			case "getColumn":
			case "column":
				return A_Object.stdResult(this.mGetColumn(cr));

			case "getTitles":
				return this.mGetTitles(cr);
			case "setTitles":
				this.mSetTitles(cr);
				return A_Object.stdResult(this);
			case "firstRowTitles":
				this.mSetFirstRowTitles(cr);
				return A_Object.stdResult(this);

			case "+":
			case "add":
				return A_Object.stdResult(this.mAdd(cr));
			case "addRow":
			case "addRows":
				return A_Object.stdResult(this.mAddRows(cr));

			case "++":
			case "concat":
				return A_Object.stdResult(this.mConcat(cr));
			case "addAll":
				return A_Object.stdResult(this.mAddAll(cr));

			case "each":
				return this.mEach(cr, false);
			case "eachMap":
				return this.mEach(cr, true);

			case "search":
				return A_Object.stdResult(this.mSearch(cr, false, true));
			case "searchFirst":
				return A_Object.stdResult(this.mSearch(cr, true, true));
			case "searchLast":
				return A_Object.stdResult(this.mSearch(cr, true, false));

			// case "getString": result= getString(c); break;

			case "width":
				/**
				 * °width()Int # Return the width of the table
				 */
				cr.argsNone();
				return A_Object.stdResult(new Int(this.tab.width()));

			case "sort":
				return A_Object.stdResult(this.mSort(cr));
			case "shuffle":
				return A_Object.stdResult(this.mShuffle(cr));
			case "reverse":
				return A_Object.stdResult(this.mReverse(cr));

			case "filter":
				return A_Object.stdResult(this.mFilter(cr, method));
			case "map":
				return A_Object.stdResult(this.mMap(cr, method));
			case "reduce":
				return A_Object.stdResult(this.mReduce(cr, method));
			case "amount":
				return A_Object.stdResult(this.mAmount(cr, method));

			case "rotate":
				return A_Object.stdResult(this.mRotate(cr));
			case "columns":
				return A_Object.stdResult(this.mColumns(cr));
			case "rows":
				return A_Object.stdResult(this.mRows(cr));

			case "deleteRow":
			case "deleteRows":
				return A_Object.stdResult(this.mDelete(cr));

			case "contains":
				return A_Object.stdResult(this.mContains(cr));

			case "style":
				return A_Object.stdResult(this.mStyle(cr));

			default:
				return null;
		}
	}

	@Override
	protected JMo_Table copy(final CallRuntime cr) {
		return new JMo_Table(this.tab.copy());
	}

	@Override
	protected I_Object getFirst(final CallRuntime cr) {
		return this.tab.size() == 0
			? Nil.NIL
			: this.iToList(this.tab.get(0));
	}

	@Override
	protected I_Object getLast(final CallRuntime cr) {
		final int len = this.tab.size();
		return len == 0
			? Nil.NIL
			: this.iToList(this.tab.get(len - 1));
	}

	@Override
	protected void mSequenceSetPut(final CallRuntime cr, final boolean lazy) {
		final I_Object[] orc = cr.argsVar(this, 2, 2);

		if(orc.length > 3) {
			this.sequenceDeepSet(cr, orc, 1, orc[0], lazy);
			return;
		}
		final int row = Lib_Convert.getIntValue(cr, orc[1]);
		final int col = Lib_Convert.getIntValue(cr, orc[2]);

		if(lazy) {

			while(this.tab.size() < row) {
				final I_Object[] newRow = new I_Object[this.tab.width()];
				for(int i = 0; i < this.tab.width(); i++)
					newRow[i] = Nil.NIL;
				this.tab.add(newRow);
			}
			if(col > this.tab.width())
				throw new RuntimeError(cr, "Can't add columns to Table", "Table has a fixed width, so I can't add/access column " + col);
		}
		this.tab.set(col - 1, row - 1, orc[0]);
	}

	@Override
	protected void sequenceContent(final CallRuntime cr, final I_Object[] values) {

		for(int y = 0; y < this.tab.size(); y++) {
			final JMo_List in = (JMo_List)cr.argType(values[y], JMo_List.class);
			final SimpleList<I_Object> row = in.getInternalCollection();
			if(row.size() != this.tab.width())
				throw new RuntimeError(cr, "Invalid amount of items in row", "Width of table is " + this.tab.width() + ", but got " + row.size() + " items in argument " + (y + 1) + ".");
			for(int x = 0; x < this.tab.width(); x++)
				this.tab.set(x, y, row.get(x));
		}
	}

	@Override
	protected boolean sequenceEmpty() {
		return this.tab.isEmpty();
	}

	@Override
	protected I_Object sequenceGetPull(final CallRuntime cr, final boolean lazy, final I_Object arg) {
		return this.mGetRow(cr, false);

//		final I_Object[] xy = cr.argsVar(this, 2, 2);
//		if(xy.length > 2)
//			return this.sequenceDeepGet(cr, xy, 0, lazy);
//
//		int iRow = Lib_Convert.getIntValue(cr, xy[0]);
//		int iCol = Lib_Convert.getIntValue(cr, xy[1]);
//		iRow = Lib_Sequence.realPosition(cr, iRow, this.tab.size(), lazy);
//		iCol = Lib_Sequence.realPosition(cr, iCol, this.tab.width(), lazy);
//
//		if(lazy && (iCol == -1 || iRow == -1))
//			return Nil.NIL;
//
//		Lib_Error.ifNotBetween(cr, 1, this.tab.width(), iCol, "column");
//		Lib_Error.ifNotBetween(cr, 1, this.tab.size(), iRow, "row");
//		return this.tab.get(iCol - 1, iRow - 1);
	}

	@Override
	protected I_Object sequenceSelectGet(final CallRuntime cr, final I_Object key, final boolean lazy) {
		final int oi = Lib_Convert.getIntValue(cr, key);
		final int pos = Lib_Sequence.realPosition(cr, oi, this.tab.size(), lazy);
		return pos == -1 ? Nil.NIL : this.iToList(this.tab.getRow(pos - 1));
	}

	@Override
	protected void sequenceSet(final CallRuntime cr, final I_Object key, final I_Object value, final boolean lazy) { //TODO setRow
		final SimpleList<I_Object> cols = ((JMo_List)cr.argType(value, JMo_List.class)).getInternalCollection();

		final int row = Lib_Convert.getIntValue(cr, key);

		if(lazy) {
			final int width = this.tab.width();

			while(this.tab.size() < row) {
				final I_Object[] newRow = new I_Object[width];
				for(int i = 0; i < width; i++)
					newRow[i] = Nil.NIL;
				this.tab.add(newRow);
			}
		}
		Lib_Error.ifNotBetween(cr, 1, this.tab.size(), row, "row");
		Lib_Error.ifNotBetween(cr, 1, this.tab.width(), cols.size(), "columns");

		for(int col = 0; col < this.tab.width(); col++)
			this.tab.set(col, row - 1, cols.get(col)); // -1
	}

	@Override
	protected int sequenceSize() {
		return this.tab.size();
	}

	private I_Object iToList(final Collection<I_Object> it) {
		final SimpleList<I_Object> al = new SimpleList<>(it.size());
		al.addAll(it);
		return new JMo_List(al);
	}

	private I_Object iToList(final I_Object[] it) {
		return new JMo_List(ConvertArray.toSimpleList(it));
	}

	private I_Object iToMap(final I_Object[] it) {
		final SimpleList<I_Object> objects = new SimpleList<>(it.length);
		Collections.addAll(objects, it);

		if(this.titles != null) {
			final SimpleList<I_Object> keys = new SimpleList<>(this.titles.length);
			Collections.addAll(keys, this.titles);
			return new JMo_Map(keys, objects);
		}
		else {
			final SimpleList<I_Object> t2 = new SimpleList<>(this.tab.width());
			for(int col = 1; col <= this.tab.width(); col++)
				t2.add(new Int(col));
			return new JMo_Map(t2, objects);
		}
	}

	private int[] iVarArgsToIntArray(final CallRuntime cr, final I_Object[] args, final int offset, final int min, final int max) {
		final int[] result = new int[args.length - offset];
		for(int i = offset; i < args.length; i++)
			result[i - offset] = Lib_Convert.getIntValue(cr, cr.argType(args[i], Int.class)) - 1;

		return result;
	}

	/**
	 * °+ ^ add
	 * °add(Object o, ... )Same # Add one row to the table
	 */
	private JMo_Table mAdd(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 0);
		if(args.length != this.tab.width())
			throw new RuntimeError(cr, "Invalid amount of items to add.", "Width of table is " + this.tab.width() + ", but got " + cr.argCount() + " items.");
		this.tab.add(args);
		return this;
	}

	/**
	 * °addAll(Table other)Same # Adds all rows to the current table.
	 */
	private JMo_Table mAddAll(final CallRuntime cr) {
		final JMo_Table tab2 = (JMo_Table)cr.args(this, JMo_Table.class)[0];
		tab2.getInternalObject().forEach(a-> this.tab.add(a));
		return this;
	}

	/**
	 * °addRow ^ addRows
	 * °addRows(List l, ...)Same # Add rows to the table
	 */
	private JMo_Table mAddRows(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 1);

		for(final I_Object arg : args) {
			final JMo_List pl = (JMo_List)cr.argType(arg, JMo_List.class);
			final SimpleList<I_Object> list = pl.getInternalCollection();
			if(list.size() != this.tab.width())
				throw new RuntimeError(cr, "Invalid size of row", "Table width is " + this.tab.width() + ", but should add a row with " + list.size() + " items.");
			final I_Object[] row = list.toArray(new I_Object[list.size()]);
			this.tab.add(row);
		}
		return this;
	}

//	/**
//	 * °getXY(IntNumber col_x, IntNumber row_y)Object # Get one item with at position X,Y
//	 */
//	private I_Object getItem(final CallRuntime cr) {
//		final I_Object[] args = cr.args(this, A_IntNumber.class, A_IntNumber.class);
//		final int x = Lib_Convert.getIntValue(cr, args[0]);
//		final int y = Lib_Convert.getIntValue(cr, args[1]);
//		return this.tab.get(x, y);
//	}

	/**
	 * °amount(List each)§Int # Check every row and return the amount of trues. The result of each must be Bool.
	 * °amount(VarLet var, List each)§Int # Check every row and return the amount of trues. The result of each must be Bool.
	 */
	private Int mAmount(final CallRuntime cr, final String method) {
		Lib_Error.ifArgs(cr.argCount(), 1, 2, cr, this);
		int result = 0;

		for(final I_Object[] test : this.tab) {
			final Bool ok = (Bool)cr.copyEach(method).argsEach(this, 0, test, Bool.class);
			if(ok.getValue())
				result++;
		}
		return new Int(result);
	}

//	/**
//	 * get(IntNumber row)List # Returns a new list with the specified row
//	 * get(IntNumber col, IntNumber row)Object? # Returns the content of the field at col/row
//	 */
//	private I_Object get(final CallRuntime cr) {
//		if(cr.parCount() == 1)
//			return this.getRow(cr, false);
//		final I_Object[] xy = cr.args(this, A_IntNumber.class, A_IntNumber.class);
//		final int iRow = Lib_Convert.getIntValue(cr, xy[0]);
//		final int iCol = Lib_Convert.getIntValue(cr, xy[1]);
//		Lib_Error.ifNotBetween(cr, 1, this.tab.width(), iCol, "column");
//		Lib_Error.ifNotBetween(cr, 1, this.tab.size(), iRow, "row");
//		return this.tab.get(iCol, iRow);
//	}

	/**
	 * °columns(IntNumber column...)Table # Creates a new Table with a copy of the specified columns. Negative numbers are from right to left
	 */
	private JMo_Table mColumns(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 0);
		final int parLen = args.length;
		final int[] parsi = new int[parLen];

		for(int i = 0; i < parLen; i++) {
			final int par = Lib_Convert.getIntValue(cr, cr.argType(args[i], A_IntNumber.class));
			parsi[i] = Lib_Sequence.realPosition(cr, par, this.tab.width(), false);
		}
		final ArrayTable<I_Object> newTab = new ArrayTable<>(parLen);

		for(int row = 0; row < this.tab.size(); row++) {
			final I_Object[] items = new I_Object[parLen];
			for(int i = 0; i < parLen; i++)
				items[i] = this.tab.get(parsi[i] - 1, row);
			newTab.add(items);
		}
		return new JMo_Table(newTab);
	}

	/**
	 * °++^concat
	 * °concat(Table other)Table # Creates a new combined table.
	 */
	private JMo_Table mConcat(final CallRuntime cr) {
		final JMo_Table tab2 = (JMo_Table)cr.args(this, JMo_Table.class)[0];
		final ArrayTable<I_Object> newTab = new ArrayTable<>(this.tab.width());
		this.tab.forEach(a-> newTab.add(a));
		tab2.getInternalObject().forEach(a-> newTab.add(a));
		return new JMo_Table(newTab);
	}

	/**
	 * °contains(Object obj)Bool # Returns true, if this table contains the given object.
	 */
	private Bool mContains(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_Object.class)[0];
		for(int x = 0; x < this.tab.width(); x++)
			if(this.tab.getColumn(x).contains(arg))
				return Bool.TRUE;
		return Bool.FALSE;
	}

	/**
	 * °deleteRow ^ deleteRows
	 * °deleteRows(IntNumber rows...)Same # Removes 'rows' from this table.
	 */
	private JMo_Table mDelete(final CallRuntime cr) { // TODO Testen!!!!
		final I_Object[] args = cr.argsVar(this, 1, 0);

		final int len = args.length;
		final int[] rows = new int[len];
		for(int i = 0; i < len; i++)
			rows[i] = Lib_Convert.getIntValue(cr, args[i]);

		final Integer duplicate = Lib_Array.getNextDuplicate(rows);
		if(duplicate != null)
			throw new RuntimeError(cr, "Duplicated row position", "Same row can't removed twice: " + duplicate);
		Lib_Array.sortGnome(rows, true);

		for(final int row : rows) {
			Lib_Error.ifNotBetween(cr, 1, this.tab.size(), row, "row");
			this.tab.remove(row - 1);
		}
		return this;
	}

	/**
	 * °each()%List? # Walk throw each row
	 * °each(Object o)%List # Walk throw each row, but use always object o.
	 * °each(VarLet var)%List # Walk throw each row and assign the row to var
	 * °eachMap()%List? # Create a map for each row and walk throw each of them
	 * °eachMap(Object o)%List # Walk throw each row, but use always object o.
	 * °eachMap(VarLet var)%List # Create a map for each row and walk throw each of them and store it in var.
	 */
	private ObjectCallResult mEach(final CallRuntime crOld, final boolean map) {
		crOld.argsNone();

		if(crOld.getStream() == null && crOld.getCallBlock() == null) // TODO Use library Function ... maybe in Lib_Exec / CurProc?!?
			Err.impossible("No block, no stream, what should I do?");

		final LoopHandle handle = new LoopHandle(this);
		final CallRuntime crNew = crOld.copyLoop(handle);
		I_Object result = Nil.NIL;

		for(final I_Object[] it : this.tab) {
			handle.startLap();
			final I_Object itO = map ? this.iToMap(it) : this.iToList(it);
			result = Lib_Exec.execBlockStream(crNew, itO);

			if((result = Lib_Exec.loopResult(result)) instanceof Return)
				return ((Return)result).getLoopResult();
		}
		return new ObjectCallResult(result, true);
	}

	/**
	 * °filter(List each)§Table # Creates a new table with selected rows. A FuncLet will be executed, result of 'each' must be Bool.
	 * °filter(VarLet var, List each)§Table # Creates a new table with selected items. Each item will be assigned to 'var' and 'each' is processed. A FuncLet will be executed. The result of 'each' must be Bool.
	 */
	private JMo_Table mFilter(final CallRuntime cr, final String method) {
		Lib_Error.ifArgs(cr.argCount(), 1, 2, cr, this);
		final ArrayTable<I_Object> result = new ArrayTable<>(this.tab.width());

		for(int p = 0; p < this.tab.size(); p++) {
			final I_Object[] test = this.tab.get(p);
			final JMo_List list = new JMo_List(test);
			// MOut.temp(test, cr.call.gInternPars());
			// Bool ok = (Bool)cr.args(test, Bool.class)[0];
			final Bool ok = (Bool)cr.copyEach(method).argsEach(this, 0, new I_Object[]{list}, Bool.class);
			if(ok.getValue())
				result.add(test);
		}
		return new JMo_Table(result);
	}

	/**
	 * °column(IntNumber col)List # Returns a copy of a column
	 */
	private I_Object mGetColumn(final CallRuntime cr) {
		final I_Object x = cr.args(this, A_IntNumber.class)[0];
		final int ix = Lib_Convert.getIntValue(cr, x);
		Lib_Error.ifNotBetween(cr, 1, this.tab.width(), ix, "column");
		return this.iToList(this.tab.getColumn(ix - 1));
	}

	/**
	 * °row(IntNumber row)List # Returns a copy of a row.
	 * °rowMap(IntNumber row)Map # Returns one row as a map with titles.
	 */
	private I_Object mGetRow(final CallRuntime cr, final boolean map) {
		final I_Object y = cr.args(this, A_IntNumber.class)[0];
		final int iy = Lib_Convert.getIntValue(cr, y);
		Lib_Error.ifNotBetween(cr, 1, this.tab.size(), iy, "row");
		final I_Object[] it = this.tab.get(iy - 1);
		return map ? this.iToMap(it) : this.iToList(it);
	}

//	/**
//	 * °set(Object obj, IntNumber col_x, IntNumber row_y)Same # Set object obj to position col_x, row_y
//	 */
//	private JMo_Table set(final CallRuntime cr) {
//		final I_Object[] xyo = cr.args(this, I_Object.class, A_IntNumber.class, A_IntNumber.class);	//TODO go deep
//		final int row = Lib_Convert.getIntValue(cr, xyo[1]);
//		final int col = Lib_Convert.getIntValue(cr, xyo[2]);
//		this.tab.set(col, row, xyo[0]);
//		return this;
//	}

	/**
	 * °getTitles()List # Returns a List with all titles of the table.
	 */
	private ObjectCallResult mGetTitles(final CallRuntime cr) {
		final SimpleList<I_Object> al = new SimpleList<>(this.titles.length);
		Collections.addAll(al, this.titles);
		return A_Object.stdResult(new JMo_List(al));
	}

	/**
	 * °init(Int amount, Object o)Same # Add 'amount' rows, filled with 'o' to this Table
	 */
	private void mInit(final CallRuntime cr) {
		final I_Object[] args = cr.args(this, Int.class, I_Object.class);
		final int r = Lib_Convert.getIntValue(cr, args[0]);
		final I_Object f = args[1];

		for(int ir = 0; ir < r; ir++) {
			final I_Object[] row = new I_Object[this.tab.width()];
			for(int iw = 0; iw < this.tab.width(); iw++)
				row[iw] = f;
			this.tab.add(row);
		}
	}

	/**
	 * °map(List each)§Table # Creates a copy of this table, and execute 'each' for every row. A FuncLet will be executed, result of 'each' must be a List.
	 * °map(VarLet var, List each)§Table # Creates a copy of the table, and execute 'var' and 'proc' with every row.
	 */
	private JMo_Table mMap(final CallRuntime cr, final String method) {
		Lib_Error.ifArgs(cr.argCount(), 1, 2, cr, this);
		final ArrayTable<I_Object> result = new ArrayTable<>(this.tab.width());

		for(int p = 0; p < this.tab.size(); p++) {
			final I_Object[] test = this.tab.get(p);
			final JMo_List testr = (JMo_List)cr.copyEach(method).argsEach(this, 0, test, JMo_List.class);
			final I_Object[] row = testr.getInternalCollection().toArray(new I_Object[this.tab.width()]);
			result.addRow(row);
		}
		return new JMo_Table(result);
	}

	/**
	 * °reduce(Object init, List each)§Object # Reduces the table to a single value.
	 * °reduce(Object init, VarLet var, List each)§Object # Reduces the table to a single value.
	 */
	private I_Object mReduce(final CallRuntime cr, final String method) {
		Lib_Error.ifArgs(cr.argCount(), 2, 3, cr, this);
		I_Object sum = cr.argsOneAdvance(this, 0, I_Object.class);

		for(final I_Object[] test : this.tab) {
			final I_Object[] each = {sum, new JMo_List(test)};
//			final Call c = new Call(cr.getSurrBlock(), null, method, cr.call.parCalls, cr.getDebugInfo());
//			sum = cr.copyCall(c, false).parsEach(this, 1, test2, I_Object.class);
			sum = cr.copyEach(method).argsEach(this, 1, each, I_Object.class);
		}
		return sum;
	}

	/**
	 * °reverse()Same # Reverses the rows in this table
	 */
	private I_Object mReverse(final CallRuntime cr) {
		cr.argsNone();
		this.tab.reverse();
		return this;
	}

	/**
	 * °rotate()Table # Create a new table which will be rotated to the right
	 * °rotate(MagicPosition dir)Table # Create a new table which will be rotated to the _LEFT or to the _RIGHT
	 */
	private I_Object mRotate(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 0, 1);
		final ArrayTable<I_Object> newTab = new ArrayTable<>(this.tab.size());
		POSITION_H dir = POSITION_H.RIGHT;

		if(args.length == 1) {
			final POSITION p = ((MagicPosition)cr.argType(args[0], MagicPosition.class)).get();
			if(p != POSITION.LEFT && p != POSITION.RIGHT)
				throw new RuntimeError(cr, "Invalid direction", "A table can only be rotated to _LEFT or _RIGHT.");
			dir = (POSITION_H)p;
		}
		if(dir == POSITION_H.LEFT)
			for(int col = this.tab.width() - 1; col >= 0; col--) {
				final Collection<I_Object> row = this.tab.getColumn(col);
				final I_Object[] rowArray = row.toArray(new I_Object[row.size()]);
				newTab.add(rowArray);
			}
		else
			for(int col = 0; col < this.tab.width(); col++) {
				final Collection<I_Object> row = this.tab.getColumn(col);
				final I_Object[] rowArray = row.toArray(new I_Object[row.size()]);
				Lib_Array.reverse(rowArray);
				newTab.add(rowArray);
			}

		return new JMo_Table(newTab);
	}

	/**
	 * °rows(IntNumber row...)Table # Creates a new Table with a copy of the specified rows. Negative numbers are from last to first row.
	 */
	private JMo_Table mRows(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 0);

		final ArrayTable<I_Object> newTab = new ArrayTable<>(this.tab.width());

		for(final I_Object arg : args) {
			int parRow = Lib_Convert.getIntValue(cr, cr.argType(arg, A_IntNumber.class));
			parRow = Lib_Sequence.realPosition(cr, parRow, this.tab.size(), false);
			newTab.add(this.tab.getRow(parRow - 1));
		}
		return new JMo_Table(newTab);
	}

	/**
	 * °search(Atomic|RegEx search, Int columns...)Table # Returns all rows, that matches 'search'.
	 * °searchFirst(Atomic|RegEx search, Int columns...)List? # Returns the first value, that matches 'search'. Nil, if nothings found.
	 * °searchLast(Atomic|Regex search, Int columns...)List? # Returns the first value, that matches 'search'. Nil, if nothings found.
	 */
	private I_Object mSearch(final CallRuntime cr, final boolean one, final boolean first) {
		final I_Object[] args = cr.argsVar(this, 1, 1);
		final I_Object searchObj = cr.argTypeExt(args[0], Str.class, JMo_RegEx.class);
		final ArrayTable<I_Object> result = new ArrayTable<>(this.tab.width());

		final boolean useRegex = searchObj instanceof JMo_RegEx;
		final String searchStr = useRegex
			? ((JMo_RegEx)searchObj).getValue()
			: Lib_Convert.getStringValue(cr, searchObj);

		final int[] columnIndexes = args.length == 1
			? Lib_Array.intRange(0, this.tab.width() - 1)
			: this.iVarArgsToIntArray(cr, args, 1, 1, this.tab.width());

		final int size = this.tab.size();
		final int yStart = first ? 0 : size - 1;
		final int yEnd = first ? size : -1;
		final int yStep = first ? 1 : -1;

		for(int y = yStart; y != yEnd; y += yStep) {
			boolean add = false;

			for(final int x : columnIndexes) {
				final I_Object o = this.tab.get(x, y);
				final String s = Lib_Convert.getStringValue(cr, o);

				final boolean hit = useRegex
					? s.matches(searchStr)
					: s.equals(searchStr);

				if(hit) {
					add = true;
					break;
				}
			}

			if(add) {
				result.add(this.tab.get(y));
				if(one)
					break;
			}
		}

		// --- Return ---
		if(result.size() == 0)
			return Nil.NIL;

		if(one) {
			final I_Object[] row = result.getRow(0);
			final SimpleList<I_Object> row2 = new SimpleList<>();
			Collections.addAll(row2, row);
			return new JMo_List(row2);
		}
		else
			return new JMo_Table(result);
	}

	/**
	 * °firstRowTitles()Same # The first row will be removed and used as titles.
	 */
	private void mSetFirstRowTitles(final CallRuntime cr) {
		cr.argsNone();
		if(this.tab.size() == 0)
			throw new RuntimeError(cr, "Invalid amount of rows", "The table must contain at least one row to be set as titles.");
		final I_Object[] headers = this.tab.remove(0);
		this.setTitles(cr, headers);
	}

	/**
	 * °setTitles(List l)Same # Set the title for every column.
	 */
	private void mSetTitles(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 0);
		this.setTitles(cr, args);
	}

	/**
	 * °shuffle()Same # Shuffle all rows of this table
	 */
	private I_Object mShuffle(final CallRuntime cr) {
		cr.argsNone();
		this.tab.sortRandom();
		return this;
	}

	/**
	 * °sort(IntNumber... columns)Same # Order this table according to the parameters. Positive numbers are ascending, negative are descending.
	 */
	private I_Object mSort(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 0, 0);
		final int parLen = args.length;
		final int tabWidth = this.tab.width();
		if(parLen > tabWidth)
			throw new RuntimeError(cr, "Too much parameters", "This table has only " + tabWidth + " colums. Got " + parLen + " columns to sort.");

		final int[] colOrder = new int[parLen];
		for(int i = 0; i < parLen; i++)
			colOrder[i] = Lib_Convert.getIntValue(cr, cr.argType(args[i], A_IntNumber.class));

		for(final int i : colOrder)
			Lib_Error.ifNotBetween(cr, 1, this.tab.width(), Math.abs(i), "column");

		this.tab.sort(colOrder);
		return this;
	}

	/**
	 * °style(Str style)Str # Style this table according the given style and return a Str.
	 */
	private Str mStyle(final CallRuntime cr) {
		final I_Object arg = cr.args(this, Str.class)[0];
		final String style = Lib_Convert.getStringValue(cr, arg);

		final List<String> parts = ConvertString.toList(':', style);
		Lib_Error.ifNotBetween(cr, 0, 3, parts.size(), "Amount of parts");

		final String result = Lib_TableStyle.compute(cr, this.tab, parts.toArray(new String[3])); // Size 3 needed!
		return new Str(result);
	}

}
