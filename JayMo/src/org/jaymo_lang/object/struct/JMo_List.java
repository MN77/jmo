/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.struct;

import java.util.Collections;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.I_AutoBlockList;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.model.ObjectManager;
import org.jaymo_lang.model.VarArgsCallBuffer;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.JMo_RegEx;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Dec;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.passthrough.I_Mem;
import org.jaymo_lang.object.pseudo.A_MemLet;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.ATOMIC;
import org.jaymo_lang.util.Lib_AtomConv;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Exec;
import org.jaymo_lang.util.Lib_Output;
import org.jaymo_lang.util.Lib_Sequence;
import org.jaymo_lang.util.Lib_StrFormat;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.struct.I_Series;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.type.Lib_Compare;
import de.mn77.base.data.util.Lib_Random;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class JMo_List extends A_Sequence implements I_AutoBlockDo, I_AutoBlockList {

	private SimpleList<I_Object>    list;
	private final VarArgsCallBuffer init;
	private boolean                 fixedLength = false;
	private boolean                 fixedTypes  = false;
	private boolean                 fixedValues = false; // = read only
	private String                  fixedType   = null;


	/**
	 * !List of objects
	 * +List(Object o1, Object o2, ...) # List(6,9,15,23,12)
	 * +[Object o1, Object o2, ...] # [3,6,1,'a']
	 * %.each
	 */
	public JMo_List() {
		this.list = new SimpleList<>();
		this.init = null;
	}

	public JMo_List(final Call... ca) {
		this.list = null;
		this.init = new VarArgsCallBuffer(ca);
	}

	public JMo_List(final I_Object[] oa) {
		this.list = new SimpleList<>();
		Collections.addAll(this.list, oa);
		this.init = null;
	}

	public JMo_List(final SimpleList<I_Object> list) {
		this.list = list;
		this.init = null;
	}

	@Override
	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.mEach(cr).obj;
	}

	@Override
	public SimpleList<I_Object> autoBlockToList(final CallRuntime cr) {
		return this.list;
	}

	@Override
	public boolean equals(final Object obj) {
		return this.iEquals(obj, false);
	}

	@Override
	public boolean equalsLazy(final Object obj) {
		return this.iEquals(obj, true);
	}

	@Override
	public SimpleList<I_Object> getInternalCollection() {
		return this.list;
	}

	@Override
	public void init(final CallRuntime cr) {

		// Prevent Vars/Consts in List
		if(this.list != null && this.init == null) {
			for(int i = 0; i < this.list.size(); i++)
				this.list.set(i, Lib_Convert.getValue(cr, this.list.get(i)));
			return;
		}

		// Call and add arguments
		if(this.init != null) {
			this.list = new SimpleList<>();

			// Ranges to List
			final I_Object[] ios = this.init.init(cr, this);
			Collections.addAll(this.list, ios);
		}
	}

	public void internalAdd(final I_Object o) {
		if(this.fixedLength)
			Err.forbidden(o);
		this.list.add(o);
	}

	@Override
	public I_Object sequenceDeepGet(final CallRuntime cr, final I_Object[] keys, final int offset, final boolean lazy) {
		final A_IntNumber oi = (A_IntNumber)cr.argType(keys[offset], A_IntNumber.class);
		final int i = Lib_Convert.getIntValue(cr, oi);
		final int pos = Lib_Sequence.realPosition(cr, i, this.list.size(), lazy);
		if(pos == -1 || pos > this.list.size())
			if(lazy)
				return Nil.NIL;
			else
				Lib_Error.ifNotBetween(cr, 1, this.list.size(), pos, "position in list");

		if(offset == keys.length - 1)
			return this.list.get(pos - 1);
		else {
			final I_Object deeper = this.list.get(pos - 1);

			if(!(deeper instanceof I_DeepGetSet)) {
				final StringBuilder sb = new StringBuilder();

				for(int j = 0; j <= offset; j++) {
					if(j > 0)
						sb.append(',');
					sb.append(keys[j].toString());
				}
				throw new RuntimeError(cr, "Invalid type of item.", "Item at position (" + sb.toString() + ") isn't a sequence, so I can't go into it!");
			}
			return ((I_DeepGetSet)deeper).sequenceDeepGet(cr, keys, offset + 1, lazy);
		}
	}

	@Override
	public void sequenceDeepSet(final CallRuntime cr, final I_Object[] keys, final int offset, final I_Object value, final boolean lazy) {
		final A_IntNumber oi = (A_IntNumber)cr.argType(keys[offset], A_IntNumber.class);
		final int wanted = Lib_Convert.getIntValue(cr, oi);

		if(lazy) {
			this.list.ensureCapacity(wanted);

			while(Math.abs(wanted) > this.list.size())
				this.list.add(Nil.NIL);
		}
		final int pos = Lib_Sequence.realPosition(cr, wanted, this.list.size(), lazy);

		Lib_Error.ifNotBetween(cr, 1, this.list.size(), pos, "position");
		this.iFixedValues(cr);

		if(offset == keys.length - 1)
			this.list.set(pos - 1, value);
		else {
			I_Object deeper = this.list.get(pos - 1);

			if(deeper == Nil.NIL && lazy) {
				deeper = new JMo_List();
				this.list.set(pos - 1, deeper);
			}
			if(!(deeper instanceof A_Sequence))
				throw new RuntimeError(cr, "Invalid type of item.", "Item at position " + (offset + 1) + " isn't a sequence, so I can't go into it!");
			final A_Sequence deeper2 = (A_Sequence)deeper;
			deeper2.sequenceDeepSet(cr, keys, offset + 1, value, lazy);
		}
	}

	/**
	 * °freeze ^ readOnly
	 * °readOnly()Same # Make this List read only.
	 */
	public void setReadOnly() {
		this.fixedLength = true;
		this.fixedTypes = true;
		this.fixedValues = true;
	}

	@Override
	public String toString() {
		return "List<" + this.list.size() + '>';
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final boolean limit = type == STYPE.IDENT || type == STYPE.NESTED;

		final StringBuilder sb = new StringBuilder();
//		sb.append(type == STYPE.DESCRIBE ? "List(" : '[');
		sb.append('[');

		if(this.list != null) {

			for(final I_Object o : this.list) {

				if(o == this)
					sb.append(A_Sequence.CYCLE_IDENT);
				else {
					String s = o.toString(cr, type.getNested());
					if(type == STYPE.DESCRIBE)
						s = Lib_Output.indentLines(s, false);
					sb.append(s);
				}
				sb.append(',');

				if(limit && sb.length() >= JayMo.NESTED_MAX)
					return this.toString();
			}
			Lib_Output.removeEnd(sb, ',');
		}
//		sb.append(type == STYPE.DESCRIBE ? ")" : ']');
		sb.append(']');

		return sb.toString();
	}

	@Override
	protected ObjectCallResult call3(final CallRuntime cr, final String method) {

		switch(method) {
			case "+": // Must create a new List
			case "merge":
				return A_Object.stdResult(this.mMerge(cr));
			case "-": // Must create a new List
			case "sub":
				return A_Object.stdResult(this.mSubtract(cr));
			case "*": // Must create a new List
			case "mul":
				return A_Object.stdResult(this.mMultiply(cr));
			case "/": // Must create a new List
			case "div":
				return A_Object.stdResult(this.mDivide(cr));
			case "++": // Must create a new List
			case "join":
			case "concat":
				return A_Object.stdResult(this.mConcat(cr));
			case "--": // Must create a new List
//			case "dec":
			case "without":
				return A_Object.stdResult(this.mWithout(cr));

			case "hasNext":
				return A_Object.stdResult(this.mHasNext(cr));

			case "add":
			case "append":
				return A_Object.stdResult(this.mAdd(cr));
			case "delete":
				return A_Object.stdResult(this.mRemovePos(cr));
			case "detach":
				return A_Object.stdResult(this.mDetach(cr, null));
			case "detachFirst":
				return A_Object.stdResult(this.mDetach(cr, 1));
			case "detachLast":
				return A_Object.stdResult(this.mDetach(cr, -1));
			case "remove":
				return A_Object.stdResult(this.mRemoveItems(cr));
			case "insert":
				return A_Object.stdResult(this.mInsert(cr));
			case "begin":
				return A_Object.stdResult(this.mBegin(cr));
			case "clear":
				return A_Object.stdResult(this.mClear(cr));
			case "swap":
				return A_Object.stdResult(this.mSwap(cr));
			case "adjust":
				return A_Object.stdResult(this.mAdjust(cr));

			case "addAll":
				return A_Object.stdResult(this.mAddAll(cr));
			case "subAll":
//			case "subtractAll":
			case "removeAll":
				return A_Object.stdResult(this.mRemoveAll(cr));
			case "intersect":
			case "slice":
				return A_Object.stdResult(this.mSlice(cr));
			case "init":
				this.mInitList(cr);
				return A_Object.stdResult(this);

			case "shuffle":
				return A_Object.stdResult(this.mShuffle(cr));
			case "reverse":
				return A_Object.stdResult(this.mReverse(cr));
			case "uniq":
			case "unique":
				return A_Object.stdResult(this.mUnique(cr));
			case "only":
				return A_Object.stdResult(this.mOnly(cr));
			case "contains":
				return A_Object.stdResult(this.mContains(cr));
			case "search":
			case "searchFirst":
				return A_Object.stdResult(this.mSearch(cr, true));
			case "searchLast":
				return A_Object.stdResult(this.mSearch(cr, false));
			case "each":
				return this.mEach(cr);
			case "eachVertical":
				return this.mEachVertical(cr);
			// case "eachDeep": return each(c);
//			case "combine":
			case "implode":
				return A_Object.stdResult(this.mImplode(cr));
			case "unfold":
				return A_Object.stdResult(this.mUnfold(cr));
			case "flatten":
				return A_Object.stdResult(this.mFlatten(cr));
			case "fill":
				return A_Object.stdResult(this.mFill(cr));

			case "min":
				return A_Object.stdResult(this.mMin(cr));
			case "max":
				return A_Object.stdResult(this.mMax(cr));
			case "sum":
				return A_Object.stdResult(this.mSum(cr));
			case "avg":
			case "average":
				return A_Object.stdResult(this.mAverage(cr));
			case "count":
				return A_Object.stdResult(this.mCount(cr));
			case "median":
				return A_Object.stdResult(this.mMedian(cr));

			case "cut":
				return A_Object.stdResult(this.mCut(cr)); // from, length
			case "part":
				return A_Object.stdResult(this.mPart(cr)); // from, to
			case "end":
				return A_Object.stdResult(this.mEndPos(cr)); // TODO .end(-2)
			case "left":
				return A_Object.stdResult(this.mLeft(cr));
			case "right":
				return A_Object.stdResult(this.mRight(cr));
			case "start":
				return A_Object.stdResult(this.mStartPos(cr));

			// Functional - FuncLet
			case "filter":
				return A_Object.stdResult(this.mFilter(cr, method));
			case "map":
				return A_Object.stdResult(this.mMap(cr, method));
			case "sort":
				return A_Object.stdResult(this.mSort(cr, method));
			case "reduce":
				return A_Object.stdResult(this.mReduce(cr, method));
			case "amount":
				return A_Object.stdResult(this.mAmount(cr, method));

			/**
			 * °fixLength()Same # Fix the lenght of this List
			 * °fixTypes()Same # Fix the type of each position in this List
			 * °fix()Same # Fix length and types of this List
			 */
			case "fixLength":
				cr.argsNone();
				this.fixedLength = true;
				return A_Object.stdResult(this);
			case "fixTypes":
				cr.argsNone();
				this.fixedTypes = true;
				return A_Object.stdResult(this);
			case "fixType":
				this.mFixType(cr);
				return A_Object.stdResult(this);
			case "fix":
				this.fixedLength = true;
				this.fixedTypes = true;
				return A_Object.stdResult(this);
			case "freeze":
			case "readOnly":
				cr.argsNone();
				this.setReadOnly();
				return A_Object.stdResult(this);

			case "toMap":
				return A_Object.stdResult(this.mToMap(cr));
			case "toUniqueMap":
				return A_Object.stdResult(this.mToUniqueMap(cr));

			case "asStr":
//			case "asString":
				return A_Object.stdResult(this.mAs(cr, ATOMIC.STR));
			case "asInt":
				return A_Object.stdResult(this.mAs(cr, ATOMIC.INT));
			case "asDec":
				return A_Object.stdResult(this.mAs(cr, ATOMIC.DEC));
			case "asDouble":
				return A_Object.stdResult(this.mAs(cr, ATOMIC.DOUBLE));
			case "asBool":
				return A_Object.stdResult(this.mAs(cr, ATOMIC.BOOL));
			case "asChar":
				return A_Object.stdResult(this.mAs(cr, ATOMIC.CHAR));
			case "asByte":
				return A_Object.stdResult(this.mAs(cr, ATOMIC.BYTE));

			// VarLet
			case "store":
				return A_Object.stdResult(this.mStore(cr));

			default:
				return null;
		}
	}

	/**
	 * °copy()List # Creates a copy of this list.
	 */
	@Override
	protected JMo_List copy(final CallRuntime cr) {
		final SimpleList<I_Object> copy = this.list.copy();
		return new JMo_List(copy);
	}

	@Override
	protected I_Object getFirst(final CallRuntime cr) {
		return this.list.size() == 0
			? Nil.NIL
			: this.list.get(0);
	}

	@Override
	protected I_Object getLast(final CallRuntime cr) {
		final int len = this.list.size();
		return len == 0
			? Nil.NIL
			: this.list.get(len - 1);
	}

	@Override
	protected void mSequenceSetPut(final CallRuntime cr, final boolean lazy) {
		final I_Object[] oa = cr.argsVar(this, 2, 1);

		if(oa.length > 2)
			this.sequenceDeepSet(cr, oa, 1, oa[0], lazy);
		else
			this.sequenceSet(cr, oa[1], oa[0], lazy);
	}

	@Override
	protected void sequenceContent(final CallRuntime cr, final I_Object[] values) {
		for(int i = 0; i < this.list.size(); i++)
			this.list.set(i, values[i]);
	}

	@Override
	protected boolean sequenceEmpty() {
		return this.list.isEmpty();
	}

	@Override
	protected I_Object sequenceGetPull(final CallRuntime cr, final boolean lazy, final I_Object arg) {
		final I_Atomic oav = (I_Atomic)cr.argType(arg, I_Atomic.class);
		final int oi = Lib_Convert.getIntValue(cr, oav);
		final int pos = Lib_Sequence.realPosition(cr, oi, this.list.size(), lazy);
		return pos == -1 ? Nil.NIL : this.list.get(pos - 1);
	}

	@Override
	protected I_Object sequenceSelectGet(final CallRuntime cr, final I_Object key, final boolean lazy) {
		final int oi = Lib_Convert.getIntValue(cr, key);
		final int pos = Lib_Sequence.realPosition(cr, oi, this.list.size(), lazy);
		return pos == -1 ? Nil.NIL : this.list.get(pos - 1);
	}

	@Override
	protected void sequenceSet(final CallRuntime cr, final I_Object pos2, I_Object o, final boolean lazy) {
		final int wanted = Lib_Convert.getIntValue(cr, pos2);

		if(lazy) {
			this.list.ensureCapacity(wanted);
			while(Math.abs(wanted) > this.list.size())
				this.list.add(Nil.NIL);
		}
		final int pos = Lib_Sequence.realPosition(cr, wanted, this.list.size(), lazy);

		Lib_Error.ifNotBetween(cr, 1, this.list.size(), pos, "position");
		this.iFixedValues(cr);

		if(this.fixedTypes) {
			final I_Object curObj = this.list.get(pos - 1);
			if(curObj != Nil.NIL)
				o = Lib_Type.typeCheck(cr, o, Lib_Type.getName(o), Lib_Type.getName(curObj), "list item");
		}
		this.list.set(pos - 1, o);
	}

	/**
	 * °getLen ^ getSize
	 * °getLength ^ getSize
	 * °getSize()Int # Returns the amount of items in this list
	 */
	@Override
	protected int sequenceSize() {
		return this.list.size();
	}

	private JMo_List iCutCopy(final CallRuntime cr, final int start, final int end) {
		Lib_Error.ifTooSmall(cr, start, end);
		final SimpleList<I_Object> result = new SimpleList<>(end - start + 1);
		for(int i = start - 1; i < end; i++)
			result.add(this.list.get(i));
		return new JMo_List(result);
	}

	private boolean iEquals(final Object obj, final boolean lazy) {

		if(obj instanceof JMo_List) {
			final SimpleList<I_Object> other = ((JMo_List)obj).list;
			if(this.list.size() != other.size())
				return false;
			for(int i = 0; i < this.list.size(); i++)
				if(lazy) {
					if(!this.list.get(i).equalsLazy(other.get(i)))
						return false;
				}
				else if(!this.list.get(i).equals(other.get(i)))
					return false;
			return true;
		}
		else
			return false;
	}

	private void iFixedLength(final CallRuntime cr) {
		if(this.fixedLength)
			throw new RuntimeError(cr, "Can't change size of fixed List", "Adding/Removing elements is not allowed!");
	}

	private void iFixedType(final CallRuntime cr, final I_Object obj) {
		if(this.fixedType != null)
			if(!Lib_Type.isInstanceOf(obj, this.fixedType))
				throw new RuntimeError(cr, "Invalid object type for fixed List.", "This List accepts only <" + this.fixedType + ">, but got object with type: " + Lib_Type.getTypeString(obj));
	}

	private void iFixedType(final CallRuntime cr, final I_Object[] oa) {
		if(this.fixedType != null)
			for(final I_Object obj : oa)
				this.iFixedType(cr, obj);
	}

	private void iFixedValues(final CallRuntime cr) {
		if(this.fixedValues)
			throw new RuntimeError(cr, "This List is read only", "Changes are not allowed!");
	}

	private void internalFlatten(final SimpleList<I_Object> result, final JMo_List current) {
		for(final I_Object item : current.getInternalCollection())
			if(item instanceof JMo_List)
				this.internalFlatten(result, (JMo_List)item);
			else
				result.add(item);
	}

	/**
	 * #°+^add
	 * °append^add
	 * °add(Object... o)Same # Add these objects to the end of the list.
	 */
	private JMo_List mAdd(final CallRuntime cr) {
		final I_Object[] oa = cr.argsVar(this, 1, 0);
		this.iFixedLength(cr);
		this.iFixedType(cr, oa);

		Collections.addAll(this.list, oa);
		return this;
	}

	/**
	 * °addAll(List other)Same # Adds all items to the current list.
	 */
	private JMo_List mAddAll(final CallRuntime cr) {
		final JMo_List list2 = (JMo_List)cr.args(this, JMo_List.class)[0];
		this.iFixedLength(cr);
		if(this.fixedType != null)
			for(final I_Object obj : list2.getInternalCollection())
				this.iFixedType(cr, obj);
		this.list.addAll(list2.getInternalCollection());
		return this;
	}

	/**
	 * °adjust(IntNumber length, Object fill)Same # Ensures that the list has the specified length.
	 */
	private JMo_List mAdjust(final CallRuntime cr) {
		final I_Object[] args = cr.args(this, A_IntNumber.class, I_Object.class);
		this.iFixedLength(cr);
		final int len = Lib_Convert.getIntValue(cr, args[0]);
		final I_Object fill = args[1];
		this.iFixedType(cr, fill);
		Lib_Error.ifTooSmall(cr, 0, len);

		while(this.list.size() > len)
			this.list.remove(this.list.size() - 1);
		while(this.list.size() < len)
			this.list.add(fill);
		return this;
	}

	/**
	 * °amount(Object each)§Int # Check every item and return the amount of trues. The result of each must be Bool.
	 * °amount(VarLet var, Object each)§Int # Check every item and return the amount of trues. The result of each must be Bool.
	 */
	private Int mAmount(final CallRuntime cr, final String method) {
		Lib_Error.ifArgs(cr.argCount(), 1, 2, cr, this);
		int result = 0;

		for(final I_Object test : this.list) {
			final Bool ok = (Bool)cr.copyEach(method).argsEach(this, 0, new I_Object[]{test}, Bool.class);
			if(ok.getValue())
				result++;
		}
		return new Int(result);
	}

	/**
	 * °asStr()List # Returns a List where all items are converted to Str.
	 * °asInt()List # Returns a List where all items are converted to Int.
	 * °asDec()List # Returns a List where all items are converted to Dec.
	 * °asDouble()List # Returns a List where all items are converted to Double.
	 * °asBool()List # Returns a List where all items are converted to Bool.
	 * °asChar()List # Returns a List where all items are converted to Char.
	 * °asByte()List # Returns a List where all items are converted to Byte.
	 */
	private I_Object mAs(final CallRuntime cr, final ATOMIC type) {
		cr.argsNone();
		final SimpleList<I_Object> newList = new SimpleList<>(this.list.size());

		for(final I_Object obj : this.list) {
			I_Object conv = obj;
			if(obj != Nil.NIL)
				if(obj instanceof I_Atomic)
					conv = Lib_AtomConv.convert(cr, (I_Atomic)obj, type);
				else if(type == ATOMIC.STR)
					conv = new Str(Lib_Convert.getStringValue(cr, obj));
				else
					throw new RuntimeError(cr, "Conversion error", "Item in list is not an atomic value: " + obj.toString());
			newList.add(conv);
		}
		return new JMo_List(newList);
	}

	/**
	 * °avg ^ average
	 * °average()Double # Calculates the average of all numbers. Objects that could not be converted to a decimal number will be ignored.
	 */
	private I_Object mAverage(final CallRuntime cr) {
		cr.argsNone();
		double result = 0d;
		for(final I_Object o : this.list)
			try {
				final double d = Lib_Convert.getDoubleValue(cr, o);
				result += d / this.list.size();
			}
			catch(final RuntimeError re) {}
		return new JMo_Double(result); // Dec ?!?
	}

	/**
	 * °begin(Object o)Same # Insert object 'o' at the first position.
	 */
	private JMo_List mBegin(final CallRuntime cr) {
		final I_Object o = cr.args(this, (Class<?>)null)[0];
		this.iFixedLength(cr);
		this.iFixedType(cr, o);

		this.list.add(0, o);
		return this;
	}

	/**
	 * °clear()Same # Removes all items from this list.
	 */
	private JMo_List mClear(final CallRuntime cr) {
		cr.argsNone();
		this.iFixedLength(cr);
		this.list.clear();
		return this;
	}

	/**
	 * °++^concat
	 * °join^concat
	 * °concat(List other)List # Creates a new list with current items and the items of the other list.
	 * // concatenate
	 */
	private JMo_List mConcat(final CallRuntime cr) {
		final JMo_List list2 = (JMo_List)cr.args(this, JMo_List.class)[0];
		final SimpleList<I_Object> newList = new SimpleList<>();
		newList.addAll(this.list);
		newList.addAll(list2.getInternalCollection());
		return new JMo_List(newList);
	}

	/**
	 * °contains(Object search)Bool # Returns 'true' if the list contains the searched object.
	 */
	private Bool mContains(final CallRuntime cr) {
		final I_Object o = cr.args(this, I_Object.class)[0];
		for(final I_Object lo : this.list)
			if(lo.equals(o))
				return Bool.TRUE;
		return Bool.FALSE;
	}

	/**
	 * °count(Object o)Int # Returns the number of all occurrences of this object.
	 */
	private I_Object mCount(final CallRuntime cr) {
		final I_Object search = cr.args(this, I_Object.class)[0];
		int result = 0;
		for(final I_Object o : this.list)
			if(o.equals(search))
				result++;
		return new Int(result);
	}

	/**
	 * °cut(Int start, Int len)List # Cut a part of this list, starting from 'start' with a length of 'len'.
	 */
	private JMo_List mCut(final CallRuntime cr) {
		final I_Object[] oa = cr.args(this, Int.class, Int.class);
		final int start = ((Int)oa[0]).getValue();
		int len = ((Int)oa[1]).getValue();
		Lib_Error.ifEmpty(cr, this.list.size(), "List");
		Lib_Error.ifNotBetween(cr, 1, this.list.size(), start, "start");
//		Lib_Error.ifTooLow(cr, 0, len);
		if(len == 0)
			return new JMo_List(new SimpleList<>());
		if(len > this.list.size())
			len = this.list.size() - start + 1;
		return this.iCutCopy(cr, start, Math.min(this.list.size(), start + len - 1));
	}

	/**
	 * °detach(Int pos)Object # Remove object at position and return it.
	 * °detachFirst()Object # Remove first object from this list and return it.
	 * °detachLast()Object # Remove last object from this list and return it.
	 */
	private I_Object mDetach(final CallRuntime cr, final Integer pre) {
		int arg = 0;

		if(pre == null) {
			final Int o = (Int)cr.args(this, Int.class)[0];
			arg = Lib_Convert.getIntValue(cr, o);
		}
		else {
			cr.argsNone();
			arg = pre;
		}
		this.iFixedLength(cr);
		final int pos = Lib_Sequence.realPosition(cr, arg, this.list.size(), false);
		return this.list.remove(pos - 1);
	}

	/**
	 * °/^div
	 * °div(IntNumber divisor)List # Divide this list in peaces.
	 */
	private JMo_List mDivide(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int divisor = Lib_Convert.getIntValue(cr, arg);
		final SimpleList<I_Object> newList = new SimpleList<>();
		final int len = this.list.size();

		if(len % divisor != 0)
			throw new RuntimeError(cr, "Invalid length of list", "A List can only divided in equal peaces! " + len + " / " + divisor + " = " + len / divisor);

		final int part = len / divisor;
		SimpleList<I_Object> partList = null;

		for(final I_Object o : this.list) {
			if(partList == null)
				partList = new SimpleList<>();
			partList.add(o);

			if(partList.size() == part) {
				newList.add(new JMo_List(partList));
				partList = null;
			}
		}
		return new JMo_List(newList);
	}

	/**
	 * °each()%Object # Walk throw each item
	 */
	private ObjectCallResult mEach(final CallRuntime crOld) {
		crOld.argsNone();

		if(crOld.getStream() == null && crOld.getCallBlock() == null)
			throw new CodeError(crOld, "No Stream or Block for 'each'", null);

		final LoopHandle handle = new LoopHandle(this);
		final CallRuntime crNew = crOld.copyLoop(handle);
		I_Object result = this;

		for(final I_Object it : this.list) {
			handle.startLap();
			result = Lib_Exec.execBlockStream(crNew, it);

			if((result = Lib_Exec.loopResult(result)) instanceof Return)
				return ((Return)result).getLoopResult();
		}
		return new ObjectCallResult(result, true);
	}

	/**
	 * °eachVertical()%List # If this list contains other lists or ranges, walk through each row like a table.
	 */
	private ObjectCallResult mEachVertical(final CallRuntime crOld) {
		crOld.argsNone();

		if(crOld.getStream() == null && crOld.getCallBlock() == null)
			throw new CodeError(crOld, "No Stream or Block for 'eachAll'", null);

		final LoopHandle handle = new LoopHandle(this);
		final CallRuntime crNew = crOld.copyLoop(handle);
		I_Object result = this;

		int maxItems = 1;

		for(int pos = 1; pos <= maxItems; pos++) {
			handle.startLap();
			final SimpleList<I_Object> cur_al = new SimpleList<>(this.list.size());

			for(I_Object item : this.list) {
				item = Lib_Convert.getValue(crNew, item); // TODO hm ... necessary?

				if(item instanceof I_AutoBlockList) {
					final SimpleList<I_Object> abl = ((I_AutoBlockList)item).autoBlockToList(crNew.copyNull());
					final int len = abl.size();
					if(len > maxItems)
						maxItems = len;
					final I_Object o = len >= pos ? abl.get(pos - 1) : Nil.NIL;
					cur_al.add(o);
				}
				else
					cur_al.add(pos == 1 ? item : Nil.NIL);
			}
			final JMo_List cur = new JMo_List(cur_al);
			result = Lib_Exec.execBlockStream(crNew, cur);

			if((result = Lib_Exec.loopResult(result)) instanceof Return)
				return ((Return)result).getLoopResult();
		}
		return new ObjectCallResult(result, true);
	}

	/**
	 * °end(Int pos)List # Return a new List with all items from first to position 'pos'.
	 */
	private JMo_List mEndPos(final CallRuntime cr) {
		final I_Object o = cr.args(this, Int.class)[0];
		final int arg = ((Int)o).getValue();
		Lib_Error.ifNotBetween(cr, 1, this.list.size(), arg, "position");
		return this.iCutCopy(cr, 1, arg);
	}

	/**
	 * °fill(Str form)Str # Fills all {}-placeholders of form with values from current list
	 */
	private Str mFill(final CallRuntime cr) {
		final String form = ((Str)cr.args(this, Str.class)[0]).getValue();
		return Lib_StrFormat.fill(cr, form, this.list.iterator());
	}

	/**
	 * °filter(Object each)§List # Creates a new list with selected items. A FuncLet will be executed, result of 'each' must be Bool.
	 * °filter(VarLet var, Object each)§List # Creates a new list with selected items. Each item will be assigned to 'var' and 'each' is processed. A FuncLet will be executed. The result of 'each' must be Bool.
	 */
	private JMo_List mFilter(final CallRuntime cr, final String method) {
		Lib_Error.ifArgs(cr.argCount(), 1, 2, cr, this);
		final SimpleList<I_Object> result = new SimpleList<>(this.list.size());

		for(int p = 0; p < this.list.size(); p++) {
			final I_Object test = this.list.get(p);
			// MOut.temp(test, cr.call.gInternPars());
			// Bool ok = (Bool)cr.args(test, Bool.class)[0];
			final Bool ok = (Bool)cr.copyEach(method).argsEach(this, 0, new I_Object[]{test}, Bool.class);
			if(ok.getValue())
				result.add(test);
		}
		return new JMo_List(result);
	}

	/**
	 * °fixType(Str type)Same # Fix the type for all elements in this List
	 */
	private void mFixType(final CallRuntime cr) {
		final Str o = (Str)cr.args(this, Str.class)[0];
		final String type = Lib_Convert.getStringValue(cr, o);
		if(!ObjectManager.isTypeKnown(cr, type))
			throw new RuntimeError(cr, "Unknown type", "Unknown type: <" + type + '>');
		this.fixedType = type;

		int counter = 0;

		for(final I_Object item : this.list) {
			counter++;

			if(!Lib_Type.isInstanceOf(item, this.fixedType)) {
//				final String sTypes = Lib_Type.typesToString(item);
				final String sTypes = "<" + item.getTypeName() + '>';
				throw new RuntimeError(cr, "Invalid type of item in list", "Type <" + this.fixedType + "> needed, but type of item at position " + counter + " is: " + sTypes);
			}
		}
	}

	/**
	 * °flatten()List # Create a copy of the list, where the contents of all included lists are copied directly into the new list.
	 * TODO Flatten Sequence? Set, ByteArray, ...
	 */
	private JMo_List mFlatten(final CallRuntime cr) {
		cr.argsNone();
		final SimpleList<I_Object> result = new SimpleList<>();
		this.internalFlatten(result, this);
		return new JMo_List(result);
	}

	/**
	 * °hasNext()Bool # Returns true if this list is not empty.
	 */
	private Bool mHasNext(final CallRuntime cr) {
		cr.argsNone();
		return Bool.getObject(!this.sequenceEmpty());
	}

	/**
	 * #°combine ^ implode
	 * °implode()Str # Combine all elements in this list to a string.
	 * °implode(Atomic delimiter)Str # Combines all elements in this list to a string, separated through 'delimiter'.
	 */
	private Str mImplode(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 0, 1);

		if(args.length == 0) {
			final StringBuilder sb = new StringBuilder();
			for(final I_Object o : this.list)
				sb.append(o.toString());
			return new Str(sb.toString());
		}
		// if(args.length==1) {
		final I_Atomic atomic = (I_Atomic)cr.argType(args[0], I_Atomic.class);
		final String delimiter = Lib_Convert.getStringValue(cr, atomic);
		final StringBuilder sb = new StringBuilder();

		for(int i = 0; i < this.list.size(); i++) {
			final I_Object o = this.list.get(i);
			final String s = Lib_Convert.getStringValue(cr, o);
			sb.append(s);
			if(i != this.list.size() - 1)
				sb.append(delimiter);
		}
		return new Str(sb.toString());
	}

	/**
	 * °init(IntNumber amount, Object value)Same # Add 'amount' objects of 'value' to this List.
	 */
	private void mInitList(final CallRuntime cr) {
		final I_Object[] args = cr.args(this, A_IntNumber.class, I_Object.class);
		final I_Object obj = args[1];
		final int num = Lib_Convert.getIntValue(cr, args[0]);
		this.list.ensureCapacity(this.list.size() + num);

		for(int i = 0; i < num; i++)
			this.list.add(obj);
	}

//	private Obj_String sub(S_Object[] oa) {
//		MOut.dev(this.value, (Object)oa);
//		Err.ifNot(1, oa.length);
//		if(oa[0].getClass() == Obj_String.class)
//			return new Obj_String( this.value - ((Obj_String)oa[0]).gValue() );
//
//		throw Err.todo(oa[0].getClass());
//	}
//
//	public Obj_List multi(S_Object[] oa) {
//		Err.ifNot(1, oa.length);
//		if(oa[0].getClass() == Obj_Integer.class) {
//			StringBuffer sb=new StringBuffer();
//			int count=((Obj_Integer)oa[0]).gValue();
//			for(int i=0; i<count; i++)
//				sb.append(this.gValue());
//			return new Obj_List(sb.toString());
//		}
//
//		throw Err.todo(oa[0].getClass());
//	}

	/**
	 * °insert(Object o, IntNumber position)Same # Insert object 'o' at the specified position.
	 */
	private JMo_List mInsert(final CallRuntime cr) {
		final I_Object[] oa = cr.args(this, null, A_IntNumber.class);
		this.iFixedLength(cr);
		this.iFixedType(cr, oa);

		final int arg = Lib_Convert.getIntValue(cr, oa[1]);
		final int pos = Lib_Sequence.realPosition(cr, arg, this.list.size() + 1, false);
		this.list.add(pos - 1, oa[0]);
		return this;
	}

	/**
	 * °left(Int count)List # Returns a new List with 'count' items from the left (start of this List).
	 *
	 * TODO: Hard or soft?!? .left(0) / .left(1000)
	 */
	private JMo_List mLeft(final CallRuntime cr) {
		final I_Object o = cr.args(this, Int.class)[0];
		final int left = ((Int)o).getValue();
		Lib_Error.ifTooSmall(cr, 0, left);
		if(left == 0)
			return new JMo_List(new SimpleList<>());
		if(left > this.list.size())
			return new JMo_List(this.list.copy());
//		Lib_Error.ifTooHigh(cr, list.size(), left);
		return this.iCutCopy(cr, 1, Math.min(left, this.list.size()));
	}

	/**
	 * °map(Object each)§List # Creates a copy of the list, and execute 'each' with every item. A FuncLet will be executed, result of 'each' must be Bool.
	 * °map(VarLet var, Object each)§List # Creates a copy of the list, and execute 'var' and 'proc' with every item.
	 */
	private JMo_List mMap(final CallRuntime cr, final String method) {
		Lib_Error.ifArgs(cr.argCount(), 1, 2, cr, this);
		final SimpleList<I_Object> result = new SimpleList<>(this.list.size());

		for(int p = 0; p < this.list.size(); p++) {
			final I_Object test = this.list.get(p);
			final I_Object[] each = {test};
			final I_Object testr = cr.copyEach(method).argsEach(this, 0, each, I_Object.class);
			result.add(testr);
		}
		return new JMo_List(result);
	}

	/**
	 * °max()Object? # Returns the largest atomic value. Objects that could not be converted to a decimal number will be ignored.
	 */
	private I_Object mMax(final CallRuntime cr) {
		cr.argsNone();
		I_Object result_o = null;
		Double result_d = null;

		for(final I_Object o : this.list)
			try {
				final double d = Lib_Convert.getDoubleValue(cr, o);

				if(result_o == null || d > result_d) {
					result_o = o;
					result_d = d;
				}
			}
			catch(final RuntimeError re) {}

		return result_o == null
			? Nil.NIL
			: result_o;
	}

	/**
	 * °median()Dec # Returns the median, the central value of an ordered list.
	 */
	private JMo_Dec mMedian(final CallRuntime cr) {
		cr.argsNone();
		final int size = this.list.size();
		if(size == 0)
			return new JMo_Dec(Dec.ZERO);

		final SimpleList<Double> copy = new SimpleList<>(size);
		for(int i = 0; i < size; i++)
			copy.add(Lib_Convert.getDoubleValue(cr, this.list.get(i)));

		copy.sort((d1, d2)-> {
			return d1 == d2 ? 0 : d1 > d2 ? +1 : -1;
		});

		double result = 0d;

		if(copy.size() % 2 == 1)
			result = copy.get(copy.size() / 2);
		else {
			final double d1 = copy.get(copy.size() / 2 - 1);
			final double d2 = copy.get(copy.size() / 2);
			result = d1 / 2 + d2 / 2;
		}

		return JMo_Dec.valueOf(cr, result);
	}

	/**
	 * °+^merge
	 * °merge(Object other)List # Merges current items and 'other' in a new list.
	 */
	private JMo_List mMerge(final CallRuntime cr) {
		final I_Object other = cr.args(this, I_Object.class)[0];
		final SimpleList<I_Object> newList = new SimpleList<>(this.list.size() + 1);

		newList.addAll(this.list);
		newList.add(other);

		return new JMo_List(newList);
	}

	/**
	 * °min()Object? # Returns the smallest atomic value. Objects that could not be converted to a decimal number will be ignored.
	 */
	private I_Object mMin(final CallRuntime cr) {
		cr.argsNone();
		I_Object result_o = null;
		Double result_d = null;

		for(final I_Object o : this.list)
			try {
				final double d = Lib_Convert.getDoubleValue(cr, o);

				if(result_o == null || d < result_d) {
					result_o = o;
					result_d = d;
				}
			}
			catch(final RuntimeError re) {}

		return result_o == null
			? Nil.NIL
			: result_o;
	}

	/**
	 * °*^mul
	 * °mul(IntNumber)List # Multiplys the current List to create a multidimension list
	 */
	private JMo_List mMultiply(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int value = Lib_Convert.getIntValue(cr, arg);
		Lib_Error.ifTooSmall(cr, 1, value);

		final SimpleList<I_Object> result = new SimpleList<>(value);
		for(int i = 0; i < value; i++)
			result.add(new JMo_List(this.list.copy()));

		return new JMo_List(result);
	}

	/**
	 * °only(Object o, ...)List # Creates a new List with only the selected Items.
	 * Regex can be used.
	 */
	private JMo_List mOnly(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 0);

		final SimpleList<I_Object> result = new SimpleList<>();

		for(int p = 0; p < this.list.size(); p++) {
			final I_Object test = this.list.get(p);
			for(final I_Object arg : args)
				if(arg instanceof JMo_RegEx) {
					final JMo_RegEx regex = (JMo_RegEx)arg;
					final String teststr = Lib_Convert.getStringValue(cr, test);
					if(teststr.matches(regex.getValue()))
						result.add(test);
				}
				else if(arg instanceof Str) {
					final String search = Lib_Convert.getStringValue(cr, arg);
					final String teststr = Lib_Convert.getStringValue(cr, test);
					if(teststr.equals(search))
						result.add(test);
				}
				else if(test.compareTo(cr, arg) == 0)
					result.add(test);
		}
		return new JMo_List(result);
	}

	/**
	 * °part(Int start, Int end)List # Return a new List with all items from position 'start' up to position 'end'.
	 */
	private JMo_List mPart(final CallRuntime cr) {
		final I_Object[] oa = cr.args(this, Int.class, Int.class);
		final int start = ((Int)oa[0]).getValue();
		final int end = ((Int)oa[1]).getValue();
		final int size = this.list.size();
		Lib_Error.ifEmpty(cr, this.list.size(), "List");
		Lib_Error.ifNotBetween(cr, 1, Math.min(end, size), start, "start");
		Lib_Error.ifNotBetween(cr, Math.max(1, start), size, end, "end");
		return this.iCutCopy(cr, start, Math.min(this.list.size(), end));
	}

	/**
	 * °reduce(Object init, Object each)§Object # Reduces the list to a single value.
	 * °reduce(Object init, VarLet var, Object each)§Object # Reduces the list to a single value.
	 */
	private I_Object mReduce(final CallRuntime cr, final String method) {
		Lib_Error.ifArgs(cr.argCount(), 2, 3, cr, this);
		I_Object sum = cr.argsOneAdvance(this, 0, I_Object.class);

		for(final I_Object test : this.list) {
			final I_Object[] each = {sum, test};
//			final Call c = new Call(cr.getSurrBlock(), null, method, cr.call.parCalls, cr.getDebugInfo());
//			sum = cr.copyCall(c, false).parsEach(this, 1, test2, I_Object.class);
			sum = cr.copyEach(method).argsEach(this, 1, each, I_Object.class);
		}
		return sum;
	}

	/**
	 * °subAll ^ removeAll
	 * #°subtractAll ^ removeAll
	 * °removeAll(List other)Same # Removes all objects from this list, that are found in the second list.
	 */
	private JMo_List mRemoveAll(final CallRuntime cr) {
		final JMo_List list2 = (JMo_List)cr.args(this, JMo_List.class)[0];
		this.iFixedLength(cr);
		this.list.removeAll(list2.list);
		return this;
	}

	/**
	 * °remove(Object o...)Same # Remove all elements that match to an argument
	 */
	private JMo_List mRemoveItems(final CallRuntime cr) {
		final I_Object[] oa = cr.argsVar(this, 1, 0);
		this.iFixedLength(cr);

		for(final I_Object o : oa)
			for(int i = this.list.size() - 1; i >= 0; i--)
				if(this.list.get(i).equals(o))
					this.list.remove(i);
		return this;
	}

	/**
	 * °delete(Int pos)Same # Remove item at position
	 */
	private JMo_List mRemovePos(final CallRuntime cr) {
		final Int arg = (Int)cr.args(this, Int.class)[0];
		this.iFixedLength(cr);

		final int argv = Lib_Convert.getIntValue(cr, arg);
		final int pos = Lib_Sequence.realPosition(cr, argv, this.list.size(), false);
		this.list.remove(pos - 1);
		return this;
	}

	/**
	 * °reverse()Same # Inverts this list, so that the first item is the last and the last is the first
	 */
	private JMo_List mReverse(final CallRuntime cr) {
		cr.argsNone();
		this.iFixedValues(cr);
		final SimpleList<I_Object> dest = new SimpleList<>();
		for(final I_Object o : this.list)
			dest.add(0, o); // Theres maybe a better solutions
		this.list = dest;
		return this;
	}

	/**
	 * °right(Int count)List # Returns a new List with 'count' items from the right (end of this List).
	 *
	 * TODO: Hard or lazy?!? .left(0) / .left(1000)
	 */
	private JMo_List mRight(final CallRuntime cr) {
		final I_Object o = cr.args(this, Int.class)[0];
		final int right = ((Int)o).getValue();
		Lib_Error.ifTooSmall(cr, 0, right);
		if(right == 0)
			return new JMo_List(new SimpleList<>());
		if(right > this.list.size())
			return new JMo_List(this.list.copy());
//		Lib_Error.ifTooHigh(cr, list.size(), right);
		return this.iCutCopy(cr, Math.max(1, this.list.size() - right + 1), this.list.size());
	}

	/**
	 * °search ^ searchFirst
	 * °searchFirst(Object search)Int? # Returns the first position of this object, 'nil' if nothing is found.
	 * °searchLast(Object search)Int? # Returns the last position of this object, 'nil' if nothing is found.
	 */
	private A_Immutable mSearch(final CallRuntime cr, final boolean first) {
		final I_Object o = cr.args(this, I_Object.class)[0];

		if(first) {
			for(int i = 0; i < this.list.size(); i++)
				if(this.list.get(i).compareTo(cr, o) == 0)
					return new Int(i + 1);
		}
		else
			for(int i = this.list.size() - 1; i >= 0; i--)
				if(this.list.get(i).compareTo(cr, o) == 0)
					return new Int(i + 1);
		return Nil.NIL;
	}

	/**
	 * °shuffle()Same # Shuffle the values in this List
	 */
	private JMo_List mShuffle(final CallRuntime cr) {
		cr.argsNone();
		this.iFixedValues(cr);
		final int size = this.list.size();
		final int[] rnd = Lib_Random.getIntArraySet(0, size - 1);
		final SimpleList<I_Object> dest = new SimpleList<>(size);
		for(final int i : rnd)
			dest.add(this.list.get(i));
		this.list = dest;
		return this;
	}

	/**
	 * °intersect ^ slice
	 * °slice(List other)List # Creates a new list with the intersection of both lists.
	 */
	private JMo_List mSlice(final CallRuntime cr) {
		final JMo_List list2 = (JMo_List)cr.args(this, JMo_List.class)[0];
		final SimpleList<I_Object> list3 = new SimpleList<>();
		for(final I_Object o1 : this.list)
			if(list2.list.contains(o1))
				list3.add(o1);
		return new JMo_List(list3);
	}

	/**
	 * °sort()Same # Sort the list
	 * °sort(Bool each)Same # Sort this list by passing every pair as a List to 'each' and fetch the boolean result.
	 * °sort(FuncLet fl)Same # Sort this list by passing every pair as a List to 'fl' and fetch the boolean result.
	 * °sort(VarLet var, Bool b)Same # Sort this list by assign every pair as a List to 'var' and fetch the the result of 'b'.
	 */
	private JMo_List mSort(final CallRuntime cr, final String method) {
		final int parCount = cr.argCount();
		Lib_Error.ifArgs(parCount, 0, 2, cr, this);
		this.iFixedValues(cr);

		if(parCount == 0) {
			cr.argsNone();

			this.list.sort((o1, o2)-> {
				Object so1 = o1;
				Object so2 = o2;

				if(o1 instanceof I_Atomic)
					so1 = ((I_Atomic)o1).getValue();
				if(o2 instanceof I_Atomic)
					so2 = ((I_Atomic)o2).getValue();

				return Lib_Compare.isEqual(so1, so2)
					? 0
					: Lib_Compare.isGreater(so1, so2)
						? +1
						: -1;
			});

			return this;
		}
		int smallest;

		for(int l = 1; l < this.list.size(); l++) {
			smallest = l;

			for(int m = l + 1; m <= this.list.size(); m++) {
				final I_Object buffer1 = this.list.get(smallest - 1);
				final I_Object buffer2 = this.list.get(m - 1);
				final I_Object[] test = {buffer1, buffer2};

//				final Call c = new Call(cr.getSurrBlock(), null, method, cr.call.parCalls, cr.getDebugInfo());
//				final Bool correct = (Bool)cr.copyCall(c, false).parsEach(this, 0, test, Bool.class);
				final Bool correct = (Bool)cr.copyEach(method).argsEach(this, 0, test, Bool.class);
				if(!correct.getValue())
					smallest = m;
			}

			// Swap
			if(l != smallest) {
				final int pos1 = smallest - 1;
				final int pos2 = l - 1;
				final I_Object buffer1 = this.list.get(pos1);
				final I_Object buffer2 = this.list.get(pos2);
				this.list.set(pos1, buffer2);
				this.list.set(pos2, buffer1);
			}
		}
		return this;
	}

	/**
	 * °start(Int pos)List # Return a new List with all items from position 'pos' to the end.
	 */
	private JMo_List mStartPos(final CallRuntime cr) {
		final I_Object o = cr.args(this, Int.class)[0];
		final int arg = ((Int)o).getValue();
		final int size = this.list.size();
		Lib_Error.ifNotBetween(cr, 1, size, arg, "position");
		return this.iCutCopy(cr, arg, size);
	}

	/**
	 * °store(VarLet v, ...)Same # Store all values in variables
	 */
	private JMo_List mStore(final CallRuntime cr) {
		// Direct access, cause the raw "Var" is needed
//		Lib_Par.checkBlock(cr, false);
		final I_Object[] args = cr.getArgs(this);

		if(args.length != this.list.size())
			throw new CodeError(cr, "Invalid amount of arguments", "Got " + args.length + ", need equal to listsize: " + this.list.size());

		for(int pi = 0; pi < this.list.size(); pi++) {
			final I_Object arg = cr.argTypeExt(args[pi], A_MemLet.class, Nil.class);

			if(arg != Nil.NIL) {
				final I_Mem v = ((A_MemLet)arg).getMem();
				v.let(cr, cr, this.list.get(pi));
			}
		}
		return this;
	}

	/**
	 * °-^sub
	 * °sub(Object other)List # Create a new list with removed item.
	 */
	private JMo_List mSubtract(final CallRuntime cr) {
		final I_Object other = cr.args(this, I_Object.class)[0];
		final SimpleList<I_Object> newList = new SimpleList<>();

		for(final I_Object o : this.list)
			if(!o.equals(other))
				newList.add(o);

		return new JMo_List(newList);
	}

	/**
	 * °sum()Double # Adds up all the objects in this list.
	 */
	private I_Object mSum(final CallRuntime cr) {
		cr.argsNone();
		double result = 0d;
		for(final I_Object o : this.list)
			result += Lib_Convert.getDoubleValue(cr, o);
		return new JMo_Double(result);
	}

	/**
	 * °swap(IntNumber pos1, IntNumber pos2)Same # Swap two positions.
	 */
	private JMo_List mSwap(final CallRuntime cr) {
		final I_Object[] args = cr.args(this, A_IntNumber.class, A_IntNumber.class);
//		this.iFixed(cr);
		int pos1 = Lib_Convert.getIntValue(cr, args[0]);
		int pos2 = Lib_Convert.getIntValue(cr, args[1]);
		Lib_Error.ifNotBetween(cr, 1, this.list.size(), pos1, "position 1");
		Lib_Error.ifNotBetween(cr, 1, this.list.size(), pos2, "position 2");
		pos1--;
		pos2--;

		final I_Object buffer = this.list.get(pos1);
		this.list.set(pos1, this.list.get(pos2));
		this.list.set(pos2, buffer);
		return this;
	}

	/**
	 * °toMap(List keys)Map # Creates a new Map by adding the keys from a List
	 */
	private JMo_Map mToMap(final CallRuntime cr) {
		final JMo_List keys = (JMo_List)cr.args(this, JMo_List.class)[0];
		return new JMo_Map(keys.list.copy(), this.list.copy());
	}

	/**
	 * °toUniqueMap()Map # Generates a Map with the list items as keys and the number of occurrences as values.
	 */
	private JMo_Map mToUniqueMap(final CallRuntime cr) {
		cr.argsNone();
		final SimpleList<I_Object> keys = new SimpleList<>();
		final SimpleList<I_Object> values = new SimpleList<>();
		final boolean[] used = new boolean[this.list.size()];

		for(int p = 0; p < this.list.size() - 1; p++)
			if(!used[p]) {
				final I_Object search = this.list.get(p);
				keys.add(search);
				int amount = 1;

				for(int q = this.list.size() - 1; q > p; q--)
					if(!used[q] && this.list.get(q).equals(search)) {
						amount++;
						used[q] = true;
					}

				values.add(new Int(amount));
			}

		return new JMo_Map(keys, values);
	}

	/**
	 * °unfold()%List # Create a new list, where all 'folded' objects (like Range, List, ...) will be unfolded.
	 */
	private JMo_List mUnfold(final CallRuntime cr) {
		cr.argsNone();
		final SimpleList<I_Object> result = new SimpleList<>(this.list.size());

		for(final I_Object item : this.list)
			if(item instanceof I_AutoBlockList) {
				final SimpleList<I_Object> abl = ((I_AutoBlockList)item).autoBlockToList(cr.copyNull());
				result.addAll(abl);
			}
			else
				result.add(item);
		return new JMo_List(result);
	}

	/**
	 * °uniq ^ unique
	 * °unique()Same # Remove duplicate Items
	 */
	private JMo_List mUnique(final CallRuntime cr) {
		cr.argsNone();
		this.iFixedLength(cr);

		for(int p = 0; p < this.list.size() - 1; p++) {
			final I_Object search = this.list.get(p);
			for(int q = this.list.size() - 1; q > p; q--)
				if(this.list.get(q).equals(search))
					this.list.remove(q);
		}
		return this;
	}

	/**
	 * °-- ^ without
	 * °without(List other)List # Creates a new List, where all items from 'other' are removed.
	 */
	private JMo_List mWithout(final CallRuntime cr) {
		final JMo_List other = (JMo_List)cr.args(this, JMo_List.class)[0];
		final I_Series<? extends I_Object> other2 = ((A_Sequence)other).getInternalCollection();
		final SimpleList<I_Object> newList = new SimpleList<>();

		for(final I_Object o : this.list)
			if(!other2.contains(o))
				newList.add(o);
		// No error, if items in 'other aren't in current list

		return new JMo_List(newList);
	}

}
