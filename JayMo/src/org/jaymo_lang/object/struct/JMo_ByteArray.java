/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.struct;

import java.nio.charset.Charset;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.JMo_Charset;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.I_Integer;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_Byte;
import org.jaymo_lang.object.atom.JMo_Long;
import org.jaymo_lang.object.atom.JMo_Short;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Exec;
import org.jaymo_lang.util.Lib_Output;
import org.jaymo_lang.util.Lib_Sequence;

import de.mn77.base.data.numsys.Lib_Hex;
import de.mn77.base.data.struct.I_Series;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 13.03.2021
 *
 * @apiNote The size of the byte[] is fix, the values can be changed
 */
public class JMo_ByteArray extends A_Sequence {

	private byte[]              data;
	private final ArgCallBuffer arg1;


	public JMo_ByteArray(final byte[] ba) {
		this.data = ba;
		this.arg1 = null;
	}

	public JMo_ByteArray(final Call arg) {
		this.arg1 = new ArgCallBuffer(0, arg);
	}

	public JMo_ByteArray(final int size) {
		this.data = new byte[size];
		this.arg1 = null;
	}

	@Override
	public boolean equals(final Object obj) {

		if(obj instanceof JMo_ByteArray) {
			final byte[] other = ((JMo_ByteArray)obj).data;
			if(this.data.length != other.length)
				return false;
			for(int i = 0; i < this.data.length; i++)
				if(this.data[i] != other[i])
					return false;
			return true;
		}
		else
			return false;
	}

	@Override
	public boolean equalsLazy(final Object obj) {
		return this.equals(obj);
	}

	@Override
	public I_Series<? extends I_Object> getInternalCollection() {
//		throw new Err_Runtime("");
		throw Err.invalid();
	}

	public byte[] getValue() {
		return this.data;
	}

	@Override
	public void init(final CallRuntime cr) {

		if(this.arg1 != null) {
			final A_IntNumber a1 = this.arg1.init(cr, this, A_IntNumber.class);
			final int size = Lib_Convert.getIntValue(cr, a1);
			this.data = new byte[size];
		}
	}

	public I_Object sequenceDeepGet(final CallRuntime cr, final I_Object[] keys, final int offset, final boolean lazy) {
		throw Err.invalid(keys, offset, lazy);
	}

	public void sequenceDeepSet(final CallRuntime cr, final I_Object[] keys, final int offset, final I_Object value, final boolean lazy) {
		throw Err.invalid(keys, offset, value, lazy);
	}

	@Override
	public String toString() {
		return this.getTypeName() + '<' + this.data.length + '>';
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {

		switch(type) {
			case NESTED:
			case IDENT:
				return this.toString();
			default:
				final StringBuilder sb = new StringBuilder();
				sb.append(this.getTypeName());
				sb.append('[');

				for(final byte element : this.data) {
					sb.append("" + element);
					if(type == STYPE.DESCRIBE)
						sb.append('b');
					sb.append(",");
				}
				Lib_Output.removeEnd(sb, ',');

				sb.append("]");
				return sb.toString();
		}
	}

	@Override
	protected ObjectCallResult call3(final CallRuntime cr, final String method) {

		switch(method) {
			case "toList":
				return A_Object.stdResult(this.mToList(cr));

			case "toByte":
				return A_Object.stdResult(this.mToIntNumber(cr, 1));
			case "toShort":
				return A_Object.stdResult(this.mToIntNumber(cr, 2));
			case "toInt":
				return A_Object.stdResult(this.mToIntNumber(cr, 4));
			case "toLong":
				return A_Object.stdResult(this.mToIntNumber(cr, 8));

//			case "toHex":
			case "toStrHex":
				return A_Object.stdResult(this.mToStrHex(cr));
//			case "toStrUTF8":
//			case "toStrUTF16":
//			case "toStrASCII":
			case "arrange": // convert, compose, asStr?
				return A_Object.stdResult(this.mArrange(cr));

			case "init":
			case "fill":
				return A_Object.stdResult(this.mInit(cr));

			case "cut":
				return A_Object.stdResult(this.mCut(cr)); // from, length
			case "part":
				return A_Object.stdResult(this.mPart(cr)); // from, to
			case "left":
				return A_Object.stdResult(this.mLeft(cr));
			case "right":
				return A_Object.stdResult(this.mRight(cr));
			case "start":
				return A_Object.stdResult(this.mStartPos(cr));
			case "end":
				return A_Object.stdResult(this.mEndPos(cr)); // TODO .end(-2)

			case "expandLeft":
				return A_Object.stdResult(this.mExpand(cr, false));
			case "expandRight":
				return A_Object.stdResult(this.mExpand(cr, true));

			case "+":
//			case "join":
			case "add":
			case "append":
				return A_Object.stdResult(this.mAdd(cr));
			case "-":
//			case "sub":
			case "delete":
				return A_Object.stdResult(this.mRemovePos(cr));

			case "insert":
				return A_Object.stdResult(this.mInsert(cr));
			case "begin":
				return A_Object.stdResult(this.mBegin(cr));
			case "++":
			case "concat":
				return A_Object.stdResult(this.mConcat(cr));

			case "contains":
				return A_Object.stdResult(this.mContains(cr));
			case "search":
			case "searchFirst":
				return A_Object.stdResult(this.mSearch(cr, true));
			case "searchLast":
				return A_Object.stdResult(this.mSearch(cr, false));

			case "each":
				return this.mEach(cr);
		}
		return null;
	}

	@Override
	protected A_Sequence copy(final CallRuntime cr) {
		final int len = this.data.length;
		final byte[] copy = new byte[len];
		System.arraycopy(this.data, 0, copy, 0, len);
		return new JMo_ByteArray(copy);
	}

	@Override
	protected I_Object getFirst(final CallRuntime cr) {
		return new JMo_Byte(this.data[0]);
	}

	@Override
	protected I_Object getLast(final CallRuntime cr) {
		return new JMo_Byte(this.data[this.data.length - 1]);
	}

	@Override
	protected void mSequenceSetPut(final CallRuntime cr, final boolean lazy) {
		final I_Object[] oa = cr.args(this, JMo_Byte.class, I_Integer.class); //.parsVarArgs(this, 2, 1);

//		if(oa.length > 2)
//			this.sequenceDeepSet(cr, oa, 1, oa[0], lazy);
//		else
		this.sequenceSet(cr, oa[1], oa[0], lazy);
	}

	@Override
	protected void sequenceContent(final CallRuntime cr, final I_Object[] values) {

		for(int i = 0; i < this.data.length; i++) {
			final A_IntNumber in = (A_IntNumber)cr.argType(values[i], A_IntNumber.class);
			this.data[i] = Lib_Convert.getByteValue(cr, in);
		}
	}

	@Override
	protected boolean sequenceEmpty() {
		return this.data.length == 0;
	}

	@Override
	protected I_Object sequenceGetPull(final CallRuntime cr, final boolean lazy, final I_Object arg) {
		final I_Atomic oav = (I_Atomic)cr.argType(arg, I_Atomic.class);
		final int oi = Lib_Convert.getIntValue(cr, oav);
		final int pos = Lib_Sequence.realPosition(cr, oi, this.data.length, lazy);
		return pos == -1 ? Nil.NIL : new JMo_Byte(this.data[pos - 1]);
	}

	@Override
	protected I_Object sequenceSelectGet(final CallRuntime cr, final I_Object key, final boolean lazy) {
		final int oi = Lib_Convert.getIntValue(cr, key);
		final int pos = Lib_Sequence.realPosition(cr, oi, this.data.length, lazy);
		return pos == -1 ? Nil.NIL : new JMo_Byte(this.data[pos - 1]);
	}

	@Override
	protected void sequenceSet(final CallRuntime cr, final I_Object posObj, final I_Object obj, final boolean lazy) {
		final int posWanted = Lib_Convert.getIntValue(cr, posObj);
		cr.argType(obj, JMo_Byte.class);
		final byte value = Lib_Convert.getByteValue(cr, obj);

		if(lazy && Math.abs(posWanted) > this.data.length)
			throw new RuntimeError(cr, "Cannot expand a ByteArray!", "Size is: " + this.data.length + "  Wanted position is: " + posWanted);
		final int posReal = Lib_Sequence.realPosition(cr, posWanted, this.data.length, lazy);
		Lib_Error.ifNotBetween(cr, 1, this.data.length, posReal, "position");
		this.data[posReal - 1] = value;

//		I_Object[] setArgs = cr.args(this, I_Integer.class, JMo_Byte.class);
//		this.data[ Lib_Convert.getIntValue(cr, setArgs[0])] = Lib_Convert.getByteValue(cr, setArgs[1]);
	}

	@Override
	protected int sequenceSize() {
		return this.data.length;
	}

	private JMo_ByteArray iCutCopy(final CallRuntime cr, final int start, final int end) {
		Lib_Error.ifTooSmall(cr, start, end);
		final int newLen = end - start + 1;
		final byte[] result = new byte[newLen];
		System.arraycopy(this.data, start - 1, result, 0, newLen);
		return new JMo_ByteArray(result);
	}

	/**
	 * °+^add
	 * °append^add
	 * °add(Byte... b)ByteArray # Create a new ByteArray, with added bytes.
	 */
	private JMo_ByteArray mAdd(final CallRuntime cr) {
		final I_Object[] oa = cr.argsVar(this, 1, 0);
		final byte[] baNew = new byte[this.data.length + oa.length];
		System.arraycopy(this.data, 0, baNew, 0, this.data.length);

		for(int i = 0; i < oa.length; i++) {
			final byte b = ((JMo_Byte)cr.argType(oa[i], JMo_Byte.class)).getValue();
			baNew[this.data.length + i] = b;
		}
		return new JMo_ByteArray(baNew);
	}

	/**
	 * #°toStrUTF8()Str # Converts this ByteArray to a String by using a UTF-8 Charset.
	 * °arrange()Str # Converts this ByteArray to a String by using the default Charset.
	 * °arrange(Charset cs)Str # Converts this ByteArray to a String by using a Charset.
	 */
	private Str mArrange(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 0, 1);

		if(args.length == 0)
			return new Str(new String(this.data));
		else {
			final JMo_Charset charset = (JMo_Charset)cr.argType(args[0], JMo_Charset.class);
			final Charset cs = charset.getCharset();
			return new Str(new String(this.data, cs));
		}
	}

	/**
	 * °begin(Byte b)ByteArray # Return a new ByteArray, where 'b' was inserted at the first position.
	 */
	private JMo_ByteArray mBegin(final CallRuntime cr) {
		final byte b = ((JMo_Byte)cr.args(this, JMo_Byte.class)[0]).getValue();
		final int len = this.data.length;
		final byte[] baNew = new byte[len + 1];
		baNew[0] = b;
		System.arraycopy(this.data, 0, baNew, 1, len);
		return new JMo_ByteArray(baNew);
	}

	/**
	 * °++^concat
	 * °concat(ByteArray other)ByteArray # Creates a new ByteArray with concated current items and the items of the other Array.
	 */
	private JMo_ByteArray mConcat(final CallRuntime cr) {
		final byte[] data2 = ((JMo_ByteArray)cr.args(this, JMo_ByteArray.class)[0]).data;
		final byte[] baNew = new byte[this.data.length + data2.length];
		System.arraycopy(this.data, 0, baNew, 0, this.data.length);
		System.arraycopy(data2, 0, baNew, this.data.length, data2.length);
		return new JMo_ByteArray(baNew);
	}

	/**
	 * °contains(Byte search)Bool # Returns true, if this ByteArray contains 'search'.
	 */
	private Bool mContains(final CallRuntime cr) {
		final byte b = ((JMo_Byte)cr.args(this, JMo_Byte.class)[0]).getValue();
		for(final byte lo : this.data)
			if(lo == b)
				return Bool.TRUE;
		return Bool.FALSE;
	}

	/**
	 * °cut(Int start, Int len)ByteArray # Return a new ByteArray from start position 'len' bytes.
	 */
	private JMo_ByteArray mCut(final CallRuntime cr) {
		final I_Object[] oa = cr.args(this, Int.class, Int.class);
		final int start = ((Int)oa[0]).getValue();
		int len = ((Int)oa[1]).getValue();
		Lib_Error.ifEmpty(cr, this.data.length, "List");
		Lib_Error.ifNotBetween(cr, 1, this.data.length, start, "start");
//		Lib_Error.ifTooLow(cr, 0, len);
		if(len == 0)
			return new JMo_ByteArray(new byte[0]);
		if(len > this.data.length)
			len = this.data.length - start + 1;
		return this.iCutCopy(cr, start, Math.min(this.data.length, start + len - 1));
	}

	/**
	 * °each()%Byte # Walk throw each item
	 */
	private ObjectCallResult mEach(final CallRuntime crOld) {
		crOld.argsNone();

		if(crOld.getStream() == null && crOld.getCallBlock() == null)
			throw new CodeError(crOld, "No Stream or Block for 'each'", null);

		final LoopHandle handle = new LoopHandle(this);
		final CallRuntime crNew = crOld.copyLoop(handle);
		I_Object result = this;

		for(final byte it : this.data) {
			handle.startLap();
			result = Lib_Exec.execBlockStream(crNew, new JMo_Byte(it));

			if((result = Lib_Exec.loopResult(result)) instanceof Return)
				return ((Return)result).getLoopResult();
		}
		return new ObjectCallResult(result, true);
	}

	/**
	 * °end(Int pos)ByteArray # Return a copy of this ByteArray, up to position 'pos'
	 */
	private JMo_ByteArray mEndPos(final CallRuntime cr) {
		final I_Object arg = cr.args(this, Int.class)[0];
		final int end = ((Int)arg).getValue();
		Lib_Error.ifNotBetween(cr, 1, this.data.length, end, "position");
		return this.iCutCopy(cr, 1, end);
	}

	/**
	 * °expandLeft(Int length)ByteArray # Create a copy of this array and add 'length' fields to the left side
	 * °expandRight(Int length)ByteArray # Create a copy of this array and add 'length' fields to the right side
	 */
	private I_Object mExpand(final CallRuntime cr, final boolean right) {
		final Int arg = (Int)cr.args(this, Int.class)[0];
		final int toAdd = arg.getValue();
		Lib_Error.ifTooSmall(cr, 1, toAdd);
		final byte[] baNew = new byte[this.data.length + toAdd];
		final int destPos = right ? 0 : toAdd;
		System.arraycopy(this.data, 0, baNew, destPos, this.data.length);
		return new JMo_ByteArray(baNew);
	}

	/**
	 * °fill ^ init
	 * °init(Byte b)Same # Init all values of this ByteArray with 'b'.
	 */
	private JMo_ByteArray mInit(final CallRuntime cr) {
		final JMo_Byte arg = (JMo_Byte)cr.args(this, JMo_Byte.class)[0];
		final byte b = Lib_Convert.getByteValue(cr, arg);
		for(int i = 0; i < this.data.length; i++)
			this.data[i] = b;
		return this;
	}

	/**
	 * °insert(Byte b, IntNumber position)ByteArray # Create new ByteArray where 'b' is inserted at 'position'
	 */
	private JMo_ByteArray mInsert(final CallRuntime cr) {
		final I_Object[] oa = cr.args(this, JMo_Byte.class, A_IntNumber.class);
		int pos = Lib_Convert.getIntValue(cr, oa[1]);
		final byte val = ((JMo_Byte)oa[0]).getValue();
		final int len = this.data.length;

		Lib_Error.ifIs(cr, 0, pos, "position");
		Lib_Error.ifNotBetween(cr, len + 1, pos, "position");

		if(pos < 0)
			pos = Lib_Sequence.realPosition(cr, pos, len, false) + 1;

		final byte[] baNew = new byte[len + 1];
		baNew[pos - 1] = val;

		if(pos > 1)
			System.arraycopy(this.data, 0, baNew, 0, pos - 1);
		if(pos <= this.data.length)
			System.arraycopy(this.data, pos - 1, baNew, pos, this.data.length - pos + 1);
		return new JMo_ByteArray(baNew);
	}

	/**
	 * °left(Int len)ByteArray # Return a new ByteArray with 'len' items starting from the first.
	 */
	private JMo_ByteArray mLeft(final CallRuntime cr) {
		final I_Object o = cr.args(this, Int.class)[0];
		final int left = ((Int)o).getValue();
		Lib_Error.ifNotBetween(cr, 1, this.data.length, left, "length");
		return this.iCutCopy(cr, 1, Math.min(left, this.data.length));
	}

	/**
	 * °part(Int start, Int end)ByteArray # Return a copy of all items between 'start' and 'end'.
	 */
	private JMo_ByteArray mPart(final CallRuntime cr) {
		final I_Object[] oa = cr.args(this, Int.class, Int.class);
		final int start = ((Int)oa[0]).getValue();
		final int end = ((Int)oa[1]).getValue();
		final int size = this.data.length;
		Lib_Error.ifEmpty(cr, this.data.length, "ByteArray");
		Lib_Error.ifNotBetween(cr, 1, Math.min(end, size), start, "start");
		Lib_Error.ifNotBetween(cr, Math.max(1, start), size, end, "end");
		return this.iCutCopy(cr, start, Math.min(this.data.length, end));
	}

	/**
	 * °- ^ delete
	 * °delete(Int pos)ByteArray # Create a new ByteArray where item at 'pos' is removed
	 */
	private JMo_ByteArray mRemovePos(final CallRuntime cr) {
		final Int o = (Int)cr.args(this, Int.class)[0];
		final int oi = Lib_Convert.getIntValue(cr, o);
		Lib_Error.ifNotBetween(cr, 1, this.data.length, oi, "position");
		final byte[] baNew = new byte[this.data.length - 1];
		if(oi > 1)
			System.arraycopy(this.data, 0, baNew, 0, oi - 1);
		if(oi < this.data.length)
			System.arraycopy(this.data, oi, baNew, oi - 1, this.data.length - oi);
		return new JMo_ByteArray(baNew);
	}

	/**
	 * °right(Int len)List # Return a new ByteArray with 'len' items up to the last.
	 *
	 * TODO: Hard or lazy?!? .left(0) / .left(1000)
	 */
	private JMo_ByteArray mRight(final CallRuntime cr) {
		final I_Object o = cr.args(this, Int.class)[0];
		final int right = ((Int)o).getValue();
		Lib_Error.ifNotBetween(cr, 1, this.data.length, right, "length");
		return this.iCutCopy(cr, Math.max(1, this.data.length - right + 1), this.data.length);
	}

	/**
	 * °search ^ searchFirst
	 * °searchFirst(Byte search)Int? # Returns the first position of this Byte, 'nil' if nothing is found.
	 * °searchLast(Byte search)Int? # Returns the last position of this Byte, 'nil' if nothing is found.
	 */
	private A_Immutable mSearch(final CallRuntime cr, final boolean first) {
		final byte search = ((JMo_Byte)cr.args(this, JMo_Byte.class)[0]).getValue();

		if(first) {
			for(int i = 0; i < this.data.length; i++)
				if(this.data[i] == search)
					return new Int(i + 1);
		}
		else
			for(int i = this.data.length - 1; i >= 0; i--)
				if(this.data[i] == search)
					return new Int(i + 1);
		return Nil.NIL;
	}

	/**
	 * °start(Int pos)ByteArray # Return a copy of this ByteArray, starting at position 'pos'
	 */
	private JMo_ByteArray mStartPos(final CallRuntime cr) {
		final I_Object arg = cr.args(this, Int.class)[0];
		final int start = ((Int)arg).getValue();
		final int end = this.data.length;
		Lib_Error.ifNotBetween(cr, 1, end, start, "position");
		return this.iCutCopy(cr, start, end);
	}

	/**
	 * °toByte()Byte # Converts a ByteArray with length of 1 to a Byte.
	 * °toShort()Short # Converts a ByteArray with length of 2 to a Short.
	 * °toInt()Int # Converts a ByteArray with length of 4 to a Int.
	 * °toLong()Long # Converts a ByteArray with length of 8 to a Long.
	 */
	private A_IntNumber mToIntNumber(final CallRuntime cr, final int bytes) {
		cr.argsNone();
		Lib_Error.ifNot(cr, bytes, this.data.length, "length");

		switch(bytes) {
			case 1:
				return new JMo_Byte(this.data[0]);
			case 2:
				short vs = this.data[1];
				vs += this.data[0] << 8;
				return new JMo_Short(vs);
			case 4:
				int vi = this.data[3];
				vi += this.data[2] << 8;
				vi += this.data[1] << 16;
				vi += this.data[0] << 24;
				return new Int(vi);
			case 8:
				int vl = this.data[7];
				vl += this.data[6] << 8;
				vl += this.data[5] << 16;
				vl += this.data[4] << 24;
				vl += this.data[3] << 32;
				vl += this.data[2] << 40;
				vl += this.data[1] << 48;
				vl += this.data[0] << 56;
				return new JMo_Long(vl);

			default:
				throw Err.impossible(bytes);
		}
	}

	/**
	 * °toList()List # Convert this ByteArray to a List
	 */
	private JMo_List mToList(final CallRuntime cr) {
		cr.argsNone();
		final SimpleList<I_Object> list = new SimpleList<>(this.data.length);

		for(final byte element : this.data)
			list.add(new JMo_Byte(element));

		return new JMo_List(list);
	}

	/**
	 * °toStrHex()Str # Converts this ByteArray to a Hex-String.
	 */
	private Str mToStrHex(final CallRuntime cr) {
		cr.argsNone();
		return new Str(Lib_Hex.bytesToHexHighString(this.data));
	}

}
