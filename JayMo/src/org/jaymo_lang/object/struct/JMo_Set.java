/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.struct;

import java.util.Collections;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.I_AutoBlockList;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.model.ObjectManager;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Exec;
import org.jaymo_lang.util.Lib_Output;
import org.jaymo_lang.util.Lib_Sequence;
import org.jaymo_lang.util.Lib_StrFormat;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.struct.I_Series;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.SimpleSet;


/**
 * @author Michael Nitsche
 * @created 26.01.2021
 */
public class JMo_Set extends A_Sequence implements I_AutoBlockDo, I_AutoBlockList {

	private SimpleSet<I_Object> set;
	private Call[]              init;
	private String              fixedType = null;


	/**
	 * !Set of objects
	 * +Set(Object o1, Object o2, ...) # Set(6,9,15,23,12)
	 * #evtl.: +[Object o1, Object o2, ...]? # [3,6,1,'a']?
	 * %.each
	 */
	public JMo_Set() {
		this.set = new SimpleSet<>();
		this.init = null;
	}

	public JMo_Set(final Call... ca) {
		this.set = null;
		this.init = ca;
	}

	public JMo_Set(final I_Object[] oa) {
		this.set = new SimpleSet<>();
		Collections.addAll(this.set, oa);
		this.init = null;
	}

	public JMo_Set(final SimpleSet<I_Object> set) {
		this.set = set;
		this.init = null;
	}

	@Override
	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.each(cr).obj;
	}

	@Override
	public SimpleList<I_Object> autoBlockToList(final CallRuntime cr) {
		final SimpleList<I_Object> al = new SimpleList<>(this.set.size());
		al.addAll(this.set);
		return al;
	}

	@Override
	public boolean equals(final Object obj) {

		if(obj instanceof JMo_Set) {
			final SimpleSet<I_Object> other = ((JMo_Set)obj).set;
			if(this.set.size() != other.size())
				return false;
			for(int i = 0; i < this.set.size(); i++)
				if(!this.set.get(i).equals(other.get(i)))
					return false;
			return true;
		}
		else
			return false;
	}

	@Override
	public boolean equalsLazy(final Object obj) {
		return this.equals(obj);
	}

//	public MSet<I_Object> getInternalObject() {
//		return this.set;
//	}

//	public void internalAdd(final I_Object o) {
//		this.set.add(o);
//	}

	@Override
	public I_Series<? extends I_Object> getInternalCollection() {
		return this.set;
	}

	@Override
	public void init(final CallRuntime cr) {

		// Call and add arguments
		if(this.init != null) {
			this.set = new SimpleSet<>();

			for(final Call ca : this.init) {
				I_Object o = cr.execInit(ca, this);
				o = Lib_Convert.getValue(cr, o);
				this.set.add(o);
			}
			this.init = null;
		}
	}

	@Override
	public I_Object sequenceDeepGet(final CallRuntime cr, final I_Object[] keys, final int offset, final boolean lazy) {
		final A_IntNumber oi = (A_IntNumber)cr.argType(keys[offset], A_IntNumber.class);
		final int i = Lib_Convert.getIntValue(cr, oi);
		final int pos = Lib_Sequence.realPosition(cr, i, this.set.size(), lazy);
		if(pos == -1 || pos > this.set.size())
			if(lazy)
				return Nil.NIL;
			else
				Lib_Error.ifNotBetween(cr, 1, this.set.size(), pos, "position in list");

		if(offset == keys.length - 1)
			return this.set.get(pos - 1);
		else {
			final I_Object deeper = this.set.get(pos - 1);

			if(!(deeper instanceof I_DeepGetSet)) {
				final StringBuilder sb = new StringBuilder();

				for(int j = 0; j <= offset; j++) {
					if(j > 0)
						sb.append(',');
					sb.append(keys[j].toString());
				}
				throw new RuntimeError(cr, "Invalid type of item.", "Item at position (" + sb.toString() + ") isn't a sequence, so I can't go into it!");
			}
			return ((I_DeepGetSet)deeper).sequenceDeepGet(cr, keys, offset + 1, lazy);
		}
	}

	@Override
	public void sequenceDeepSet(final CallRuntime cr, final I_Object[] keys, final int offset, final I_Object value, final boolean lazy) {
		final A_IntNumber oi = (A_IntNumber)cr.argType(keys[offset], A_IntNumber.class);
		final int wanted = Lib_Convert.getIntValue(cr, oi);

		if(lazy)
			while(Math.abs(wanted) > this.set.size())
				this.set.add(Nil.NIL);
		final int pos = Lib_Sequence.realPosition(cr, wanted, this.set.size(), lazy);

		Lib_Error.ifNotBetween(cr, 1, this.set.size(), pos, "position");

		if(offset == keys.length - 1)
			this.set.set(pos - 1, value);
		else {
			I_Object deeper = this.set.get(pos - 1);

			if(deeper == Nil.NIL && lazy) {
				deeper = new JMo_Set();
				this.set.set(pos - 1, deeper);
			}
			if(!(deeper instanceof A_Sequence))
				throw new RuntimeError(cr, "Invalid type of item.", "Item at position " + (offset + 1) + " isn't a sequence, so I can't go into it!");
			final A_Sequence deeper2 = (A_Sequence)deeper;
			deeper2.sequenceDeepSet(cr, keys, offset + 1, value, lazy);
		}
	}

	@Override
	public String toString() {
		return "Set<" + this.set.size() + '>';
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
//		if(type == STYPE.IDENT)
//			return this.toString();

		final StringBuilder sb = new StringBuilder();
		sb.append("Set[");

		if(this.set != null) {

			for(final I_Object o : this.set) {
				String s = o == this ? A_Sequence.CYCLE_IDENT : o.toString(cr, type.getNested());
				if(type == STYPE.DESCRIBE)
					s = Lib_Output.indentLines(s, false);
				sb.append(s);
				sb.append(',');
			}
			Lib_Output.removeEnd(sb, ',');
		}
		sb.append(']');
		return sb.toString();
	}

	@Override
	protected ObjectCallResult call3(final CallRuntime cr, final String method) {

		switch(method) {
			case "+":
			case "add":
				return A_Object.stdResult(this.mAdd(cr));
			case "take": // integrate, complete, grab, take, have, inc, fill, stock, load, addIfUnknown, addLazy, must, need
				// put is already used as: global lazy get
				return A_Object.stdResult(this.mTake(cr));	// TODO Was ist jetzt genau der Unterschied zwischen .put und .take?!?
			case "-":
//			case "sub":
			case "delete":
				return A_Object.stdResult(this.mDelete(cr));
			case "--":
//			case "dec":
			case "remove":
				return A_Object.stdResult(this.mRemoveItems(cr));
			case "clear":
				return A_Object.stdResult(this.mClear(cr));

			case "addAll":
				return A_Object.stdResult(this.mAddAll(cr));
			case "subAll":
			case "subtractAll":
			case "removeAll":
				return A_Object.stdResult(this.mSubAll(cr));

			case "contains":
				return A_Object.stdResult(this.mContains(cr));
			case "each":
				return this.each(cr);
			// case "eachDeep": return each(c);
			case "combine":
			case "implode":
				return A_Object.stdResult(this.mImplode(cr));
			case "fill":
				return A_Object.stdResult(this.mFill(cr));

			case "min":
				return A_Object.stdResult(this.mMin(cr));
			case "max":
				return A_Object.stdResult(this.mMax(cr));
			case "sum":
				return A_Object.stdResult(this.mSum(cr));
			case "avg":
			case "average":
				return A_Object.stdResult(this.mAverage(cr));

			// Functional - FuncLet
			case "filter":
				return A_Object.stdResult(this.mFilter(cr, method));
			case "reduce":
				return A_Object.stdResult(this.mReduce(cr, method));
			case "amount":
				return A_Object.stdResult(this.mAmount(cr, method));

			case "fixType":
				this.mFixType(cr);
				return A_Object.stdResult(this);

			case "toList":
				return A_Object.stdResult(this.mToList(cr));

			default:
				return null;
		}
	}

	/**
	 * °copy()Set # Create a copy of this Set.
	 */
	@Override
	protected JMo_Set copy(final CallRuntime cr) {
		cr.argsNone();
		final SimpleSet<I_Object> copy = this.set.copy();
		return new JMo_Set(copy);
	}

	@Override
	protected I_Object getFirst(final CallRuntime cr) {
		return this.set.size() == 0
			? Nil.NIL
			: this.set.get(0);
	}

	@Override
	protected I_Object getLast(final CallRuntime cr) {
		final int len = this.set.size();
		return len == 0
			? Nil.NIL
			: this.set.get(len - 1);
	}

	@Override
	protected void mSequenceSetPut(final CallRuntime cr, final boolean lazy) {
		final I_Object[] oa = cr.argsVar(this, 2, 1);

		if(lazy && Lib_Convert.getIntValue(cr, oa[1]) > this.set.size() + 1)
			throw new CodeError(cr, "Invalid use of put", "A set can be extended only by a maximum of one element.");

		if(oa.length > 2)
			this.sequenceDeepSet(cr, oa, 1, oa[0], lazy);
		else
			this.sequenceSet(cr, oa[1], oa[0], lazy);
	}

	@Override
	protected void sequenceContent(final CallRuntime cr, final I_Object[] values) {
		this.set.clear();

		Collections.addAll(this.set, values);
	}

	@Override
	protected boolean sequenceEmpty() {
		return this.set.isEmpty();
	}

	@Override
	protected I_Object sequenceGetPull(final CallRuntime cr, final boolean lazy, final I_Object arg) {
		final I_Atomic oav = (I_Atomic)cr.argType(arg, I_Atomic.class);
		final int oi = Lib_Convert.getIntValue(cr, oav);
		final int pos = Lib_Sequence.realPosition(cr, oi, this.set.size(), lazy);
		return pos == -1 ? Nil.NIL : this.set.get(pos - 1);
	}

	@Override
	protected I_Object sequenceSelectGet(final CallRuntime cr, final I_Object key, final boolean lazy) {
		final int oi = Lib_Convert.getIntValue(cr, key);
		final int pos = Lib_Sequence.realPosition(cr, oi, this.set.size(), lazy);
		return pos == -1 ? Nil.NIL : this.set.get(pos - 1);
	}

	@Override
	protected void sequenceSet(final CallRuntime cr, final I_Object pos2, final I_Object o, final boolean lazy) {
		final int wanted = Lib_Convert.getIntValue(cr, pos2);

		if(lazy)
			while(Math.abs(wanted) > this.set.size())
				this.set.add(Nil.NIL);
		final int pos = Lib_Sequence.realPosition(cr, wanted, this.set.size(), lazy);

		Lib_Error.ifNotBetween(cr, 1, this.set.size(), pos, "position");

		this.set.set(pos - 1, o);
	}

	@Override
	protected int sequenceSize() {
		return this.set.size();
	}

	/**
	 * °each()%Object # Walk throw each item
	 * °each(Object o)%Object # Walk throw each item, but use always object o.
	 * °each(VarLet var)%Object # Walk throw each item and assign the row to var
	 */
	private ObjectCallResult each(final CallRuntime crOld) {
		crOld.argsNone();

		if(crOld.getStream() == null && crOld.getCallBlock() == null)
			throw new CodeError(crOld, "No Stream or Block for 'each'", null);

		final LoopHandle handle = new LoopHandle(this);
		final CallRuntime crNew = crOld.copyLoop(handle);
		I_Object result = this;

		for(final I_Object it : this.set) {
			handle.startLap();
			result = Lib_Exec.execBlockStream(crNew, it);

			if((result = Lib_Exec.loopResult(result)) instanceof Return)
				return ((Return)result).getLoopResult();
		}
		return new ObjectCallResult(result, true);
	}

	private void iFixedType(final CallRuntime cr, final I_Object obj) {
		if(this.fixedType != null)
			if(!Lib_Type.isInstanceOf(obj, this.fixedType))
				throw new RuntimeError(cr, "Invalid object type for fixed Set.", "This Set accepts only <" + this.fixedType + ">, but got object with type: " + Lib_Type.getTypeString(obj));
	}

	private void iFixedType(final CallRuntime cr, final I_Object[] oa) {
		if(this.fixedType != null)
			for(final I_Object obj : oa)
				this.iFixedType(cr, obj);
	}

	/**
	 * °+^add
	 * °add(Object... o)Same # Add all objects and throw an error if an object allready exist.
	 */
	private JMo_Set mAdd(final CallRuntime cr) {
		final I_Object[] oa = cr.argsVar(this, 1, 0);
		this.iFixedType(cr, oa);

		for(final I_Object o : oa)
			if(!this.set.put(o))
				throw new RuntimeError(cr, "Object already known", "This set already contains: " + o.toString(cr, STYPE.IDENT));

		return this;
	}

	/**
	 * °addAll(List other)Same # Adds all items to the current list.
	 */
	private JMo_Set mAddAll(final CallRuntime cr) {
		final A_Sequence other = (A_Sequence)cr.args(this, A_Sequence.class)[0];
		if(this.fixedType != null)
			for(final I_Object obj : other.getInternalCollection())
				this.iFixedType(cr, obj);
		this.set.addAll(other.getInternalCollection());
		return this;
	}

	/**
	 * °amount(Object each)§Int # Check every item and return the amount of trues. The result of each must be Bool.
	 * °amount(VarLet var, Object each)§Int # Check every item and return the amount of trues. The result of each must be Bool.
	 */
	private Int mAmount(final CallRuntime cr, final String method) {
		Lib_Error.ifArgs(cr.argCount(), 1, 2, cr, this);
		int result = 0;

		for(final I_Object test : this.set) {
			final Bool ok = (Bool)cr.copyEach(method).argsEach(this, 0, new I_Object[]{test}, Bool.class);
			if(ok.getValue())
				result++;
		}
		return new Int(result);
	}

	/**
	 * °avg ^ average
	 * °average()Double # Calculate the average value of the items in this Set.
	 */
	private I_Object mAverage(final CallRuntime cr) {
		cr.argsNone();
		double result = 0d;

		for(final I_Object o : this.set) {
			final double d = Lib_Convert.getDoubleValue(cr, o);
			result += d / this.set.size();
		}
		return new JMo_Double(result);
	}

	/**
	 * °clear()Same # Removes all items from this list.
	 */
	private JMo_Set mClear(final CallRuntime cr) {
		cr.argsNone();
		this.set.clear();
		return this;
	}

	/**
	 * °contains(Object search)Bool # Returns true, if this Set contains the given object.
	 */
	private Bool mContains(final CallRuntime cr) {
		final I_Object o = cr.args(this, I_Object.class)[0];
		for(final I_Object lo : this.set)
			if(lo.equals(o))
				return Bool.TRUE;
		return Bool.FALSE;
	}

	/**
	 * °- ^ delete
	 * °delete(Int pos)Same # Remove item at position
	 */
	private JMo_Set mDelete(final CallRuntime cr) {
		final Int o = (Int)cr.args(this, Int.class)[0];
		final int oi = Lib_Convert.getIntValue(cr, o);
		Lib_Error.ifNotBetween(cr, 1, this.set.size(), oi, "position");
		this.set.remove(oi - 1);
		return this;
	}

	/**
	 * °fill(Str form)Str # Fills all {}-placeholders of 'form' with values from current set
	 */
	private Str mFill(final CallRuntime cr) {
		final String form = ((Str)cr.args(this, Str.class)[0]).getValue();
		return Lib_StrFormat.fill(cr, form, this.set.iterator());
	}

	/**
	 * °filter(Object each)§List # Creates a new list with selected items. A FuncLet will be executed, result of 'each' must be Bool.
	 * °filter(VarLet var, Object each)§List # Creates a new list with selected items. Each item will be assigned to 'var' and 'each' is processed. A FuncLet will be executed. The result of 'each' must be Bool.
	 */
	private JMo_List mFilter(final CallRuntime cr, final String method) {
		Lib_Error.ifArgs(cr.argCount(), 1, 2, cr, this);
		final SimpleList<I_Object> result = new SimpleList<>(this.set.size());

		for(final I_Object test : this.set) {
			// MOut.temp(test, cr.call.gInternPars());
			// Bool ok = (Bool)cr.args(test, Bool.class)[0];
			final Bool ok = (Bool)cr.copyEach(method).argsEach(this, 0, new I_Object[]{test}, Bool.class);
			if(ok.getValue())
				result.add(test);
		}
		return new JMo_List(result);
	}

	/**
	 * °fixType(Str type)Same # Allow only one type in this Set.
	 */
	private void mFixType(final CallRuntime cr) {
		final Str o = (Str)cr.args(this, Str.class)[0]; // TODO Allow <Type>
		final String type = Lib_Convert.getStringValue(cr, o);
		if(!ObjectManager.isTypeKnown(cr, type))
			throw new RuntimeError(cr, "Unknown type", "Unknown type: " + type);
		this.fixedType = type;

		int counter = 0;

		for(final I_Object item : this.set) {
			counter++;

			if(!Lib_Type.isInstanceOf(item, this.fixedType)) {
				final String sTypes = Lib_Type.typesToString(item);
				throw new RuntimeError(cr, "Invalid type of item in set", "Type \"" + this.fixedType + "\" needed, but types of item at position " + counter + " are: " + sTypes);
			}
		}
	}

	/**
	 * °combine ^ implode
	 * °implode()Str # Convert each item to a string and concat all together.
	 * °implode(Atomic delimiter)Str # Convert each item to a string and concat all with 'delimiter'.
	 */
	private Str mImplode(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 0, 1);

		if(args.length == 0) {
			final StringBuilder sb = new StringBuilder();
			for(final I_Object o : this.set)
				sb.append(o.toString());
			return new Str(sb.toString());
		}
		// if(args.length==1) {
		final I_Atomic atomic = (I_Atomic)cr.argType(args[0], I_Atomic.class);
		final String delimiter = Lib_Convert.getStringValue(cr, atomic);
		final StringBuilder sb = new StringBuilder();

		for(int i = 0; i < this.set.size(); i++) {
			final I_Object o = this.set.get(i);
			final String s = Lib_Convert.getStringValue(cr, o);
			sb.append(s);
			if(i != this.set.size() - 1)
				sb.append(delimiter);
		}
		return new Str(sb.toString());
	}

	/**
	 * °max()Object # Return the 'greatest' item in this Set.
	 */
	private I_Object mMax(final CallRuntime cr) {
		cr.argsNone();
		I_Object result_o = null;
		Double result_d = null;

		for(final I_Object o : this.set) {
			final double d = Lib_Convert.getDoubleValue(cr, o);

			if(result_o == null || d > result_d) {
				result_o = o;
				result_d = d;
			}
		}
		return result_o;
	}

	/**
	 * °min()Object # Return the 'smallest' item in this Set.
	 */
	private I_Object mMin(final CallRuntime cr) {
		cr.argsNone();
		I_Object result_o = null;
		Double result_d = null;

		for(final I_Object o : this.set) {
			final double d = Lib_Convert.getDoubleValue(cr, o);

			if(result_o == null || d < result_d) {
				result_o = o;
				result_d = d;
			}
		}
		return result_o;
	}

	/**
	 * °reduce(Object init, Object each)§Object # Reduces the list to a single value.
	 * °reduce(Object init, VarLet var, Object each)§Object # Reduces the list to a single value.
	 */
	private I_Object mReduce(final CallRuntime cr, final String method) {
		Lib_Error.ifArgs(cr.argCount(), 2, 3, cr, this);
		I_Object sum = cr.argsOneAdvance(this, 0, I_Object.class);

		for(final I_Object test : this.set) {
			final I_Object[] each = {sum, test};
//			final Call c = new Call(cr.getSurrBlock(), null, method, cr.call.parCalls, cr.getDebugInfo());
//			sum = cr.copyCall(c, false).parsEach(this, 1, test2, I_Object.class);
			sum = cr.copyEach(method).argsEach(this, 1, each, I_Object.class);
		}
		return sum;
	}

	/**
	 * °-- ^ remove
	 * #°dec ^ remove
	 * °remove(Object o...)Same # Remove all elements that match to an argument
	 */
	private JMo_Set mRemoveItems(final CallRuntime cr) {
		final I_Object[] oa = cr.argsVar(this, 1, 0);
		for(final I_Object o : oa)
			for(int i = this.set.size() - 1; i >= 0; i--)
				if(this.set.get(i).equals(o))
					this.set.remove(i);
		return this;
	}

	/**
	 * °subAll ^ removeAll
	 * °subtractAll ^ removeAll
	 * °removeAll(List other)Same # Removes all objects from this list, that are found in the second list.
	 */
	private JMo_Set mSubAll(final CallRuntime cr) {
		final A_Sequence list2 = (A_Sequence)cr.args(this, A_Sequence.class)[0];
		this.set.removeAll(list2.getInternalCollection());
		return this;
	}

	/**
	 * °sum()Double # Summarize all items in this Set.
	 */
	private I_Object mSum(final CallRuntime cr) {
		cr.argsNone();
		double result = 0d;
		for(final I_Object o : this.set)
			result += Lib_Convert.getDoubleValue(cr, o);
		return new JMo_Double(result);
	}

	/**
	 * °take(Object... o)Same # Add all objects that are currently not in the set.
	 * TODO Warum nicht 'put' ?!? take?!?
	 */
	private JMo_Set mTake(final CallRuntime cr) {
		final I_Object[] oa = cr.argsVar(this, 1, 0);
		this.iFixedType(cr, oa);

		for(final I_Object o : oa)
			this.set.put(o);
		return this;
	}

	/**
	 * °toList()List # Creates a new List with all items in this Set
	 */
	private JMo_List mToList(final CallRuntime cr) {
		cr.argsNone();
		return new JMo_List(this.autoBlockToList(cr));
	}

}
