/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.pseudo;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.data.struct.BufferQueue;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 27.04.2019
 */
public class MultiCall extends A_Object {

	private final BufferQueue<I_Object> items    = new BufferQueue<>();
	private final ArgCallBuffer         arg;
	private boolean                     executed = false;
	private final int                   key;
	private I_Object                    stream   = null;


	public MultiCall(final int key, final Call c) {
		this.key = key;
		this.arg = new ArgCallBuffer(0, c);
	}

	public void exec(final CallRuntime cr, final Block current, final I_Object streamIT) {
		if(this.executed)
//			Err.invalid("Doppelt ausführen?");
			return;
		this.executed = true;

		final SimpleList<I_Object> il = cr.vce.useMultiCall().get(this.key);
		for(final I_Object item : il)
			this.items.add(item);
	}

	public I_Object getCurrent() {
		return this.items.current();
	}

	public I_Object getNext() {
		return this.items.next();
	}

	public boolean hasNext() {
		return !this.items.isEmpty();
	}

	@Override
	public void init(final CallRuntime cr) {
		this.arg.init(cr, this.stream, I_Object.class);
	}

	public void resetPointer() {
		this.items.reset();
	}

	public void setStream(final I_Object o) {
		this.stream = o;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {

		switch(type) {
			case REGULAR:
			case NESTED:
				return "MultiCall";
			default:
				return "MultiCall(" + this.arg.toString(cr, STYPE.IDENT) + ")(" + this.items.toString() + ")";
		}
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		throw Err.forbidden(cr); // MultiCall is only a placeholder!
	}

}
