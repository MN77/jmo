/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.pseudo;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.magic.var.MV_THIS;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Output;


/**
 * @author Michael Nitsche
 * @created 09.01.2020
 */
public class FuncLet extends A_Object {

	private final Call call;
	private Call[]     args = null;
	private String     type = null;


	public FuncLet(final Call call) {
		this.call = call;
	}

	public I_Object eachExec(final CallRuntime cr, final I_Object obj) {
		final Call c = new Call(cr, obj);
		final CallRuntime cr2 = cr.copyFunctional(c, new Call[]{c}, cr.getDebugInfo());
		return this.exec(cr2);
	}

	public I_Object exec(final CallRuntime crOld) {
//		crOld.args(null); // No check here!

		Call[] execPars = crOld.call.argCalls;

		if(this.args != null) {
			if(execPars != null && execPars.length > 0)
				//				MOut.temp(this.args, execPars);
				throw new CodeError(crOld, "Parameter already set for FuncLet!", "Already set parameters: " + this.args.length);
			execPars = this.args;
		}
		final CallRuntime crNew = crOld.copyFunctional(this.call, execPars, crOld.getDebugInfo()); // Remove access to surrounding variables

		return crNew.exec(this, false);
	}

	@Override
	public void init(final CallRuntime cr) {
		this.type = cr.getType().getName();
		final Call[] cpars = this.call.argCalls;

		if(cpars != null && cpars.length > 0) {
			final int len = cpars.length;
			this.args = new Call[len];

//			--- Read all variables, use only the current value and create new Calls with that ---
			for(int i = 0; i < len; i++) {
				final Call c = cpars[i];
				I_Object o = cr.execInit(c, this);
				o = Lib_Convert.getValue(cr, o);
				this.args[i] = new Call(cr, o);
			}
		}
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {

		switch(type) {
			case NESTED:
//				return this.getTypeName();
			case REGULAR:
				final String ident = this.call.object != null && this.call.object instanceof MV_THIS && this.type != null
					? this.type
					: this.call.object.toString(null, STYPE.IDENT);

				final StringBuilder sb = new StringBuilder();
				sb.append(ident);
				sb.append('.');
				sb.append(this.call.method);

				sb.append('(');
				if(this.args != null && this.args.length > 0) {

					for(final Call arg : this.args) {
						sb.append(arg.toString(null, STYPE.IDENT));
						sb.append(',');
					}
					Lib_Output.removeEnd(sb, ',');
				}
				sb.append(')');

				return sb.toString();
			default:
				final StringBuilder sb2 = new StringBuilder();
				sb2.append("FuncLet[");

				if(this.call.object != null && this.call.object instanceof MV_THIS && this.type != null) {
					final String p = this.args != null
						? Lib_Convert.toString(cr, false, ",", this.args)
						: "";
					sb2.append(this.type + '.' + this.call.method + '(' + p + ')');
				}
				else
					sb2.append(this.call.toString(cr, STYPE.IDENT));

				sb2.append("]");
				return sb2.toString();
		}
	}


//	@Override
//	public String toStringIdent(CallRuntime cr) {
//		return "FuncLet["+this.call.toStringIdent(cr)+"]";
//	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "exec":
				return new ObjectCallResult(this.exec(cr), false);

			default:
				return null;
		}
	}

}
