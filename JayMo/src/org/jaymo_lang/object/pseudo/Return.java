/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.pseudo;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 14.04.2018
 */
public class Return extends A_Object {

	/*
	 * Level:
	 * - Next/Continue For Loops: Breaks the current block, continue loop
	 * - Break For Loops: Breaks the current block and the loop
	 * - Return/End Breaks the current function
	 * - Exit Breaks the complete Application
	 */
	public enum LEVEL {
		NEXT,
		BREAK,
		RETURN,
//		EXIT
	}


	private final LEVEL   level;
	private I_Object      arg;
	private final boolean withoutValue;


	public Return(final LEVEL level, final I_Object arg) {
		this.level = level;
		this.withoutValue = arg == null;
		this.arg = this.withoutValue ? Nil.NIL : arg;
	}

	public LEVEL getLevel() {
		return this.level;
	}

	public ObjectCallResult getLoopResult() {

		switch(this.level) {
			case NEXT:
				return null;
			case BREAK:
				return new ObjectCallResult(this.getResult(), true);
			case RETURN:
				return new ObjectCallResult(this, true);
			default:
				throw Err.impossible(this.getLevel());
		}
	}

	public I_Object getResult() {
		return this.arg;
	}

	@Override
	public void init(final CallRuntime cr) {
		cr.getStrict().checkClearFlow(cr.getDebugInfo());
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final StringBuilder sb = new StringBuilder();

		switch(this.level) {
			case NEXT:
				sb.append("Next");
				break;
			case BREAK:
				sb.append("Break");
				break;
			case RETURN:
				sb.append("Return");
				break;
		}
		sb.append('(');
		sb.append(this.arg.toString(cr, type.getNested()));
		sb.append(')');
		return sb.toString();
	}

	public void updateEmptyResult(final I_Object it) {
		if(this.withoutValue)
			this.arg = it;
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
//		throw Err.forbidden(cr.getMethod(), cr);
//		throw new ExecError(cr,"Return, Break, Continue, Exit have no ");
		return null;
	}

}
