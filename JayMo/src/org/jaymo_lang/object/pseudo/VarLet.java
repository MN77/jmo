/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.pseudo;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.passthrough.Var;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Function;


/**
 * @author Michael Nitsche
 * @created 01.10.2019
 *
 * @implNote
 *           ConstLet makes no sense! {1,3}:A ... the const will not allow 2
 */
public class VarLet extends A_MemLet {

	private final Var var;


	public VarLet(final Var var) {
		this.var = var;
	}

	@Override
	public Var getMem() {
		return this.var;
	}

	@Override
	public String toString() {
//		return ":" + this.var.toString();
		return "VarLet[" + this.var.getName() + "]";
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return this.toString();
	}

	@Override
	protected ObjectCallResult call3(final CallRuntime cr, final String method) {
		if(method.startsWith("::@"))
			if(method.equals("::@varChanged")) {
				this.var.eventAddHandler(cr, "@varChanged", cr.call);
				return new ObjectCallResult(this, true);
			}
			else
				throw new CodeError(cr, "Unknown event", "Got definition for: " + method.substring(2));

		final byte isVarLetFunction = Lib_Function.isVarletFunction(method);

		if(isVarLetFunction > -1) {
//			if(method.endsWith("Let"))
			final String mathMethod = method.substring(0, method.length() - 3);

			final I_Object oldValue = this.var.get(cr);
//			final I_Object oldValue = cr.vce.vars.get(cr, this, null);
			if(oldValue == null)
				throw new RuntimeError(cr, "Variable is not initialized", "The variable '" + this.var.getName() + "' has no value/type, please initialize it first.");
			final I_Object newValue = Lib_Function.mCalcLet(cr, mathMethod, isVarLetFunction, oldValue);
			this.var.let(cr, cr, newValue, true);
			return new ObjectCallResult(this, false); // TODO true?
		}
		return null;
	}

}
