/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.JayMoError;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.Instance;
import org.jaymo_lang.util.Lib_Comply;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Exec;

import de.mn77.base.data.struct.keypot.StringPotList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 19.05.2018
 */
public abstract class A_EventObject extends A_Object {

	private StringPotList<CallRuntime> eventHandlers;


	public final void eventAddHandler(final CallRuntime cr, final String event, final Call c) {
		if(cr.argCount() != 0)
			throw Err.impossible(cr, event, c);

		if(this.eventHandlers == null)
			this.eventHandlers = new StringPotList<>();

//		if(event.equals("@"))
//			event = this.iResolveDefault(cr);

		if(!this.validateEvent(cr, event)) {

			if(this instanceof Instance) {
				final I_Object parent = ((Instance)this).internalGetParent();

				if(parent != null && parent instanceof A_EventObject) {
					((A_EventObject)parent).eventAddHandler(cr, event, c);
					return;
				}
			}
			if(event.indexOf('.') > -1)
				throw new CodeError(cr, "Invalid event handler definition", "No attached stream allowed, but got: " + event);
			if(!Lib_Comply.checkEventName(event))
				throw new CodeError(cr, "Invalid event name", "Got: " + event);

			throw new CodeError(cr, "Unknown event", "Got handler for: " + event);
		}
//		CallRuntime crNew = cr.copyCall(c, true);	// Okay, access to surrounding vars ... but with the state at executing the event!!!

//		final String[] varsToCopy = cr.call.getEventCopyVars(); // To copy vars is not clear and leads to misunderstandings
//		final CallRuntime crNew = cr.copyEventHandler(c, varsToCopy);

		final CallRuntime crNew = cr.copyEventHandler(c);
		this.eventHandlers.add(event, crNew);
	}

	public final boolean eventHasHandler(final CallRuntime cr, final String event) {
		if(this.eventHandlers == null)
			return false;
		final Iterable<?> pot = this.eventHandlers.get(event);
		return pot != null; // pot is null if theres no items
	}

	/**
	 * @apiNote Run event with error handling. This is for events, that runs not in main thread. For example Gui-Events.
	 * @implNote Synchronize this with App.iExec
	 */
	public final void eventRun(final CallRuntime cr, final String event, final I_Object blockIT) { // TODO Check: Static, local?
		cr.getApp().registerFork();

		try {
			this.eventRunRaw(cr, event, blockIT);
		}
		catch(final Throwable t) {
			// No exit here, cause that would destroy a GUI-App
			Lib_Error.handleThreadErrorEnd(cr, t);
		}
		finally {
			cr.getApp().checkExit(true);
		}
	}

	/**
	 * @apiNote This is only for events which are running in main thread
	 * @implNote No error handling here! This must be done in the calling structure!
	 */
	public final void eventRunRaw(final CallRuntime cr, final String event, final I_Object blockIT) { // TODO Check: Static, local?
		Err.ifNull(cr, event, blockIT);
		if(!event.startsWith("@"))
			throw new JayMoError(cr, "Invalid event name", "Leading @ is missing!");

		if(this.eventHandlers != null) { // && this.eventHandlers.getKeys().contains(event)) {
			final Iterable<CallRuntime> handlers = this.eventHandlers.get(event);

			if(handlers != null)
				for(final CallRuntime eventCR : handlers) {
					//				S_Object res=c.exec(blockIT); // This leads to a Cycle
					//				S_Object res=
					//				c.getBlock().exec(cr, blockIT);
					//				c.getBlock().execEvent(cr, blockIT);

					//				eventCR.call.getBlock().exec(eventCR, blockIT);	// Used before Lib_Exec
					final I_Object result = Lib_Exec.execBlockStream(eventCR, blockIT);
					if(result instanceof Return)
						Lib_Exec.checkExceptionIsEnd(cr, (Return)result);
				}
		}
	}

	@Override
	public abstract void init(CallRuntime cr);

//	 * Default implementation, can be override
//	@Override
//	public String toString(CallRuntime cr, final boolean nested) {
//		return Lib_Type.getName(this);
//	}

	public abstract boolean validateEvent(final CallRuntime cr, final String event);

	@Override
	protected final ObjectCallResult call2(final CallRuntime cr, final String method) {
		final char c0 = method.charAt(0);

		// --- Event-Handler ---
		if(c0 == ':' && method.startsWith("::")) {
			final String m = cr.call.method.substring(2);

			A_EventObject obj = this; // Use Var direct // for @varChanged

//			if(!m.equals(Var.CHANGED_EVENT)) {
			final I_Object o = Lib_Convert.getValue(cr, this);
			if(o instanceof A_EventObject)
				obj = (A_EventObject)o; // Use the value of the Var
			else
				throw new CodeError(cr, "Invalid event-definition", "This object has no events: " + o);
//			}
			obj.eventAddHandler(cr, m, cr.call);
			return new ObjectCallResult(this, true);
		}

		// --- Call/Run Event ---
		if(c0 == '@') {
//			if(cr.parCount() != 0)	// One pare is allowed!
//				throw new CodeError(cr, "Invalid event-definition", "No argument allowed for events"); // "Invalid call of event"
			if(cr.hasStream())
				throw new CodeError(cr, "Invalid call of a event", "Call of event " + method + " with stream");
			if(cr.hasBlock())
				throw new CodeError(cr, "Invalid call of a event", "Call of event " + method + " with block");

//			if(method.equals("@"))
//				method = this.iResolveDefault(cr);

			if(this != cr.instance)
				throw new CodeError(cr, "Invalid call of a event", "An event can only be started within the type definition: " + method);

			if(this.validateEvent(cr, method))
				return this.callEvent(cr, method);
			else if(this instanceof Instance) {
				final I_Object parent = ((Instance)this).internalGetParent();
				if(parent != null && parent instanceof A_EventObject)
					return ((A_EventObject)parent).call2(cr, method); // call2, so it check also parent parent
			}
			else
				throw new CodeError(cr, "Unknown event", "Event unknown and can't be started: " + method);
		}
		// --- Something else ---
		return this.callMethod(cr, method);
	}

	// --- For Override ---

	/** @apiNote For override **/
	protected ObjectCallResult callEvent(final CallRuntime cr, final String event) {
		throw new CodeError(cr, "Invalid call of a event", "This event can't be thrown this way: " + event);
	}

	protected abstract ObjectCallResult callMethod(CallRuntime cr, final String method);

//	/** @apiNote For override **/
//	protected String resolveDefault() {
//		return null;
//	}

	// --- Private ---

//	private String iResolveDefault(final CallRuntime cr) {
//		final String real = this.resolveDefault();
//		if(real == null)
//			throw new CodeError(cr, "Invalid call of default event", "This type has no default event " + cr.getType().getName());
//		return real;
//	}

}
