/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import org.jaymo_lang.error.AssertError;
import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.ReturnException;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.Block;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.I_Atomic;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.JMo_Type;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.passthrough.Const;
import org.jaymo_lang.object.passthrough.I_Mem;
import org.jaymo_lang.object.passthrough.I_PassThrough;
import org.jaymo_lang.object.passthrough.TempConst;
import org.jaymo_lang.object.passthrough.Var;
import org.jaymo_lang.object.pseudo.A_MemLet;
import org.jaymo_lang.object.pseudo.ConstLet;
import org.jaymo_lang.object.pseudo.FuncLet;
import org.jaymo_lang.object.pseudo.JMo_Error;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.pseudo.Return.LEVEL;
import org.jaymo_lang.object.pseudo.VarLet;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.sys.JMo_Cmd;
import org.jaymo_lang.object.sys.JMo_Java;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Enum;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Exec;
import org.jaymo_lang.util.Lib_Output;
import org.jaymo_lang.util.Lib_StrFormat;
import org.jaymo_lang.util.Lib_Try;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.form.FormString;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err_Runtime;
import de.mn77.base.thread.A_Thread;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public abstract class A_Object implements I_Object {

	private enum COMPARSION {
		STRICT,
		NORMAL,
		LAZY
	}

	private enum PIPE_TYPE {
		LIVE,
		OUTPUT,
		ERROR,
		BUFFER
	}

//	private static int ID_COUNTER = 0;


	private boolean init = false;
//	private final int id;


//	protected A_Object() {
//		this.id = ++A_Object.ID_COUNTER;
//	}


	/**
	 * Helper-Function for call2-cases
	 */
	public static ObjectCallResult stdResult(final I_Object result) {
		return new ObjectCallResult(result, false);
	}

	public final ObjectCallResult call(final CallRuntime cr) {
		// --- Preparations ---
		final String method = cr.call.method;

		if(method == null && this instanceof I_Atomic)
			return new ObjectCallResult(this, false);

		final char c0 = method.charAt(0);

		if(c0 <= 'Z' && c0 >= 'A' && !(this instanceof JMo_Java))
			if(Lib_Enum.isEnum(method)) {
				final I_Object result = Lib_Convert.getValue(cr, this).getConstant(cr, method);
				if(result == null)
					throw new CodeError(cr, "Unknown constant", "Constant/Enum is not defined in <" + Lib_Convert.getValue(cr, this).getTypeName() + ">: " + method);
				return new ObjectCallResult(result, false);
			}
			else
				throw new CodeError(cr, "Misplaced new object", "Maybe missing comma before: '" + method + "'");

		final boolean pass = this instanceof I_PassThrough;
		ObjectCallResult result = null;

		if(pass) // Pass through
			result = this.call2(cr, method);
		else {
			result = this.iCall(cr, method);

			// --- Auto-Property --- // Sync this with FuncManager.get
			if(result == null && cr.getStrict().isValid_AutoProperty() && c0 >= 'a' && c0 <= 'z')
				if(cr.argCount() == 0) {
					/*
					 * Auto-Get
					 * main.title.print
					 * date.day
					 */
					final String methodGet = "get" + Character.toUpperCase(c0) + method.substring(1);
					result = this.iCall(cr, methodGet);
				}
				else {
					/*
					 * Auto-Set
					 * main.title("Foo").size(600,400).run
					 * date.day(1).print
					 */
					final String methodSet = "set" + Character.toUpperCase(c0) + method.substring(1);
					result = this.iCall(cr, methodSet);
				}
		}

		// --- Got no result, maybe unknown function
		if(result == null || result.obj == null) {
			final String typename = this instanceof JMo_Java
				? this.toString() // For Java-Objects, show more details!
				: this.getType(cr).toString();

			throw new CodeError(cr, "Unknown function", typename + "." + method);
		}
		result.obj.initOnce(cr); // Without this is possible, but some objects will be not initialized!
		return result;
	}

	public final int compareTo(final CallRuntime cr, final I_Object o) {
		if(o.hashCode() == this.hashCode() || this.equals(o))
			return 0;

		final Boolean result = this.isGreater2(o);

		if(result == null) {
			final String message = "Not comparable";
			final String detail = this.toString() + " <---> " + o.toString();
			if(cr != null)
				throw new RuntimeError(cr, message, detail);
			else
				throw new Err_Runtime(message, detail);
		}
		else
			return result ? 1 : -1;
	}

	public final int compareTo(final I_Object o) {
		return this.compareTo(null, o);
	}

	public boolean equalsLazy(final Object obj) {
		return this.equals(obj);
	}

	public boolean equalsStrict(final Object obj) {
		return this == obj;
	}

	/** Can be overwritten **/
	public A_Immutable getConstant(final CallRuntime cr, final String name) {
		return null;
	}

	// --- Abstract ---

	/**
	 * #°type ^ getType
	 * °type()Type # Returns the type of this object
	 */
	public final JMo_Type getType(final CallRuntime cr) {
		final Class<?> cl = this.getClass();
		final String name = Lib_Type.getName(cl, this);
		return new JMo_Type(name, cr);
	}


	// --- Optional for override ---

	public final String getTypeName() {
		final Class<?> cl = this.getClass();
		return Lib_Type.getName(cl, this);
	}

	public final String[] getTypeNames() {
		return Lib_Type.getTypeNames(this);
	}

	public abstract void init(CallRuntime cr);

	public void initOnce(final CallRuntime cr) {
		if(this.init)
			return;
		this.init = true;
		this.init(cr);
	}

	public boolean isInit() {
		return this.init;
	}

	/**
	 * °debug(Object args...)Same # Create a line with debug informations and the given arguments, and send it to StdOut.
	 */
	public I_Object mDebug(final CallRuntime cr) {
		if(!cr.getApp().isDebug())
			return this;

		final I_Object[] args = cr.argsVar(this, 0, 0);

		final StringBuilder sb = new StringBuilder();
		final I_Object file = cr.getDebugInfo().getFile();
		if(file != Nil.NIL)
			sb.append(file.toString());
		sb.append(":");
		sb.append(cr.getDebugInfo().getLine().toString());
		sb.append(" | ");
		sb.append(Lib_Type.toIdentWithType(cr, this));

		if(args != null && args.length > 0) {
			sb.append(" | ");
			boolean first = true;

			for(final I_Object arg : args) {
				if(!first)
					sb.append(",");
				sb.append(arg.toString());
				first = false;
			}
		}
		Lib_Output.out(cr.getApp(), sb.toString().trim(), true);
		return this;
	}


	// --- Different public methods ---

	/**
	 * @implNote
	 *           passToStream == go
	 *           equals == is
	 */
	public ObjectCallResult mDoIf(final CallRuntime cr, final boolean not, final boolean passToStream, final boolean equals) {
		I_Object[] args = equals ? cr.argsVar(this, 1, 0) : cr.argsFlex(this, 0, 1);
		final Call stream = cr.getStream();
		final Block block = cr.getCallBlock();

		if(args != null && args.length == 0)
			args = null;

		if(args == null && stream == null && block == null)
			return new ObjectCallResult(this, true);

		boolean check = false; // Default

		if(equals)
			for(final I_Object arg : args) {
//				final A_Atomic par0 = (A_Atomic)cr.argType(arg, A_Atomic.class);
//				check = this.getValue().equals(par0.getValue());
				check = this.equals(arg);
				if(check)
					break;
			}
		else {
			I_Object base = this;
			if(args != null && args.length == 1)
				base = cr.argType(args[0], Bool.class);
			else if(!(this instanceof Bool))
				throw new RuntimeError(cr, "Invalid type of object for if-function", "Type must be <Bool>, but is: " + this.getType(cr).toString());
			check = ((Bool)base).getValue();
		}
		if(not)
			check = !check;

		I_Object res = passToStream
			? this
			: Bool.getObject(check);

		res = Lib_Exec.execIf(cr, check, res, passToStream);
		return new ObjectCallResult(res, true);
	}

	/**
	 * °print()Same # Send the string, which will be generated by the toStr method, including a line break to StdOut.
	 * °print(Object obj)Same # Send the given argument including a line break to StdOut.
	 * °echo()Same # Send the string, which will be generated by the toStr method, without a line break to StdOut.
	 * °echo(Object obj)Same # Send the given argument without a line break to StdOut.
	 *
	 * for override
	 **/
	public I_Object mPrint(final CallRuntime cr, final boolean newline) {
		final I_Object[] args = cr.argsFlex(this, 0, 1);

		if(args != null && args.length == 1) // only 1 possible
			Lib_Output.out(cr, args[0], newline);
		else
			Lib_Output.out(cr, this, newline);

		return this;
	}

	/**
	 * @implNote
	 *           Needed for empty instance, used with FuncLet
	 */
	public void setAlreadyInit() {
		this.init = true;
	}


	// --- Protected ---

	/**
	 * Default implementation, that should be override
	 */
	@Override
	public String toString() {
		return this.getTypeName();
	}


	// --- Private Call-Functions ---

	/**
	 * Default implementation, that should be override
	 */
	public String toString(final CallRuntime cr, final STYPE type) {
		return this.getTypeName();
	}

	protected abstract ObjectCallResult call2(final CallRuntime cr, final String method);

	/** For override **/
	protected Boolean isGreater2(final I_Object o) {
		return null;
	}

	/**
	 * °async()%Nil # Execute block and stream in a new thread.
	 *
	 * @implNote Synchronize this with "App.iExec" and "A_EventObject.eventRun"
	 */
	protected ObjectCallResult mAsync(final CallRuntime cr) {
		cr.getStrict().checkSandbox(cr, ".async");
		cr.argsNone();
		final I_Object this2 = this;
		cr.getApp().registerFork();

		final Thread t = new A_Thread() {

			@Override
			public void task() {

				try {
					Lib_Exec.execBlockStream(cr, this2);
				}
				catch(final Throwable t) {
					// No exit here, cause that would destroy a GUI-App
					Lib_Error.handleThreadErrorEnd(cr, t);
				}
				finally {
					cr.getApp().checkExit(true);
				}
			}

		};
		t.start();
		return new ObjectCallResult(Nil.NIL, true);
	}

	/**
	 * °pass()Object # Execute Block and Stream, return the result.
	 */
	protected I_Object mPass(final CallRuntime crOld) {
		crOld.argsNone();
//		Lib_Exec.checkLoopWithout(crOld, args);		// Problem with Group: (3*(1/3)

		final CallRuntime crNew = crOld.copyPass();
//		if(args.length == 0 && cr.getCallBlock() == null && cr.getStream() == null)
////			throw new ExecError(cr, "A 'default' without argument, block and stream makes no sense!", "");
//			return this;

		return Lib_Exec.execBlockStream(crNew, this);
	}

	/**
	 * °sync()%Object # Synchronized execution of block and stream.
	 * °sync(Object obj)%Object # Synchronized execution of block and stream by using 'obj' as initial object.
	 */
	protected ObjectCallResult mSync(final CallRuntime cr) {
		cr.getStrict().checkSandbox(cr, ".sync");

		synchronized(this) {
			final I_Object[] args = cr.argsFlex(this, 0, 1);
			final Call stream = cr.getStream();
			final Block block = cr.getCallBlock();
			final int parlen = args.length;

			if(parlen == 0 && stream == null && block == null)
				throw new CodeError(cr, "Invalid call", "No argument, no stream, no block, for 'times' ... what to do?");

			I_Object res = this;
			final I_Object it = parlen == 0
				? this
				: args[0];

			if(block != null)
				try {
					res = block.exec(cr, it);
				}
				catch(final ReturnException e) {
					final Return temp = e.get();
					res = temp.getResult();

					switch(temp.getLevel()) {
						case BREAK:
							return temp.getLoopResult();
						default:
							throw new CodeError(cr, "Invalid exit for block", "Got " + temp.getLevel() + ", need " + LEVEL.BREAK);
					}
				}
			if(stream != null)
				res = cr.execInit(stream, it);

			return new ObjectCallResult(res, true);
		}
	}

//	private I_Object hint(final CallRuntime cr, final boolean args) {
//		cr.getStrict().checkSave(cr, "Hints");
//		final Str search = (Str)cr.args(this, Str.class)[0];
//
//		final MethodList ml = new MethodList();
//
//		// AutoObject direkt abfragen
//		if(this instanceof A_AutoObject)
//			((A_AutoObject)this).getMethods(ml);
//
//		// Ergänzen mit Infos aus den Hints-Files
//		final HintManager hints = cr.getApp().getHints();
//		if(args)
//			hints.searchFuncPars(ml, this.getTypeName(), search.getValue());
//		else
//			hints.searchFunc(ml, this.getTypeName(), search.getValue());
//
//		final String result = args
//			? ml.getPars(search.getValue())
//			: ml.get(search.getValue());
//
//		Lib_Output.out(cr.getApp(), result, true);
//		return this;
//	}

	/**
	 * @apiNote
	 *          Execute common functions
	 * @implNote
	 *           Update "blockedFunctions" at the top!
	 */
	private ObjectCallResult iCall(final CallRuntime cr, final String method) {

		switch(method) {
			// case "hint": ...
//			case "…":	//TODO ???
//				return A_Object.stdResult(this.hint(cr, false));
//			case "(…":	//TODO ???
//				return A_Object.stdResult(this.hint(cr, true));

			case "equally":
			case "isEqually":
			case "compares":
//			case "isLike":
			case "==~":
				return A_Object.stdResult(this.mIsEqual(cr, COMPARSION.LAZY, false));
			case "equals":
			case "isEqual":
			case "==":
				return A_Object.stdResult(this.mIsEqual(cr, COMPARSION.NORMAL, false));
//			case "is":	// already used
			case "same":
			case "isSame":
			case "===":
				return A_Object.stdResult(this.mIsEqual(cr, COMPARSION.STRICT, false));

			case "diffly":
			case "isDiffly":
//			case "isNotLike":
			case "!=~":
				return A_Object.stdResult(this.mIsEqual(cr, COMPARSION.LAZY, true));
			case "differs":
			case "isDifferent":
			case "!=":
			case "<>":
				return A_Object.stdResult(this.mIsEqual(cr, COMPARSION.NORMAL, true));
//			case "isNot":	// already used
			case "other":
			case "isOther":
			case "!==":
				return A_Object.stdResult(this.mIsEqual(cr, COMPARSION.STRICT, true));
			case "compare":
			case "compareTo":
				return A_Object.stdResult(this.mCompareTo(cr));

//			case "getType":
			case "type": // Necessary for self created Types
				cr.argsNone();
				return A_Object.stdResult(this.getType(cr));
//			case "getTypes":
			case "types": // Necessary for self created Types
				return A_Object.stdResult(this.mTypes(cr));
			case "isType":
			case "inherits":
				return A_Object.stdResult(this.mInheritsType(cr));
			case "assertType":
				return A_Object.stdResult(this.mAssertType(cr));
			case "assert":
			case "assertIf":
				return A_Object.stdResult(this.mAssertIf(cr));
			case "assertIs":
				return A_Object.stdResult(this.mAssertIs(cr));
//			case "objectId":
//			case "objectID":
//				return A_Object.stdResult(this.objectId(cr));

			case "isError":
				return A_Object.stdResult(this.mIsError(cr));
			case "isNil":
				return A_Object.stdResult(this.mIsNil(cr));
			case "isNotNil": // TODO isValid
				return A_Object.stdResult(this.mIsNotNil(cr));
			case "replaceNil":
				return A_Object.stdResult(this.mReplaceNil(cr));
			case "ifExec":
				return A_Object.stdResult(this.mIfExec(cr));
			case "ifNil":
				return this.mIfNil(cr, false);
			case "ifNotNil":
				return this.mIfNil(cr, true);

//			case "caseIf":
//				return this.switch_if(cr, method);
//			case "case":
//			case "caseIs":
//				return this.switch_is(cr);

			case "use":
				return this.mUse(cr);
//			case "useNil":
			case "nilUse":
				return this.mNilUse(cr);
			case "typeUse":
				return this.mTypeUse(cr);
//			case "errorUse":

			case "which":
				return A_Object.stdResult(this.mWhich(cr));

//			case "toString":	// Overwriting Java-Function?
			case "toStr":
				return A_Object.stdResult(this.mToStr(cr));
//			case "toDebug":
			case "toIdent":
//			case "toStrIdent":
				return A_Object.stdResult(this.mToStrIdent(cr));
			case "toDescribe":
//			case "toStrDescribe":
				return A_Object.stdResult(this.mToStrDescribe(cr));
			case "print":
				return A_Object.stdResult(this.mPrint(cr, true));
			case "echo":
				return A_Object.stdResult(this.mPrint(cr, false));
			case "debug":
				return A_Object.stdResult(this.mDebug(cr));
			case "printErr":
				return A_Object.stdResult(this.mPrintErr(cr, true));
			case "echoErr":
				return A_Object.stdResult(this.mPrintErr(cr, false));
			case "printLog":
				return A_Object.stdResult(this.mPrintLog(cr));
			case "ident":
				this.mIdent(cr);
				return A_Object.stdResult(this);
			case "describe":
				this.mDescribe(cr);
				return A_Object.stdResult(this);
			case "format":
				return A_Object.stdResult(this.mFormat(cr));

			case "while":
				return this.mWhile(cr, method);
			case "repeat":
				return this.mRepeat(cr, method);

			case "proc":
				return A_Object.stdResult(this.mProcTee(cr, false));
			case "tee":
				return A_Object.stdResult(this.mProcTee(cr, true));
//			case "finally":
//			case "go":
//			case "default":
			case "pass":
				return new ObjectCallResult(this.mPass(cr), true);
//			case "do": // Calls Auto-Block-Function		// Removed: call the real function (exp.: 'each') is much clearer
//				cr.getStrict().checkAutoBlock(cr);
//				if(!(this instanceof I_AutoBlockDo))
//					throw new ExecError(cr, "This object has no auto-block-function!", "Object: " + this.toString());
////				final I_Object doResult = this instanceof I_AutoBlockDo
////					? ((I_AutoBlockDo)this).autoBlockDo(cr)
////					: defaultGo(cr);
//				final I_Object doResult = ((I_AutoBlockDo)this).autoBlockDo(cr);
//				return new ObjectCallResult(doResult, true);

			/**
			 * °if()%Bool # Execute block and stream, if the current object is 'true'.
			 * °ifNot()%Bool # Execute block and stream, if the current object is 'false'.
			 * °passIf()%Atomic # Pass the current object, if the current object is 'true'.
			 * °passIfNot()%Atomic # Pass the current object, if the current object is 'false'.
			 *
			 * °if(Bool b)%Bool # Execute block and stream, if the argument is 'true'.
			 * °ifNot(Bool b)%Bool # Execute block and stream, if the argument is 'false'.
			 * °passIf(Bool b)%Atomic # Pass the current object, if the argument is 'true'.
			 * °passIfNot(Bool b)%Atomic # Pass the current object, if the argument is 'false'.
			 *
			 * °at ^ is
			 * °is(Atomic b...)%Bool # Execute block and stream, if the current object equals to one of the given arguments.
			 * °atNot ^ isNot
			 * °isNot(Atomic... b)%Bool # Execute block and stream, if no argument equals to the current object.
			 * °passAt ^ passIs
			 * °passAtNot ^ passIsNot
			 * °passIs(Atomic... b)%Atomic # Pass the current object, if it equals to one of the given arguments.
			 * °passIsNot(Atomic... b)%Atomic # Pass the current object, if no argument equals to the current object.
			 */
			// With argument allowed, so this is available for every Object, not only for Bool
			case "if":
				return this.mDoIf(cr, false, false, false);
			case "ifNot":
				return this.mDoIf(cr, true, false, false);
			case "at":
			case "is":
				return this.mDoIf(cr, false, false, true);
			case "atNot":
			case "isNot":
				return this.mDoIf(cr, true, false, true);
			case "passIf":
				return this.mDoIf(cr, false, true, false);
			case "passIfNot":
				return this.mDoIf(cr, true, true, false);
			case "passAt":
			case "passIs":
				return this.mDoIf(cr, false, true, true);
			case "passAtNot":
			case "passIsNot":
				return this.mDoIf(cr, true, true, true);

			case "breakAt":
			case "breakIs":
				return this.mBreakAt(cr, false);
			case "breakAtNot":
			case "breakIsNot":
				return this.mBreakAt(cr, true);
			case "breakIf":
				return this.mBreakIf(cr, false);
			case "breakIfNot":
				return this.mBreakIf(cr, true);

			case "pipeLive":
				return this.mPipe(cr, PIPE_TYPE.LIVE);
			case "pipe":
			case "pipeOutput":
				return this.mPipe(cr, PIPE_TYPE.OUTPUT);
			case "pipeError":
				return this.mPipe(cr, PIPE_TYPE.ERROR);
			case "pipeBuffer":
				return this.mPipe(cr, PIPE_TYPE.BUFFER);

			/**
			 * °try()%Object # Execute argument, stream or block. If an error occures return it, elsewhere return current Object
			 * °tryUse(Object alternative)Object # Execute block and stream. If an error occures return 'alternative'.
			 * °tryLazy()Object? # Try block and stream and return result. On error Nil will be returned.
			 */
			case "try":
				return Lib_Try.mErrTry(cr, this);
			case "tryUse":
				return Lib_Try.mErrTryUse(cr, this);
			case "catch":
				return this.mErrCatch(cr);
			case "tryLazy":
				return Lib_Try.mTryLazy(cr, this);

			case "mem":
				return this.mMem(cr);
			case "convertMem":
				return this.mConvertMem(cr);
//			case "varlet":
//			case "varLet":
//			case "handle":
			case "toTempLet":
//			case "toTemplet":
				return A_Object.stdResult(this.mToTempLet(cr));

			case "sync":
				return this.mSync(cr);
			case "async":
				return this.mAsync(cr);

			// Internal
			case "¶": // EndOfMultiCall  // Look: Parser_Call.parseParameterItem
				return this.mEndMultiCall(cr);

			case "+=>":
			case "-=>":
			case "*=>":
			case "/=>":
			case "%=>":
				return this.mCalcLetRight(cr, method);
			case "=>":
				return this.mLetRight(cr, false);
//			case "=~>":
			case "~=>":
				return this.mLetRight(cr, true);

			// To next instance
			default:
				return this.call2(cr, method);
		}
	}

	/**
	 * °assert ^ assertIf
	 * °assertIf(Bool b)Object # If 'b' is true, return the current object. Otherwise throw an error.
	 * °assertIf(Bool b, Str message)Object # If 'b' is true, return the current object. Otherwise throw an error including 'message'.
	 * TODO: .assertThat? = annahme dass
	 */
	private I_Object mAssertIf(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 1, 2);
		final boolean b = Lib_Convert.getBoolValue(cr, cr.argType(args[0], Bool.class));

		if(b)
			return this;
		else {
			final String message = args.length == 2 ? Lib_Convert.getStringValue(cr, args[1]) : "Invalid assertion, current object is: " + this.toString();
			throw new AssertError(cr, "Assert-Error!", message);
		}
	}

	/**
	 * °assertIs(Object o...)Same # If current object equals to o, return it. Otherwise throw an error.
	 */
	private I_Object mAssertIs(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 0);
		for(final I_Object arg : args)
			if(this.equals(arg))
				return this;

		final StringBuilder sb = new StringBuilder();
		sb.append("Current object should be [");
		sb.append(Lib_Convert.toString(cr, false, ",", args));
		sb.append("] but is: ");
		sb.append(this.toString(cr, STYPE.REGULAR));
		throw new AssertError(cr, "Assert-Error!", sb.toString());
	}

	/**
	 * °assertType(Str typename)Object # If the current object is or extends from type "typename", return it. Otherwise throw an error.
	 * °assertType(Type type)Object # If the current object is or extends from 'type', return it. Otherwise throw an error.
	 */
	private I_Object mAssertType(final CallRuntime cr) {
		final I_Object arg = cr.argsExt(this, new Class<?>[]{Str.class, JMo_Type.class})[0];
		final String s = arg instanceof Str ? Lib_Convert.getStringValue(cr, arg) : ((JMo_Type)arg).getName();
		Lib_Type.checkValidity(s, cr);
		if(Lib_Type.isInstanceOf(this, s))
			return this;
		throw new AssertError(cr, "Invalid type of current object!", "Type should be <" + s + ">, but is " + this.getType(cr).toString(cr, STYPE.REGULAR));
	}

	/**
	 * °breakAt ^ breakIs
	 * °breakIs(Atomic... a)Object # Break the current loop, if the current object is one of the arguments
	 * °breakAtNot ^ breakIsNot
	 * °breakIsNot(Atomic... a)Object # Break the current loop, if the current object is one of the arguments
	 */
	private ObjectCallResult mBreakAt(final CallRuntime cr, final boolean not) {
		final I_Object[] args = cr.argsVar(this, 1, 0);
		boolean valid = false;

		if(!not) {
			for(final I_Object arg : args)
				if(this.equals(arg)) {
					final Return rb = new Return(LEVEL.BREAK, this);
					return new ObjectCallResult(rb, true);
				}
			valid = true;
		}
		else
			for(final I_Object arg : args)
				if(this.equals(arg)) {
					valid = true;
					break;
				}

		if(valid) {
			final I_Object result = Lib_Exec.execBlockStream(cr, this);
			return new ObjectCallResult(result, true);
		}
		else {
			final Return rb = new Return(LEVEL.BREAK, this);
			return new ObjectCallResult(rb, true);
		}
	}

	/**
	 * °breakIf(Bool b)Object # Terminate the current loop, if the argument is true.
	 * °breakIf(VarLet v, Bool b)Object # Assign current object to 'v' and terminate the current loop, if the argument is true.
	 * °breakIfNot(Bool b)Object # Terminate the current loop, if the argument is false.
	 * °breakIfNot(VarLet v, Bool b)Object # Assign current object to 'v' and terminate the current loop, if the argument is false.
	 */
	private ObjectCallResult mBreakIf(final CallRuntime cr, final boolean not) {
		final Bool arg = (Bool)cr.argsEach(this, 0, new I_Object[]{this}, Bool.class);

		if(not ? arg == Bool.FALSE : arg == Bool.TRUE) {
			final Return rb = new Return(LEVEL.BREAK, this);
			return new ObjectCallResult(rb, true);
		}
		final I_Object result = Lib_Exec.execBlockStream(cr, this);
		return new ObjectCallResult(result, true);
	}

	/**
	 * °+=>()
	 * °-=>()
	 * °*=>()
	 * °/=>()
	 * °%=>()
	 *
	 * @implNote An variable must be already defined!
	 */
	private ObjectCallResult mCalcLetRight(final CallRuntime cr, String method) {
		// Direct access, cause the raw "Var" is needed

//		if(!cr.getStrict().isValid_AutoPass())
//			cr.blockForbidden();

		final I_Object[] args = cr.getArgs(this);
		Lib_Error.ifArgs(args.length, 1, 1, cr, this);
		final Var v = cr.argTypeMem(args[0], Var.class);

		method = method.substring(0, 2);
		final Call thisCall = new Call(cr, this);
		final Call callx = new Call(cr.getSurrBlock(), v, method, new Call[]{thisCall}, cr.getDebugInfo());
		final CallRuntime cr2 = cr.copyCall(callx, false);
//		return v.call2(cr2, method);
		final I_Object newObject = v.call2(cr2, method).obj;

//		I_Object newObject = v.get(cr);
		final CallRuntime crNew = cr.copyPass();
		final I_Object result = Lib_Exec.execBlockStream(crNew, newObject);
		return new ObjectCallResult(result, true);
	}

	/**
	 * °compare ^ compareTo
	 * °compareTo(Object obj)Int # Returns 0 if current and obj are equal, 1 if current is greater than obj, -1 if current is lesser than obj.
	 */
	private final Int mCompareTo(final CallRuntime cr) {
		final I_Object arg = cr.args(this, I_Object.class)[0];
		int result = this.compareTo(cr, arg);
		if(result > 1)
			result = 1;
		if(result < -1)
			result = -1;
		return new Int(result);
	}

	/**
	 * °convertMem( VarLet vl )Object # Convert and assign the current object to the VarLet
	 */
	private ObjectCallResult mConvertMem(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_MemLet.class)[0];
		final I_Mem var = ((A_MemLet)arg).getMem();
		var.let(cr, cr, this, true);
		return new ObjectCallResult(var, false); // Use the converted value
	}

	/**
	 * °describe()Same # Print out the full explaining of the current object.
	 * °describe(Object obj)Same # Print out the full explaining of the given object.
	 */
	private final void mDescribe(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 0, 1);
		final I_Object obj = args.length == 0
			? this
			: args[0];
		final String s = obj.toString(cr, STYPE.DESCRIBE);
		Lib_Output.out(cr.getApp(), s, true);
	}

	/**
	 * No documentation here
	 */
	private ObjectCallResult mEndMultiCall(final CallRuntime cr) {
		final Int multiCallKey = (Int)cr.args(this, Int.class)[0];
		cr.vce.useMultiCall().add(multiCallKey.getValue(), this);
		return new ObjectCallResult(this, true);
	}

	/**
	 * °catch()%Object? # If the current object is an error, execute block and stream, and return the result.
	 */
	private ObjectCallResult mErrCatch(final CallRuntime cr) {
		cr.argsNone(); // TODO 1 Argument?!?
		final Call stream = cr.getStream();
		final Block block = cr.getCallBlock();

		I_Object result = this;

		if(this instanceof JMo_Error) {
			if(block != null)
				try {
					result = block.exec(cr, result);
				}
				catch(final ReturnException e) {
					return new ObjectCallResult(e.get(), true); // Send to upper level
				}
			if(stream != null)
				result = cr.execInit(stream, result);
		}
		return new ObjectCallResult(result, true);
	}

	/**
	 * °format(Str form)Str # Fills every {}-placeholder in 'form' with current value.
	 */
	private Str mFormat(final CallRuntime cr) {
		final I_Object arg = cr.args(this, Str.class)[0];
		final String format = Lib_Convert.getStringValue(cr, arg);
		return Lib_StrFormat.fill(cr, format, this);
	}

	/**
	 * °ident()Same # Print out the ident explaining of the current object.
	 * °ident(Object obj)Same # Print out the ident explaining of the given argument.
	 */
	private final void mIdent(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 0, 1);
		final I_Object obj = args.length == 0
			? this
			: args[0];
		final String s = obj.toString(cr, STYPE.IDENT);
		Lib_Output.out(cr.getApp(), s, true);
	}

	/**
	 * °ifExec(Object args...)Object # If this object is a FuncLet, execute it with args.
	 */
	private I_Object mIfExec(final CallRuntime cr) {
//		final I_Object[] o = cr.parsFlex(null, this, 0, Integer.MAX_VALUE);	// No check here!
		if(this instanceof FuncLet)
			return ((FuncLet)this).exec(cr);
		return this;
	}

	/**
	 * °ifNil()%Bool # If this object is nil, execute Block and Stream. Otherwise pass 'false' to the stream.
	 * °ifNotNil()%Bool # If this object is not nil, execute Block and Stream. Otherwise pass 'false' to the stream.
	 */
	private ObjectCallResult mIfNil(final CallRuntime cr, final boolean not) {
		cr.argsNone();
		final Call stream = cr.getStream();
		final Block block = cr.getCallBlock();

		if(stream == null && block == null)
			return new ObjectCallResult(this, true);

		boolean check = this == Nil.NIL;
		if(not)
			check = !check;

		I_Object res = Bool.getObject(check);
		res = Lib_Exec.execIf(cr, check, res, false);
		return new ObjectCallResult(res, true);
	}

	/**
	 * °isType ^ inherits
	 * °inherits(Str typename)Bool # Returns true if the current object is of this type, either directly or inherited. Otherwise return false.
	 * °inherits(Type type)Bool # Returns true if the current object is of this type, either directly or inherited. Otherwise return false.
	 */
	private Bool mInheritsType(final CallRuntime cr) {
		final I_Object arg = cr.argsExt(this, new Class<?>[]{Str.class, JMo_Type.class})[0];

		if(arg instanceof Str) {
			final String s = Lib_Convert.getStringValue(cr, arg);
			Lib_Type.checkValidity(s, cr);
			return Bool.getObject(Lib_Type.isInstanceOf(this, s));
		}
		else {
			final String s = ((JMo_Type)arg).getName();
			return Bool.getObject(Lib_Type.isInstanceOf(this, s));
		}
	}

	/**
	 * °==~ ^ equally
	 * °isEqually ^ equally
	 * °compares ^ equally
	 * °equally(Object other)Bool # Returns true if current and other object are equally.
	 *
	 * °== ^ equals
	 * °isEqual ^ equals
	 * °equals(Object other)Bool # Returns true if current and other object are equal.
	 *
	 * °=== ^ same
	 * °isSame ^ same
	 * °same(Object other)Bool # Returns true if corrent and other object are the same.
	 *
	 *************************************
	 *
	 * °!=~ ^ diffly
	 * °isDiffly ^ diffly
	 * °diffly(Object other)Bool # Returns true if current and other object are diffly.
	 *
	 * °!= ^ differs
	 * °<> ^ differs
	 * °isDifferent ^ differs
	 * °differs(Object other)Bool # Returns true if current and other object are different.
	 *
	 * °!== ^ other
	 * °isOther ^ other
	 * °other(Object other)Bool # Returns true if current and other object are not the same.
	 */
	private I_Object mIsEqual(final CallRuntime cr, final COMPARSION comparsion, final boolean not) {
		final Object arg = cr.args(this, I_Object.class)[0];
		boolean result = comparsion == COMPARSION.NORMAL
			? this.equals(arg)
			: comparsion == COMPARSION.LAZY
				? this.equalsLazy(arg)
				: this.equalsStrict(arg);
		if(not)
			result = !result;

		return Bool.getObject(result);
	}

	/**
	 * °isError()Bool # Returns true if this object is an Error.
	 */
	private Bool mIsError(final CallRuntime cr) {
		cr.argsNone();
		return Bool.getObject(this instanceof JMo_Error);
	}

	/**
	 * °isNil()Bool # Returns true if this object is Nil.
	 */
	private Bool mIsNil(final CallRuntime cr) {
		cr.argsNone();
//		return Bool.getObject(this == Nil.NIL);
		return Bool.getObject(this == Nil.NIL);
	}

	/**
	 * °isNotNil()Bool # Returns true if this object is not Nil.
	 */
	private Bool mIsNotNil(final CallRuntime cr) {
		cr.argsNone();
		return Bool.getObject(this != Nil.NIL);
	}

//	private ObjectCallResult divide(final CallRuntime cr) {
//		Lib_Error.ifPars(cr.parCount(), 2, 2, cr);
//		final Bool par1 = (Bool)cr.parsOneAdvance(this, 0, Bool.class);
//		final boolean b = Lib_Convert.getBoolValue(cr, par1);
//
//		I_Object result = null;
//		if(b) {
//			result = cr.parsOneAdvance(this, 1, I_Object.class);
//		}
//		else {
//			final CallRuntime crNew = cr.copyLoop(new Handle_Loop(this));	//TODO cr.execBlockStream() ?!?
//			result = Lib_Exec.execBlockStream(crNew, new BlockExecArgs(), this);
//		}
//		return new ObjectCallResult(result, true);
//	}

	/**
	 * °=>()
	 * °~=>()
	 *
	 * @implNote If a Block is available, the Var/Const is defined for this block!
	 */
	private ObjectCallResult mLetRight(final CallRuntime cr, final boolean convert) {
		// Direct access, cause the raw "Var" is needed

		final CallRuntime crNew = cr.copyPass();
		final CallRuntime crLet = cr.hasBlock() ? crNew : cr;

		final I_Object[] args = cr.getArgs(this);
		Lib_Error.ifArgs(args.length, 1, 1, cr, this);
		final I_Mem arg = cr.argTypeMem(args[0], I_Mem.class);
		I_Object newObject = null;

		if(arg instanceof Var) {
			final Var v = (Var)arg;
			v.let(crLet, crLet, this, convert);
			newObject = convert ? v.get(crLet) : this;
		}
		else {
			final Const c = (Const)arg;
			// 'convert' will be ignored!!! Const has no type, so theres no dest type to convert to
			c.let(crLet, crLet, this, convert);
			newObject = this; //c.get(cr);
		}
		final I_Object result = Lib_Exec.execBlockStream(crNew, newObject);
		return new ObjectCallResult(result, true);
	}

	/**
	 * °mem( MemLet vl )Object # Assign the current object to the VarLet
	 */
	private ObjectCallResult mMem(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_MemLet.class)[0];
		final I_Mem mem = ((A_MemLet)arg).getMem();
		mem.let(cr, cr, this, false);
		return new ObjectCallResult(this, false); // Use the converted value
	}

	/**
	 * °nilUse(Object ifNil, Object ifNotNil)§Object # If the current object is nil, then use the first object, otherwise use the second
	 */
	private ObjectCallResult mNilUse(final CallRuntime cr) {
		Lib_Error.ifArgs(cr.argCount(), 2, 2, cr, this);
		I_Object result = cr.argsOneAdvance(this, this == Nil.NIL ? 0 : 1, I_Object.class);

		final CallRuntime crNew = cr.copyPass();
		result = Lib_Exec.execBlockStream(crNew, result);
		return new ObjectCallResult(result, true);
	}

	/**
	 * °pipe ^ pipeOutput
	 * °pipeOutput(Str cmd)Str # Converts the current object with '.toStr' to an string, append it with a single space to the given command and execute this. This function returns the output from stdOut of the command.
	 * °pipeError(Str cmd)Str # Converts the current object with '.toStr' to an string, append it with a single space to the given command and execute this. This function returns the output from stdErr of the command.
	 * °pipeLive(Str cmd)Int # Converts the current object with '.toStr' to an string, append it with a single space to the given command and execute this. This function returns the exit value of the command.
	 * °pipeBuffer(Str cmd)FunctionMap # Converts the current object with '.toStr' to an string, append it with a single space to the given command and execute this. This function returns a FunctionMap.
	 */
	private final ObjectCallResult mPipe(final CallRuntime cr, final PIPE_TYPE type) {
		final I_Object arg = cr.args(this, Str.class)[0];
//		cr.getStrict().checkSave(cr, "Str.command");

		final String argStr = this.toString(cr, STYPE.REGULAR);
		final String argQuoteStr = FormString.quote(argStr, '"', '\\', false);
		final String cmd = Lib_Convert.getStringValue(cr, arg) + ' ' + argQuoteStr;
		final String cmdMet = type == PIPE_TYPE.LIVE
			? "live"
			: type == PIPE_TYPE.OUTPUT
				? "output"
				: type == PIPE_TYPE.ERROR
					? "error"
					: "buffer";

		final Call cmdArgCall = new Call(cr, new Str(cmd));
		final JMo_Cmd cmdObj = new JMo_Cmd(cmdArgCall);
		final Call cmdCall = new Call(cr.getSurrBlock(), cmdObj, cmdMet, null, cr.getDebugInfo());
		final CallRuntime cmdCR = cr.copyCall(cmdCall, false);

		final I_Object result = cmdCR.exec(this, true);
		return new ObjectCallResult(result, false);
	}

	/**
	 * °printErr()Same # Send the string, which will be generated by the toStr method, including a line break to StdErr.
	 * °printErr(Object obj)Same # Send the string representation of the given argument including a line break to StdErr.
	 * °echoErr()Same # Send the string, which will be generated by the toStr method, without a line break to StdErr.
	 * °echoErr(Object obj)Same # Send the string representation of the given argument without a line break to StdErr.
	 **/
	private final I_Object mPrintErr(final CallRuntime cr, final boolean newline) {
		final I_Object[] args = cr.argsFlex(this, 0, 1);

		if(args != null && args.length == 1) // only 1 possible
			Lib_Output.err(cr, args[0], newline);
		else
			Lib_Output.err(cr, this, newline);

		return this;
	}

	/**
	 * °printLog()Same # Send the string representation of the current object to the log file.
	 * °printLog(Object obj)Same # Send the string representation of the given argument to the log file.
	 **/
	private final I_Object mPrintLog(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 0, 1);
		final I_Object obj = args != null && args.length == 1 // only 1 possible
			? args[0]
			: this;

		Lib_Output.log(cr, obj);
		return this;
	}

	/**
	 * ||#proc()Object Processes the associated block and returns the result
	 * °proc(Object arg)Object # Process 'Par', if applicable execute a 'FuncLet' and return the result
	 * °proc(MemLet v, Object arg)Object # Assign the current Object to 'v', process 'arg', if applicable execute a 'FuncLet' and return the result
	 * ||#tee()Object
	 * °tee(Object arg)Object # Process 'Par', if applicable execute a 'FuncLet' and returns the current object
	 * °tee(MemLet v, Object arg)Object # Assign the current Object to 'v', process 'arg', if applicable execute a 'FuncLet' and return the current object
	 */
	private I_Object mProcTee(final CallRuntime cr, final boolean tee) {
		// No Block allowed
		I_Object result = null;

		if(cr.argCount() == 2) {
			final A_MemLet v = (A_MemLet)cr.argsOneAdvance(this, 0, A_MemLet.class);
			v.getMem().let(cr, cr, this);
			result = cr.argsOneAdvance(this, 1, I_Object.class);
		}
		else
			result = cr.args(this, I_Object.class)[0];
		if(result instanceof FuncLet)
			result = ((FuncLet)result).eachExec(cr, this);
		return tee ? this : result;
	}

	/**
	 * °repeat()§Object # Execute and repeat block and stream until the current object (must be a VarLet) has the value 'false'
	 * °repeat(Bool b)§Object # Repeat block and stream until 'b' is 'false'
	 * °repeat(FuncLet f)§Object # Repeat block and stream until 'f' returns 'false'
	 * °repeat(VarLet var, Bool b)§Object # Assign current object to 'var' and repeat block and stream until 'b' is 'false'
	 */
	private ObjectCallResult mRepeat(final CallRuntime crOld, final String method) {
		if(crOld.getStream() == null && crOld.getCallBlock() == null) // && args==null
			return new ObjectCallResult(this, true);

		boolean varlet = false;

		if(crOld.argCount() == 0) {
			varlet = true;
			if(!(this instanceof VarLet))
				throw new RuntimeError(crOld, "Invalid condition for 'while'!", "Got no arguments and current object isn't a VarLet.");
		}
		final LoopHandle handle = new LoopHandle(this);
		final CallRuntime crNew = crOld.copyLoop(handle);

		I_Object result = this;
		I_Object check = Nil.NIL;
		if(varlet)
			check = ((VarLet)this).getMem().get(crOld);
		else
			check = crOld.copyEach(method).argsEach(this, 0, new I_Object[]{result}, Bool.class);
		boolean b = Lib_Convert.getBoolValue(crNew, check);

		do {
			handle.startLap();
			result = Lib_Exec.execBlockStream(crNew, result);

			if((result = Lib_Exec.loopResult(result)) instanceof Return)
				return ((Return)result).getLoopResult();

			if(varlet)
				check = ((VarLet)this).getMem().get(crOld);
			else
				check = crOld.copyEach(method).argsEach(this, 0, new I_Object[]{result}, Bool.class);
			b = Lib_Convert.getBoolValue(crNew, check);
		}
		while(b);
		return new ObjectCallResult(result, true);
	}

	/**
	 * °replaceNil(Object arg)Object # If this Object is Nil, return the argument.
	 */
	private I_Object mReplaceNil(final CallRuntime cr) {
		final I_Object o = cr.args(this, I_Object.class)[0];
		return this == Nil.NIL
			? o
			: this;
	}

	/**
	 * °toStr()Str # Returns the string representation of this object
	 */
	private Str mToStr(final CallRuntime cr) {
		cr.argsNone();
		return new Str(this.toString(cr, STYPE.REGULAR));
	}

	/**
	 * #°toDescribe ^ toStrDescribe
	 * °toDescribe()Str # Returns the fully descriptive representation of this object
	 */
	private Str mToStrDescribe(final CallRuntime cr) {
		cr.argsNone();
		return new Str(this.toString(cr, STYPE.DESCRIBE));
	}

	/**
	 * ###°objectId()Int # Returns the ID of this object.
	 */
//	private Int objectId(CallRuntime cr) {
//		cr.argsNone();
//		return new Int(this.id);
//	}

	/**
	 * #°toIdent ^ toStrIdent
	 * °toIdent()Str # Returns a string which identifies this object
	 */
	private Str mToStrIdent(final CallRuntime cr) {
		cr.argsNone();
		return new Str(this.toString(cr, STYPE.IDENT));
	}

	/**
	 * #°toTemplet ^ toTempLet
	 * °toTempLet()ConstLet # Creates a temporary ConstLet for the current object
	 */
	private I_Object mToTempLet(final CallRuntime cr) {
		cr.argsNone();
		final TempConst tmpVar = new TempConst();
		tmpVar.let(cr, cr, this);
		return new ConstLet(tmpVar);
	}

	/**
	 * °types()List # Returns a list with the hierarchy of types from this object.
	 */
	private JMo_List mTypes(final CallRuntime cr) {
		cr.argsNone();
		final String[] types = Lib_Type.getTypeNames(this);
		final SimpleList<I_Object> list = new SimpleList<>(types.length);
		for(final String type : types)
			list.add(new JMo_Type(type, cr));
		return new JMo_List(list);
	}

	/**
	 * °typeUse(Type#Str type, Object ifHit, Object ifOther)§Object # If the current object is from type 'type', use the first object, otherwise use the second
	 */
	private ObjectCallResult mTypeUse(final CallRuntime cr) {
		Lib_Error.ifArgs(cr.argCount(), 3, 3, cr, this);
		final I_Object typeNeeded = cr.argsOneAdvanceExt(this, 0, new Class[]{Str.class, JMo_Type.class});

		final String typeNeeded2 = typeNeeded instanceof Str ? Lib_Convert.getStringValue(cr, typeNeeded) : ((JMo_Type)typeNeeded).getName();
		Lib_Type.checkValidity(typeNeeded2, cr);
//		final boolean hit = this.getTypeName().equals(Lib_Convert.getStringValue(cr, typeNeeded));
		final boolean hit = Lib_Type.isInstanceOf(this, typeNeeded2);
		I_Object result = cr.argsOneAdvance(this, hit ? 1 : 2, I_Object.class);

		final CallRuntime crNew = cr.copyPass();
		result = Lib_Exec.execBlockStream(crNew, result);
		return new ObjectCallResult(result, true);
	}

	/**
	 * °use(Object a, Object b)§Object # If the current value is true, return the argument 'a', otherwise return 'b'
	 * °use(Bool x, Object a, Object b)§Object # If 'x' is true, return the argument 'a', otherwise return 'b'
	 */
	private ObjectCallResult mUse(final CallRuntime cr) {
		Lib_Error.ifArgs(cr.argCount(), 2, 3, cr, this);
		I_Object result = null;

		if(cr.argCount() == 3) {
//			Lib_Error.ifPars(cr.parCount(), 3, 3, cr);
			final Bool b = (Bool)cr.argsOneAdvance(this, 0, Bool.class);
			final boolean hit = Lib_Convert.getBoolValue(cr, b);
			result = cr.argsOneAdvance(this, hit ? 1 : 2, I_Object.class);
		}
		else {
			if(!Lib_Type.typeIs(cr, this, Bool.class))
				throw new RuntimeError(cr, "Invalid type of current object", "If current object isn't a 'Bool' value, use 3 arguments 'use(Bool, Object, Object)'");

			final int option = ((Bool)this).getValue() ? 0 : 1;
			result = cr.argsOneAdvance(this, option, I_Object.class);
		}
		final CallRuntime crNew = cr.copyPass();
		result = Lib_Exec.execBlockStream(crNew, result);
		return new ObjectCallResult(result, true);
	}

	/**
	 * °which(Bool args...)Int? # Returns the Nr of the first matching argument. If nothing is true, nil will be returned.
	 */
	private I_Object mWhich(final CallRuntime cr) {

		for(int parIdx = 0; parIdx < cr.argCount(); parIdx++) {
			final Bool b1 = (Bool)cr.argsOneAdvance(this, parIdx, Bool.class);
			if(b1 == Bool.TRUE)
				return new Int(parIdx + 1);
		}
		return Nil.NIL;
	}

	/**
	 * °while()§Object # Repeat block and stream until the current object (must be a VarLet) has the value 'false'
	 * °while(Bool b)§Object # Repeat block and stream until 'b' is 'false'
	 * °while(FuncLet f)§Object # Repeat block and stream until 'f' returns 'false'
	 * °while(VarLet var, Bool b)§Object # Assign current object to 'var' and repeat block and stream until 'b' is 'false'
	 */
	private ObjectCallResult mWhile(final CallRuntime crOld, final String method) {
		boolean varlet = false;

		if(crOld.argCount() == 0) {
			varlet = true;
			if(!(this instanceof VarLet))
				throw new RuntimeError(crOld, "Invalid condition for 'while'!", "Got no arguments and current object isn't a VarLet.");
		}
		if(crOld.getStream() == null && crOld.getCallBlock() == null) // && args==null
			return new ObjectCallResult(this, true);

		final LoopHandle handle = new LoopHandle(this);
		final CallRuntime crNew = crOld.copyLoop(handle);

		I_Object result = this;
		boolean b = false;
		I_Object check = Nil.NIL;
		if(varlet)
			check = ((VarLet)this).getMem().get(crOld);
		else
			check = crOld.copyEach(method).argsEach(this, 0, new I_Object[]{result}, Bool.class);

		b = Lib_Convert.getBoolValue(crNew, check);

		while(b) {
			handle.startLap();
			result = Lib_Exec.execBlockStream(crNew, result);

			if((result = Lib_Exec.loopResult(result)) instanceof Return)
				return ((Return)result).getLoopResult();

			if(varlet)
				check = ((VarLet)this).getMem().get(crOld);
			else
				check = crOld.copyEach(method).argsEach(this, 0, new I_Object[]{result}, Bool.class);

			b = Lib_Convert.getBoolValue(crNew, check);
		}
		return new ObjectCallResult(result, true);
	}


	/**
	 * #°caseIf(Bool b)§%%Object
	 */
//	private ObjectCallResult switch_if(final CallRuntime cr, final String method) {
//		if(this == Nil.NIL)
//			return new ObjectCallResult(this, true);
//
//
////		final Bool arg = (Bool)cr.parsBlock(this, Bool.class)[0];
////		final Bool arg = (Bool)cr.parsBlockEach(this, new I_Object[]{this.value.get()}, Bool.class)[0];
//		I_Object result = this;
//		final I_Object[] each = new I_Object[]{result};
//		final Bool arg = (Bool)cr.copyEach(method).argsEach(this, 0, each, Bool.class);
//		cr.blockNecessary(); //	A 'case' without block makes no sense!
//
////		final Call stream = cr.getStream();
////		final Block block = cr.getCallBlock();
//
////		final CallRuntime crNew = crOld.copyPass();	// TODO ?!?!?
//
//		boolean hit = false;
//
//		if(arg.getValue()) {
//			hit = true;
//			result = Lib_Exec.execBlockStream(cr, this);
////				this.total = result;
//		}
//		else
//			result = Lib_Exec.execOnlyStream(cr, this);
//
//		return new ObjectCallResult(hit ? Nil.NIL : this, true);
//
//
//
//		// Exec Block
////		I_Object res = Lib_Exec.execBlock(cr, new BlockExecArgs(args), this);
////		if(arg.getValue()) {
////			try {
////				res = block.exec(cr, res); //[1];
////			}
////			catch(final ReturnException e) {
////				res = e.get(); // Send to upper level
////			}
////			return new ObjectCallResult(res, true);
////		}
////		else {
////			if(stream != null)
////				res = cr.execInit(stream, this);
////			return new ObjectCallResult(res, true);
////		}
//	}

	/**
	 * #°case ^ caseIs
	 * #°caseIs(Object o)%%Object
	 */
//	private ObjectCallResult switch_is(final CallRuntime cr) {	// TODO Soweit in der Form okay. Nachteil: alle nachfolgenden ".case" und der gesamte Stream wird jedesmal voll abgearbeitet!
//		final I_Object arg = cr.args(this, (Class<?>)null)[0];
//
////		A 'is' without block makes no sense!
//		cr.blockNecessary();
//
////		final Call stream = cr.getStream();
////		final Block block = cr.getCallBlock();
//
//		I_Object result = this;
//
////		if(this instanceof JMo_List) {
////			MOut.line(this);
////			result = ((JMo_List)this).getInternalCollection().get(0); // TODO check size
////			return new ObjectCallResult( result , true);
////		}
////		if(this == Nil.NIL) {
////			MOut.line(this);
////			result = ((JMo_List)this).getInternalCollection().get(0); // TODO check size
////			return new ObjectCallResult( result , true);
////		}
//
////		final CallRuntime crNew = crOld.copyPass();	// TODO ?!?!?
//
//
//		if(this.equals(arg)) {
//
////			SimpleList<I_Object> al = new SimpleList<>();
////			al.add(result);
////			result = new JMo_List(al);
////			result = Nil.NIL;
//
//
//			result = Lib_Exec.execBlockStream(cr, result);
////				this.total = result;
//
////			if(cr.call.getStream() != null) {
////				result;
////			}
////			else {
////			}
//
//		}
//		else {
//			result = Lib_Exec.execOnlyStream(cr, this);
//
////			if(result instanceof JMo_List) {
////				MOut.line(result);
////				result = ((JMo_List)result).getInternalCollection().get(0); // TODO check size
////				return new ObjectCallResult( result , true);
////			}
//		}
//
//
//
//
////		return new ObjectCallResult(this, true);
////		MOut.temp(hit, result, this);
//		return new ObjectCallResult( result , true);
////		return new ObjectCallResult( this , true);
//
//
//
////		// Exec Block
////		if(this.value.get().equals(arg)) {
////			try {
////				res = block.exec(cr, res); //[1];
////			}
////			catch(final ReturnException e) {
////				res = e.get(); // Send to upper level
////			}
////			return new ObjectCallResult(res, true);
////		}
////		else {
////			if(stream != null)
////				res = cr.execInit(stream, this);
////			return new ObjectCallResult(res, true);
////		}
//	}

}
