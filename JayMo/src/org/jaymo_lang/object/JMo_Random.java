/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.A_Number;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Char;
import org.jaymo_lang.object.atom.I_Integer;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.data.constant.CHARS;
import de.mn77.base.data.util.Lib_Random;


/**
 * @author Michael Nitsche
 * @created 28.03.2018
 */
public class JMo_Random extends A_Object implements I_Object {

	/**
	 * +Random() # Random
	 */
	@Override
	public void init(final CallRuntime cr) {}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		// WICHTIG TODO Prüfen, wenn keine Arguments oder 'with' verwendet werden!

		switch(method) {
			case "newInt":
			case "int":
				return A_Object.stdResult(this.mGetInt(cr));

			case "newDec":
			case "dec":
				return A_Object.stdResult(this.mGetDec(cr));

			case "newDouble":
			case "double":
				return A_Object.stdResult(this.mGetDouble(cr));

			case "newBool":
			case "bool":
				return A_Object.stdResult(this.mGetBool(cr));

			case "newChar":
			case "char":
				return A_Object.stdResult(this.mGetChar(cr));

			case "newStr":
//			case "newString":
			case "str":
//			case "string":
				return A_Object.stdResult(this.mGetStr(cr));

			default:
				return null;
		}
	}

	private String iParsToOptions(final CallRuntime cr, final I_Object[] args, final int offset) {
		String options = "";

		for(int i = offset; i < args.length; i++) {
			final I_Object arg = args[i];

			final I_Object p = cr.argType(arg, I_Object.class);
			if(p instanceof Str)
				options += ((Str)p).getValue();
			else if(p instanceof Char)
				options += ((Char)p).getValue();
			else if(p instanceof JMo_Range)
				options += ((JMo_Range)p).computeString(cr);
			else if(p instanceof I_Integer) {
				final int val = Lib_Convert.getIntValue(cr, p);
				Lib_Error.ifNotBetween(cr, 0, 9, val, "Integer-Number");
				options += val;
			}
			else
				throw new RuntimeError(cr, "Invalid arguments", "Only <Str>, <Char>, <Range> and Integer-Values(0-9) are allowed. Got: " + arg);
		}
		if(options.length() == 0)
			throw new RuntimeError(cr, "Invalid arguments", "Missing argument(s) with available characters!");

		return options;
	}

	/**
	 * °bool ^ readBool
	 * °newBool()Bool # Get a random Bool value
	 */
	private Bool mGetBool(final CallRuntime cr) {
		cr.argsNone();
		final boolean r = Lib_Random.getBool();
		return Bool.getObject(r);
	}

	/**
	 * °char ^ newChar
	 * °newChar()Char # Get a single char ( A-Z, a-z )
	 * °newChar(Object o...)Char # Get a random char from multiple selections of characters. Possiple selections are: chars 'a', strings "abc", ranges 'a'..'z', integers 0 up to 9
	 */
	private Char mGetChar(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 0, 0);
		final String options = args.length > 0 ? this.iParsToOptions(cr, args, 0) : CHARS.ABC_UPPER + CHARS.ABC_LOWER;

		final int r = Lib_Random.getInt(0, options.length() - 1);
		return new Char(options.charAt(r));
	}

	/**
	 * °dec ^ newDec
	 * °newDec()Dec # Get a decimal number in the full range of Dec
	 * °newDec(Number max)Dec # Get a decimal number between 1 and max
	 * °newDec(Number min, Number max)Dec # Get a decimal number between min and max
	 */
	private JMo_Dec mGetDec(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 0, 2);
		final int len = args.length;

		if(len == 0) {
			final double r = Lib_Random.getDouble(Double.MIN_VALUE, Double.MAX_VALUE);
			return JMo_Dec.valueOf(cr, r);
		}

		if(len == 1) {
			final I_Object p1 = cr.argType(args[0], A_Number.class);
			final double d1 = Lib_Convert.getDoubleValue(cr, p1);
			final double r = Lib_Random.getDouble(1, d1); // 1 bis max
			return JMo_Dec.valueOf(cr, r);
		}
		// if(len == 2) {
		final I_Object p1 = cr.argType(args[0], A_Number.class);
		final I_Object p2 = cr.argType(args[1], A_Number.class);
		final double d1 = Lib_Convert.getDoubleValue(cr, p1);
		final double d2 = Lib_Convert.getDoubleValue(cr, p2);
//		Lib_Error.ifTooLow(cr, d1, d2);
		final double r = Lib_Random.getDouble(d1, d2);
		return JMo_Dec.valueOf(cr, r);
	}

	/**
	 * °double ^ newDouble
	 * °newDouble()Dec # Get a decimal number in the full range of Dec
	 * °newDouble(Number max)Dec # Get a decimal number between 1 and max
	 * °newDouble(Number min, Number max)Dec # Get a decimal number between min and max
	 */
	private JMo_Double mGetDouble(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 0, 2);
		final int len = args.length;

		if(len == 0) {
			final double r = Lib_Random.getDouble(Double.MIN_VALUE, Double.MAX_VALUE);
			return new JMo_Double(r);
		}

		if(len == 1) {
			final I_Object p1 = cr.argType(args[0], A_Number.class);
			final double d1 = Lib_Convert.getDoubleValue(cr, p1);
			final double r = Lib_Random.getDouble(1, d1); // 1 bis max
			return new JMo_Double(r);
		}
		// if(len == 2) {
		final I_Object p1 = cr.argType(args[0], A_Number.class);
		final I_Object p2 = cr.argType(args[1], A_Number.class);
		final double d1 = Lib_Convert.getDoubleValue(cr, p1);
		final double d2 = Lib_Convert.getDoubleValue(cr, p2);
//		Lib_Error.ifTooLow(cr, d1, d2);
		final double r = Lib_Random.getDouble(d1, d2);
		return new JMo_Double(r);
	}

	/**
	 * °int ^ newInt
	 * °newInt()Int # Get a int number in the full range of Int
	 * °newInt(IntNumber max)Int # Get a int number between 1 and max
	 * °newInt(IntNumber min, IntNumber max)Int # Get a int number between min and max
	 */
	private Int mGetInt(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 0, 2);
		final int len = args.length;

		if(len == 0) {
			final int r = Lib_Random.getInt();
			return new Int(r);
		}

		if(len == 1) {
			final I_Object p1 = cr.argType(args[0], A_IntNumber.class);
			final int i1 = Lib_Convert.getIntValue(cr, p1);
			final int r = Lib_Random.getInt(1, i1); // 1 bis max
			return new Int(r);
		}
		// if(len == 2) {
		final I_Object p1 = cr.argType(args[0], A_IntNumber.class);
		final I_Object p2 = cr.argType(args[1], A_IntNumber.class);
		final int i1 = Lib_Convert.getIntValue(cr, p1);
		final int i2 = Lib_Convert.getIntValue(cr, p2);
		Lib_Error.ifTooSmall(cr, i1, i2);
		final int r = Lib_Random.getInt(i1, i2);
		return new Int(r);
	}

	/**
	 * °str ^ newStr
	 * #°string ^ newStr
	 * #°newString ^ newStr
	 * °newStr(IntNumber length, Object o...)Str # Get a random Str from multiple selections of characters. Possiple selections are: chars 'a', strings "abc", ranges 'a'..'z', integers 0 up to 9
	 */
	private Str mGetStr(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 1);
		final int len = Lib_Convert.getIntValue(cr, cr.argType(args[0], A_IntNumber.class));

		final String source = this.iParsToOptions(cr, args, 1);

		final StringBuffer sb = new StringBuffer();

		for(int i = 1; i <= len; i++) {
			final int r = Lib_Random.getInt(0, source.length() - 1);
			sb.append(source.charAt(r));
		}
		return new Str(sb.toString());
	}

}
