/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 24.08.2019
 */
public class JMo_Use extends A_Object implements I_AutoBlockDo, I_ControlObject { // TODO: In Classic?!?

	private final ArgCallBuffer parCond;
	private final ArgCallBuffer parThen;
	private final ArgCallBuffer parElse;
	private I_Object            with = null;


	/**
	 * +Use(Object condition, Object then, Object else) # Use(false, 7, 3)
	 * %.get
	 */
	public JMo_Use(final Call parCond, final Call parThen, final Call parElse) {
		Err.ifNull(parCond, parThen, parElse);
		this.parCond = new ArgCallBuffer(0, parCond);
		this.parThen = new ArgCallBuffer(1, parThen);
		this.parElse = new ArgCallBuffer(2, parElse);
	}

	public I_Object autoBlockDo(final CallRuntime cr) {
		return ((A_Object)this.with).mPass(cr);
	}

	@Override
	public void init(final CallRuntime cr) {
		final Bool cond = this.parCond.init(cr, this, Bool.class);
		final boolean condition = Lib_Convert.getBoolValue(cr, cond);
		this.with = condition
			? this.parThen.init(cr, this, I_Object.class)
			: this.parElse.init(cr, this, I_Object.class);
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final STYPE subType = type.getNested();
		final StringBuilder sb = new StringBuilder();
		sb.append(this.getTypeName());
		sb.append('(');
		sb.append(this.parCond.toString(cr, subType));
		sb.append(',');
		sb.append(this.parThen.toString(cr, subType));
		sb.append(',');
		sb.append(this.parElse.toString(cr, subType));
		sb.append(")");
		return sb.toString();
	}

	/**
	 * °get()Object # Returns object1 at condition true, otherwise object2
	 */
	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "get":
//				cr.argsNone();
//				return A_Object.stdResult(this.with);
				return new ObjectCallResult(this.autoBlockDo(cr), true);
			default:
				return null;
		}
	}

}
