/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;


/**
 * @author Michael Nitsche
 * @created 17.10.2019
 *
 * @implNote
 *           Java-Output example:
 *           [lkj, oiu, [3, t], {asdf=987, lkjh=234}, java.lang.Object@28a418fc, 4,5,6
 *           7,8,9, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
 *
 */
public interface I_ObjectToString {

	String getTypeName();

	String toString();

//	default String toString(CallRuntime cr, final STYPE type) {
//		return this.getTypeName();
//	}

	String toString(CallRuntime cr, final STYPE type);

}
