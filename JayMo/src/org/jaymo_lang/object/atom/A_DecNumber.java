/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.util.Lib_Math;


/**
 * @author Michael Nitsche
 * @created 21.11.2019
 */
public abstract class A_DecNumber extends A_Number {

	/**
	 * °isFinite()Bool # Returns true, if the current number is finite.
	 * °isInfinite()Bool # Returns true, if the current number is infinite.
	 * °isNaN()Bool # Returns true, if this is not a valid number.
	 */
	@Override
	protected ObjectCallResult call4(final CallRuntime cr, final String method) {

		switch(method) {
			case "decimal":
				return A_Object.stdResult(this.mDecimal(cr));
			case "decimals":
				return A_Object.stdResult(this.mDecimals(cr));

			case "isFinite":
				cr.argsNone();
				final Object oif = this.getValue();
				final boolean isFinite = oif instanceof Dec
					? true
					: oif instanceof Float
						? Float.isFinite((Float)oif)
						: Double.isFinite((Double)oif);
				return A_Object.stdResult(Bool.getObject(isFinite));
			case "isInfinite":
				cr.argsNone();
				final Object oii = this.getValue();
				final boolean isInfinite = oii instanceof Dec
					? false
					: oii instanceof Float
						? Float.isInfinite((Float)oii)
						: Double.isInfinite((Double)oii);
				return A_Object.stdResult(Bool.getObject(isInfinite));
			case "isNaN":
				cr.argsNone();
				final Object oin = this.getValue();
				final boolean isNaN = oin instanceof Dec
					? false
					: oin instanceof Float
						? Float.isNaN((Float)oin)
						: Double.isNaN((Double)oin);
				return A_Object.stdResult(Bool.getObject(isNaN));

			case "roundDec":
				return A_Object.stdResult(this.mRoundDec(cr));

			default:
				return this.call5(cr, method);
		}
	}

	protected abstract ObjectCallResult call5(CallRuntime cr, String method);

	@Override
	protected Int getLength(final CallRuntime cr) {
		String s = this.toString(cr, STYPE.DESCRIBE);

		// Modifier
		final char modif = s.charAt(s.length() - 1);
		if(modif >= 'a' && modif <= 'z')
			s = s.substring(0, s.length() - 1);

		// Remove .0
		if(s.endsWith(".0"))
			s = s.substring(0, s.length() - 2);

		return new Int(s.length());
	}

	/**
	 * °decimal()Int # Returns the decimal places as number.
	 */
	private I_Object mDecimal(final CallRuntime cr) {
		cr.argsNone();
		final Number n = (Number)this.getValue();
		final String dec = FilterString.from(".", false, n.toString());
		return new Int(Integer.parseInt(dec.substring(1)));
	}

	/**
	 * °decimals()Int # Returns the amount of decimal places.
	 */
	private I_Object mDecimals(final CallRuntime cr) {
		cr.argsNone();
		final Number n = (Number)this.getValue();
		final String dec = FilterString.from(".", false, n.toString());
		return new Int(dec.length() - 1);
	}

	/**
	 * °roundDec(Int decimals)DecNumber # Round this number up to 'decimals'.
	 */
	private I_Atomic mRoundDec(final CallRuntime cr) {
		final I_Object arg = cr.args(this, Int.class)[0];

		if(this instanceof JMo_BigDec)
			throw new RuntimeError(cr, "Invalid object type", "Can't round BigDecimal number.");

		final double value = ((Number)this.getValue()).doubleValue();

		final int i = Lib_Convert.getIntValue(cr, arg);
		if(i < 0 || i > 10)
			throw new CodeError(cr, "Invalid argument", "Got " + i + ", allowed = 0-10");
		return this instanceof JMo_Dec
			? JMo_Dec.valueOf(cr, Lib_Math.round(value, i))
			: new JMo_Double(Lib_Math.round(value, i));
	}

}
