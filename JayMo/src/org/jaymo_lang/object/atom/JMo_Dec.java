/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import java.math.BigDecimal;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.ATOMIC;
import org.jaymo_lang.util.Lib_AtomicMath;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.convert.ConvertSequence;
import de.mn77.base.error.Err;
import de.mn77.base.error.Err_Runtime;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class JMo_Dec extends A_DecNumber implements I_Decimal, I_Atomic {

	public static final BigDecimal TOP_VALUE = new BigDecimal("18446744073.709551615");
	private final Dec              value;


	public static JMo_Dec valueOf(final CallRuntime cr, final double d) {
		if(Double.isFinite(d))
			try {
				return new JMo_Dec(Dec.valueOf(d));
			}
			catch(final Err_Runtime e) {
				throw new RuntimeError(cr, e.getMessage(), ConvertSequence.toString(", ", e.getDetails()));
			}

		if(Double.isNaN(d))
			return new JMo_Dec(Dec.ZERO);

		throw new RuntimeError(cr, "Value out of bounds", "Got: " + (d == Double.POSITIVE_INFINITY ? ATOMIC.INFINITY : ATOMIC.INFINITY_NEGATIVE));
	}

	/**
	 * ! Object of a immutable 64 bit decimal number. Dec is limited to 9 decimals.
	 * # between +/-4,9E-324 and +/-1,7E+308.
	 * + 123.456
	 * + -123.456
	 * + 123c
	 * + -123c
	 */
	public JMo_Dec(final Dec val) {
		this.value = val;
	}

	public ATOMIC getEnum() {
		return ATOMIC.DEC;
	}

	public Dec getValue() {
		return this.value;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final String s = this.value.toString();
		return type == STYPE.DESCRIBE
			? s + 'c'
			: s;
	}

	@Override
	protected ObjectCallResult call5(final CallRuntime cr, final String method) {
		return null;
	}

	@Override
	protected A_Atomic getMaxValue(final CallRuntime cr) {
		return new JMo_Dec(Dec.MAX_VALUE);
	}

	@Override
	protected A_Atomic getMinValue(final CallRuntime cr) {
		return new JMo_Dec(Dec.MIN_VALUE);
	}

	@Override
	protected A_Number numberCalc0(final CallRuntime cr, final NOP0 op) {

		switch(op) {
			case NEG:
				return new JMo_Dec(this.value.neg());
			case ABS:
				return new JMo_Dec(this.value.abs());
			case EXP:
//				return new Dec(Math.exp(this.value));
				return new JMo_Dec(this.value.exp());
			case INC:
				return new JMo_Dec(this.value.inc());
			case DEC:
				return new JMo_Dec(this.value.dec());
			case POW:
				return new JMo_Dec(this.value.pow());
			case ROOT:
//				return new Dec(Math.sqrt(this.value));
				return new JMo_Dec(this.value.root());
			case LOG10:
//				return new Dec(Math.log10(this.value));
				return new JMo_Dec(this.value.log10());
			case LOGN:
//				return new Dec(Math.log(this.value));
				return new JMo_Dec(this.value.logN());
		}
		throw Err.impossible(op);
	}

	@Override
	protected A_Number numberCalc1(final CallRuntime cr, final NOP1 op, final A_Number arg) {
		if(arg instanceof I_BigNumber)
			return Lib_AtomicMath.calcBigDec(cr, op, BigDecimal.valueOf(this.value.doubleValue()), Lib_Convert.getBigDecimalValue(cr, arg));
		else if(arg instanceof JMo_Long || arg instanceof JMo_Float || arg instanceof JMo_Double)
			return Lib_AtomicMath.calcDouble(cr, op, this.value.doubleValue(), Lib_Convert.getDoubleValue(cr, arg));
		else
			return Lib_AtomicMath.calcDec(cr, op, this.value, Lib_Convert.getDecValue(cr, arg));
	}

	@Override
	protected A_Number numberCalcSame(final CallRuntime cr, final A_Number num, final boolean inc) {
		final Dec arg = Lib_Convert.getDecValue(cr, num); // TODO Wirklich double verwenden? Besser gleich long mit getDecValue
		return new JMo_Dec(inc ? this.value.add(arg) : this.value.sub(arg));
	}

}
