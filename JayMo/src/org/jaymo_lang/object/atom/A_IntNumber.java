/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import java.math.BigInteger;

import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Exec;

import de.mn77.base.data.numsys.NumSys_Binary;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.util.Lib_Math;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 30.10.2019
 */
public abstract class A_IntNumber extends A_Number implements I_AutoBlockDo, I_Integer {

	protected enum BITWISE {
		AND,
		OR,
		XOR
	}


	@Override
	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.mTimes(cr, false).obj;
	}

	@Override
	protected final ObjectCallResult call4(final CallRuntime cr, final String method) {

		switch(method) {
			case "shiftLeft":
			case "<<":
				return A_Object.stdResult(this.mShift(cr, false));
			case "shiftRight":
			case ">>":
				return A_Object.stdResult(this.mShift(cr, true)); // case ">>>":

			case "&":
			case "bitAnd":
				return A_Object.stdResult(this.mBitwise(cr, BITWISE.AND));
			case "|":
			case "bitOr":
				return A_Object.stdResult(this.mBitwise(cr, BITWISE.OR));
			case "^":
			case "bitXor":
				return A_Object.stdResult(this.mBitwise(cr, BITWISE.XOR));
			case "!":
			case "bitNot":
				return A_Object.stdResult(this.mBitwiseNot(cr));

//			case "eachTo":
//				return this.eachTo(cr);
//			case "rangeTo":
//				return A_Object.stdResult(this.range(cr));

			case "toHex":
				return A_Object.stdResult(this.mToHex(cr));
			case "chr":
				return A_Object.stdResult(this.mChr(cr));
			case "toBin":
				return A_Object.stdResult(this.mToBin(cr));
			case "bytes":
			case "toByteArray":
				cr.argsNone();
				return A_Object.stdResult(this.toByteArray(cr));

			case "each":
			case "times":
			case "timesUp":
				return this.mTimes(cr, false);
			case "timesDown":
				return this.mTimes(cr, true);
			case "for":
				return this.mFor(cr);

			case "isEven":
				return A_Object.stdResult(this.mIsEven(cr));
			case "isOdd":
				return A_Object.stdResult(this.mIsOdd(cr));
			case "isBetween":
				return A_Object.stdResult(this.mIsBetween(cr));

			case "divisible":
				return A_Object.stdResult(this.mDivisible(cr));
			case "checksum":
				return A_Object.stdResult(this.mChecksum(cr));

			default:
				return this.call5(cr, method);
		}
	}

	protected abstract ObjectCallResult call5(CallRuntime cr, String method);

	@Override
	protected Int getLength(final CallRuntime cr) {
//		final String s = "" + this.getValue();
		final String s = this.toString(cr, STYPE.REGULAR);
		return new Int(s.length());
	}

	/**
	 * °each ^ timesUp
	 * °times ^ timesUp
	 * °timesUp()%Long Counts value times up
	 * °timesUp(Object o)%Object Counts value times up and return o
	 * °timesDown()%Long Counts value times down
	 * °timesDown(Object o)%Object Counts value times down and return o
	 */
	protected ObjectCallResult mTimes(final CallRuntime crOld, final boolean reverse) {
		crOld.argsNone();
		if(crOld.getStream() == null && crOld.getCallBlock() == null)
			//			throw new CodeError(crOld, "Invalid call", "No Argument, no Stream, no Block, for 'times' ... what to do?");
			return new ObjectCallResult(reverse ? new JMo_Long(1) : this, true);

		final int value = this.getIntValue(crOld);

		final int start = reverse ? value : 1;
		final int end = reverse ? 0 : value + 1;
		final int step = reverse ? -1 : 1;

		final LoopHandle handle = new LoopHandle(this, reverse ? "timesDown" : "timesUp");
		final CallRuntime crNew = crOld.copyLoop(handle);
		I_Object result = this;

		for(int i = start; i != end; i += step) {
			handle.startLap();
			result = Lib_Exec.execBlockStream(crNew, new Int(i));

			if((result = Lib_Exec.loopResult(result)) instanceof Return)
				return ((Return)result).getLoopResult();
		}
		return new ObjectCallResult(result, true);
	}

	/**
	 * °bytes ^ toByteArray
	 * °toByteArray()ByteArray # Returns a ByteArray with the byte representation of this number.
	 */
	protected abstract JMo_ByteArray toByteArray(CallRuntime cr);

	/**
	 * °& ^ bitAnd
	 * °bitAnd(IntNumber n)IntNumber # Bitwise AND.
	 * °^ ^ bitXor
	 * °bitXor(IntNumber n)IntNumber # Bitwise XOR.
	 * °# ^ bitOr
	 * °bitOr(IntNumber n)IntNumber # Bitwise OR.
	 */
	private I_Object mBitwise(final CallRuntime cr, final BITWISE op) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];

		if(this instanceof JMo_BigInt || arg instanceof JMo_BigInt) {
			final BigInteger valBig = Lib_Convert.getBigIntegerValue(cr, this);
			final BigInteger parBig = Lib_Convert.getBigIntegerValue(cr, arg);

			switch(op) {
				case AND:
					return new JMo_BigInt(valBig.and(parBig));
				case OR:
					return new JMo_BigInt(valBig.or(parBig));
				case XOR:
					return new JMo_BigInt(valBig.xor(parBig));
			}
		}
		else if(this instanceof JMo_Long || arg instanceof JMo_Long) {
			final long valLong = Lib_Convert.getLongValue(cr, this);
			final long parl = Lib_Convert.getLongValue(cr, arg);

			switch(op) {
				case AND:
					return new JMo_Long(valLong & parl);
				case OR:
					return new JMo_Long(valLong | parl);
				case XOR:
					return new JMo_Long(valLong ^ parl);
			}
		}
		else {
			final int valInt = Lib_Convert.getIntValue(cr, this);
			final int pari = Lib_Convert.getIntValue(cr, arg);

			switch(op) {
				case AND:
					return new Int(valInt & pari);
				case OR:
					return new Int(valInt | pari);
				case XOR:
					return new Int(valInt ^ pari);
			}
		}
		throw Err.impossible(op, this);
	}

	/**
	 * °! ^ bitNot
	 * °bitNot()IntNumber # Bitwise NOT.
	 */
	private I_Object mBitwiseNot(final CallRuntime cr) {
		cr.argsNone();

		if(this instanceof JMo_BigInt) {
			final BigInteger valBig = Lib_Convert.getBigIntegerValue(cr, this);
			return new JMo_BigInt(valBig.not());
		}
		else if(this instanceof JMo_Long) {
			final long valLong = Lib_Convert.getLongValue(cr, this);
			return new JMo_Long(~valLong);
		}
		else {
			final int valInt = Lib_Convert.getIntValue(cr, this);
			return new Int(~valInt);
		}
	}

	/**
	 * °checksum()Int # Returns the checksum
	 */
	private Int mChecksum(final CallRuntime cr) {
		cr.argsNone();
		final long l = ((Number)this.getValue()).longValue();
		return new Int(Lib_Math.checksum(l));
	}

	/**
	 * °chr()Chars # Return the character(s) represented by this Unicode value
	 */
	private A_Chars mChr(final CallRuntime cr) {
		cr.argsNone();

		final long ln = ((Number)this.getValue()).longValue();

		if(ln < 65536)
			return new Char((char)ln);
		else {
			final int high = (int)(ln >> 16);
			final int low = (int)(ln - (high << 16));
			return new Str("" + (char)high + (char)low);
		}
	}

	/**
	 * °divisible()List # Generates a list with all numbers through this number is divisible.
	 */
	private JMo_List mDivisible(final CallRuntime cr) {
		cr.argsNone();
		// boolean negative= this.value<0;
		final SimpleList<I_Object> list = new SimpleList<>();

		if(this instanceof JMo_Long) {
			final long value = ((Number)this.getValue()).longValue();
			final long i = Math.abs(value);
			for(int fi = 1; fi <= i / 2; fi++)
				if(i / (double)fi == i / fi)
					list.add(new JMo_Long(fi));
			if(i != 0)
				list.add(new JMo_Long(i)); // Teilbar durch sich selbst
		}
		else {
			final int iv = ((Number)this.getValue()).intValue();
			final int i = Math.abs(iv);
			for(int fi = 1; fi <= i / 2; fi++)
				if(i / (double)fi == i / fi)
					list.add(new Int(fi));
			if(i != 0)
				list.add(new Int(i)); // Teilbar durch sich selbst
		}
		return new JMo_List(list);
	}

	/**
	 * °for(Bool test, Object calc)%Object # Creates a loop, starting from the current object.
	 */
	private ObjectCallResult mFor(final CallRuntime crOld) {
		final LoopHandle handle = new LoopHandle(this);
		final CallRuntime crNew = crOld.copyLoop(handle);
		I_Object result = this;

		// Test before
		I_Object runObj = crOld.argEach(this, 0, result, Bool.class);
		boolean run = Lib_Convert.getBoolValue(crOld, runObj);

		while(run) {
			handle.startLap();
			result = Lib_Exec.execBlockStream(crNew, result);

			if((result = Lib_Exec.loopResult(result)) instanceof Return)
				return ((Return)result).getLoopResult();

			// New calc
			result = crOld.argEach(this, 1, result, I_Object.class);

			// Test after
			runObj = crOld.argEach(this, 0, result, Bool.class);
			run = Lib_Convert.getBoolValue(crOld, runObj);
		}
		return new ObjectCallResult(result, true);
	}

	/**
	 * °isBetween(IntNumber min, IntNumber max)Bool Returns true if the value is between min and max, otherwise false
	 */
	private Bool mIsBetween(final CallRuntime cr) {
		final I_Object[] oa = cr.args(this, A_IntNumber.class, A_IntNumber.class);
		final long value = ((Number)this.getValue()).longValue();
		final long i1 = ((Number)((A_IntNumber)oa[0]).getValue()).longValue();
		final long i2 = ((Number)((A_IntNumber)oa[1]).getValue()).longValue();
		return Bool.getObject(value >= i1 && value <= i2);
	}

	/**
	 * °isEven()Bool Returns true if the value is even, otherwise false
	 */
	private Bool mIsEven(final CallRuntime cr) {
		cr.argsNone();
		final long value = ((Number)this.getValue()).longValue();
		return Bool.getObject(value % 2 == 0);
	}

//	/**
//	 * #°rangeTo(IntNumber dest)Range # Creates a range to dest
//	 */
//	private JMo_Range range(final CallRuntime cr) {
//		final I_Object dest = cr.args(this, A_Number.class)[0];
//		return JMo_Range.createNew(cr, this, dest);
//	}

//	/**
//	 * #°eachTo(IntNumber dest)%Object # Creates a range to dest and execute block and stream
//	 */
//	private ObjectCallResult eachTo(final CallRuntime cr) {
//		final I_Object dest = cr.args(this, A_Number.class)[0];
//		final JMo_Range range = JMo_Range.createNew(cr, this, dest);
//		if(cr.call.getBlock() == null && cr.call.getStream() == null)
//			return A_Object.stdResult(range);
//		else {
//			range.init(cr);
//			return new ObjectCallResult(range.autoBlockDo(cr), true);
//		}
//	}

	/**
	 * °isOdd()Bool Returns true if the value is odd, otherwise false
	 */
	private Bool mIsOdd(final CallRuntime cr) {
		cr.argsNone();
		final long value = ((Number)this.getValue()).longValue();
		return Bool.getObject(value % 2 != 0);
	}

	/**
	 * °<< ^ shiftLeft
	 * °shiftLeft()IntNumber # Bitwise shift to the left by 1
	 * °shiftLeft(IntNumber i)IntNumber # Bitwise shift to the left
	 * °>> ^ shiftRight
	 * °shiftRight()IntNumber # Bitwise shift to the right by 1
	 * °shiftRight(IntNumber i)IntNumber # Bitwise shift to the right
	 */
	private I_Object mShift(final CallRuntime cr, final boolean right) {
		final Number val = (Number)this.getValue();
		final I_Object[] args = cr.argsFlex(this, 0, 1);
		final A_IntNumber arg = args.length == 0 ? new Int(1) : (A_IntNumber)cr.argType(args[0], A_IntNumber.class);

		if(this instanceof JMo_Long || arg instanceof JMo_Long) {
			final long parl = Lib_Convert.getLongValue(cr, arg);
			if(right)
				return new JMo_Long(val.longValue() >> parl);
			else
				return new JMo_Long(val.longValue() << parl);
		}
		else {
			final int pari = Lib_Convert.getIntValue(cr, arg);
			if(right)
				return new Int(val.intValue() >> pari);
			else
				return new Int(val.intValue() << pari);
		}
	}

	/**
	 * °toBin()Str # Returns a binary representation of this number as string
	 */
	private Str mToBin(final CallRuntime cr) {
		cr.argsNone();
		final String s = NumSys_Binary.toBin(this.getIntValue(cr));
		return new Str(s);
	}

	/**
	 * °toHex()Str Converts the value to a Hex-Value in a String.
	 */
	private I_Object mToHex(final CallRuntime cr) {
		cr.argsNone();
		final long value = ((Number)this.getValue()).longValue();
		final String s = Long.toHexString(value);
		return new Str(s);
	}

}
