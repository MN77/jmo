/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.COMPARE;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.ATOMIC;
import org.jaymo_lang.util.Lib_AtomConv;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_NumberStyle;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.form.FormNumber;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 07.10.2018
 */
public abstract class A_Number extends A_Atomic {

	@Override
	public final Boolean isGreater3(final I_Atomic o) {
		if(!(o instanceof A_Number))
			return null;

		final Double value = ((Number)this.getValue()).doubleValue();
		final double arg = Lib_Convert.getDoubleValue(null, o);

		return value.compareTo(arg) > 0;
	}

	@Override
	protected final ObjectCallResult call3(final CallRuntime cr, final String method) {

		switch(method) {
			/**
			 * °+ ^ add
			 * °add(Number n)Number # Addition
			 * °- ^ sub
			 * °sub(Number n)Number # Subtraction
			 * °* ^ mul
			 * °mul(Number? n)Object # Multiplication
			 * °/ ^ div
			 * °div(Number n)Dec # Division
			 * °/~ ^ divFloor
			 * °floorDiv ^ divFloor
			 * °divFloor(Number n)Int # Division and removing all decimals.
			 * °% ^ mod
			 * °mod(Number n)Int # Modulo
			 * °exp()DecNumber # Calculate Euler's number e raised to the power of a the value
			 * °log(Number n)Dec # Logarithm
			 * °log10() # Logarithm to the base of 10
			 * °logn ^ ln
			 * °ln() # Natural logarithm to the base of the euler constant
			 * °abs()Number # Absolute value.
			 */
			case "+":
			case "add":
				return A_Object.stdResult(this.mArithm(cr, NOP1.ADD, true)); // String, Char possible
			case "-":
			case "sub":
				return A_Object.stdResult(this.mArithm(cr, NOP1.SUB, false));
			case "*":
			case "mul":
				return A_Object.stdResult(this.mArithm(cr, NOP1.MUL, true)); // String, Char possible
			case "/":
			case "div":
				return A_Object.stdResult(this.mArithm(cr, NOP1.DIV, false));
			case "/~":
			case "floorDiv":
			case "divFloor":
				return A_Object.stdResult(this.mDivFloor(cr));
			case "%":
			case "mod":
				return A_Object.stdResult(this.mArithm(cr, NOP1.MOD, false));

			case "log":
				return A_Object.stdResult(this.mArithm(cr, NOP1.LOG, false));
			case "ln":
			case "logn":
				return A_Object.stdResult(this.mArithm(cr, NOP0.LOGN));
//			case "log2":
//				return A_Object.stdResult(this.mArithm(cr, NOP0.LOG2));
			case "log10":
				return A_Object.stdResult(this.mArithm(cr, NOP0.LOG10));
			case "exp":
				return A_Object.stdResult(this.mArithm(cr, NOP0.EXP));

			case "++":
			case "inc":
				return A_Object.stdResult(this.mArithmSame(cr, true));
			case "--":
			case "dec":
				return A_Object.stdResult(this.mArithmSame(cr, false));

			/**
			 * #°! ^ neg
			 * #°not ^ neg
			 * °neg()Number # Returns the negative value
			 */
//			case "!":
//			case "not":
			case "neg":
				return A_Object.stdResult(this.mArithm(cr, NOP0.NEG));
			case "abs":
				return A_Object.stdResult(this.mArithm(cr, NOP0.ABS));

			case "**":
			case "pow":
				return A_Object.stdResult(cr.hasArguments() ? this.mArithm(cr, NOP1.POW, false) : this.mArithm(cr, NOP0.POW));
			case "sqrt":
				cr.argsNone(); // No arguments allowed for SquareRoot
				return A_Object.stdResult(this.mArithm(cr, NOP0.ROOT));
			case "//":
			case "root": // TODO radix?!?
				return A_Object.stdResult(cr.hasArguments() ? this.mArithm(cr, NOP1.ROOT, false) : this.mArithm(cr, NOP0.ROOT));

			// Round-Functions here, for better interoperability with // and /~
			case "roundDown":
			case "floor":
			case "~":
				return A_Object.stdResult(this.mFloor(cr));
			case "roundUp":
			case "roof":
			case "ceil":
				return A_Object.stdResult(this.mRoof(cr)); // ceil
//			case "≈":
			case "round":
				return A_Object.stdResult(this.mRound(cr));

			case "style":
				return A_Object.stdResult(this.mStyle(cr));
			case "width":
				return A_Object.stdResult(this.mWidth(cr));

			case "limit":
				return A_Object.stdResult(this.mLimit(cr));

			/**
			 * °cos()DecNumber # Cosine function
			 * °sin()DecNumber # Sine function
			 * °tan()DecNumber # Tangent function
			 * °acos()DecNumber # Arc cosine function
			 * °asin()DecNumber # Arc sine function
			 * °atan()DecNumber # Arc tangent function
			 */
			case "cos":
				cr.argsNone();
				Lib_Error.ifBigNumber(cr, this);
				return this.iCircularUseDec()
					? A_Object.stdResult(JMo_Dec.valueOf(cr, Math.cos(((Number)this.getValue()).doubleValue())))
					: A_Object.stdResult(new JMo_Double(Math.cos(((Number)this.getValue()).doubleValue())));
			case "sin":
				cr.argsNone();
				Lib_Error.ifBigNumber(cr, this);
				return this.iCircularUseDec()
					? A_Object.stdResult(JMo_Dec.valueOf(cr, Math.sin(((Number)this.getValue()).doubleValue())))
					: A_Object.stdResult(new JMo_Double(Math.sin(((Number)this.getValue()).doubleValue())));
			case "tan":
				cr.argsNone();
				Lib_Error.ifBigNumber(cr, this);
				return this.iCircularUseDec()
					? A_Object.stdResult(JMo_Dec.valueOf(cr, Math.tan(((Number)this.getValue()).doubleValue())))
					: A_Object.stdResult(new JMo_Double(Math.tan(((Number)this.getValue()).doubleValue())));

			case "acos":
				cr.argsNone();
				Lib_Error.ifBigNumber(cr, this);
				return this.iCircularUseDec()
					? A_Object.stdResult(JMo_Dec.valueOf(cr, Math.acos(((Number)this.getValue()).doubleValue())))
					: A_Object.stdResult(new JMo_Double(Math.acos(((Number)this.getValue()).doubleValue())));
			case "asin":
				cr.argsNone();
				Lib_Error.ifBigNumber(cr, this);
				return this.iCircularUseDec()
					? A_Object.stdResult(JMo_Dec.valueOf(cr, Math.asin(((Number)this.getValue()).doubleValue())))
					: A_Object.stdResult(new JMo_Double(Math.asin(((Number)this.getValue()).doubleValue())));
			case "atan":
				cr.argsNone();
				Lib_Error.ifBigNumber(cr, this);
				return this.iCircularUseDec()
					? A_Object.stdResult(JMo_Dec.valueOf(cr, Math.atan(((Number)this.getValue()).doubleValue())))
					: A_Object.stdResult(new JMo_Double(Math.atan(((Number)this.getValue()).doubleValue())));

//			case "valueMin":
//				return A_Object.stdResult(this.valueMin(cr));
//			case "valueMax":
//				return A_Object.stdResult(this.valueMax(cr));

//			case "ratio":
//			case "proportion":
//				return A_Object.stdResult(this.ratio(cr));
		}
		return this.call4(cr, method);
	}

	protected abstract ObjectCallResult call4(CallRuntime cr, String method);

	@Override
	protected final Bool mComparsion(final CallRuntime cr, final COMPARE m) {
		final I_Object paro = cr.argsExt(this, new Class<?>[]{Bool.class, A_Number.class})[0]; // Theoretisch möglich: I_Atomic	// Früher noch: Char.class
		final double value = ((Number)this.getValue()).doubleValue();
		final double arg = Lib_Convert.getDoubleValue(cr, paro);

		switch(m) {
			case G:
				return Bool.getObject(value > arg);
			case L:
				return Bool.getObject(value < arg);
			case GE:
				return Bool.getObject(value >= arg);
			case LE:
				return Bool.getObject(value <= arg);
			default:
				throw Err.impossible(m);
		}
	}

	protected abstract A_Number numberCalc0(CallRuntime cr, NOP0 op);

	protected abstract A_Number numberCalc1(CallRuntime cr, NOP1 op, A_Number arg);

//	protected abstract A_Number valueMin(CallRuntime cr);
//	protected abstract A_Number valueMax(CallRuntime cr);

	protected abstract A_Number numberCalcSame(CallRuntime cr, A_Number arg, boolean inc);

	private boolean iCircularUseDec() {
		return !(this instanceof JMo_Long || this instanceof JMo_Double || this instanceof JMo_Float);
	}

	private I_Atomic mArithm(final CallRuntime cr, final NOP0 op) {
		cr.argsNone();
		return this.numberCalc0(cr, op);
	}

	/**
	 * °** ^ pow
	 * °pow()Number # Calculate the power of this number
	 * °pow(Number n)Number # Calcuate the power of this number by 'n'.
	 * °// ^ sqrt
	 * °sqrt() # Calculate the square root of this number.
	 * °root() # Calculate the square root of this number.
	 * °root(Number n) # Calculate the 'n' root of this number.
	 * °++ ^ inc
	 * °inc() # Increment this number by 1.
	 * °inc(Number n) # Increment this number by 'n'.
	 * °-- ^ dec
	 * °dec() # Decrement this number by 1.
	 * °dec(Number n) # Decrement this number by 'n'.
	 */
	private I_Object mArithm(final CallRuntime cr, final NOP1 op, final boolean strPossible) {
		final I_Object arg = cr.argsFlex(this, 1, 1)[0];

		if(strPossible) {
			if(arg == Nil.NIL)
				return Nil.arithmetic(cr, op, this);

			final A_Atomic argAtomic = (A_Atomic)cr.argType(arg, A_Atomic.class);

			if(argAtomic instanceof A_Number)
				return this.numberCalc1(cr, op, (A_Number)argAtomic);
			else if(argAtomic instanceof Bool)
				return this.numberCalc1(cr, op, new Int(Lib_Convert.getIntValue(cr, argAtomic)));
			else
				return this.number_opStr(cr, op, argAtomic);
		}
		else {
			final A_Number argNumber = (A_Number)cr.argType(arg, A_Number.class);
			return this.numberCalc1(cr, op, argNumber);
		}
	}

	private I_Atomic mArithmSame(final CallRuntime cr, final boolean inc) {

		if(cr.hasArguments()) {
			final A_Number num = (A_Number)cr.args(this, A_Number.class)[0];
			return this.numberCalcSame(cr, num, inc);
		}
		else
			return this.numberCalc0(cr, inc ? NOP0.INC : NOP0.DEC);
	}

	private I_Object mDivFloor(final CallRuntime cr) {
		I_Atomic val = (I_Atomic)this.mArithm(cr, NOP1.DIV, false);
		val = Lib_AtomConv.convert(cr, val, ATOMIC.INT);
		final int i = ((Int)val).getValue();
		return i >= 0 ? val : new Int(i - 1);
	}

	/**
	 * °~ ^ roundDown
	 * °floor ^ roundDown
	 * °roundDown()IntNumber # Returns the integer part of the current value.
	 */
	private A_IntNumber mFloor(final CallRuntime cr) {
		cr.argsNone();
		if(this instanceof A_IntNumber)
			return (A_IntNumber)this;
		if(this instanceof JMo_BigDec)
			return new JMo_BigInt(((JMo_BigDec)this).getValue().round(JMo_BigDec.CONTEXT_DOWN).toBigInteger());

		final double value = ((Number)this.getValue()).doubleValue();
		return new JMo_Long((long)Math.floor(value));
	}

	/**
	 * °limit(Number min, Number max)Number # Limit the value between min and max
	 */
	private A_Number mLimit(final CallRuntime cr) {
		final I_Object[] args = cr.args(this, A_Number.class, A_Number.class);
		Lib_Error.ifBigNumber(cr, args[0]);
		Lib_Error.ifBigNumber(cr, args[1]);
		final double min = Lib_Convert.getDoubleValue(cr, args[0]);
		final double max = Lib_Convert.getDoubleValue(cr, args[1]);

		if(min > max)
			throw new RuntimeError(cr, "Min. is greater than max.!", "Min.: " + min + "  Max.: " + max);

		double d = ((Number)this.getValue()).doubleValue();

		if(d > max)
			d = max;
		if(d < min)
			d = min;

		return (A_Number)Lib_AtomConv.convert(cr, new JMo_Double(d), this.getEnum());
	}

	/**
	 * °roof ^ roundUp
	 * °ceil ^ roundUp
	 * °roundUp()IntNumber # Returns the next integer number, that is equal or greater the current value.
	 */
	private A_IntNumber mRoof(final CallRuntime cr) {
		cr.argsNone();
		if(this instanceof A_IntNumber)
			return (A_IntNumber)this;
		if(this instanceof JMo_BigDec)
			return new JMo_BigInt(((JMo_BigDec)this).getValue().round(JMo_BigDec.CONTEXT_UP).toBigInteger());

		final double value = ((Number)this.getValue()).doubleValue();
		return new JMo_Long((long)Math.ceil(value));
	}

	/**
	 * # °≈ ^ round
	 * °round()IntNumber # Round this number to a integer number.
	 */
	private A_IntNumber mRound(final CallRuntime cr) {
		cr.argsNone();
		if(this instanceof A_IntNumber)
			return (A_IntNumber)this;
		if(this instanceof JMo_BigDec)
			return new JMo_BigInt(((JMo_BigDec)this).getValue().round(JMo_BigDec.CONTEXT).toBigInteger());

		final double value = ((Number)this.getValue()).doubleValue();
		return new JMo_Long(Math.round(value));
	}

	/**
	 * °style(Str format)Str # Formats the number to a string. Special chars are: 0#?~,.:;_*
	 */
	private Str mStyle(final CallRuntime cr) {
		final Str o = (Str)cr.args(this, Str.class)[0];
		final String styled = Lib_NumberStyle.style(cr, this, o.getValue());
		return new Str(styled);
	}

	/**
	 * °width(IntNumber min)Str # Converts the number to a string with minimum length
	 */
	private Str mWidth(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int width = Lib_Convert.getIntValue(cr, arg);
		final Number raw = (Number)this.getValue();
		final String form = FormNumber.right(width, raw.longValue());
		return new Str(form);
	}

	/* Operationen für Nicht-Nummern (Char,String) */
	private final A_Chars number_opStr(final CallRuntime cr, final NOP1 op, final A_Atomic p) {
		final String arg = Lib_Convert.getStringValue(cr, p);

		if(op == NOP1.ADD)
			return new Str("" + this.getValue() + arg);

		if(op == NOP1.MUL) {
			if(this instanceof A_DecNumber)
				throw new CodeError(cr, "Invalid type to multiply a <Char> or <Str>", "Got " + Lib_Type.getTypeString(this) + ", please use <Int>");

			final String s = Lib_String.sequence(arg, ((Number)this.getValue()).longValue());
			return new Str(s);
		}
		throw new CodeError(cr, "Unknown type or function", this.getValue() + " " + op + " " + Lib_Type.getName(p));
	}

//	/**
//	 * #°ratio(Number relation, Number wanted)Number # Use the 'rule of three'
//	 */
//	private A_Number ratio(final CallRuntime cr) {
//		final I_Object[] args = cr.args(this, A_Number.class, A_Number.class);
//		final double base = Lib_Convert.getDoubleValue(cr, args[0]);
//		final double target = Lib_Convert.getDoubleValue(cr, args[1]);
//
//		double d = ((Number)this.getValue()).doubleValue();
//		d = d * target / base;
//		return new Dec(d);
//	}

}
