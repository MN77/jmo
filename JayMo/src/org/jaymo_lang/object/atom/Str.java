/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import java.nio.charset.StandardCharsets;
import java.util.regex.PatternSyntaxException;

import org.jaymo_lang.JayMo;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.COMPARE;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.I_AutoBlockList;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.JMo_Range;
import org.jaymo_lang.object.JMo_RegEx;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.filesys.JMo_File;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.magic.con.MagicPosition;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.struct.I_DeepGetSet;
import org.jaymo_lang.object.struct.JMo_ByteArray;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.object.struct.JMo_Table;
import org.jaymo_lang.object.sys.JMo_Cmd;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.ATOMIC;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;
import org.jaymo_lang.util.Lib_Exec;
import org.jaymo_lang.util.Lib_Sequence;
import org.jaymo_lang.util.Lib_StrFormat;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.HtmlChars;
import de.mn77.base.data.constant.ALIGN;
import de.mn77.base.data.constant.position.Lib_Position;
import de.mn77.base.data.constant.position.POSITION;
import de.mn77.base.data.constant.position.POSITION_H;
import de.mn77.base.data.convert.ConvertChar;
import de.mn77.base.data.convert.ConvertString;
import de.mn77.base.data.filter.FilterString;
import de.mn77.base.data.form.FormString;
import de.mn77.base.data.group.Group2;
import de.mn77.base.data.numsys.NumSys_Binary;
import de.mn77.base.data.numsys.NumSys_Hex;
import de.mn77.base.data.search.SearchText;
import de.mn77.base.data.struct.I_Collection;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.struct.table.ArrayTable;
import de.mn77.base.data.struct.table.type.TypeTable2;
import de.mn77.base.data.type.Lib_Compare;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class Str extends A_Chars implements I_Atomic, I_AutoBlockDo, I_AutoBlockList, I_DeepGetSet {

	private final String value;


	/**
	 * !Object of a immutable character string.
	 * +\"Hello World!\"
	 * %.each
	 */
	public Str(final String val) {
		Err.ifNull(val);
		this.value = val;
	}

	@Override
	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.each(cr).obj;
	}

	@Override
	public SimpleList<I_Object> autoBlockToList(final CallRuntime cr) {
		final SimpleList<I_Object> result = new SimpleList<>(this.value.length());
		for(final char c : this.value.toCharArray())
			result.add(new Char(c));
		return result;
	}

	public ATOMIC getEnum() {
		return ATOMIC.STR;
	}

	public String getValue() {
		return this.value;
	}

	@Override
	public Boolean isGreater3(final I_Atomic o) {
		if(o instanceof Str)
//			return this.value.compareTo(((Str)o).getValue());
			return Lib_Compare.isGreaterString(this.value, ((Str)o).getValue());
		return null;
	}

	@Override
	public I_Object sequenceDeepGet(final CallRuntime cr, final I_Object[] keys, final int offset, final boolean lazy) {
		final A_IntNumber oi = (A_IntNumber)cr.argType(keys[offset], A_IntNumber.class);
		final int i = Lib_Convert.getIntValue(cr, oi);
		final int pos = Lib_Sequence.realPosition(cr, i, this.value.length(), lazy);
		if(pos == -1 || pos > this.value.length())
			if(lazy)
				return Nil.NIL;
			else
				Lib_Error.ifNotBetween(cr, 1, this.value.length(), pos, "position in list");

		if(offset != keys.length - 1) {
			final StringBuilder sb = new StringBuilder();

			for(int j = 0; j <= offset; j++) {
				if(j > 0)
					sb.append(',');
				sb.append(keys[j].toString());
			}
			throw new RuntimeError(cr, "Invalid type of item.", "Item at position (" + sb.toString() + ") is a single <Char>, so I can't go deeper!");
		}
		return new Char(this.value.charAt(pos - 1));
	}

	@Override
	public void sequenceDeepSet(final CallRuntime cr, final I_Object[] keys, final int offset, final I_Object value, final boolean lazy) {
		throw new RuntimeError(cr, "Invalid type of item.", "A String is immutable, so it can't be modified!");
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {

		switch(type) {
			case REGULAR:
				return this.value;
			case NESTED:
				return this.value.length() <= JayMo.NESTED_MAX
					? this.value
					: this.value.substring(0, JayMo.NESTED_MAX) + "…";
			case IDENT:
				final String si = FormString.escapeSpecialChars(this.value, false, true);
				return si.length() <= JayMo.NESTED_MAX
					? "\"" + si + "\""
					: "\"" + si.substring(0, JayMo.NESTED_MAX - 3) + "…\"";
			case DESCRIBE:
				final String sd = FormString.escapeSpecialChars(this.value, false, true);
				return "\"" + sd + "\"";
			default:
				throw Err.impossible(type);
		}
	}

	@Override
	protected ObjectCallResult call4(final CallRuntime cr, final String method) {

		switch(method) {
			/**
			 * °isEmpty()Bool # Returns true, if this string contains no chars.
			 */
			case "isEmpty":
				cr.argsNone();
				return A_Object.stdResult(Bool.getObject(this.value.isEmpty()));

			case "-":
			case "sub":
				return A_Object.stdResult(this.mSubtract(cr));

			case "begin":
				return A_Object.stdResult(this.mBegin(cr));

			case "cut":
				return A_Object.stdResult(this.mCut(cr));
			case "part":
				return A_Object.stdResult(this.mPart(cr));

			case "left":
				return A_Object.stdResult(this.mLeft(cr));
			case "right":
				return A_Object.stdResult(this.mRight(cr));
			case "first":
				return A_Object.stdResult(this.mFirstLast(cr, false));
			case "last":
				return A_Object.stdResult(this.mFirstLast(cr, true));

			case "end":
				return A_Object.stdResult(this.mEndPos(cr));
			case "start":
				return A_Object.stdResult(this.mStartPos(cr));
//			case "before":
//				return A_Object.stdResult(this.beforePos(cr));
//			case "after":
//				return A_Object.stdResult(this.afterPos(cr));
			case "limit":
				return A_Object.stdResult(this.mLimit(cr));

			case "from":
			case "fromFirst":
				return A_Object.stdResult(this.mFromFirst(cr, true));
			case "after":
			case "afterFirst":
				return A_Object.stdResult(this.mFromFirst(cr, false));
			case "till":
			case "tillFirst":
				return A_Object.stdResult(this.mTillFirst(cr, true));
			case "before":
			case "beforeFirst":
				return A_Object.stdResult(this.mTillFirst(cr, false));

			case "fromLast":
				return A_Object.stdResult(this.mFromLast(cr, true));
			case "afterLast":
				return A_Object.stdResult(this.mFromLast(cr, false));
			case "tillLast":
				return A_Object.stdResult(this.mTillLast(cr, true));
			case "beforeLast":
				return A_Object.stdResult(this.mTillLast(cr, false));

			case "/":
			case "div":
				return A_Object.stdResult(this.mDivide(cr));
			case "explode":
			case "split":
				return A_Object.stdResult(this.mSplit(cr));
			case "splitKeep":
				return A_Object.stdResult(this.mSplitKeep(cr));
			case "group":
				return A_Object.stdResult(this.mGroup(cr));
			case "lines":
				return A_Object.stdResult(this.mLines(cr));
			case "words":
				return A_Object.stdResult(this.mWords(cr));
			case "field":
				return A_Object.stdResult(this.mField(cr));
			case "fields":
				return A_Object.stdResult(this.mFields(cr));
			case "table":
				return A_Object.stdResult(this.mTable(cr));
			case "scan":
				return A_Object.stdResult(this.mScan(cr));
			case "capital":
			case "capitalFirst":
				return A_Object.stdResult(this.mCapital(cr, true));
			case "capitalAll":
				return A_Object.stdResult(this.mCapital(cr, false));
			case "trim":
				return A_Object.stdResult(this.mTrim(cr, true, true));
//			case "ltrim":
			case "trimLeft":
				return A_Object.stdResult(this.mTrim(cr, true, false));
//			case "rtrim":
			case "trimRight":
				return A_Object.stdResult(this.mTrim(cr, false, true));
			case "width":
				return A_Object.stdResult(this.mWidth(cr));

			case "startsWith":
				return A_Object.stdResult(this.mStartsWith(cr));
//			case "has":
			case "contains":
				return A_Object.stdResult(this.mContains(cr));
			case "endsWith":
				return A_Object.stdResult(this.mEndsWith(cr));
//			case "index":	// indexOf
			case "search":
			case "searchFirst":
				return A_Object.stdResult(this.mSearch(cr, true));
			case "searchLast":
				return A_Object.stdResult(this.mSearch(cr, false));
			case "count":
				return A_Object.stdResult(this.mCount(cr));

			case "replace":
				return A_Object.stdResult(this.mReplace(cr));
			case "match":
				return A_Object.stdResult(this.mMatch(cr));
			case "set":
				return A_Object.stdResult(this.mSetChar(cr, false));
			case "put":
				return A_Object.stdResult(this.mSetChar(cr, true));
			case "reverse":
				return A_Object.stdResult(this.mReverse(cr));
			case "insert":
				return A_Object.stdResult(this.mInsert(cr));

			case "align":
				return A_Object.stdResult(this.mAlign(cr));
			case "fill":
				return A_Object.stdResult(this.mFill(cr));

			case "quote":
				return A_Object.stdResult(this.mQuote(cr));
			case "unquote":
				return A_Object.stdResult(this.mUnquote(cr));

			case "escapeSlashes":
				return A_Object.stdResult(this.mEscape(cr, false));
			case "escape":
			case "escapeAll":
				return A_Object.stdResult(this.mEscape(cr, true));
			case "unescape":
				return A_Object.stdResult(this.mUnescape(cr));

			case "htmlSpecial":
			case "htmlSpecialChars":
				return A_Object.stdResult(this.mHtmlSpecialChars(cr));
			case "htmlEntities":
				return A_Object.stdResult(this.mHtmlEntities(cr));
			case "htmlDecode":
				return A_Object.stdResult(this.mHtmlDecode(cr));

			case "jmo":
			case "jaymo":
				return A_Object.stdResult(this.mExecJAYMO(cr));
//			case "command":
			case "cmd":
				return A_Object.stdResult(this.mExecCMD(cr));

			case "parseHex":
//			case "fromHex":
//			case "hexToInt":
				return A_Object.stdResult(this.mParseHex(cr));
			case "parseUnicode":
				return A_Object.stdResult(this.mParseUnicode(cr));
			case "parseBin":
				return A_Object.stdResult(this.mParseBin(cr));

			case "chars":
			case "toChars":
				return A_Object.stdResult(this.mToChars(cr));
			case "each":
				return this.each(cr);
			case "bytes":
			case "toByteArray":
				return A_Object.stdResult(this.mToByteArray(cr));

			/**
			 * #°toRegex ^ toRegEx
			 * °toRegEx()RegEx # Convert this String to a Regular expression.
			 */
//			case "toRegex":
			case "toRegEx":
				cr.argsNone();
				return A_Object.stdResult(new JMo_RegEx(this.value));

			case "unite":
				return A_Object.stdResult(this.mUnite(cr));
			case "uniq":
			case "unique":
				return A_Object.stdResult(this.mUnique(cr));
			case "only":
				return A_Object.stdResult(this.mOnly(cr));

			case "filter":
				return A_Object.stdResult(this.mFilter(cr, method));
			case "map":
				return A_Object.stdResult(this.mMap(cr, method));
			case "sort":
				return A_Object.stdResult(this.mSort(cr, method));
			case "reduce":
				return A_Object.stdResult(this.mReduce(cr, method));
			case "amount":
				return A_Object.stdResult(this.mAmount(cr, method));

			case "fileSet":
				return A_Object.stdResult(this.mFileWrite(cr, false));
			case "fileAdd":
			case "fileAppend":
				return A_Object.stdResult(this.mFileWrite(cr, true));

			default:
				return null;
		}
	}

	@Override
	protected boolean charsIsBlank() {
		return this.value.isBlank();
	}

	@Override
	protected boolean charsIsCaseDown() {
		for(final char c : this.value.toCharArray())
			if(!Character.isLowerCase(c))
				return false;
		return !this.value.isEmpty();
	}

	@Override
	protected boolean charsIsCaseUp() {
		for(final char c : this.value.toCharArray())
			if(!Character.isUpperCase(c))
				return false;
		return !this.value.isEmpty();
	}

	@Override
	protected boolean charsIsNumber() {
		for(final char c : this.value.toCharArray())
			if(!Character.isDigit(c))
				return false;
		return !this.value.isEmpty();
	}

	@Override
	protected A_Atomic getMaxValue(final CallRuntime cr) {
		throw this.newErrorMinMaxValue(cr);
	}

	@Override
	protected A_Atomic getMinValue(final CallRuntime cr) {
		return new Str("");
	}

	@Override
	protected Str mAdd(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 0);
		String result = this.value;
		for(final I_Object arg : args)
			result += Lib_Convert.getStringValue(cr, arg);
		return new Str(result);
	}

	@Override
	protected Str mCaseDown(final CallRuntime cr) {
		cr.argsNone();
		return new Str(this.value.toLowerCase());
	}

	@Override
	protected Str mCaseUp(final CallRuntime cr) {
		cr.argsNone();
		return new Str(this.value.toUpperCase());
	}

	@Override
	protected Str mChangeChar(final CallRuntime cr) {
		final I_Object[] oa = cr.args(this, A_IntNumber.class, A_Chars.class);
		int pos = Lib_Convert.getIntValue(cr, oa[0]);
		pos = Lib_Sequence.realPosition(cr, pos, this.value.length(), false);
		final I_Object o = oa[1];
		final String n = Lib_Convert.getStringValue(cr, o);
		final String l = pos == 1
			? ""
			: this.value.substring(0, pos - 1);
		final String r = pos == this.value.length()
			? ""
			: this.value.substring(pos);
		return new Str(l + n + r);
		// this.value=new String(ca);
	}

	@Override
	protected I_Object mCharAt(final CallRuntime cr, final boolean lazy) {
		int pos = Lib_Convert.getIntValue(cr, cr.args(this, A_IntNumber.class)[0]);
		pos = Lib_Sequence.realPosition(cr, pos, this.value.length(), lazy);
		if(lazy && (pos < 1 || pos > this.value.length()))
			return Nil.NIL;
		return new Char(this.value.charAt(pos - 1));
	}

	/** TODO vergleicht aktuell bei >< nur die Länge **/
	@Override
	protected Bool mComparsion(final CallRuntime cr, final COMPARE m) {
		final I_Object o = cr.args(this, A_Chars.class)[0]; // Möglich: I_Atomic.class, früher: Str, Char, Int
		final String arg = Lib_Convert.getStringValue(cr, o);

		switch(m) {
			case G:
				return Bool.getObject(this.value.compareTo(arg) > 0);
			case L:
				return Bool.getObject(this.value.compareTo(arg) < 0);
			case GE:
				return Bool.getObject(this.value.compareTo(arg) >= 0);
			case LE:
				return Bool.getObject(this.value.compareTo(arg) <= 0);
			default:
				throw Err.impossible(m);
		}
	}

	@Override
	protected Str mMultiply(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int i = Lib_Convert.getIntValue(cr, arg);
		final String s = Lib_String.sequence(this.value, i);
		return new Str(s);
	}

	@Override
	protected JMo_List mSelect(final CallRuntime cr, final boolean lazy) {
		final I_Object[] args = cr.argsVar(this, 0, 0);
		final SimpleList<I_Object> list = new SimpleList<>(args.length);

		for(final I_Object arg : args) {
			int pos = Lib_Convert.getIntValue(cr, cr.argType(arg, A_IntNumber.class));
			pos = Lib_Sequence.realPosition(cr, pos, this.value.length(), lazy);
			if(lazy && (pos < 1 || pos > this.value.length()))
				list.add(Nil.NIL);
			else
				list.add(new Char(this.value.charAt(pos - 1)));
		}
		return new JMo_List(list);
	}

	@Override
	protected final Str mUpdate(final CallRuntime cr, final boolean lazy) {
		final I_Object[] oa = cr.argsVar(this, 1, 1);
		final I_Object obj = cr.argType(oa[0], null);
		final char c = Lib_Convert.getCharValue(cr, obj);
		final StringBuilder sb = new StringBuilder(this.value);

		for(int i = 1; i < oa.length; i++) {
			final int pos1 = Lib_Convert.getIntValue(cr, cr.argType(oa[i], A_IntNumber.class));
			int pos2 = Lib_Sequence.realPosition(cr, pos1, sb.length(), lazy);

//			MOut.temp(pos1, pos2, lazy, sb, c);
			if(pos2 < 0 && lazy) {
				pos2 = pos1;
				while(pos2 > sb.length())
					sb.append(' ');
			}
			sb.setCharAt(pos2 - 1, c);
		}
		return new Str(sb.toString());
	}

	/**
	 * °each()%Object # Walk throw each char
	 * #°each(Object o)%Object # Walk throw each char, but use always object o.
	 * #°each(VarLet v)%Object # Walk throw each char and assign it to the var
	 */
	private ObjectCallResult each(final CallRuntime crOld) {
		crOld.argsNone();
		Lib_Exec.checkLoopWithout(crOld);

		final LoopHandle handle = new LoopHandle(this);
		final CallRuntime crNew = crOld.copyLoop(handle);
		I_Object result = Nil.NIL;

		for(final char c : this.value.toCharArray()) {
			handle.startLap();
			final Char it = new Char(c);
			result = Lib_Exec.execBlockStream(crNew, it);

			if((result = Lib_Exec.loopResult(result)) instanceof Return)
				return ((Return)result).getLoopResult();
		}
		return new ObjectCallResult(result, true);
	}

	/**
	 * °align(MagicPosition pos, IntNumber width)Str # Align the string to the left,center,right with that minimum width
	 */
	private Str mAlign(final CallRuntime cr) {
		final I_Object[] args = cr.args(this, MagicPosition.class, A_IntNumber.class);
		final int arg = Lib_Convert.getIntValue(cr, args[1]);
		final MagicPosition pos1 = (MagicPosition)Lib_Convert.getValue(cr, args[0]);
		final POSITION pos2 = pos1.get();
		if(!Lib_Position.isHorizontal(pos2))
			throw new RuntimeError(cr, "Invalid position", "Only left,center and right are alowed. Got: " + pos1);
		final ALIGN align = Lib_Position.toAlign((POSITION_H)pos2);
		final String s = FormString.width(arg, ' ', this.value, align, false);
		return new Str(s);
	}

	/**
	 * °amount(Object each)§Int # Check every char and return the amount of trues. The result of 'each' must be Bool.
	 * °amount(VarLet var, Object each)§Int # Check every char and return the amount of trues. The result of 'each' must be Bool.
	 */
	private Int mAmount(final CallRuntime cr, final String method) {
		Lib_Error.ifArgs(cr.argCount(), 1, 2, cr, this);
		int result = 0;

		for(final char test : this.value.toCharArray()) {
			final Bool ok = (Bool)cr.copyEach(method).argsEach(this, 0, new I_Object[]{new Char(test)}, Bool.class);
			if(ok.getValue())
				result++;
		}
		return new Int(result);
	}

	/**
	 * °begin(Object o)Str # Inserts an object at the beginning of the string
	 */
	private Str mBegin(final CallRuntime cr) {
		final I_Object arg = cr.args(this, I_Object.class)[0];
		final String ps = Lib_Convert.getStringValue(cr, arg);
		return new Str(ps + this.value);
	}

	/**
	 * °capital ^ capitalFirst
	 * °capitalFirst()Str # Change the first char to upper case
	 * °capitalAll()Str # Change the first char of every word to upper case
	 */
	private Str mCapital(final CallRuntime cr, final boolean onlyFirst) {
		cr.argsNone();
//		final String s = this.value.toLowerCase();
		final String result = Lib_String.capitalize(this.value, onlyFirst);
		return new Str(result);
	}

	/**
	 * °contains(Atomic part)Bool # Return true, if the string contains this part
	 */
	private Bool mContains(final CallRuntime cr) {
		final I_Atomic arg = (I_Atomic)cr.args(this, I_Atomic.class)[0];
		final int index = this.value.indexOf(Lib_Convert.getStringValue(cr, arg));
		return Bool.getObject(index > -1);
	}

	/**
	 * °count(Char c)Int # Returns the number of all occurrences of this character.
	 */
	private Int mCount(final CallRuntime cr) {
		final I_Object arg = cr.args(this, Char.class)[0];
		final int result = SearchText.countChar(this.value, ((Char)arg).getValue());
		return new Int(result);
	}

	/**
	 * °cut(IntNumber from, IntNumber len)Str # Cuts the string from position 'from' with a length of 'len'
	 */
	private Str mCut(final CallRuntime cr) {
		final I_Object[] args = cr.args(this, A_IntNumber.class, A_IntNumber.class);
		final int from = Lib_Convert.getIntValue(cr, args[0]);
		final int len = Lib_Convert.getIntValue(cr, args[1]);
		Lib_Error.ifNotBetween(cr, 1, this.value.length(), from, "'from'");
		Lib_Error.ifNotBetween(cr, 0, this.value.length() - from + 1, len, "'length'");
		final int to = from + len;
		final String s = this.value.substring(from - 1, to - 1);
		return new Str(s);
	}

	/**
	 * °/ ^ div
	 * °div(IntNumber x)List # Split this string in x peaces
	 */
	private JMo_List mDivide(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int divisor = Lib_Convert.getIntValue(cr, arg);
		final int len = this.value.length();
		Lib_Error.ifNotBetween(cr, 1, len, divisor, "Argument");

		final JMo_List list = new JMo_List();
		final float part = (float)len / divisor;

		int start = 0;

		for(float val = 0; start < len; val += part) {
			final int end = Math.min(len, Math.round(val + part));
			final String s = this.value.substring(start, end);
			list.internalAdd(new Str(s));
			start = end;
		}
		return list;
	}

	/**
	 * Lazy handling of invalid positions
	 * Equal to left, but negative value is allowed
	 * °end(IntNumber pos)Str # Get a copy up to this position. Negative value are counting from the right side.
	 */
	private Str mEndPos(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		int to = Lib_Convert.getIntValue(cr, arg);
		if(to == 0)
			throw new RuntimeError(cr, "Invalid position in string", "Position can't be 0");
		if(to >= this.value.length())
			return new Str(this.value);
		if(to < 0 && Math.abs(to) > this.value.length())
			return new Str("");
		if(to < 0)
			to = this.value.length() + to + 1;
		final String s = this.value.substring(0, to);
		return new Str(s);
	}

	/**
	 * °endsWith(Atomic end...)Bool # Tests, if the string ends with one of these values.
	 */
	private Bool mEndsWith(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 0);

		for(final I_Object o : args) {
			final String s = Lib_Convert.getStringValue(cr, cr.argType(o, I_Atomic.class));
			if(this.value.endsWith(s))
				return Bool.TRUE;
		}
		return Bool.FALSE;
	}

	/**
	 * °escape ^ escapeAll
	 * °escapeSlashes()Str # Add a backslash before every other backslash and NUL-Character
	 * °escapeAll()Str # Escape all special chars
	 */
	private I_Object mEscape(final CallRuntime cr, final boolean full) {
		cr.argsNone();
		final String result = full
			? FormString.escapeSpecialChars(this.value, false, false)
			: FormString.escapeSlashes(this.value);

		return new Str(result);
	}

	/**
	 * °cmd()Cmd # Create a shell-command with this string
	 */
	private I_Object mExecCMD(final CallRuntime cr) {
		cr.argsNone();
		cr.getStrict().checkSandbox(cr, "Str.cmd");

		final Call c = new Call(cr, this);
		return new JMo_Cmd(c);
	}

	/**
	 * °jmo ^ jaymo
	 * °jaymo()Str? # Execute this string as a jaymo-command.
	 */
	private A_Immutable mExecJAYMO(final CallRuntime cr) {
		cr.argsNone();
		return Lib_Exec.execJayMo(cr, this.value, "Str.jaymo", null);
	}

	/**
	 * °field(Atomic delimiter, IntNumber column)Str # Split this String by 'delimiter' and return that 'column'
	 */
	private Str mField(final CallRuntime cr) {
		final I_Object[] oa = cr.args(this, I_Atomic.class, A_IntNumber.class);
		final String delimiter = Lib_Convert.getStringValue(cr, oa[0]);
		final String[] sa = this.value.split(delimiter);

		final int col = Lib_Convert.getIntValue(cr, oa[1]);

//		Lib_Error.ifNotBetween(cr, 1, sa.length, col, "Column");
//		return new Str(sa[col - 1]); // col > sa.length ? ""

		Lib_Error.ifTooSmall(cr, 1, col);
		return col > sa.length ? new Str("") : new Str(sa[col - 1]);
	}

	/**
	 * °fields(Atomic delimiter, IntNumber columns...)List # Split this String by 'delimiter' and return that 'columns'
	 */
	private JMo_List mFields(final CallRuntime cr) {
		final I_Object[] oa = cr.argsVar(this, 1, 1);
		final I_Object parDelimiter = cr.argType(oa[0], I_Atomic.class);
		final String delimiter = Lib_Convert.getStringValue(cr, parDelimiter);
		final String[] sa = this.value.split(delimiter);

		final SimpleList<I_Object> al = new SimpleList<>(sa.length);	// oa.length ?!?
		if(oa.length == 1)
			for(final String s : sa)
				al.add(new Str(s));
		else
			for(int i = 1; i < oa.length; i++) {
				final int col = Lib_Convert.getIntValue(cr, oa[i]);
				Lib_Error.ifTooSmall(cr, 1, col);
				final Str s = col > sa.length ? new Str("") : new Str(sa[col - 1]);
				al.add(s);
			}

		return new JMo_List(al);
	}

	/**
	 * °fileSet(Str|File file)Same # Overwrite or replace the contant of an existing file with this string
	 * °fileSet(Str|File file, Charset cs)Same # Overwrite or replace the contant of an existing file with this string
	 * °fileAdd ^ fileAppend
	 * °fileAppend(Str|File)Same # Append this string to a new or existing file.
	 * °fileAppend(Str|File file, Charset cs)Same # Append this string to a new or existing file.
	 */
	private Str mFileWrite(final CallRuntime cr, final boolean append) {
		final I_Object[] args = cr.argsFlex(this, 1, 2);

		// Link to File.set / File.append
		final JMo_File file = new JMo_File(new Call(cr, args[0]));
		file.init(cr);
		final String method = append ? "append" : "set";

		final Call fileArg0 = new Call(cr, this);
		final Call[] fileArgs = cr.argCount() == 1
			? new Call[]{fileArg0}
			: new Call[]{fileArg0, new Call(cr, args[1])};

		final Call fileCall = new Call(cr.getSurrBlock(), file, method, fileArgs, cr.getDebugInfo());
		final CallRuntime fileCr = cr.copyCall(fileCall, false);

		file.call(fileCr);
		return this;
	}

	/**
	 * °fill(...)Str # Fills all {}-placeholders with values. An overflow of values or placeholders will be handled lazily.
	 *
	 * @implNote
	 *           Currently:
	 *           If there are more values than placeholders, they will be dropped.
	 *           If there are more placeholders than values, they will be hidden.
	 */
	private Str mFill(final CallRuntime cr) {
		final I_Object[] oa = cr.argsVar(this, 0, 0);
		int oaPos = 0;
		int pos = 0;
		final StringBuilder sb = new StringBuilder();

		while(pos < this.value.length()) {
			final Group2<Boolean, String> part = Lib_StrFormat.getNext(this.value, pos);
			pos += part.o2.length();

			if(!part.o1) // No placeholder
				sb.append(part.o2);
			else if(oaPos >= oa.length) { // Less values than placeholders
//				sb.append(part.o2);	// Don't do anything
			}
			else {
				sb.append(Lib_StrFormat.format(cr, part.o2, oa[oaPos]));
				oaPos++;
			}
		}
//		if(oaPos < oa.length)	{} // More values than placeholders
		return new Str(sb.toString());
	}

	/**
	 * °filter(Object each)§Str # Creates a new string with selected chars. The result of 'each' must be Bool.
	 * °filter(VarLet var, Object each)§Str # Creates a new string with selected chars. The result of 'each' must be Bool.
	 */
	private Str mFilter(final CallRuntime cr, final String method) {
		Lib_Error.ifArgs(cr.argCount(), 1, 2, cr, this);
		final StringBuilder result = new StringBuilder();

		for(int p = 0; p < this.value.length(); p++) {
			final char test = this.value.charAt(p);
			final Char testc = new Char(test);
			// MOut.temp(test, cr.call.gInternPars());
			// Bool ok = (Bool)cr.args(test, Bool.class)[0];
			final Bool ok = (Bool)cr.copyEach(method).argsEach(this, 0, new I_Object[]{testc}, Bool.class);
			if(ok.getValue())
				result.append(test);
		}
		return new Str(result.toString());
	}

	/**
	 * °first()Char # Get the first char of the String
	 * °last()Char # Get the last char of the String
	 */
	private Char mFirstLast(final CallRuntime cr, final boolean last) {
		cr.argsNone();
		if(this.value.length() == 0)
			throw new RuntimeError(cr, "Empty String", "Can't get first or last Char.");
		return new Char(this.value.charAt(last ? this.value.length() - 1 : 0));
	}

	/**
	 * °from ^ fromFirst
	 * °fromFirst(Object... o)Str # Returns a copy of the String from the first matching argument
	 * °after ^ afterFirst
	 * °afterFirst(Object... o)Str # Returns a copy of the String after the first matching argument
	 */
	private Str mFromFirst(final CallRuntime cr, final boolean withDelimiter) {
		final I_Object[] args = cr.argsVar(this, 1, 0);
//		if(arg instanceof JMo_Regex) {
//			String regex = arg.toString();
//			String[] sa = this.value.split(regex);
//			return sa.length > 0
//		 		? new Str(sa[0])
//		 		: new Str(""); // Nil?!?
//		}

		int idx = -1;
		String lastDelimiter = null;

		for(final I_Object arg : args) {
			final String delimiter = Lib_Convert.getStringValue(cr, arg);
			final int idx2 = this.value.indexOf(delimiter);

			if(idx2 > -1 && (idx == -1 || idx2 < idx)) {
				idx = idx2;
				lastDelimiter = delimiter;
			}
		}
		final String result = idx >= 0
			? this.value.substring(idx + (withDelimiter ? 0 : lastDelimiter.length()))
			: ""; // Nil?!?
		return new Str(result);
	}

	/**
	 * °fromLast(Object... o)Str # Returns a copy of the String from the last matching argument
	 * °afterLast(Object... o)Str # Returns a copy of the String after the last matching argument
	 */
	private Str mFromLast(final CallRuntime cr, final boolean withDelimiter) {
		final I_Object[] args = cr.argsVar(this, 1, 0);

		int beginindex = -1;
		String lastDelimiter = null;

		for(final I_Object arg : args) {
			final String delimiter = Lib_Convert.getStringValue(cr, arg);
			final int idx2 = this.value.lastIndexOf(delimiter);

			if(idx2 > -1 && (beginindex == -1 || idx2 > beginindex)) {
				beginindex = idx2;
				lastDelimiter = delimiter;
			}
		}
		final String result = beginindex >= 0
			? this.value.substring(beginindex + (withDelimiter ? 0 : lastDelimiter.length()))
			: ""; // Nil?!?
		return new Str(result);
	}

	/**
	 * °group(IntNumber length)List # Split this string into groups of the specified length
	 */
	private JMo_List mGroup(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];

		final int step = Lib_Convert.getIntValue(cr, arg);
		Lib_Error.ifTooSmall(cr, 1, step);

		final JMo_List list = new JMo_List();
		final String str = this.value;
		// String[] sa = this.value.split("(?<=\\G.{"+count+"})");
		for(int start = 0; start < str.length(); start += step)
			list.internalAdd(new Str(str.substring(start, Math.min(str.length(), start + step))));
		return list;
	}

	/**
	 * °htmlDecode()Str # Convert all HTML entitys to the representing chars.
	 */
	private I_Object mHtmlDecode(final CallRuntime cr) {
		cr.argsNone();
		final String result = HtmlChars.htmlEntitiesDecode(this.value);
		return new Str(result);
	}

	/**
	 * °htmlEntities()Str # Convert all convertable chars to their HTML entity
	 */
	private I_Object mHtmlEntities(final CallRuntime cr) {
		cr.argsNone();
		final String result = HtmlChars.htmlEntities(this.value);
		return new Str(result);
	}

	/**
	 * °htmlSpecial ^ htmlSpecialChars
	 * °htmlSpecialChars()Str # Converts some chars, which have a special meaning in HTML: &, ', ", <, >
	 */
	private I_Object mHtmlSpecialChars(final CallRuntime cr) {
		cr.argsNone();
		final String result = HtmlChars.htmlSpecialChars(this.value);
		return new Str(result);
	}

	/**
	 * °insert(Atomic ins, IntNumber pos)Str # Create a string where "ins" is inserted at position "pos"
	 */
	private I_Object mInsert(final CallRuntime cr) {
		final I_Object[] args = cr.args(this, A_Atomic.class, A_IntNumber.class);
		final String ins = Lib_Convert.getStringValue(cr, args[0]);
		final int pos = Lib_Convert.getIntValue(cr, args[1]);
		final int len = this.value.length();
		final int rpos = Lib_Sequence.realPosition(cr, pos, len + 1, false);
		final String result = Lib_String.insert(ins, rpos, this.value);
		return new Str(result);
	}

	/**
	 * Ähnlich zu .to
	 * °left(IntNumber amount)Str # Get a copy of the amount of chars from the left side
	 */
	private Str mLeft(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int to = Lib_Convert.getIntValue(cr, arg);
		if(to > this.value.length())
			return this;
		Lib_Error.ifTooSmall(cr, 1, to);
		final String s = this.value.substring(0, to);
		return new Str(s);
	}

	/**
	 * °limit(IntNumber maxLength)Str # Returns only the first/left 'maxLength' chars of this string.
	 * °limit(IntNumber maxLength, Atomic suffix)Str # If this string longer than 'maxLength' cut it and add 'suffix'.
	 */
	private Str mLimit(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 1, 2);

		final int max = Lib_Convert.getIntValue(cr, cr.argType(args[0], A_IntNumber.class));
		Lib_Error.ifTooSmall(cr, 0, max);

		final String suffix = args.length == 1
			? null
			: Lib_Convert.getStringValue(cr, cr.argType(args[1], A_Atomic.class));

		if(this.value.length() > max) {
			String temp = this.value.substring(0, max);
			if(suffix != null)
				temp = temp + suffix;
			return new Str(temp);
		}

		return this;
	}

	/**
	 * °lines()List # Returns all lines of this string.
	 */
	private JMo_List mLines(final CallRuntime cr) {
		cr.argsNone();
		final String[] sa = this.value.split("\n");
		final JMo_List list = new JMo_List();
		for(final String s : sa)
			list.internalAdd(new Str(s));
		return list;
	}

	/**
	 * °map(Object each)§Str # Creates a copy of the string, and execute 'each' with every char.
	 * °map(VarLet var, Object each)§Str # Creates a copy of the string, and execute 'var' and 'each' with every char.
	 */
	private Str mMap(final CallRuntime cr, final String method) {
		Lib_Error.ifArgs(cr.argCount(), 1, 2, cr, this);
		final StringBuilder result = new StringBuilder();

		for(int p = 0; p < this.value.length(); p++) {
			final Char test = new Char(this.value.charAt(p));
			final I_Object[] each = {test};
			final I_Object testr = cr.copyEach(method).argsEach(this, 0, each, I_Object.class);
			result.append(testr);
		}
		return new Str(result.toString());
	}

	/**
	 * °match(Regex reg)Bool # Returns true, if the string matches this regular expression
	 */
	private Bool mMatch(final CallRuntime cr) {
		final I_Object o = cr.args(this, JMo_RegEx.class)[0];
		final String reg = ((JMo_RegEx)o).getValue();
		return Bool.getObject(this.value.matches(reg));
	}

	/**
	 * °only(Range|Chars c...)Str # Removes all characters that do not match any of the given arguments.
	 */
	private I_Object mOnly(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 0);

		final String[] allowed = new String[args.length];

		for(int i = 0; i < args.length; i++) {
			final I_Object arg = cr.argTypeExt(args[i], A_Chars.class, JMo_Range.class);

			if(arg instanceof Char)
				allowed[i] = "" + ((Char)arg).getValue();
			else if(arg instanceof Str)
				allowed[i] = ((Str)arg).value;
			else if(arg instanceof JMo_Range)
				allowed[i] = ((JMo_Range)arg).computeString(cr);
			else
				Err.impossible(arg);
		}
		final StringBuilder sb = new StringBuilder(this.value.length());

		for(final char c : this.value.toCharArray()) {
			boolean okay = false;

			for(final String chars : allowed)
				if(chars.indexOf(c) >= 0) {
					okay = true;
					break;
				}

			if(okay)
				sb.append(c);
		}
		return new Str(sb.toString());
	}

	/**
	 * °parseBin()Int # Converts a Binary-String to a Int
	 */
	private Int mParseBin(final CallRuntime cr) {
		cr.argsNone();
		final int i = NumSys_Binary.fromBin(this.value);
		return new Int(i);
	}

	/**
	 * #°fromHex^hexToInt
	 * #°hexToInt()Int # Converts an Hex-String to a Int
	 * °parseHex()Int # Converts an Hex-String to Int
	 */
	private Int mParseHex(final CallRuntime cr) {
		cr.argsNone();
		final int i = NumSys_Hex.fromHex(this.value);
		return new Int(i);
	}

	/**
	 * °parseUnicode()Char # Converts an Unicode-Char-String to a single Char
	 */
	private Char mParseUnicode(final CallRuntime cr) {
		cr.argsNone();
		final char c = ConvertChar.parseUnicode(this.value);
		return new Char(c);
	}

	/**
	 * °part(IntNumber from, IntNumber to)Str # Cuts the string from position 'from' to position 'to'
	 */
	private Str mPart(final CallRuntime cr) {
		final I_Object[] args = cr.args(this, A_IntNumber.class, A_IntNumber.class);
		final int from = Lib_Convert.getIntValue(cr, args[0]);
		final int to = Lib_Convert.getIntValue(cr, args[1]);
		Lib_Error.ifNotBetween(cr, 1, this.value.length(), from, "'from'");
		Lib_Error.ifNotBetween(cr, from, this.value.length(), to, "'length'");
		final String s = this.value.substring(from - 1, to);
		return new Str(s);
	}

	/**
	 * °quote(Int format=2, Bool escapeSpecialChars=false)Str # Quotes the string with the selected format
	 * 1: Quote a string with '', insideprefix is \ also escapes backslash and 0
	 * 2: Quote a string with "", insideprefix is \ also escapes backslash and 0
	 * 3: Quote a string with '', insideprefix is ' also escapes backslash and 0
	 * 4: Quote a string with "", insideprefix is " also escapes backslash and 0
	 */
	private I_Object mQuote(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 0, 2);
		int format = 2;
		boolean escape = false;

		if(args.length >= 1) {
			final I_Object arg = cr.argType(args[0], A_IntNumber.class);
			format = Lib_Convert.getIntValue(cr, arg);
		}

		if(args.length == 2) {
			final I_Object arg = cr.argType(args[1], Bool.class);
			escape = Lib_Convert.getBoolValue(cr, arg);
		}

		switch(format) {
			case 1:
				return new Str(FormString.quote(this.value, '\'', '\\', escape));
			case 2:
				return new Str(FormString.quote(this.value, '"', '\\', escape));
			case 3:
				return new Str(FormString.quote(this.value, '\'', '\'', escape));
			case 4:
				return new Str(FormString.quote(this.value, '"', '"', escape));
			default:
				throw new RuntimeError(cr, "Invalid quote format", "Allowed are 1-4, but got: " + format);
		}
	}

	/**
	 * °reduce(Object init, Object each)§Object # Reduces the string to a single value.
	 * °reduce(Object init, VarLet var, Object each)§Object # Reduces the string to a single value.
	 */
	private I_Object mReduce(final CallRuntime cr, final String method) {
		Lib_Error.ifArgs(cr.argCount(), 2, 3, cr, this);
		I_Object sum = cr.argsOneAdvance(this, 0, I_Object.class);

		for(int p = 0; p < this.value.length(); p++) {
			final Char test = new Char(this.value.charAt(p));
			final I_Object[] each = {sum, test};
			sum = cr.copyEach(method).argsEach(this, 1, each, I_Object.class);
		}
		return sum;
	}

	/**
	 * °replace(Atomic search, Atomic repl)Str # Replace all occurrences with the replacement
	 * °replace(Regex search, Atomic repl)Str # Replace all occurrences with the replacement
	 */
	private Str mReplace(final CallRuntime cr) {
		final I_Object[] args = cr.argsExt(this, new Class<?>[]{I_Atomic.class, JMo_RegEx.class}, new Class<?>[]{I_Atomic.class});
		final String s2 = Lib_Convert.getStringValue(cr, args[1]);

		if(args[0] instanceof JMo_RegEx) {
			final String regex = ((JMo_RegEx)args[0]).getValue();

			try {
				final String result = this.value.replaceAll(regex, s2);
				return new Str(result);
			}
			catch(final PatternSyntaxException e) {
				throw new RuntimeError(cr, "Invalid Pattern Syntax", e.getMessage());
			}
		}
		final String s1 = Lib_Convert.getStringValue(cr, args[0]);
		final String result = this.value.replace(s1, s2);
		return new Str(result);
	}

	/**
	 * °reverse()Str # Create a string where all chars are in reversed order
	 */
	private I_Object mReverse(final CallRuntime cr) {
		cr.argsNone();
		final int len = this.value.length();
		final char[] source = this.value.toCharArray();
		final char[] target = new char[len];
		for(int i = 0; i < len; i++)
			target[i] = source[len - 1 - i];
		return new Str(new String(target));
	}

	/**
	 * °right(IntNumber amount)Str # Get a copy of the amount of chars from the right side
	 */
	private Str mRight(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int begin = Lib_Convert.getIntValue(cr, arg);
		final int len = this.value.length();
		if(begin > len)
			return this;
		Lib_Error.ifTooSmall(cr, 1, begin);
		final String s = this.value.substring(len - begin);
		return new Str(s);
	}

	/**
	 * °scan(Atomic open, Atomic close)List # Scan this String and return a list with all parts between 'start' and 'end'.
	 */
	private JMo_List mScan(final CallRuntime cr) {
		final I_Object[] oa = cr.args(this, I_Atomic.class, I_Atomic.class);
		final String start = Lib_Convert.getStringValue(cr, oa[0]);
		final String end = Lib_Convert.getStringValue(cr, oa[1]);

		final SimpleList<I_Object> result = new SimpleList<>();
		final int len = this.value.length();
		int idx = 0;
		int next = 0;
		boolean open = false;

		while(idx < len)
			if(!open) {
				next = this.value.indexOf(start, idx);
				if(next == -1)
					return new JMo_List(result);
				idx = next + start.length();
				open = true;
			}
			else {
				next = this.value.indexOf(end, idx);

				if(next == -1) {
					result.add(new Str(this.value.substring(idx)));
					return new JMo_List(result);
				}
				result.add(new Str(this.value.substring(idx, next)));
				idx = next + end.length();
				open = false;
			}

		if(open)
			result.add(new Str(this.value.substring(idx)));

		return new JMo_List(result);
	}

	/**
	 * °search ^ searchFirst
	 * °searchFirst(Object search)Int? # Returns the position of the first hit or nil if nothing is found.
	 * °searchLast(Object search)Int? # Returns the position of the last hit or nil if nothing is found.
	 */
	private A_Immutable mSearch(final CallRuntime cr, final boolean first) {
		final I_Object arg = cr.args(this, I_Object.class)[0];
		final String search = Lib_Convert.getStringValue(cr, arg);
		final int idx = first ? this.value.indexOf(search) : this.value.lastIndexOf(search);
		return idx < 0
			? Nil.NIL
			: new Int(idx + 1);
	}

	/**
	 * °set(Char c, Int pos)Str # Create a new String, where Char at position 'pos' is changed with 'c'
	 * °put(Char c, Int pos)Str # Create a new String, where Char at position 'pos' is changed with 'c'. If needed, the String will be expanded.
	 */
	private Str mSetChar(final CallRuntime cr, final boolean lazy) {
		final I_Object[] args = cr.args(this, Char.class, A_IntNumber.class);
		final char c = Lib_Convert.getCharValue(cr, args[0]);
		final int pos1 = Lib_Convert.getIntValue(cr, args[1]);
		int pos = Lib_Sequence.realPosition(cr, pos1, this.value.length(), lazy);

		String base = this.value;

		if(pos == -1) {
			final int posAbs = Math.abs(pos1);

			if(pos1 < 0) {
				pos = 1;
				base = Lib_String.sequence(' ', posAbs - base.length()) + base;
			}
			else {
				pos = posAbs;
				base += Lib_String.sequence(' ', posAbs - base.length());
			}
		}
		String result = pos <= 1 ? "" : base.substring(0, pos - 1);
		result += c;
		result += base.substring(pos);
		return new Str(result);
	}

	/**
	 * °sort()§Str # Sort the chars
	 * °sort(Object each)§Str # Sort the chars by passing every pair to 'each'. The result of 'each' must be Bool.
	 * °sort(VarLet var, Object each)§Str # Sort the chars by passing every pair to 'var' and 'each' The result of 'each' must be Bool.
	 *
	 * TODO Bad performance! Optimize it!
	 */
	private Str mSort(final CallRuntime cr, final String method) {
		final int parCount = cr.argCount();
		Lib_Error.ifArgs(parCount, 0, 2, cr, this);

		final SimpleList<Character> list = new SimpleList<>(this.value.length());
		for(final char c : this.value.toCharArray())
			list.add(c);

		if(parCount == 0) {
			cr.argsNone();
			list.sort((o1, o2)-> {
				return Lib_Compare.isEqual(o1, o2) ? 0 : Lib_Compare.isGreater(o1, o2) ? +1 : -1;
			});

			final StringBuilder sb = new StringBuilder();
			for(final Character c : list)
				sb.append(c);
			return new Str(sb.toString());
		}
		int smallest;

		for(int l = 1; l < list.size(); l++) {
			smallest = l;

			for(int m = l + 1; m <= list.size(); m++) {
				final Character buffer1 = list.get(smallest - 1);
				final Character buffer2 = list.get(m - 1);
				final I_Object[] test = {new Char(buffer1), new Char(buffer2)};
				final Bool correct = (Bool)cr.copyEach(method).argsEach(this, 0, test, Bool.class);
				if(!correct.getValue())
					smallest = m;
			}

			if(l != smallest) {
				final int pos1 = smallest - 1;
				final int pos2 = l - 1;
				final Character buffer1 = list.get(pos1);
				final Character buffer2 = list.get(pos2);
				list.set(pos1, buffer2);
				list.set(pos2, buffer1);
			}
		}
		final StringBuilder sb = new StringBuilder();
		for(final Character c : list)
			sb.append(c);
		return new Str(sb.toString());
	}

	/**
	 * °explode^split
	 * °split()List # Split this string into chars
	 * °split(Regex regex)List # Split the string according to this regex
	 * °split(Chars delimiter)List # Split the string at all occurrences of this delimiter
	 */
	private JMo_List mSplit(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 0, 1);
		final I_Object arg = args.length == 0 ? null : cr.argTypeExt(args[0], new Class<?>[]{A_Chars.class, JMo_RegEx.class});

		final JMo_List list = new JMo_List();

		if(args.length == 0 || arg instanceof Str && ((Str)arg).value.length() == 0)
			for(final char c : this.value.toCharArray())
				list.internalAdd(new Char(c));
		else if(arg instanceof JMo_RegEx) {
			final String regex = ((JMo_RegEx)arg).getValue();
			final String[] sa = this.value.split(regex);
			for(final String s : sa)
				list.internalAdd(new Str(s));
		}
		else {
			final String delimiter = "" + ((I_Atomic)arg).getValue();
			final Iterable<String> sl = ConvertString.toList(delimiter, this.value);
			for(final String s : sl)
				list.internalAdd(new Str(s));
		}

		return list;
	}

	/**
	 * °splitKeep(Atomic delimiter)List # Splits the list and copy also the delimiter
	 * °splitKeep(List listOfDelimiter)List # Splits the list and copy also the delimiter
	 */
	private JMo_List mSplitKeep(final CallRuntime cr) {
		final I_Object o = cr.argsExt(this, new Class<?>[]{I_Atomic.class, JMo_List.class})[0];
		// .checkPars(cr, this, (Class<?>)null);
		TypeTable2<String, Boolean> tab = null;

		if(o instanceof I_Atomic)
			tab = ConvertString.splitToTable(this.value, "" + o.toString());

		else if(o instanceof JMo_List) {
			final JMo_List delimiters = (JMo_List)o;
			final SimpleList<String> al = new SimpleList<>(delimiters.getInternalCollection().size());

			for(final I_Object delimiter : delimiters.getInternalCollection()) {
				if(!(delimiter instanceof I_Atomic))
					throw new RuntimeError(cr, "Non-Atomic Delimiter", "Got: " + Lib_Type.getName(delimiter.getClass(), delimiter));
				al.add(Lib_Convert.getStringValue(cr, delimiter));
			}
			tab = ConvertString.splitToTable(this.value, al.toArray(new String[al.size()]));
		}

		else
			throw Err.todo(o);

		final I_Collection<String> data = tab.getColumn0();
		final SimpleList<I_Object> newList = new SimpleList<>(data.size());

		for(final String s : data)
			newList.add(new Str(s));

		return new JMo_List(newList);
	}

	/**
	 * Lazy handling of invalid positions
	 * °start(IntNumber pos)Str # Get a copy up to this position. Negative value are counting from the right side.
	 */
	private Str mStartPos(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		int begin = Lib_Convert.getIntValue(cr, arg);
		if(begin == 0) // || Math.abs(begin) > this.value.length()
			throw new RuntimeError(cr, "Invalid position in string", "Position can't be 0");
		if(begin < 0)
			begin = this.value.length() + begin + 1;
		if(begin < 1 || begin > this.value.length())
			return new Str("");
		final String s = this.value.substring(begin - 1);
		return new Str(s);
	}

	/**
	 * °startsWith(Atomic arg...)Bool # Return true, if the string starts with one of the parameters
	 */
	private Bool mStartsWith(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 1, 0);

		for(final I_Object o : args) {
			final String s = Lib_Convert.getStringValue(cr, cr.argType(o, I_Atomic.class));
			if(this.value.startsWith(s))
				return Bool.TRUE;
		}
		return Bool.FALSE;
	}

	/**
	 * °- ^ sub
	 * °sub(Object c)Str # Removes the char from the string.
	 */
	private Str mSubtract(final CallRuntime cr) {
		final I_Object o = cr.args(this, I_Object.class)[0];
		return new Str(this.value.replace("" + Lib_Convert.getStringValue(cr, o), ""));
	}

	/**
	 * °table(Atomic columnDelimiter)Table # Create a Table from this String, by splitting it by lines and 'columnDelimiter'
	 */
	private JMo_Table mTable(final CallRuntime cr) {
		final I_Object arg = cr.args(this, I_Atomic.class)[0];
		final String delimiter = Lib_Convert.getStringValue(cr, arg);
		final String[] lines = this.value.split("\n");
		final int len = lines.length;
		final String[][] splitLines = new String[len][0];
		for(int i = 0; i < len; i++)
//			splitLines[i] = lines[i].split(delimiter);	// Problems with Regex
			splitLines[i] = ConvertString.toStringArray(delimiter, lines[i]);

		int max_width = 1; // Minimum is 1
		for(int i = 0; i < len; i++)
			max_width = Math.max(splitLines[i].length, max_width);

		final ArrayTable<I_Object> tab = new ArrayTable<>(max_width);

		for(int r = 0; r < len; r++) {
			final I_Object[] row = new I_Object[max_width];
			for(int c = 0; c < max_width; c++)
				row[c] = splitLines[r].length - 1 >= c ? new Str(splitLines[r][c]) : new Str("");
			tab.add(row);
		}
		return new JMo_Table(tab);
	}

	/**
	 * °till ^ tillFirst
	 * °tillFirst(Object... to)Str # Returns a copy of the String up to the first matching 'to' argument
	 * °before ^ beforeFirst
	 * °beforeFirst(Object... to)Str # Returns a copy of the String before the first matching 'to' argument
	 */
	private Str mTillFirst(final CallRuntime cr, final boolean withDelimiter) {
		final I_Object[] args = cr.argsVar(this, 1, 0);

//		if(arg instanceof JMo_Regex) {
//			String regex = arg.toString();
//			Matcher m = Pattern.compile(regex).matcher(this.value);
//			MOut.temp(this.value.matches(regex));
//			if(!m.matches())
//				return new Str("");
//			int idx = m.start();
//			return new Str(this.value.substring(0, idx));
//		}

		int idx = -1;
		int dlen = 0;

		for(final I_Object arg : args) {
			final String delimiter = Lib_Convert.getStringValue(cr, arg);
			final int idx2 = this.value.indexOf(delimiter);

			if(idx2 >= 0 && (idx == -1 || idx2 < idx)) {
				idx = idx2;
				dlen = delimiter.length();
			}
		}
		if(!withDelimiter)
			dlen = 0;
		return idx >= 0
			? new Str(this.value.substring(0, idx + dlen))
			: this; // Nil?!?
	}

	/**
	 * °tillLast(Object... to)Str # Returns a copy of the String up to the last matching 'to' argument
	 * °beforeLast(Object... to)Str # Returns a copy of the String before the last matching 'to' argument
	 */
	private Str mTillLast(final CallRuntime cr, final boolean withDelimiter) {
		final I_Object[] args = cr.argsVar(this, 1, 0);

		int idx = -1;
		int dlen = 0;

		for(final I_Object arg : args) {
			final String delimiter = Lib_Convert.getStringValue(cr, arg);
			final int idx2 = this.value.lastIndexOf(delimiter);

			if(idx2 >= 0 && (idx == -1 || idx2 > idx)) {
				idx = idx2;
				dlen = delimiter.length();
			}
		}
		if(!withDelimiter)
			dlen = 0;
		return idx >= 0
			? new Str(this.value.substring(0, idx + dlen))
			: this; // Nil?!?
	}

	/**
	 * °bytes ^ toByteArray
	 * °toByteArray()ByteArray # Return a ByteArray, containing the UTF8-Bytes of this string.
	 */
	private JMo_ByteArray mToByteArray(final CallRuntime cr) {
		cr.argsNone();
		final byte[] ba = this.value.getBytes(StandardCharsets.UTF_8);
		return new JMo_ByteArray(ba);
	}

	/**
	 * °chars ^ toChars
	 * °toChars()List # Return a list with all chars
	 */
	private JMo_List mToChars(final CallRuntime cr) {
		cr.argsNone();
		final char[] ca = this.value.toCharArray();
		final SimpleList<I_Object> al = new SimpleList<>(ca.length);
		for(final char c : ca)
			al.add(new Char(c));
		return new JMo_List(al);
	}

	/**
	 * °trim()Str # Removes leading and ending: Space, Tab, Carriage Return, NewLine, FormFeed
	 * °trim(Char... toTrim)Str # Removes toTrim from left and right
	 * °trimLeft()Str # Removes leading: Space, Tab, Carriage Return, NewLine, FormFeed
	 * °trimLeft(Char... toTrim)Str # Removes toTrim from the left
	 * °trimRight()Str # Removes ending: Space, Tab, Carriage Return, NewLine, FormFeed
	 * °trimRight(Char... toTrim)Str # Removes toTrim from the right
	 **/
	private Str mTrim(final CallRuntime cr, final boolean left, final boolean right) {
		final I_Object[] args = cr.argsVar(this, 0, 0);

		if(args.length == 0) {
//			final String s = FilterString.trim(this.value, new char[]{' ', '\t', '\r', '\f', '\n', '\0'}, left, right);
			final String s = FilterString.trim(this.value, left, right);
			return new Str(s); // this.value.trim()
		}
		final char[] ca = new char[args.length];
		for(int i = 0; i < args.length; i++)
			ca[i] = ((Char)cr.argType(args[i], Char.class)).getValue();
		final String s = FilterString.trim(this.value, ca, left, right);
		return new Str(s); // this.value.trim()
	}

	/**
	 * °unescape()Str # Convert escaped chars to normal chars
	 */
	private I_Object mUnescape(final CallRuntime cr) {
		cr.argsNone();
		return new Str(FormString.unescapeSpecialChars(this.value));
	}

	/**
	 * °uniq ^ unique
	 * °unique()Str # Remove duplicate chars.
	 */
	private Str mUnique(final CallRuntime cr) {
		cr.argsNone();
		final StringBuilder sb = new StringBuilder(this.value.length());

		for(int p = 0; p < this.value.length(); p++) {
			final char search = this.value.charAt(p);

			boolean okay = true;
			for(int q = 0; q < p; q++)
				if(this.value.charAt(q) == search) {
					okay = false;
					break;
				}

			if(okay)
				sb.append(search);
		}
		return new Str(sb.toString());
	}

	/**
	 * °unite(Char c...)Str # Unites all or selected rows of equal chars to a single char.
	 */
	private I_Object mUnite(final CallRuntime cr) {
		final I_Object[] args = cr.argsVar(this, 0, 0);
		String temp = this.value;

		if(args.length == 0)
			temp = FilterString.uniteDoubles(temp);
		else
			for(final I_Object arg : args) {
				final Char o = (Char)cr.argType(arg, Char.class);
				final char c = o.getValue();
				temp = FilterString.uniteDoubles(c, temp);
			}

		return new Str(temp);
	}

	/**
	 * °unquote(Int format=0)Str # Unquotes a string
	 */
	private I_Object mUnquote(final CallRuntime cr) {
		final I_Object[] args = cr.argsFlex(this, 0, 1);
		int pari = 0;

		if(args.length == 1) {
			final I_Object arg = cr.argType(args[0], A_IntNumber.class);
			pari = Lib_Convert.getIntValue(cr, arg);
		}
		// trim
		final char[] trimChars = {'\t', ' '};
		final int valueLen = this.value.length();
		final int spaceLeft = valueLen - FilterString.trim(this.value, trimChars, true, false).length();
		final int spaceRight = valueLen - FilterString.trim(this.value, trimChars, false, true).length();
		final String trimmed = this.value.substring(spaceLeft, valueLen - spaceRight);
		final String left = this.value.substring(0, spaceLeft);
		final String right = this.value.substring(valueLen - spaceRight, valueLen);

		// unquote
		final StringBuilder sb = new StringBuilder(this.value.length());
		sb.append(left);

		switch(pari) {
			case 0:
				String s = FormString.unquote(trimmed, '\'', '\\');
				s = FormString.unquote(s, '"', '\\');
				sb.append(s);
				break;
			case 1:
				sb.append(FormString.unquote(trimmed, '\'', '\\'));
				break;
			case 2:
				sb.append(FormString.unquote(trimmed, '"', '\\'));
				break;
			case 3:
				sb.append(FormString.unquote(trimmed, '\'', '\''));
				break;
			case 4:
				sb.append(FormString.unquote(trimmed, '"', '"'));
				break;
			default:
				throw new RuntimeError(cr, "Invalid quote format", "Allowed are 0-4, but got: " + pari);
		}
		sb.append(right);
		return new Str(sb.toString());
	}

	/**
	 * °width(IntNumber width)Str # Cut the string or fill it up with space chars to get a string with that width
	 */
	private Str mWidth(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int width = Lib_Convert.getIntValue(cr, arg);
		Lib_Error.ifTooSmall(cr, 0, width);

		final int len = this.value.length();
		if(len == width)
			return this;
		else if(len > width)
			return new Str(this.value.substring(0, width)); //TODO Testen!
		else
			return new Str(this.value + Lib_String.sequence(' ', width - len)); //TODO Testen!
	}

	/**
	 * °words()List # Return a List with all words in this string.
	 */
	private JMo_List mWords(final CallRuntime cr) {
		cr.argsNone();
		final Iterable<String> words = ConvertString.toWords(this.value);
		final JMo_List result = new JMo_List();
		for(final String s : words)
			result.internalAdd(new Str(s));
		return result;
	}

}
