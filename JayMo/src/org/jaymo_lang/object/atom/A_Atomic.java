/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.atom;

import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.COMPARE;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.JMo_KeyValue;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.sys.JMo_Java;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.ATOMIC;
import org.jaymo_lang.util.Lib_AtomConv;


/**
 * @author Michael Nitsche
 * @created 16.02.2018
 */
public abstract class A_Atomic extends A_Immutable implements I_Atomic {

	@Override
	public boolean equals(Object arg) {
		if(arg == Nil.NIL)
			return false;

		if(arg instanceof I_Atomic)
			arg = ((I_Atomic)arg).getValue();

		Object cur = this;
		if(cur instanceof I_Atomic)
			cur = ((I_Atomic)cur).getValue();

//		if(arg.getClass() != cur.getClass())
//			return false;

		if(cur instanceof Number && arg instanceof Number)
			if(cur instanceof Double && Double.isNaN((double)cur) && arg instanceof Double && Double.isNaN((double)arg))
				return true;
			else
				return ((Number)cur).doubleValue() == ((Number)arg).doubleValue();

		if((cur instanceof Character || cur instanceof String) && (arg instanceof Character || arg instanceof String))
			return cur.toString().equals(arg.toString());

//		if((cur instanceof Number || cur instanceof Character || cur instanceof String) && (arg instanceof Number || arg instanceof Character || arg instanceof String))
//			return cur.toString().equals(arg.toString());

		return cur.equals(arg);
	}

	@Override
	public boolean equalsLazy(Object objPar) {
		if(objPar == Nil.NIL)
			return false;

		if(objPar instanceof I_Atomic)
			objPar = ((I_Atomic)objPar).getValue();
		final Object objCur = this.getValue();

		// Test direct
		if(objPar.equals(objCur))
			return true;

		// Test Bool, Number
		if((objPar instanceof Boolean || objPar instanceof Number) && (objCur instanceof Boolean || objCur instanceof Number)) {
			final Number numPar = objPar instanceof Boolean ? (Boolean)objPar ? 1 : 0 : (Number)objPar;
			final Number numCur = objCur instanceof Boolean ? (Boolean)objCur ? 1 : 0 : (Number)objCur;
			if(objCur instanceof Double && Double.isNaN((double)objCur) && objPar instanceof Double && Double.isNaN((double)objPar))
				return true;
			else
				return numCur.doubleValue() == numPar.doubleValue();
		}
		// Test String
		String strPar = objPar.toString();
		String strCur = objCur.toString();

		final String strParLower = strPar.toLowerCase();
		if(strParLower.equals("true"))
			strPar = "1";
		if(strParLower.equals("false"))
			strPar = "0";

		final String strCurLower = strCur.toLowerCase();
		if(strCurLower.equals("true"))
			strCur = "1";
		if(strCurLower.equals("false"))
			strCur = "0";

		if(strCur.endsWith(".0"))
			strCur = strCur.substring(0, strCur.length() - 2);
		if(strPar.endsWith(".0"))
			strPar = strPar.substring(0, strPar.length() - 2);

		return strPar.equals(strCur);
	}

	@Override
	public boolean equalsStrict(Object arg) {
		if(arg == Nil.NIL || !(arg instanceof I_Atomic))
			return false;
		arg = ((I_Atomic)arg).getValue();

		final Object cur = ((I_Atomic)this).getValue();

		if(arg.getClass() != cur.getClass())
			return false;

		if(cur instanceof Double && Double.isNaN((double)cur) && arg instanceof Double && Double.isNaN((double)arg))
			return true;

		return cur.equals(arg);
	}

	/**
	 * °MIN_VALUE()Atomic # Returns the minimum value of this type.
	 * °MAX_VALUE()Atomic # Returns the maximum value of this type.
	 */
	@Override
	public A_Immutable getConstant(final CallRuntime cr, final String name) {

		switch(name) {
			case "MAX_VALUE":
				return this.getMaxValue(cr);
			case "MIN_VALUE":
				return this.getMinValue(cr);
		}
		return null;
	}

	@Override
	public final void init(final CallRuntime cr) {}

	@Override
	public final Boolean isGreater2(final I_Object o) {
		if(!(o instanceof I_Atomic))
			return null;

		return this.isGreater3((I_Atomic)o);
	}

	@Override
	protected final ObjectCallResult call2(final CallRuntime cr, final String method) {
		// , boolean alsParameter
		// IT wird verworfen, da hier nicht benötigt!

		switch(method) {
//			case "getSize":	// This is not clear
//			case "getLength":
//			case "getLen":
			case "length":
			case "len":
				cr.argsNone();
				return A_Object.stdResult(this.getLength(cr));

			/**
			 * °toBool()Bool # Convert atomic object to a boolean value.
			 *
			 * °toByte()Byte # Convert atomic object to a byte number.
			 * °toShort()Short # Convert atomic object to a short number.
			 * °toInt()Int # Convert atomic object to a integer number.
			 * °toLong()Long # Convert atomic object to a long number.
			 * °toBigInt()BigInt # Convert atomic object to a big integer number.
			 *
			 * °toDec()Dec # Convert atomic object to a decimal number.
			 * °toFloat()Float # Convert atomic object to a floating decimal number.
			 * °toDouble()Double # Convert atomic object to a double decimal number.
			 * °toBigDec()BigDec # Convert atomic object to a big decimal number.
			 *
			 * °toChar()Char # Convert atomic object to a Character.
			 * °toJava()Java # Convert atomic object to a Java Object.
			 */
			case "toBool":
				cr.argsNone();
				return A_Object.stdResult(Lib_AtomConv.convert(cr, this, ATOMIC.BOOL));
			case "toChar":
				cr.argsNone();
				return A_Object.stdResult(Lib_AtomConv.convert(cr, this, ATOMIC.CHAR));

//			case "toString": // Moved to A_Object
//			case "toStr":
//				cr.argsNone();
//				return stdResult(convertTo(cr, ATOMIC.STR));

			case "toByte":
				cr.argsNone();
				return A_Object.stdResult(Lib_AtomConv.convert(cr, this, ATOMIC.BYTE));
			case "toShort":
				cr.argsNone();
				return A_Object.stdResult(Lib_AtomConv.convert(cr, this, ATOMIC.SHORT));
			case "toInt":
				cr.argsNone();
				return A_Object.stdResult(Lib_AtomConv.convert(cr, this, ATOMIC.INT));
			case "toLong":
				cr.argsNone();
				return A_Object.stdResult(Lib_AtomConv.convert(cr, this, ATOMIC.LONG));
			case "toBigInt":
				cr.argsNone();
				return A_Object.stdResult(Lib_AtomConv.convert(cr, this, ATOMIC.BIGINT));
			case "toDec":
				cr.argsNone();
				return A_Object.stdResult(Lib_AtomConv.convert(cr, this, ATOMIC.DEC));
			case "toFloat":
				cr.argsNone();
				return A_Object.stdResult(Lib_AtomConv.convert(cr, this, ATOMIC.FLOAT));
			case "toDouble":
				cr.argsNone();
				return A_Object.stdResult(Lib_AtomConv.convert(cr, this, ATOMIC.DOUBLE));
			case "toBigDec":
				cr.argsNone();
				return A_Object.stdResult(Lib_AtomConv.convert(cr, this, ATOMIC.BIGDEC));

			case "toJava":
				return A_Object.stdResult(this.mToJava(cr));

			case "isLess":
			case "<":
				return A_Object.stdResult(this.mComparsion(cr, COMPARE.L));
			case "isGreater":
			case ">":
				return A_Object.stdResult(this.mComparsion(cr, COMPARE.G));
			case "isLessOrEqual":
			case "<=":
				return A_Object.stdResult(this.mComparsion(cr, COMPARE.LE));
			case "isGreaterOrEqual":
			case ">=":
				return A_Object.stdResult(this.mComparsion(cr, COMPARE.GE));

			case "toKeyValue":
			case "->":
				return A_Object.stdResult(this.mToKeyValue(cr));

			/**
			 * #°switch()Switch
			 */
//			case "switch":final Call ca = new Call(cr.getSurrBlock(), this, cr.getDebugInfo());
//				final JMo_Switch sw = new JMo_Switch(ca);
//				return A_Object.stdResult(sw);
		}
		// Spezifische Methoden ausführen
		return this.call3(cr, method);
	}

	protected abstract ObjectCallResult call3(CallRuntime cr, String method);

	/**
	 * #°getSize ^ getLength
	 * #°getLen ^ getLength
	 * °len ^ length
	 * °length()Int # Returns the amount of chars
	 */
	protected abstract Int getLength(final CallRuntime cr);

	protected abstract A_Atomic getMaxValue(CallRuntime cr);

	protected abstract A_Atomic getMinValue(CallRuntime cr);

	protected abstract Boolean isGreater3(final I_Atomic o);

	/**
	 * °< ^ isLess
	 * °isLess(Atomic other)Bool # Returns true if the this object is lesser than 'other'
	 * °> ^ isGreater
	 * °isGreater(Atomic other)Bool # Returns true if the this object is greater than 'other'
	 * °<= ^ isLessOrEqual
	 * °isLessOrEqual(Atomic other)Bool # Returns true if the this object is lesser or equal than 'other'
	 * °>= ^ isGreaterOrEqual
	 * °isGreaterOrEqual(Atomic other)Bool # Returns true if the this object is greater or equal than 'other'
	 *
	 * Returns always Bool
	 */
	protected abstract Bool mComparsion(CallRuntime cr, COMPARE co);

	protected final RuntimeError newErrorMinMaxValue(final CallRuntime cr) {
		return new RuntimeError(cr, "Utopian value", "The requested value requires too much memory.");
	}

	/**
	 * °toJava()Java # Converts this object to a Java-Object.
	 */
	private JMo_Java mToJava(final CallRuntime cr) {
		cr.argsNone();
		return new JMo_Java(this.getValue());
	}

	/**
	 * °-> ^ toKeyValue
	 * °toKeyValue()KeyValue # Creates a KeyValue-Object
	 */
	private I_Object mToKeyValue(final CallRuntime cr) {
		final I_Object o = cr.args(this, I_Object.class)[0];
		return new JMo_KeyValue(this, o);
	}

}
