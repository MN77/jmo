/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.passthrough;

import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.I_ObjectReplace;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 03.09.2019
 */
public interface I_Mem extends I_Object, I_ObjectReplace {

	I_Object get(CallRuntime cr);

	String getMemTypeString();

	String getName();

	boolean isInitialized(CallRuntime cr);

	void let(CallRuntime cr, CallRuntime cr2, I_Object value);

	void let(CallRuntime cr, CallRuntime cr2, I_Object value, boolean convert);

	void setType(String type, DebugInfo debugInfo);

}
