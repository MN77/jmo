/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.passthrough;

import org.jaymo_lang.model.Call;
import org.jaymo_lang.object.classic.JMo_Count;


/**
 * @author Michael Nitsche
 * @created 01.10.2019
 *
 *          Object for {}
 */
public class Count extends JMo_Count implements I_PassThrough {

	public Count(final Call end_or_range) {
		super(end_or_range);
	}

	public Count(final Call start, final Call end) {
		super(start, end);
	}

	public Count(final Call start, final Call end, final Call step) {
		super(start, end, step);
	}

}
