/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.jaymo_lang.error.ErrorBase;
import org.jaymo_lang.error.ErrorBaseDebug;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.MethodList;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 21.06.2018
 * @apiNote For creating a JayMo-Type by writing regular methods and constructors.
 */
public abstract class A_AutoObject extends A_Object {

	private Method[] map = null;


	@SuppressWarnings("unchecked")
	public void getMethods(final MethodList ml) {
		this.iInit();

		for(final Method m : this.map) {
			final String name = m.getName();
			final Class<? extends I_Object> res = (Class<? extends I_Object>)m.getReturnType();
			ml.add(res, name, (Class<? extends I_Object>[])m.getParameterTypes());
		}
	}

	@Override
	public void init(final CallRuntime cr) {}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return this.getTypeName();
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		this.iInit();

		final I_Object[] pa = cr.argsVar(this, 0, 0);
		final Method met = this.iMethod(cr, method, pa);
		if(met == null)
			return null;

		try {
			I_Object res = null;

			if(pa != null && pa.length > 0) {
				final Object[] args = new Object[pa.length];
				for(int i = 0; i < args.length; i++)
					args[i] = pa[i];
				res = (I_Object)met.invoke(this, args);
			}
			else
				res = (I_Object)met.invoke(this);
			// TODO Wenn hier null zurückkommt, weil void ... meldet dass Funktion nicht vorhanden.
			// TODO Hack
			if(res == null)
				res = this;

			return new ObjectCallResult(res, false);
		}
		catch(final ErrorBase e) {
			throw new RuntimeError(cr, e.getMessage(), e.getDetail());
		}
		catch(final IllegalAccessException | IllegalArgumentException e) {
			Err.exit(e);
		}
		catch(final InvocationTargetException e) { // Ausführungs-Fehler
			final Throwable cause = e.getCause();
			if(cause instanceof ErrorBase)
				throw new RuntimeError(cr, cause.getMessage(), ((ErrorBase)cause).getDetail());
			else if(cause instanceof ErrorBaseDebug) // ErrorBaseDebug, ErrorExec, ErrorParse
				throw(ErrorBaseDebug)cause;
			else
				Err.exit(e, cr.getDebugInfo().toString());
		}
		return null;
	}

	private void iInit() {

		if(this.map == null) {
			final SimpleList<Method> ml = new SimpleList<>();

			Class<?> c = this.getClass();

			while(c != null && c != A_AutoObject.class) {
				final Method[] ma = c.getDeclaredMethods();
				for(final Method m : ma)
					if(Modifier.isPublic(m.getModifiers()))
						ml.add(m);
				c = c.getSuperclass();
			}
			this.map = ml.toArray(new Method[ml.size()]);
		}
	}

	/**
	 * TODO In die zentrale Bibliothek ... KlasseX
	 * Hat evtl. Probleme mit int und Integer
	 */
	private Method iMethod(final CallRuntime cr, final String name, final Object[] args) {
		Err.ifNull(name);
		final int parlen = args.length;
		for(final Method m : this.map)
			if(m.getName().equals(name)) {
				final Class<?>[] mpars = m.getParameterTypes();

				if(m.getParameterCount() == parlen) {
					boolean ok = true;
					for(int i = 0; i < mpars.length; i++)
						if(!mpars[i].isInstance(args[i])) {
							MOut.exit(name, i, mpars[i], args[i].getClass());
							ok = false;
							break;
						}
					if(ok)
						return m;
				}
			}
		return null;
	}

}
