/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.test;

import org.jaymo_lang.object.A_AutoObject;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Int;


/**
 * @author Michael Nitsche
 * @created 21.06.2018
 */
public class JMo_AutoTestDummy extends A_AutoObject {

	public Bool getFalse() {
		return Bool.FALSE;
	}

	public Bool getTrue() {
		return Bool.TRUE;
	}

	public Int test() {
		return new Int(1);
	}

	public Int test(final Bool b) {
		return b == Bool.TRUE ? new Int(1) : new Int(0);
	}

}
