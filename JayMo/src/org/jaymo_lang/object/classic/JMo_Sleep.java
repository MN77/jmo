/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.classic;

import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.model.VarArgsCallBuffer;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_ControlObject;
import org.jaymo_lang.object.I_HasConstructor;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.sys.Sys;


/**
 * @author Michael Nitsche
 * @created 30.01.2022
 */
public class JMo_Sleep extends A_Object implements I_ControlObject, I_HasConstructor {

	private static final int        DEFAULT = 1000;
	private final VarArgsCallBuffer buffer;


	public JMo_Sleep() {
		this.buffer = null;
	}

	/**
	 * +Sleep(Int millisec)
	 * +Sleep(Int seconds, Int millisec)
	 * +Sleep(Int minutes, Int seconds, Int millisec)
	 * +Sleep(Int hours, Int minutes, Int seconds, Int millisec)
	 */
	public JMo_Sleep(final Call... calls) {
		this.buffer = new VarArgsCallBuffer(calls);
	}

	public Return constructor(final CallRuntime cr) {
		int ms = 0;

		if(this.buffer != null) {
			final I_Object[] args = this.buffer.get();
			final int len = args.length;
			Lib_Error.ifArgs(len, 1, 4, cr, this);

			final int[] values = new int[len];
			for(int i = 0; i < len; i++)
				values[i] = Lib_Convert.getIntValue(cr, cr.argType(args[i], Int.class));

			switch(len) {
				case 1:
					ms = values[0];
					break;
				case 2:
					ms = values[0] * 1000 + values[1];
					break;
				case 3:
					ms = values[0] * 60_000 + values[1] * 1000 + values[2];
					break;
				case 4:
					ms = values[0] * 3_600_000 + values[1] * 60_000 + values[2] * 1000 + values[3];
					break;
			}
		}
		else
			ms = JMo_Sleep.DEFAULT;
		Sys.sleep(ms);
		return null;
	}

	@Override
	public void init(final CallRuntime cr) {
		cr.getStrict().checkSandbox(cr.getDebugInfo(), "Cycle");

		if(this.buffer != null)
			this.buffer.init(cr, this);
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		return null;
	}

}
