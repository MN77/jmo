/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.classic;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_ControlObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Exec;
import org.jaymo_lang.util.Lib_Output;


/**
 * @author Michael Nitsche
 * @created 14.01.2021
 */
public class JMo_Which extends A_Object implements I_AutoBlockDo, I_ControlObject {

	private final ArgCallBuffer[] args;
	private int                   value = -1;


	/**
	 * +Which(Bool b...)
	 * %.get
	 * 'item = 5
	 * 'Which( item < 2, item < 4, item == 4, item > 4 )
	 * ' ("First true argument: " + it).print
	 */
	public JMo_Which(final Call... args) {
		this.args = new ArgCallBuffer[args.length];
		for(int i = 0; i < args.length; i++)
			this.args[i] = new ArgCallBuffer(i, args[i]);
	}

	@Override
	public I_Object autoBlockDo(final CallRuntime cr) {
		final CallRuntime crNew = cr.copyPass();
		final I_Object result = this.value == -1 ? Nil.NIL : new Int(this.value);
		return Lib_Exec.execBlockStream(crNew, result);
	}

	@Override
	public void init(final CallRuntime cr) {

		for(int i = 0; i < this.args.length; i++) {
			final boolean b = Lib_Convert.getBoolValue(cr, this.args[i].init(cr, this, Bool.class));

			if(b) {
				this.value = i + 1;
				return;
			}
		}
		this.value = -1;
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final StringBuilder sb = new StringBuilder();
		sb.append("Which(");

		boolean first = true;

		for(final ArgCallBuffer arg : this.args) {
			final String s = arg.toString(cr, type.getNested());

			if(type == STYPE.DESCRIBE)
				sb.append(Lib_Output.indentLines(s, false));
			else {
				if(!first)
					sb.append(", ");
				sb.append(s);
			}
			first = false;
		}
		sb.append(')');
		return sb.toString();
	}

	/**
	 * °get()%Int? # Returns the position of the first object argument, which is 'true'.
	 */
	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "get":
				final I_Object result = this.autoBlockDo(cr);
				return new ObjectCallResult(result, true);

			default:
				return null;
		}
	}

}
