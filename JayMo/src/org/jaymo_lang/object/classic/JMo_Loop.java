/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.classic;

import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_ControlObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Exec;


/**
 * @author Michael Nitsche
 * @created 15.05.2021
 */
public class JMo_Loop extends A_Object implements I_AutoBlockDo, I_ControlObject {

	/**
	 * !Loop a block/stream
	 * +Loop()
	 * %.each
	 */
	public JMo_Loop() {}

	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.mLoop(cr).obj;
	}

	@Override
	public void init(final CallRuntime cr) {}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "each":
				return this.mLoop(cr);

			default:
				return null;
		}
	}

	/**
	 * °each()%Object # Loop block and stream until 'Break'
	 */
	private ObjectCallResult mLoop(final CallRuntime crOld) {
		crOld.argsNone();

		if(crOld.getStream() == null && crOld.getCallBlock() == null)
			return new ObjectCallResult(this, true);

		final LoopHandle handle = new LoopHandle(this);
		final CallRuntime crNew = crOld.copyLoop(handle);
		I_Object result = this;

		while(!crOld.getApp().toBeTerminated()) { // Will also break at
			handle.startLap();
			result = Lib_Exec.execBlockStream(crNew, new Int(handle.getLap()));

			if((result = Lib_Exec.loopResult(result)) instanceof Return)
				return ((Return)result).getLoopResult();
		}
		return new ObjectCallResult(Nil.NIL, true);
	}

}
