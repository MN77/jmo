/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.classic;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_ControlObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.struct.A_Sequence;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;


/**
 * @author Michael Nitsche
 * @created 04.06.2021
 */
public class JMo_Each extends A_Object implements I_AutoBlockDo, I_ControlObject {

	final ArgCallBuffer buffer;


	/**
	 * !Walk through each item in the given object.
	 * +Each(Object obj)
	 * %.each
	 */
	public JMo_Each(final Call call) {
		this.buffer = new ArgCallBuffer(0, call);
	}

	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.mEach(cr).obj;
	}

	@Override
	public void init(final CallRuntime cr) {
		if(this.buffer != null)
			this.buffer.initExt(cr, this, A_IntNumber.class, Str.class, A_Sequence.class);
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return "Each(" + this.buffer.get().toString(cr, STYPE.NESTED) + ")";
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "each":
				return this.mEach(cr);

			default:
				return null;
		}
	}

	/**
	 * °each()%Object # Walk through each item.
	 */
	private ObjectCallResult mEach(final CallRuntime cr) {
		cr.argsNone();

		if(cr.getStream() == null && cr.getCallBlock() == null)
			return new ObjectCallResult(this, true);

		final I_Object obj = this.buffer.get();
		final Call c2 = cr.call.copy(obj, "each", null);
		final CallRuntime crNew = cr.copyCall(c2, false);

		return obj.call(crNew);
	}

}
