/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.classic;

import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_ControlObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Try;


/**
 * @author Michael Nitsche
 * @created 06.11.2019
 */
public class JMo_Try extends A_Object implements I_AutoBlockDo, I_ControlObject {

	/**
	 * +Try()
	 * +Try()
	 * %.block
	 */
	public JMo_Try() {}

	@Override
	public I_Object autoBlockDo(final CallRuntime cr) {
		return Lib_Try.mErrTryBlock(cr, this).obj;
	}

	@Override
	public void init(final CallRuntime cr) {}

	/**
	 * °block()%%Object? # Executes the block. If an error occures it will be returned, elsewhere the result will be returned.
	 * °stream()Object? # No Block allowed. Executes the stream, if an error occurs it will be returned. Otherwise the stream result will be returned.
	 * °argument(Object o)Object # Executes the argument. If an error occures it will be returned, elsewhere the argument.
	 * #°call()Object # Executes block/stream. If an error occures it will be returned, elsewhere the result will be returned.
	 * #°call(Object o)Object # Executes the argument. If an error occures it will be returned, elsewhere the argument.
	 */
	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
//			case "do": // Override for If.class
//				final I_Object result = autoBlockDo(cr);
//				return new Result_Obj(result, true);

			case "block": // Default / AutoBlockDo
				return Lib_Try.mErrTryBlock(cr, this);

			case "stream":
				return Lib_Try.mErrTryStream(cr, this);

//			case "arg":
			case "argument":
				return Lib_Try.mErrTryArg(cr, this);

//			case "call":
//				return Lib_Try.mErrTry(cr, this);
		}
		return null;
	}

}
