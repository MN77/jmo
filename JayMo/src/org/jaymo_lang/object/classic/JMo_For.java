/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.classic;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_ControlObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.LoopHandle;
import org.jaymo_lang.object.atom.A_Atomic;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Exec;

import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 17.09.2020
 */
public class JMo_For extends A_Object implements I_AutoBlockDo, I_ControlObject {

	private final ArgCallBuffer value1, value2, value3;


	/**
	 * +For( Atomic? init, Bool test, Atomic? action ) # For( i=3, i<9, i=++ )
	 * %.each
	 */
	public JMo_For(final Call c1, final Call c2, final Call c3) {
		Err.ifNull(c1, c2, c3);
		this.value1 = new ArgCallBuffer(0, c1);
		this.value2 = new ArgCallBuffer(1, c2);
		this.value3 = new ArgCallBuffer(2, c3);
	}

	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.mEach(cr).obj;
	}

	@Override
	public void init(final CallRuntime cr) {
		this.value1.initExt(cr, this, A_Atomic.class, Nil.class);
//		this.value2.init(cr, this, Bool.class);
//		this.value3.init(cr, this, A_Atomic.class);
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final STYPE subType = type.getNested();

		final StringBuilder sb = new StringBuilder();
		sb.append("For(");
		sb.append(this.value1.toString(cr, subType));
		sb.append(',');
		sb.append(this.value2.toString(cr, subType));
		sb.append(',');
		sb.append(this.value3.toString(cr, subType));
		sb.append(')');
		return sb.toString();
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "each":
				return this.mEach(cr);

			default:
				return null;
		}
	}

	/**
	 * °each()%Object # Execute block and stream for every object of this loop.
	 */
	private ObjectCallResult mEach(final CallRuntime crOld) {
		crOld.argsNone();

		if(crOld.getStream() == null && crOld.getCallBlock() == null) // && args==null
			return new ObjectCallResult(this, true);

		final LoopHandle handle = new LoopHandle(this);
		final CallRuntime crNew = crOld.copyLoop(handle);

		I_Object result = this.value1.get();
		I_Object check = this.value2.getInitEach(crNew, this, result, Bool.class);
		boolean b = Lib_Convert.getBoolValue(crNew, check);

		I_Object resultPrep = result;

		while(b) {
			handle.startLap();
			result = resultPrep;
			result = Lib_Exec.execBlockStream(crNew, result);

			if((result = Lib_Exec.loopResult(result)) instanceof Return)
				return ((Return)result).getLoopResult();

			resultPrep = this.value3.getInitEach(crNew, this, result, A_Atomic.class, Nil.class);
			check = this.value2.getInitEach(crNew, this, resultPrep, Bool.class);
			b = Lib_Convert.getBoolValue(crNew, check);
		}
		return new ObjectCallResult(result, true);
	}

}
