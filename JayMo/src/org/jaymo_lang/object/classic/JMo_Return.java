/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.classic;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_ControlObject;
import org.jaymo_lang.object.I_HasConstructor;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.pseudo.Return.LEVEL;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @created 14.04.2021
 */
public class JMo_Return extends A_Object implements I_ControlObject, I_HasConstructor {

	private final ArgCallBuffer arg;


	/**
	 * +Return()
	 */
	public JMo_Return() {
		this.arg = null;
	}

	/**
	 * +Return(Object o)
	 */
	public JMo_Return(final Call c) {
		this.arg = new ArgCallBuffer(0, c);
	}

	public Return constructor(final CallRuntime cr) {
		return new Return(LEVEL.RETURN, this.arg == null ? Nil.NIL : this.arg.get());
	}

	@Override
	public void init(final CallRuntime cr) {
		if(this.arg != null)
			this.arg.init(cr, this, I_Object.class);
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		return null;
	}

}
