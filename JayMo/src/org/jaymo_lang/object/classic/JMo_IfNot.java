/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.classic;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_ControlObject;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.A_Atomic;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;


/**
 * @author Michael Nitsche
 * @created 23.09.2020
 */
public class JMo_IfNot extends A_Object implements I_AutoBlockDo, I_ControlObject {

	private final ArgCallBuffer arg;


	/**
	 * +IfNot(Bool b)
	 * %.then
	 * 'IfNot( 2+3 == 5 )
	 * ' "Is true!".print
	 * '.else
	 * ' "Is wrong!".print
	 */
	public JMo_IfNot(final Call arg) {
		this.arg = new ArgCallBuffer(0, arg);
	}

	@Override
	public I_Object autoBlockDo(final CallRuntime cr) {
		final A_Atomic witha = (A_Atomic)this.arg.get();
		return witha.mDoIf(cr, true, false, false).obj;
	}

	@Override
	public void init(final CallRuntime cr) {
		this.arg.init(cr, this, A_Atomic.class);
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final StringBuilder sb = new StringBuilder();
		sb.append("IfNot(");
		if(this.arg != null)
			sb.append(this.arg.toString(cr, type.getNested()));
		sb.append(")");
		return sb.toString();
	}

	/**
	 * °then()Object # Execute block and stream, if the argument of this object is 'true'.
	 */
	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
//			case "do": // Override for If.class
			case "then":
				final I_Object result = this.autoBlockDo(cr);
				return new ObjectCallResult(result, true);

			default:
				return null;
		}
	}

}
