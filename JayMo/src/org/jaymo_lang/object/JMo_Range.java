/*******************************************************************************
 * Copyright (C): 2018-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.I_AutoBlockList;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.atom.A_Number;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Char;
import org.jaymo_lang.object.atom.Dec;
import org.jaymo_lang.object.atom.I_Decimal;
import org.jaymo_lang.object.atom.I_Integer;
import org.jaymo_lang.object.atom.Int;
import org.jaymo_lang.object.atom.JMo_Dec;
import org.jaymo_lang.object.atom.JMo_Double;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.classic.JMo_Count;
import org.jaymo_lang.object.pseudo.Return;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Exec;
import org.jaymo_lang.util.Lib_Type;

import de.mn77.base.data.group.Group2;
import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 10.05.2018
 *
 *          A Range is static! Changing arguments does not have any effect!
 */
public class JMo_Range extends A_Object implements I_AutoBlockDo, I_AutoBlockList, I_ControlObject {

	private final ArgCallBuffer argStart;
	private final ArgCallBuffer argEnd;


	public static JMo_Range createNew(final CallRuntime cr, final I_Object from, final I_Object to) {
		return new JMo_Range(new Call(cr, from), new Call(cr, to));
	}

	/**
	 * +Range(Object end) # Range(9)
	 */
	public JMo_Range(final Call end) {
		this.argStart = new ArgCallBuffer(0, new Call(end.surrounding, new Int(1), end.debugInfo));
		this.argEnd = new ArgCallBuffer(1, end);
	}

	/**
	 * +Range(Object start, Object end) # Range(3,9)
	 * +Object start..Object end # 2..7
	 */
	public JMo_Range(final Call start, final Call end) {
		this.argStart = new ArgCallBuffer(0, start);
		this.argEnd = new ArgCallBuffer(1, end);
	}

	@Override
	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.iEach(cr, false, true).obj;
	}

	@Override
	public SimpleList<I_Object> autoBlockToList(final CallRuntime cr) {
		return ((JMo_List)this.iEach(cr, true, false).obj).getInternalCollection();
	}

	public String computeString(final CallRuntime cr) {
		final I_Object lStart = this.argStart.get();
		final I_Object lEnd = this.argEnd.get();

		// Check: Only single numbers (0-9) are allowed!
		if(lStart instanceof A_Number) {
			boolean error = false;

			if(lStart instanceof I_Decimal || lEnd instanceof I_Decimal)
				error = true;
			else {
				final int iStart = Lib_Convert.getIntValue(cr, lStart);
				if(iStart < 0 || iStart > 9)
					error = true;
				final int iEnd = Lib_Convert.getIntValue(cr, lEnd);
				if(iEnd < 0 || iEnd > 9)
					error = true;
			}
			if(error)
				throw new RuntimeError(cr, "Can't convert Range to Str", "Only chars and integer numbers between 0 and 9 are allowed!");
		}
		final char cStart = Lib_Convert.getCharValue(cr, lStart);
		final char cEnd = Lib_Convert.getCharValue(cr, lEnd);

		final StringBuffer sb = new StringBuffer();
		char cur = cStart;

		if(cStart <= cEnd)
			while(cur <= cEnd) {
				sb.append(cur);
				cur++;
			}
		else {

			while(cur > cEnd) {
				sb.append(cur);
				cur--;
			}
			sb.append(cur);
		}
		return sb.toString();
	}

	public Group2<I_Object, I_Object> getInternalValues() {
		return new Group2<>(this.argStart.get(), this.argEnd.get());
	}

	@Override
	public void init(final CallRuntime cr) {
		final I_Object s = this.argStart.initExt(cr, this, A_Number.class, Char.class);
		final I_Object e = this.argEnd.initExt(cr, this, A_Number.class, Char.class);

		if(s instanceof A_Number && !(e instanceof A_Number) || s instanceof Char && !(e instanceof Char))
			throw new RuntimeError(cr, "Different types for Range", "" + Lib_Type.getName(s) + " --> " + Lib_Type.getName(e));
	}

	@Override
	public String toString() {

		if(!this.isInit())
			return this.getTypeName();
		else {
			final StringBuilder sb = new StringBuilder();
			sb.append(this.argStart.get().toString());
			sb.append("..");
			sb.append(this.argEnd.get().toString());
			return sb.toString();
		}
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final String s = this.argStart.toString(cr, STYPE.IDENT);
		final String e = this.argEnd.toString(cr, STYPE.IDENT);

		return type == STYPE.DESCRIBE
			? "Range(" + s + ',' + e + ")"
			: s + ".." + e;
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "toList":
				return this.mToList(cr);
			case "toCount":
				return A_Object.stdResult(this.mToCount(cr));
			case "compute":
				return A_Object.stdResult(this.mCompute(cr));
			case "each":
				return this.mEach(cr);
			case "contains":
				return A_Object.stdResult(this.mContains(cr));

			default:
				return null;
		}
	}

	/**
	 * TODO Using Lib_Count.each ?!?
	 */
	private ObjectCallResult iEach(final CallRuntime cr, final boolean toList, final boolean execBlockStream) {
		final I_Object start = this.argStart.get();
		final I_Object end = this.argEnd.get();

		if(cr.call == null && execBlockStream)
			Err.invalid(cr.call, toList, execBlockStream);

		I_Object result = this;
		final SimpleList<I_Object> list = toList
			? new SimpleList<>()
			: null;

		if(execBlockStream && cr.getStream() == null && cr.getCallBlock() == null) // && args==null
			return new ObjectCallResult(end, true);

		final LoopHandle handle = new LoopHandle(this);
		final CallRuntime crNew = cr.copyLoop(handle);

		//------------- Loops -------------

		if(Lib_Type.typeIs(crNew, start, I_Integer.class, Char.class) && Lib_Type.typeIs(crNew, end, I_Integer.class, Char.class)) {
			// --- {Integer,Integer,Integer} ---
			// --- {Char,Char,Integer} ---
			int i1 = Lib_Convert.getIntValue(crNew, start, true);
			int i2 = Lib_Convert.getIntValue(crNew, end, true);
			boolean desc = i2 < i1;
			int i3 = desc
				? -1
				: 1;

			int i = 0;
			I_Object it = null;

			for(i = i1; desc
				? i >= i2
				: i <= i2; i += i3) {
				handle.startLap();
				final boolean last = desc ? i <= i2 : i >= i2;

				it = Lib_Convert.intToObject(Lib_Type.getType(crNew, start), Lib_Type.getType(crNew, end), i); // , varlet
				if(toList)
					list.add(it);
				else if(execBlockStream) {
					result = Lib_Exec.execBlockStream(crNew, it);

					if((result = Lib_Exec.loopResult(result)) instanceof Return)
						return ((Return)result).getLoopResult();
				}

				if(!last) {
					i1 = Lib_Convert.getIntValue(crNew, start, true);
					i2 = Lib_Convert.getIntValue(crNew, end, true);
					desc = i2 < i1;
					i3 = desc
						? -1
						: 1;
				}
			}
		}
		else if(Lib_Type.typeIs(crNew, start, JMo_Dec.class, I_Integer.class) && Lib_Type.typeIs(crNew, end, JMo_Dec.class, I_Integer.class)) {
			// --- => Dec ---
			Dec d1 = Lib_Convert.getDecValue(crNew, start);
			Dec d2 = Lib_Convert.getDecValue(crNew, end);
			boolean desc = d2.isLess(d1);

			for(Dec d = d1; desc
				? d.isGreaterOrEqual(d2)
				: d.isLessOrEqual(d2); d = desc ? d.dec() : d.inc()) {
//				d = Lib_Math.normalize(d, 10); // Correct fractal!!! Current limit is 10 digits!

				handle.startLap();
				final boolean last = desc ? d.isLessOrEqual(d2) : d.isGreaterOrEqual(d2);

				final I_Object it = new JMo_Dec(d);
				if(toList)
					list.add(it);
				else if(execBlockStream) {
					result = Lib_Exec.execBlockStream(crNew, it);

					if((result = Lib_Exec.loopResult(result)) instanceof Return)
						return ((Return)result).getLoopResult();
				}

				if(!last) {
					d1 = Lib_Convert.getDecValue(crNew, start);
					d2 = Lib_Convert.getDecValue(crNew, end);
					desc = d2.isLess(d1);
				}
			}
		}
		else if(Lib_Type.typeIs(crNew, start, I_Decimal.class, I_Integer.class) && Lib_Type.typeIs(crNew, end, I_Decimal.class, I_Integer.class)) {
			// --- => Decimal ---
			double d1 = Lib_Convert.getDoubleValue(crNew, start);
			double d2 = Lib_Convert.getDoubleValue(crNew, end);
			boolean desc = d2 < d1;
			double d3 = desc
				? -1
				: 1;

			for(double d = d1; desc
				? d >= d2
				: d <= d2; d += d3) {
//				d = Lib_Math.normalize(d, 10); // Correct fractal!!! Current limit is 10 digits!

				handle.startLap();
				final boolean last = desc ? d <= d2 : d >= d2;

				final I_Object it = new JMo_Double(d);
				if(toList)
					list.add(it);
				else if(execBlockStream) {
					result = Lib_Exec.execBlockStream(crNew, it);

					if((result = Lib_Exec.loopResult(result)) instanceof Return)
						return ((Return)result).getLoopResult();
				}

				if(!last) {
					d1 = Lib_Convert.getDoubleValue(crNew, start);
					d2 = Lib_Convert.getDoubleValue(crNew, end);
					desc = d2 < d1;
					d3 = desc
						? -1
						: 1;
				}
			}
		}
		else
			throw new CodeError(crNew, "Invalid arguments", this.toString(crNew, STYPE.IDENT));
		return toList
			? new ObjectCallResult(new JMo_List(list), false)
			: new ObjectCallResult(result, true);
	}

	/**
	 * °compute()Str # Computes a String with all items of this Range.
	 */
	private I_Object mCompute(final CallRuntime cr) {
		cr.argsNone();
		return new Str(this.computeString(cr));
	}

	/**
	 * °contains(Int#Char num)Bool # Returns true, if this number is within the range.
	 */
	private Bool mContains(final CallRuntime cr) {
		final I_Object o = cr.argsExt(this, new Class<?>[]{A_Number.class, Char.class})[0]; // TODO erweitern: TYPE.DEC (A_Number)
		I_Object min = this.argStart.get();
		I_Object max = this.argEnd.get();

		if(min instanceof A_Number && !(o instanceof A_Number) || min instanceof Char && !(o instanceof Char)) {
			cr.warning("Different types", "Searching <" + Lib_Type.getName(o) + "> in a Range of <" + Lib_Type.getName(min) + ">");
			return Bool.FALSE;
		}

		// Sort and exchange arguments
		if(min.compareTo(cr, max) > 0) {
			final I_Object buffer = min;
			min = max;
			max = buffer;
		}

		if(min instanceof A_Number) {
			final double io = Lib_Convert.getDoubleValue(cr, o);
			final double is = Lib_Convert.getDoubleValue(cr, min);
			final double ie = Lib_Convert.getDoubleValue(cr, max);
			final boolean b = io >= is && io <= ie;
			return Bool.getObject(b);
		}

		if(min instanceof Char) {
			final char io = ((Char)o).getValue();
			final char is = ((Char)min).getValue();
			final char ie = ((Char)max).getValue();
			final boolean b = io >= is && io <= ie;
			return Bool.getObject(b);
		}
		throw Err.impossible(min.getClass(), o.getClass());
	}

	/**
	 * °each()%Object # Execute block and stream with every item in this range.
	 */
	private ObjectCallResult mEach(final CallRuntime cr) {
		cr.argsNone();
		return this.iEach(cr, false, true);
	}

	/**
	 * °toCount()Count# Converts this range to a Count object.
	 */
	private JMo_Count mToCount(final CallRuntime cr) {
		cr.argsNone();
		final Call cstart = new Call(cr, this.argStart.get());
		final Call cend = new Call(cr, this.argEnd.get());
		return new JMo_Count(cstart, cend, null);
	}

	/**
	 * °toList()List # Create a list with all object in this range.
	 */
	private ObjectCallResult mToList(final CallRuntime cr) {
		cr.argsNone();
		return this.iEach(cr, true, false);
	}

}
