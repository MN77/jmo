/*******************************************************************************
 * Copyright (C): 2020-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.magic.var;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Error;


/**
 * @author Michael Nitsche
 * @created 17.11.2020
 */
public abstract class A_MagicVar extends A_Object {

	private final boolean allowWithoutDot;


	public A_MagicVar(final boolean allowWithoutDot) {
		this.allowWithoutDot = allowWithoutDot;
	}

	public abstract String getID();

	@Override
	public final void init(final CallRuntime cr) {}

	@Override
	public final String toString() {
		return this.getID();
	}

	/** Override allowed **/
	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		return this.getID();
	}

	public final boolean withoutDot() {
		return this.allowWithoutDot;
	}

	/**
	 * @apiNote This default implementation may be overridden.
	 */
	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		throw Lib_Error.notAllowed(cr);
	}

}
