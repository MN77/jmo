/*******************************************************************************
 * Copyright (C): 2022-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.magic.con;

import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.immute.A_Immutable;
import org.jaymo_lang.object.immute.Nil;
import org.jaymo_lang.object.magic.I_MagicGet;
import org.jaymo_lang.object.magic.I_MagicReplace;
import org.jaymo_lang.runtime.CallRuntime;


/**
 * @author Michael Nitsche
 * @since 20.01.2021
 */
public class MagicConstSelf extends A_MagicConst implements I_MagicReplace, I_MagicGet { // extends A_MagicConst

	private final String        name;
	private final ArgCallBuffer arg;
	private boolean             init = false;


	public MagicConstSelf(final String name, final Call arg) {
		this.name = name;
		this.arg = new ArgCallBuffer(0, arg);
	}

	public I_Object get(final CallRuntime cr) {

		if(this.init)
			return this.arg.get();
		else {
			this.init = true;
			return this.arg.init(cr, Nil.NIL, A_Immutable.class);
		}
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		return new ObjectCallResult(null, false);
	}

}
