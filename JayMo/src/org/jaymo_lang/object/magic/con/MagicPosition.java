/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.magic.con;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Error;

import de.mn77.base.data.constant.position.POSITION;


/**
 * @author Michael Nitsche
 * @since 06.03.2020
 */
public class MagicPosition extends A_MagicConst {

	private final POSITION pos;


	public MagicPosition(final POSITION pos) {
		this.pos = pos;
	}

	public POSITION get() { //final CurProc cr
		return this.pos;
	}

	@Override
	public String getName() {
		return this.pos.toString();
	}

//	@Override
//	public String toStringIdent(final CurProc cr) {
//		return "MagicPosition(_" + this.pos + ")";
//	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		throw Lib_Error.notAllowed(cr);
	}

}
