/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.magic.con;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Output;

import de.mn77.base.data.type.datetime.MTime;
import de.mn77.base.data.util.Lib_String;
import de.mn77.base.sys.MOut;


/**
 * @author Michael Nitsche
 * @created 02.10.2019
 */
public class MagicDebug extends A_MagicConst {

	public static final String id = "DEBUG";
//	private static final String INTRO = "DEBUG | ";
	private static final int LINE_LENGTH = 64;


	@Override
	public String getName() {
		return MagicDebug.id;
	}

	/**
	 * °note(Str text)Same # Create a debug line including the given argument, to StdOut.
	 * °line()Same # Send a debug line to StdOut.
	 * °trace()Same # Send debug trace information to StdOut.
	 * °hashVCE()Same # Create informations about the current variable/constant environment to StdOut.
	 */
	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {

		switch(method) {
			case "note":
				final String t = Lib_Convert.getStringValue(cr, cr.args(this, Str.class)[0]);
				this.iPrint(cr, t);
				break;
			case "line":
				cr.argsNone();
				this.iPrint(cr, Lib_String.sequence('-', MagicDebug.LINE_LENGTH));
				break;
			case "trace":
				this.mTrace(cr);
				break;
			case "vce":
				cr.argsNone();
				this.iPrint(cr, cr.vce.toString());
				break;

			default:
				return null;
		}
		return A_Object.stdResult(this);
	}

	private void iPrint(final CallRuntime cr, final String... sa) {
//		final String intro = MagicDebug.INTRO;
		final String intro = new MTime().toString() + " | ";

		final StringBuilder sb = new StringBuilder();

		for(final String s : sa) {
			sb.append(intro);
			sb.append(s);
			sb.append('\n');
		}
		sb.append(intro);
		sb.append("  @ ");
		sb.append(cr.getDebugInfo().toString());

//		MOut.print(sb.toString());
		Lib_Output.out(cr.getApp(), sb.toString(), true);
	}


//	private String iHash(final Object o) {
//		return Hex.toHex(o.hashCode());
//	}

	private void mTrace(final CallRuntime cr) {
		cr.argsNone();
		MOut.trace(); // Print trace
		this.iPrint(cr, Lib_String.sequence('-', MagicDebug.LINE_LENGTH));
	}

}
