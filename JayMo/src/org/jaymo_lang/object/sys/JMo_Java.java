/*******************************************************************************
 * Copyright (C): 2019-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.sys;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.error.DebugInfo;
import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.error.RuntimeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.ClassFinder;
import org.jaymo_lang.util.Lib_Convert;
import org.jaymo_lang.util.Lib_Java;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.data.type.Lib_Class;
import de.mn77.base.error.Err;


/**
 * @author Michael Nitsche
 * @created 01.07.2019
 */
public class JMo_Java extends A_Object {

	private String              classBuffer;
	private final ArgCallBuffer classNameCall;
	private final ArgCallBuffer constructorParList;
	private Object              instance      = null; // null, if only static access
	private Class<?>            instanceClass = null;


	/**
	 * +Java(String classpath, Object... args)
	 */
	public JMo_Java(final Call[] args, final DebugInfo debugInfo) {
		final int argsLen = args.length;
		if(argsLen != 1 && argsLen != 2)
			throw new CodeError("Invalid parameters for Java-constructor", "Want: (String class-name, List constructor_aruments)", debugInfo);

		this.classBuffer = null;
		this.classNameCall = new ArgCallBuffer(0, args[0]);
		this.constructorParList = args.length == 1 ? null : new ArgCallBuffer(1, args[1]);
	}

	public JMo_Java(final Class<?> cl, final Call[] args, final DebugInfo debugInfo) {
		this.instanceClass = cl;
		this.classBuffer = null;
		this.classNameCall = null;

		if(args == null || args.length == 0)
			this.constructorParList = null;
		else {
			final Call cList = new Call(args[0].surrounding, new JMo_List(args), null, null, debugInfo);
			this.constructorParList = new ArgCallBuffer(0, cList);
		}
	}

	public JMo_Java(final Object instance) {
		this.instance = instance;
		this.instanceClass = instance.getClass();
		this.classBuffer = null;
		this.classNameCall = null;
		this.constructorParList = null;
	}

	public String getFullTypeName() {
		return Lib_Java.packConv_classToFull(this.instanceClass);
	}

	public Class<?> getJavaClass() {
		return this.instanceClass;
	}

	public final String getJavaClassName() {
		return this.instanceClass.getSimpleName();
//		return this.className;
	}

	public Object getJavaInstance() {
		return this.instance;
	}

	public Object getJavaObject() {
		return this.instance == null
			? this
			: this.instance;
	}

	@Override
	public void init(final CallRuntime cr) {
		cr.getStrict().checkNoJava(cr.getDebugInfo());
		cr.getApp().strict.checkSandbox(cr, "Java");

		if(this.instance == null) {
			if(this.classNameCall != null)
				this.classNameCall.init(cr, this, Str.class);
			if(this.constructorParList != null)
				this.constructorParList.init(cr, this, JMo_List.class);

			if(this.classBuffer == null && this.classNameCall != null) {
				final I_Object oName = this.classNameCall.get();
				this.classBuffer = Lib_Convert.getStringValue(cr, oName);
			}

			if(this.classBuffer != null) {
				if(this.classBuffer.startsWith("JMo_")) // TODO check later and check only name (package?!?)
					throw new RuntimeError(cr, "It's not allowed to create JayMo-Objects via Java-Class!", this.classBuffer);

//				if(this.instance_class == null)
				this.instanceClass = this.searchClass(cr);
			}

			try {
				this.instance = this.getInstance(cr, this.instanceClass);
			}
			catch(final InstantiationException e) {
//				Err.show(e);
//				throw new ExecError(cr, "Can't create object", e.getMessage());

				this.instance = null; // For Classes with private constructor and static functions
			}
		}
	}

	@Override
	public String toString() {
		return Lib_Java.packConv_classToFull(this.instanceClass);
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		if(type != STYPE.DESCRIBE)
			return this.toString();

		final StringBuilder sb = new StringBuilder();
		sb.append("Java(\"");
		sb.append(this.instanceClass.getName());
		sb.append('"');
		sb.append(", ");
		if(this.constructorParList != null)
			sb.append(this.constructorParList.toString(cr, STYPE.DESCRIBE));
//			sb.append( this.constructorParList.toString() );
		else
			sb.append("[]");

		sb.append(")");
		return sb.toString();
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String m) {
		// --- Automatic method ---
		return Lib_Java.mExec(cr, this.instance, this.instanceClass, m, this);
	}

	private Object getInstance(final CallRuntime cr, final Class<?> cl) throws InstantiationException {

		// --- Without arguments ---
		if(this.constructorParList == null || ((JMo_List)this.constructorParList.get()).getInternalCollection().size() == 0) {
			if(cl.getConstructors().length == 0) // No accessible constructor
				return null;

			try {
				return cl.newInstance();
			}
			catch(final IllegalAccessException e) {
				final String message = e.getMessage().replace("Class " + JMo_Java.class.getName(), "You"); // "Class jmo.struct.Java2JMo"
				throw new ExternalError(cr, "Illegal access", message);
			}
			catch(final SecurityException e) {
				throw new ExternalError(cr, "Illegal access", e.getMessage());
			}
		}
		// --- With argument ---
		final JMo_List parList = (JMo_List)this.constructorParList.get();
		if(!(parList instanceof JMo_List))
			throw new RuntimeError(cr, "Invalid arguments for constructor", "Arguments are needed within a list.");

		final SimpleList<I_Object> args = parList.getInternalCollection();
		final Object[] javaPars = Lib_Java.jmoToJava(cr, args.toArray(new I_Object[args.size()]), null);

		final Class<?>[] javaParTypes = this.iJavaTypes(javaPars);
		Constructor<?> ca = null;

		try {
			ca = cl.getConstructor(javaParTypes);
		}
		catch(final NoSuchMethodException e) {
//			throw new ExecError(cr, "Invalid parameters for constructor", this.className);
			// !!! Have to run through !!!
		}
		if(ca == null)
			ca = Lib_Class.getConstructor(cl, javaPars);
		if(ca == null)
			throw new ExternalError(cr, "Invalid arguments for constructor", this.instanceClass.getName()); // TODO Ausgabe: Möglichkeiten?!?

		try {
			return ca.newInstance(javaPars);
		}
		catch(SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new ExternalError(cr, "Can't create object", e.getMessage());
		}
	}

	private Class<?>[] iJavaTypes(final Object[] javaPars) {
		final Class<?>[] javaParTypes = new Class[javaPars.length];
		for(int i = 0; i < javaPars.length; i++)
			javaParTypes[i] = Lib_Java.toJavaType(javaPars[i]);
		return javaParTypes;
	}

	private Class<?> searchClass(final CallRuntime cr) {
		Err.ifNull(this.classBuffer);
		Class<?> result = null;

		// "java.util.Random"
		final int dotIdx = this.classBuffer.indexOf('.');
		if(dotIdx >= 0) // Package can start with [a-zA-Z_]
			try {
				result = Class.forName(this.classBuffer);
			}
			catch(final ClassNotFoundException e) {
				throw new ExternalError(cr, "Java-Class not found!", this.classBuffer);
			}
		else
			result = ClassFinder.getInstance().searchJavaClass(this.classBuffer);

		// Validate!
		if(result == null)
			throw new ExternalError(cr, "Java-Class not found!", this.classBuffer);
		if(Lib_Class.isChildOf(result, I_Object.class))
			throw new ExternalError(cr, "It's not allowed to create JayMo-Objects via Java-Class!", this.classBuffer);

		return result;
	}

}
