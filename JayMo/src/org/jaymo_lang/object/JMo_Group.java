/*******************************************************************************
 * Copyright (C): 2017-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import org.jaymo_lang.error.CodeError;
import org.jaymo_lang.model.ArgCallBuffer;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.I_AutoBlockDo;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.pseudo.A_MemLet;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Output;


/**
 * @author Michael Nitsche
 * @created 27.12.2017
 */
public class JMo_Group extends A_Object implements I_AutoBlockDo, I_ControlObject {

	private final ArgCallBuffer arg;


	/**
	 * +Group( Object o ) # Group( 4+9-2 )
	 * +( Object o ) ^ Group( Object o ).get # ( 3+5+2 )
	 * %.get
	 */
	public JMo_Group(final Call arg) {
		this.arg = new ArgCallBuffer(0, arg);
	}

	public I_Object autoBlockDo(final CallRuntime cr) {
		return this.mGet(cr).obj;
	}

	/** Only for JayMo-Converter **/
	public Call getInternalPar() {
		return this.arg.getInternalCall();
	}

	@Override
	public void init(final CallRuntime cr) {
		this.arg.init(cr, this, I_Object.class);
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final StringBuilder sb = new StringBuilder();
		sb.append(this.getTypeName());
		sb.append('(');

		if(this.arg != null) {
			String sa = this.arg.toString(cr, type.getNested());
			if(type == STYPE.DESCRIBE)
				sa = Lib_Output.indentLines(sa, false);
			sb.append(sa);
		}
		sb.append(')');
		return sb.toString();
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		return method.equals("get")
			? this.mGet(cr)
			: null;
	}

	/**
	 * °get()%Object # Return the argument of this Object
	 * °get(MemLet v)%Object # Assign the argument of this Object to 'v' and also return it.
	 */
	private ObjectCallResult mGet(final CallRuntime cr) {
		final I_Object result = this.arg.get();
		final I_Object[] args = cr.argsFlex(this, 0, 1);

		if(args.length == 1) {
			cr.argType(args[0], A_MemLet.class);
			((A_MemLet)args[0]).getMem().let(cr, cr, result);
		}
		if(!cr.hasBlock())
			return A_Object.stdResult(result);
		else if(result instanceof Bool) {
			cr.getStrict().checkGroupIf(cr);
			cr.argsNone();
			return new ObjectCallResult(((Bool)result).autoBlockDo(cr), true);
		}
		else
			throw new CodeError(cr, "Invalid use of Group-Shortcut with Block.", "To use the Shortcut-If, the result of the group must be <Bool>. But type is: " + result.getType(cr).toString());
//			cr.argsNone();
//			return A_Object.stdResult(result);
	}

}
