/*******************************************************************************
 * Copyright (C): 2021-2023 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object;

import java.util.TimerTask;

import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.atom.A_IntNumber;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.thread.MTimer;


/**
 * @author Michael Nitsche
 * @created 2021-02-25
 *
 * @apiNote This cycle will be terminated at the end of the application
 * @implNote It's not necessary to register this as fork. Because of automatic termination.
 */
public class JMo_Cycle extends A_EventObject implements I_Object {

	private MTimer timer = null;


	@Override
	public void init(final CallRuntime cr) {
		cr.getStrict().checkSandbox(cr, "Cycle");
	}

	/**
	 * °@cycle # This event will be executed at every cycle.
	 */
	@Override
	public boolean validateEvent(final CallRuntime cr, final String event) {
		return event.equals("@cycle");
	}

	@Override
	protected ObjectCallResult callMethod(final CallRuntime cr, final String method) {

		switch(method) {
			case "start":
				this.mStart(cr);
				return A_Object.stdResult(this);
			case "stop":
				this.mStop(cr);
				return A_Object.stdResult(this);

			default:
				return null;
		}
	}

//	@Override
//	protected String resolveDefault() {
//		return "@cycle";
//	}

	/**
	 * °start(IntNumber millisec)Same # Start the timer.
	 */
	private void mStart(final CallRuntime cr) {
		final I_Object arg = cr.args(this, A_IntNumber.class)[0];
		final int ms = Lib_Convert.getIntValue(cr, arg);

		if(this.timer != null) {
			cr.warning("Cycle already running", "Please stop it first to start it new.");
			return;
		}
		this.timer = new MTimer();

		final TimerTask task = new TimerTask() {

			@Override
			public void run() {
//				synchronized(this) {	// TODO usefully? Maybe direct in eventRun?
				if(cr.getApp().toBeTerminated())
					JMo_Cycle.this.timer.stop();

				JMo_Cycle.this.eventRun(cr, "@cycle", JMo_Cycle.this);
//				}
			}

		};

		this.timer.addCycle(ms, task);
//		cr.getApp().registerFork();
	}

	/**
	 * °stop()Same # Stop the timer.
	 */
	private void mStop(final CallRuntime cr) {
		cr.argsNone();
		if(this.timer == null)
			return;

		this.timer.stop();
//		cr.getApp().unregisterFork();
		this.timer = null;
	}

}
