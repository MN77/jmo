Error   : Invalid source code
Message : Strict! Shortcut of variables and constants are not allowed!
Detail  : Please write 'a?' instead of 'a'!
  @ vars_without_qm_medium.jmo :5
===== RESULT =====
null
