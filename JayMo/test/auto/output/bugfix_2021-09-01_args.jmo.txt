<Err>
Error   : Runtime error
Message : Invalid Type-Name!
Detail  : Type must start with a capital letter! Got: 'x'
Call    : "abc".typeUse('x','a','b')…
Instance: Root
  @ bugfix_2021-09-01_args.jmo :2    "abc".typeUse('x','a','b')…
</Err>
===== RESULT =====
null
