nil
Error   : Runtime error
Message : Invalid value for constant
Detail  : 'XYZ = nil' is not valid, maybe try 'XYZ? = nil'
Call    : XYZ = nil
Instance: Root
  @ bugfix_2022-07-11_const_nil.jmo :8     XYZ = nil
  @ bugfix_2022-07-11_const_nil.jmo :7     Try…
  @ bugfix_2022-07-11_const_nil.jmo :10    .show
===== RESULT =====
Error["Invalid value for constant","'XYZ = nil' is not valid, maybe try 'XYZ? = nil'","bugfix_2022-07-11_const_nil.jmo:8"]
