[d,c,b,a]
[a,b,c,d]
[a,c,d,a,x]
[x,a,d,c,a]
[x,a,d,c]
---
[d,c,b,a]
[a,b,c,d]
[a,b,d,a,x]
[x,a,d,b,a]
[x,a,d,b]
===== RESULT =====
['x','a','d','b']
