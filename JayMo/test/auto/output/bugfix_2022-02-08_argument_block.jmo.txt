<Err>
Error   : Invalid source code
Message : Invalid use of block and auto-block-function.
Detail  : Don't append a block to an argument
Call    : Try…
Instance: Root
  @ bugfix_2022-02-08_argument_block.jmo :2    Try…
  @ bugfix_2022-02-08_argument_block.jmo :2    t = Try…
</Err>
===== RESULT =====
null
