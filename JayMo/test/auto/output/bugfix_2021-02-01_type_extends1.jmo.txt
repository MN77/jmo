Foo9
Foo123
<Err>
Error   : Invalid source code
Message : No Block allowed
Detail  : Function has no control functionality: Foo.foo…
Call    : Foo.foo…
Instance: Foo
  @ bugfix_2021-02-01_type_extends1.jmo :13    Foo.foo…
  @ bugfix_2021-02-01_type_extends1.jmo :13    Foo…
</Err>
===== RESULT =====
null
