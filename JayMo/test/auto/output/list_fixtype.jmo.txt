[<Int>,<IntNumber>,<Number>,<Atomic>,<Immutable>,<Object>]
This List accepts only <Int>, but got object with type: <Char>
[2i,3i,8i,5i,9i,'b']
Unknown type: <Fooo>
Type <Int> needed, but type of item at position 2 is: <Str>
[2i,"foo",8i,5i,9i,'b',[]]
===== RESULT =====
[2i,"foo",8i,5i,9i,'b',[]]
