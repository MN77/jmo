--- Variablen ---
VarLet[a]
4
4
7
7
--- Range ---
6..9
--- Table ---
[4,3]
[2,5]
3
5
--- Convert ---
21
21
2
true
a
--- Hex ---
127
7f
127
FF4A
65354
FF4A
65354
--- Begin ---
abcdefghi
abcdefghi
--- Dec ---
12.34
12.34
--- Lists ---
1
4
1
6
1
5
[7]
--- vars ---
a
b
===== RESULT =====
'b'
