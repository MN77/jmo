<Err>
Error   : Invalid source code
Message : Sandbox-Mode! This object/function is forbidden!
Detail  : Str.jaymo
Call    : "123.print".jaymo…
Instance: Root
  @ sandbox_6.jmo :3    "123.print".jaymo…
</Err>
===== RESULT =====
null
