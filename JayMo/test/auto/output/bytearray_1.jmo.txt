---------------------
<Byte> 123
---------------------
ByteArray[123]
ByteArray[1]
1
Invalid length | Value should be 2, but got: 1 | bytearray_1.jmo:33
Invalid length | Value should be 4, but got: 1 | bytearray_1.jmo:36
Invalid length | Value should be 8, but got: 1 | bytearray_1.jmo:39
---------------------
<Short> 12345
---------------------
ByteArray[48,57]
ByteArray[1,2]
Invalid length | Value should be 1, but got: 2 | bytearray_1.jmo:30
258
Invalid length | Value should be 4, but got: 2 | bytearray_1.jmo:36
Invalid length | Value should be 8, but got: 2 | bytearray_1.jmo:39
---------------------
<Int> 1234567
---------------------
ByteArray[0,18,-42,-121]
ByteArray[1,2,3,4]
Invalid length | Value should be 1, but got: 4 | bytearray_1.jmo:30
Invalid length | Value should be 2, but got: 4 | bytearray_1.jmo:33
16909060
Invalid length | Value should be 8, but got: 4 | bytearray_1.jmo:39
---------------------
<Long> 123456789
---------------------
ByteArray[0,0,0,0,7,91,-51,21]
ByteArray[1,2,3,4,5,6,7,8]
Invalid length | Value should be 1, but got: 8 | bytearray_1.jmo:30
Invalid length | Value should be 2, but got: 8 | bytearray_1.jmo:33
Invalid length | Value should be 4, but got: 8 | bytearray_1.jmo:36
101190156
---------------------------------------------------
123
ByteArray[123]
123
123
ByteArray[0,123]
123
123
ByteArray[0,0,0,123]
123
123
ByteArray[0,0,0,0,0,0,0,123]
123
---------------------------------------------------
ByteArray[64b,65b,66b,67b,68b]
---
ByteArray[64,65,66,67,68]
ByteArray<5>
ByteArray[64b,65b,66b,67b,68b]

@ABCD
@ABCD
@ABCD

@ABCD
䁁䉃�
䅀䍂�
䁁䉃�
@ABCD
䁁䉃�
䅀䍂�
䁁䉃�

@ABCD
@ABCD
@ABCD
@ABCD
===== RESULT =====
"@ABCD"
