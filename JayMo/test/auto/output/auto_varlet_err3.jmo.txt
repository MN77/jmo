3
abcdef
<Err>
Error   : Invalid source code
Message : Invalid type of argument
Detail  : Got <Int>, need <VarLet>
Call    : this.test(1)
Instance: Root
  @ auto_varlet_err3.jmo :11    this.test(f)
</Err>
===== RESULT =====
null
